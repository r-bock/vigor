#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion = 6.22
#pragma version = 1.1
#pragma hide = 1

// written by Roland Bock December 2012
// version 1
//
// This file should be aliased/shortcutted in the Igor Procedures folder so that it automatically
// loads at startup. It adds a single menu item in Data > Packages to load the Voltammetry
// software package.
// The companion function to unload the package is in the file "Voltammetry Package.ipf"


Menu "Analysis"
	Submenu "Packages"
		Submenu "RB Minions"
			"Load Voltammetry Package", /Q, LoadVoltammetryPackage()
		End
	End
End

Function LoadVoltammetryPackage()
	Execute /P/Q/Z "INSERTINCLUDE \"VM_GUI\""
	Execute /P/Q/Z "INSERTINCLUDE \"Voltammetry Package\""
	Execute /P/Q/Z "INSERTINCLUDE \"MeanAndStDev\""
	Execute /P/Q/Z "INSERTINCLUDE \"WindowHelper\""
	Execute /P/Q/Z "COMPILEPROCEDURES "
	Execute /P/Q/Z "killEmptyTable0()"
End

Function LoadVMAcquisitionPackage()
	Execute /P/Q/Z "LoadVoltammetryPackage()"	
	Execute /P/Q/Z "INSERTINCLUDE \"VM_mafPCBridge\""
	Execute /P/Q/Z "COMPILEPROCEDURES "
End