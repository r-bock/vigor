#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma IgorVersion = 6.3
#pragma ModuleName = DemonLoader
#pragma version = 1.2
#include "VM_Helpers"

// by Roland Bock, September 2014
//
// I created this file to be able to load files created by LabView (National Instruments). It 
// uses the XOP from WaveMetrics
//
// * version 1.1 (JUN 2015): corrected the scaling factor for the raw data files after discussions with
//        Jordan Yorgason, the developer of the original Demon software.
// version 1 (MAR 2015)

// *******************************************************
// ************                         C O N S T A N T S
// *******************************************************

static Constant kVersion = 1.1

Strconstant ksVM_DemonRampOffset = "OFFSET"

static Strconstant ksErrorMsg = "** ERROR (%s): %s.\r"


// ******   DEMON   specific   names    **********
// *** at top level location (root)
static Strconstant ksTopDF = "S_topLevelDF"		// str: top datafolder that contains the tdms values
static Strconstant ksFileName = "S_fileName"		// str: TDMS file name
static Strconstant ksLoadResult = "V_TDMResult"	// var: result of load file

// *** at top datafolder location (in ksTopDF)
// *****   sub folder names    ******
static Strconstant ksSubF_Scale = "Scaling Coefficients"
static Strconstant ksSubF_CV = "Command Voltage"
static Strconstant ksSubF_Data = "Data"				// only prefix, need to determine the number of data sets
static Strconstant ksSubF_Events = "Events"

// ******  Descriptors   ********
static Strconstant ksStimType = "Stimulation_Type__Paired_or_Sin"

// ****** CV Values (Command Voltage)   ********

static Strconstant ksCV_SwitchPot = "Switching_Potential"			// var: max switching potential
static Strconstant ksCV_voltOffset = "Command_Voltage_Offset"		// var: offset
static Strconstant ksCV_VRate = "Voltage_Scan_Rate"					// var: voltage scan rate
static Strconstant ksCV_Duration = "Command_Voltage_Duration__s_"	// var: cv duration
static Strconstant ksCV_numPoints = "Points_in_CV"					// var: number of points in the CV and ramp
static Strconstant ksCV_RampName = "Channel1"							// wave: ramp wave

static Strconstant ksTR_cGain = "Current_Gain__nA_V_"					// var: current gain
static Strconstant ksTR_samplingFreq = "Collection_Frequency__Hz_"	// var: sampling frequency for transient
static Strconstant ksTR_baseScan = "Pre_Stimulation_Scans"			// var: pre scans for base line
static Strconstant ksTR_transientScans = "Post_Stimulation_Scans"		// var: post stim scans
static Strconstant ksTR_CVMultiplier = "CV_Multiplier"


// *******************************************************
// ************                         M E N U
// *******************************************************

Menu "Voltammetry", dynamic
	SubMenu "Load data"
		VM_menuLoadTDM("normal"), VM_TDMSLoader()
		VM_menuLoadTDM("overwrite"), VM_TDMSLoader(overwrite=1)
		VM_organizeTDM("normal"), VM_organizeTDMSLoad()
		VM_organizeTDM("overwrite"), VM_organizeTDMSLoad(overwrite = 1)
	End
End

Function /S VM_menuLoadTDM(type)
	String type

	String text = ""
	
	#if Exists("TDMLoadData")
	
	if (exists("VM_menuLoadABF"))
		strswitch (type)
			case "normal":
				text = "\\M0:/6:[LV] Load TDM(S) data file ..."
				break
			case "overwrite":
				text = "\\M0:/7:[LV] Reload TDM(S) data file ..."
				break
		endswitch
	else
		strswitch (type)
			case "normal":
				text = "\\M0:/1:[LV] Load TDM[S] data file ..."
				break
			case "overwrite":
				text = "\\M0:/2:[LV] Reload TDM[S] data file ..."
				break
		endswitch
	endif
	
	#endif
	
	return text
End

Function /S VM_organizeTDM(type)
	String type
	
	String text = ""
	
	if (hasVMTDMData())
		strswitch (type)
			case "normal":
				text = "\\M0:/8:[LV] Organize TDM(S) load"
				break
			case "overwrite":
				text = "\\M0:/9:[LV] Re-Organize TDM(S) load"
				break
		endswitch
		
	endif
	
	return text
End

// *******************************************************
// ************        F U N C T I O N S 
// *******************************************************

#if Exists("TDMLoadData")

Function /S VM_TDMSLoader([overwrite])
	Variable overwrite
	
	overwrite = ParamIsDefault(overwrite) || overwrite <= 0 ? 0 : 1
	
	String files = SortList(ReplaceString("\r", VoltGUI#chooseFiles("tdms"), ";"), ";", 16)
	if (cmpstr(files, "") == 0)
		return ""
	endif
	// make sure it is really sorted - eliminate the dashes, convert to underscores
	Variable dashes = strsearch(files, "-", 0) >= 0
	if (dashes)
		files = SortList(ReplaceString("-", files, "_"), ";", 16)
		files = ReplaceString("_", files, "-")
	endif
	
	String fileNames = "", dataPath = ""
	Variable numFiles = ItemsInList(files)
	Variable index = 0
	
	DFREF cDF = GetDatafolderDFR()
	DFREF loadFolder = VoltHelpers#getDataLoadFolder()
	
	SetDatafolder loadFolder
	for (index = 0; index < numFiles; index += 1)	
		String file = StringFromList(index, files)
		if (index == 0)
			dataPath = ParseFilePath(1, file, ":", 1, 0)
		endif
		fileNames = AddListItem(ParseFilePath(3, file, ":", 0, 0), fileNames, ";", Inf)
		TDMLoadData /Q/O file
//		TDMLoadData "F:Data:Test:Voltammetry Tarhel:matrix_2.tdms"
	endfor
	SetDatafolder cDF
	
	printf "** loaded %g files (%s) on %s at %s.\r", numFiles, fileNames, date(), time()
	printf "**   -- from %s **\r", dataPath
	
	String type = "loaded"
	if (overwrite)
		type = "reloaded"
	endif
	
	printf "** organizing %s datafolders now ... \r", type
	
	VM_organizeTDMSLoad(overwrite=overwrite)
	
	printf "... finished.\r\r"
	
	return fileNames
End

#endif

Function VM_organizeTDMSLoad([overwrite, clean])
	Variable overwrite, clean
	
	overwrite = ParamIsDefault(overwrite) || overwrite <= 0 ? 0 : 1
	clean = ParamIsDefault(clean) || clean >= 0 ? 1 : 0
	
	if (!hasVMTDMData())
		DoAlert /T="TDMS Data Missing" 0, "I can't detect the raw data from a TDMS file to organize. Nothing more to do!"
		return 0
	endif
	
	DFREF dlf = VoltHelpers#getDataLoadFolder()
	Variable dataFolders = CountObjectsDFR(dlf, 4)
	String folderList = ""
	Variable index = 0
	for (index = 0; index < dataFolders; index += 1) 
		folderList = AddListItem(GetIndexedObjNameDFR(dlf, 4, index), folderList, ";", Inf)
	endfor

	return VM_organizeDemonLoad(folderList, loadFolder = dlf, overwrite = overwrite, clean = clean)
End


Function VM_organizeDemonLoad(folderList, [loadFolder, overwrite, clean])
	String folderList
	DFREF loadFolder
	Variable overwrite, clean
	
	if (ParamIsDefault(loadFolder) || DatafolderRefStatus(loadFolder) == 0)
		DFREF loadFolder = VoltHelpers#getDataLoadFolder()
	endif
	
	overwrite = ParamIsDefault(overwrite) || overwrite <= 0 ? 0 : 1
	clean = ParamIsDefault(clean) || clean >= 1 ? 1 : 0
	
	String part1 = "", part2 = ""
	Variable numFolders = ItemsInList(folderList)
	Variable index, epochNum, lScr, uScr
	for (index = 0; index < numFolders; index += 1)
		String folderName = StringFromList(index, folderList)
		lScr = strsearch(folderName, "_", Inf, 1)
		if (lScr < 1)
			// figure out some default values here
			lScr = strsearch(ReplaceString("-", folderName, "_"), "_", Inf, 1)
			if (lScr < 1)
				part1 = folderName
				epochNum = index
			else
				uScr = strsearch(ReplaceString("-", folderName, "_"), "_", 0)
				part1 = folderName[0, uScr - 1]
				part2 = folderName[lScr + 1, strlen(folderName)]
				epochNum = numtype(str2num(part1)) == 0 ? str2num(part1) : str2num(part2)
				if (numtype(epochNum) != 0)
					epochNum = index
				else
					DFREF tdmsFolder = loadFolder:$folderName
					moveDemonData("ep", epochNum, ldf=tdmsFolder, category=part1, overwrite=overwrite)
				endif
			endif
		else
			uScr = strsearch(folderName, "_", 0)
			part1 = folderName[0, uScr - 1]
			part2 = folderName[lScr + 1, strlen(folderName)]
			epochNum = numtype(str2num(part1)) == 0 ? str2num(part1) : str2num(part2)
			if (numtype(epochNum) != 0)
				epochNum = index
			else
				DFREF tdmsFolder = loadFolder:$folderName
				moveDemonData("ep", epochNum, ldf=tdmsFolder, category=part1, overwrite=overwrite, clean=clean)
			endif
		endif
	endfor
	
	if (clean)
		VoltHelpers#cleanUp()
	endif
	return 1
End



// *******************************************************
// ************                  S T A T I C    F U N C T I O N S 
// *******************************************************

static Function VMTDMExists()
	return (exists("TDMLoadData") == 4 ? 1 : 0)
End

static Function hasVMTDMData()
	DFREF loadFolder = VoltHelpers#getDataLoadFolder()
	Variable folders = CountObjectsDFR(loadFolder, 4)
	return folders > 0
End

static Function isSingle(dfr)
	DFREF dfr
	
	SVAR type = dfr:$ksStimType
	if (!SVAR_Exists(type))
		return -1
	endif
	return cmpstr(type, "Single") == 0
End

static Function cleanFolder(dfr)
	DFREF dfr
	
	KillDatafolder /Z dfr
	return V_flag == 0		// an error is indicated by a positive number
End



// this function checks if the default ramp wave is there and creates it from the available data
// in the demon file, if not
// 
// Parameters
// ldf			data folder reference to the load folder of the file
// 
// Optional Parameters
// overwrite	(default false 0), set to 1 to recreate the ramp wave from the latest file
//
static Function /WAVE checkDemonRampWave(ldf, [overwrite])
	DFREF ldf
	Variable overwrite

	if (ParamIsDefault(overwrite) || overwrite < 0)
		overwrite = 0
	else
		overwrite = overwrite >= 1 ? 1 : round(overwrite)
	endif

	Wave ramp
	
	DFREF package = VoltHelpers#getPackage()

	Variable rampInSec = 0.008, numPoints = 1000, offset = 0

	NVAR gRampInSec = ldf:$ksCV_Duration
	if (NVAR_Exists(gRampInSec))
		rampInSec = gRampInSec
	endif
	NVAR gNumPoints = ldf:$ksCV_numPoints
	if (NVAR_Exists(gNumPoints))
		numPoints = gNumPoints
	endif
	NVAR gOffset = ldf:$ksCV_voltOffset
	if (NVAR_Exists(gOffset))
		offset = gOffset
	endif

	DFREF rampFolder = ldf:$ksSubF_CV
	Wave demonRamp = rampFolder:$ksCV_RampName
	
	if (!WaveExists(demonRamp))
		// recreate from parameters
	else
		Wave ramp = package:$ksVM_VoltageRamp
		if (!WaveExists(ramp) || numpnts(ramp) != numpnts(demonRamp) || sum(ramp) == 0)
			Make /N=(numpnts(demonRamp)) /O package:$ksVM_VoltageRamp /WAVE=ramp
			SetScale /I x 0, rampInSec, "s", ramp
			SetScale d 0, 0, "V", ramp
			String theNote = ""
			theNote = ReplaceStringByKey(ksVM_RampKey, theNote, "dopamine")
			theNote = ReplaceNumberByKey(ksVM_RampStartPKey, theNote, 0)
			theNote = ReplaceNumberByKey(ksVM_RampEndPKey, theNote, numpnts(demonRamp) - 1)
			theNote = ReplaceNumberByKey(ksVM_RampMaxPKey, theNote, round(numpnts(demonRamp) / 2))
			theNote = ReplaceNumberByKey(ksVM_DemonRampOffset, theNote, gOffset)
			Note /K ramp, theNote
			ramp = demonRamp - offset
//			Wave ramp = VM_getRampWave(numbers = numpnts(demonRamp), length = rampInSec)
		endif
	endif
	
	return ramp
End


// This function determines the top data folder and chooses the processing based on single
// or multiple stimulation. 
// Currently only single stimulation Demon files can be processed.
static Function /WAVE moveDemonData(baseName, num, [ldf, folder, category, overwrite, clean])
	String baseName, folder, category
	Variable num, overwrite, clean
	DFREF ldf

	String errormsg
	sprintf errormsg, ksErrorMsg, "DemonLoader#moveDemonData", "%s"

	Wave resultW
	Variable haveLDF = 0
	
	clean = ParamIsDefault(clean) || clean >= 1 ? 1 : 0
	overwrite = ParamIsDefault(overwrite) || overwrite <= 0 ? 0 : 1
	if (ParamIsDefault(category))
		category = ""
	endif

	if (ParamIsDefault(ldf) || DatafolderRefStatus(ldf) == 0)
		DFREF ldf = VoltHelpers#getDataLoadFolder()
	else
		haveLDF = 1
	endif
	if (!haveLDF)
		if (ParamIsDefault(folder) || strlen(folder) == 0)
			DFREF ldf = VoltHelpers#getDataLoadFolder()
		else
			DFREF ldf = VoltHelpers#getDataLoadFolder():$folder
			if (DatafolderRefStatus(ldf) == 0)
				printf errormsg, "can't locate folder '" + folder + "'! Stopped data move"
				return resultW
			endif
		endif
	endif
	
	Wave /T channels = VoltHelpers#getChannelAssignment()
	if (DimSize(channels, 0) == 0)
		InsertPoints 0, 1, channels
		SetDimLabel 0, 0, AD0, channels
		channels[0] = "data"
	endif
		
	String name = baseName + "_" + num2str(num)
	
	SVAR stimType = ldf:$ksStimType
	if (SVAR_Exists(stimType) && cmpstr(stimType, "Single") == 0)
		checkDemonRampWave(ldf)
//		Wave resultW = moveSingleDemonData(name, ldf, avgTr = 4, avgCV = 3, category = category, overwrite = overwrite)
		Wave resultW = moveSingleDemonData(name, ldf, category = category, overwrite = overwrite)
	endif
	
	if (WaveExists(resultW) && clean)
		Variable success = cleanFolder(ldf)
	endif
	
	return resultW
End


// this function moves the 
static Function /WAVE moveSingleDemonData(name, ldf, [category, avgTr, avgCV, overwrite])
	String name, category
	DFREF ldf
	Variable avgTr, avgCV, overwrite

	avgTr = ParamIsDefault(avgTr) || avgTr <= 0 ? 0 : round(avgTr)
	avgCV = ParamIsDefault(avgCV) || avgCV <= 0 ? 0 : round(avgCV)
	overwrite = ParamIsDefault(overwrite) || overwrite <= 0 ? 0 : 1
	
	if (ParamIsDefault(category))
		category = ""
	endif
	
	Wave rawTraces

	if (DatafolderRefStatus(ldf) == 0)
		return rawTraces
	endif

	DFREF cDF = GetDatafolderDFR()
	DFREF package = VoltHelpers#getPackage()
	DFREF dataFolder = VoltHelpers#getDataLoadFolder()
	DFREF epochFolder = VoltHelpers#getEpochFolder()

	String epochNameList = ""
	Wave epochs = VM_getEpochWave()
	if (DimSize(epochs, 0) > 0)
		// we have epochs
		epochNameList = makeLblListofWave(epochs)
	endif
	
	Wave /T categories = VM_getEpochCatWave()

	Variable index, lastNum = 1, uScr = 0

	if (strlen(epochNameList) > 0 && WhichListItem(name, epochNameList) >= 0 && !overwrite)
		Variable underScores = ItemsInList(name, "_") - 1
		if (underScores <= 1)
			name += "_"
			uScr = strlen(name) - 1
			do
				name[uScr + 1, Inf] = num2str(lastNum)
				lastNum += 1
			while (WhichListItem(name, epochNameList) >= 0)
		else
			uScr = strsearch(name, "_", Inf, 1)
			if (uScr >= 0)
				lastNum = str2num(name[uScr + 1, Inf])
				if (numtype(lastNum) != 0)
					// conversion failed, so last part of name is not a number, so just attach a number
					//      to the end
					name += "_1"
				else
					lastNum += 1
					do
						name[uScr + 1, Inf] = num2str(lastNum)
						lastNum += 1
					while( WhichListItem(name, epochNameList) >= 0)
				endif
			else
				// can't locate any underscores in the name, so just attach a number to the end
				name += "_1"		
			endif
		endif
	endif

	Variable numExistEpochs = DimSize(epochs, 0)
	if (numExistEpochs != numpnts(categories))
		Redimension /N=(numExistEpochs) categories
	endif
	
	if (strlen(epochNameList) == 0 || !overwrite)
		InsertPoints /M=0 numExistEpochs, 1, epochs
		SetDimLabel 0, numExistEpochs, $name, epochs
		InsertPoints numExistEpochs, 1, categories
		SetDimLabel 0, numExistEpochs, $name, categories
	endif
	
	String rawData = "AD0"
	String resultName = name + "_traces"
	String wNote = ""
	wNote = ReplaceStringByKey(ksVM_ChannelLbl, wNote, rawData)
	wNote = ReplaceStringByKey(ksVM_ChannelDesig, wNote, "data")

	Variable points = CountObjectsDFR(ldf:$(ksSubF_Data + "1"), 1)
	if (points == 0)
		return rawTraces
	endif
	Wave data = WaveRefIndexedDFR(ldf:$(ksSubF_Data + "1"), 0)
	
	Variable columns = numpnts(data)
	
	Make /O/N=(points, columns) dataFolder:$rawData /WAVE=rawTraces

	for (index = 0; index < points; index += 1)
		Wave data = WaveRefIndexedDFR(ldf:$(ksSubF_Data + "1"), index)
		rawTraces[index][] = data[q]
	endfor
		
	NVAR gain = ldf:$ksTR_cGain
//	NVAR multiplier = ldf:$ksTR_CVMultiplier
	DFREF scaleFolder = ldf:$ksSubF_Scale
	Wave intScaleFactors = scaleFolder:$("Untitled")
	
//	Variable conversionFactor = 1
	Variable conversionFactor = WaveExists(intScaleFactors) ? intScaleFactors[1] * gain * 1e-9 : 1
	
// 	rawTraces = rawTraces / gain							// values are in nA
//	rawTraces = rawTraces / multiplier
	MatrixOp /O rawTraces = rawTraces * conversionFactor	// convert to A
	SetScale d 0, 0, "A", rawTraces
	NVAR samplingFreq = ldf:$ksTR_samplingFreq			// in Hz
	SetScale /P x 0, (1/samplingFreq), "s", rawTraces
	NVAR cvDuration = ldf:$ksCV_Duration					// cv duration in seconds
	SetScale /I y 0, cvDuration, "s", rawTraces
	
	Variable transDelta = DimDelta(rawTraces, 0)
	
	Variable numBaseScans = 0
	NVAR numBaseLine = ldf:$ksTR_baseScan
	if (!NVAR_Exists(numBaseLine))
		STRUCT VoltammetryPreferences prefs
		VoltHelpers#getTransPrefs(prefs)
	
		numBaseScans = prefs.baselinePoints
		Prompt numBaseScans, "Please enter the number of baseline scans"
		DoPrompt "Missing Baseline Number", numBaseScans
		if (V_flag)
			// user canceled so stop here!
			return rawTraces
		endif
		prefs.baselinePoints = numBaseScans
		VoltHelpers#setTransPrefs(prefs)
	else
		numBaseScans = numBaseLine
	endif

	categories[%$name] = category
	
	epochs[%$name][%endpoint] = columns
	epochs[%$name][%peakstartpoint] = numBaseScans
	epochs[%$name][%BLEndPnt] = numBaseScans - 1
	epochs[%$name][%endx] = DimSize(rawTraces, 0) * transDelta
	epochs[%$name][%peakstartx] = numBaseScans * transDelta

	wNote = ReplaceNumberByKey(ksVM_BaselineNum, wNote, numBaseScans)
	Note /K rawTraces, wNote
	Duplicate /O rawTraces, epochFolder:$resultName
	
	wNote = ""
	
	Wave image = VM_makeImageVoltammogram(rawTraces, blStart=0, blEnd=(numBaseScans - 1))
	ImageStats /M=1/G={numBaseScans - 1, points - 1, columns * 0.005, columns / 2} image
	if (V_flag < 0)
		// no max found, something wrong with the ROI?
	endif
	Variable maxRowLoc = V_maxRowLoc
	Variable maxColLoc = V_maxColLoc

	epochs[%$name][%peakPoint] = maxRowLoc
	epochs[%$name][%peakX] = maxRowLoc * transDelta

	Wave vg = VoltHelpers#getImageVoltammogram()
	Duplicate /O image, package:$(NameOfWave(vg)) /WAVE=vg
	Duplicate /O image, epochFolder:$(ReplaceString("_traces", resultName, "_img"))

	Wave cv = VM_makeCVCurves(vg, maxRowLoc, numPoints = floor(avgCV/2))
	Duplicate /O cv, epochFolder:$(ReplaceString("_traces", resultName, "_cv"))
	
	Make /N=(points) /O epochFolder:$(ReplaceString("_traces", resultName, "")) /WAVE=trace
	SetScale d 0,0, WaveUnits(image, -1), trace
	SetScale /P x 0, DimDelta(image, 0), WaveUnits(image, 0), trace
	trace = 0

	if (avgTr > 0)
		Duplicate /FREE trace, traceCol
		Variable num4avg = floor( avgTr / 2)
		Variable avgStart = maxColLoc - num4avg
		Variable avgEnd = maxColLoc + num4avg
		wNote = ReplaceNumberByKey(ksVM_AvgStartPointKey, wNote, avgStart)
		wNote = ReplaceNumberByKey(ksVM_AvgEndPointKey, wNote, avgEnd)

		for (index = avgStart; index <= avgEnd; index += 1)
			traceCol = image[p][index]
			trace += traceCol
		endfor
		trace /= num4avg + 1
	else
		wNote = ReplaceNumberByKey(ksVM_AvgStartPointKey, wNote, maxColLoc)
		wNote = ReplaceNumberByKey(ksVM_AvgEndPointKey, wNote, maxColLoc)
		trace = image[p][maxColLoc]
	endif
	Note /K trace, wNote
	
	
	VoltGUI#refreshEpochPanelList()		

	VM_makeCVPlot(cv)
	VM_showImageVoltammogram()
	
	return rawTraces
End