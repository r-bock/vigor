#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma Igorversion = 6.3
#pragma version = 1.4
#pragma ModuleName = VoltReA
#include "VM_Helpers", version >= 2.4
#include "VM_GUI", version >= 2.2

// written by Roland Bock, March 2015
//
// version 1.4 (JUN 2015): fixed panel to reflect panel background color accurately and removing the calibration
//                   keyword once the transient is re-analyzed
// version 1.3 (JUN 2015): limited the avg CV to exclude the baseline traces, i.e., the average
//           cv will potentially collect asymetrically from the peak
// version 1.2 (JUN 2015): added close button to the interface
// * version 1 (MAR 2015)


// ******************    S T A T I C    D E F I N I T I O N S    ********************

static Constant ksGUIPrefsVersion = 1


static Strconstant ksPanelName = "Epoch_ReAnalyzePanel"
static Strconstant ksPanelTitle = "Epoch Re-Analyze Panel"

static Strconstant ksREA_epochListName = "VMREA_epochsList"
static Strconstant ksREA_epochSelName = "VMREA_epochsListSel"

static Strconstant ksREA_csrUpdateFlag = "VMREA_csrUpdate"




// ******************   G U I   V A L U E   S T R U C T U R E   **************************

Structure VM_reanalyzeGUI
	int16 version
	char epoch[23]			// name of current epoch to be reanalyzed
	int32 transientPnt		// transient p value
	int32 cvPnt				// cv p value
	int16 avgTr				// average number for transient trace
	int16 avgCv				// average number for cv trace
EndStructure

static Function defaultGUIPrefs(prefs)
	STRUCT VM_reanalyzeGUI &prefs
	
	prefs.version = ksGUIPrefsVersion
	prefs.transientPnt = 250
	prefs.cvPnt = 50
	prefs.avgTr = 0
	prefs.avgCv = 0
End

// ******************   H E L P E R     F U N C T I O N S      **************************

static Function guiInit()
	DFREF package = VoltHelpers#getPackage()

	return 1
end

Function VMREA_getCsrUpdateFlag()
	NVAR flag = $(VoltHelpers#getNVarByName(ksREA_csrUpdateFlag, value = 1))
	return flag
End

Function VMREA_setCsrUpdateFlag(num)
	Variable num
	
	num = num <= 0 ? 0 : 1
	NVAR flag = $(VoltHelpers#getNVarByName(ksREA_csrUpdateFlag, value = num))
	return 1
End

// ******************   G U I     F U N C T I O N S      **************************

Function VM_ReanalyzePanel()
		
	DoWindow /F $ksPanelName
	if (V_flag)
		// window already exists
		if (WinType(ksPanelName) == 7)
			// we have a panel, so we are done, just execute the listbox function
			return 1
		endif
		// we have a window with the panel name that is not a panel -> kill it
		DoWindow /K $ksPanelName
	endif
	
	String fontName = "Verdana"
	Variable fontSize = 12
	
	STRUCT VM_reanalyzeGUI guiValues
	defaultGUIPrefs(guiValues)
	String valStr = ""
	StructPut /S guiValues, valStr

	DFREF package = VoltHelpers#getPackage()
	DFREF epochFolder = VoltHelpers#getEpochFolder()
		
	Wave gTraces = VoltHelpers#getDataTraceWave()
	Wave gImage = VoltHelpers#getImageVoltammogram()
	Wave gTransient = VoltHelpers#getEpochTransient()
	Wave gCv = VoltHelpers#getCVTrace()
	Wave ramp = VM_getRampWave()
		
	Wave pointWave = VM_getVoltgrmDispWave("points")
	Wave dispWave = VM_getVoltgrmDispWave("display")

	STRUCT VM_VGColorScale cprefs
	VoltGUI#loadCSPrefs(cprefs)
	
	Wave /T epLWave = VoltGUI#getEpochListWave()
	Wave epSWave = VoltGUI#getEpochSelWave()
	
	Extract /FREE /INDX /O epSWave, sel, epSWave == 1
	Duplicate /O /T epLWave, package:$ksREA_epochListName /WAVE=listW
	Duplicate /O epSWave, package:$ksREA_epochSelName /WAVE=selW
	if (numpnts(sel) == 0)
		selW = 0
		selW[0] = 1
	else
		selW = 0
		selW[sel[0]] = 1
	endif
	
	Variable topOffset = 10, leftOffset = 10, slOffset = 10, width = 500, height = 500
	Variable tWidth = width, tHeight = height, distance = 10
	
	NewPanel /K=1/W=(topOffset, leftOffset, width, height) /N=$ksPanelName as ksPanelTitle
	ControlInfo /W=$ksPanelName kwBackgroundColor
	Make /N=3 /FREE bkgrdColor = {V_Red, V_Green, V_Blue}
	ModifyPanel /W=$ksPanelName cbRGB=(bkgrdColor[0],bkgrdColor[1],bkgrdColor[2])
	
	leftOffset = 0
	topOffset = 0
	width = 420
	height = 320
	
	Display /W=(leftOffset, topOffset, width, height) /N=img /HOST=$ksPanelName /FG=(FL,FT,*,*)
	AppendImage gImage
	ModifyGraph margin(left)=40, userticks(left)={pointWave, dispWave}
	Label left "V"
	ModifyGraph margin(right)=90, standoff=0, width={Aspect,1}, frameStyle=2
	ColorScale /W=# /C/N=theCScale/F=0/A=RC/X=5.00/Y=0.00/E=2 image=$(NameOfWave(gImage)), frame=0.00, heightPct=100
	ModifyImage /W=# $(NameOfWave(gImage)) ctab={cprefs.min, cprefs.max, Rainbow, 0}
	Cursor /I/P/H=1/L=0/S=1 E $(NameOfWave(gImage)) 50, 250
	SetActiveSubWindow ##

	leftOffset = width
	Display /W=(leftOffset,topOffset,leftOffset + height,height) /N=tr /HOST=$ksPanelName /FG=(,FT,FR,) gTransient
	ModifyGraph standoff=0, rgb=(0,0,0)
	ModifyGraph width={Aspect,1},wbRGB=(bkgrdColor[0],bkgrdColor[1],bkgrdColor[2]),gbRGB=(bkgrdColor[0],bkgrdColor[1],bkgrdColor[2])
	SetAxis /A/N=1 left
	SetAxis /A/N=1 bottom
	SetActiveSubWindow ##
	tWidth = leftOffset + height + 20
	topOffset = height + 10
	leftOffset += 10
	SetVariable sv_trLine, pos={leftOffset,topOffset}, size={125,15}, bodyWidth=60, title="transient line"
	SetVariable sv_trLine, font=$ksVM_defPanelFont, fSize=fontSize, limits={0, DimSize(gImage, 1), 1}
	SetVariable sv_trLine, value=_NUM:guiValues.transientPnt, proc=svFunc_VMREA_setValue
	
	SetVariable sv_trLineS, pos={leftOffset + 130, topOffset}, size={80,15}
	SetVariable sv_trLineS, font=$ksVM_defPanelFont, fSize=fontSize, limits={0,0,0}, noedit=1
	SetVariable sv_trLineS, frame=0, valueBackColor=(bkgrdColor[0],bkgrdColor[1],bkgrdColor[2])
	
	slOffset = leftOffset + 135 + 80
	ListBox lb_epochs, pos={slOffset, topOffset}, size={105, 200},mode=6
	ListBox lb_epochs, font=$ksVM_defPanelFont, fSize=fontSize, userdata=valStr
	ListBox lb_epochs, listWave= listW, selWave= selW, proc=lbFunc_VMREA_reanalyzeEpoch
	
	topOffset += 15 + distance
	SetVariable sv_cvLine, pos={leftOffset,topOffset}, size={107,15}, bodyWidth=42, title="CV line"
	SetVariable sv_cvLine, font=$ksVM_defPanelFont, fSize=fontSize, limits={0, DimSize(gImage,0), 0}, noedit=1
	SetVariable sv_cvLine, valueBackColor=(61166,61166,61166)
	SetVariable sv_cvLine, value=_NUM:guiValues.cvPnt, proc=svFunc_VMREA_setValue
	
	SetVariable sv_cvLineS, pos={leftOffset + 130, topOffset}, size={60,15}
	SetVariable sv_cvLineS, font=$ksVM_defPanelFont, fSize=fontSize, limits={0,0,0}, noedit=1
	SetVariable sv_cvLineS, frame=0, valueBackColor=(bkgrdColor[0],bkgrdColor[1],bkgrdColor[2])
	
	topOffset += 15 + distance
	SetVariable sv_avgTrace, pos={leftOffset,topOffset}, size={125,15}, bodyWidth=60, title="avg trace"
	SetVariable sv_avgTrace, font=$ksVM_defPanelFont, fSize=fontSize, limits={0, DimSize(gImage, 1) / 4, 2}
	SetVariable sv_avgTrace, value=_NUM:guiValues.avgTr, proc=svFunc_VMREA_setValue

	Titlebox tb_avgTraceLbl, pos={leftOffset + 132, topOffset}, size={60, 15}, title="pnts"
	Titlebox tb_avgTraceLbl, font=$ksVM_defPanelFont, fSize=fontSize, fStyle=2, frame=0
	
	topOffset += 15 + distance
	SetVariable sv_avgCV, pos={leftOffset,topOffset}, size={125,15}, bodyWidth=60, title="avg CV "
	SetVariable sv_avgCV, font=$ksVM_defPanelFont, fSize=fontSize, limits={0, DimSize(gImage, 0), 2}
	SetVariable sv_avgCV, value=_NUM:guiValues.avgCv, proc=svFunc_VMREA_setValue
	
	Titlebox tb_avgCVLbl, pos={leftOffset + 132, topOffset}, size={60, 15}, title="traces"
	Titlebox tb_avgCVLbl, font=$ksVM_defPanelFont, fSize=fontSize, fStyle=2, frame=0

	topOffset += 15 + distance
	Button btn_autoPeak, pos={leftOffset + 50, topOffset}, size={80, 20}, title="auto peak"
	Button btn_autoPeak, font=$ksVM_defPanelFont, fSize=fontSize
	Button btn_autoPeak, proc=btnFunc_VMREA_autoPeak

	topOffset += 15 + 15 * distance
	leftOffset += 10 * distance
	Button btn_keepData, pos={leftOffset, topOffset}, size={80, 25}, title="keep"
	Button btn_keepData, font=$ksVM_defPanelFont, fSize=(1.5 * fontSize), fStyle=1
	Button btn_keepData, proc=btnFunc_VMREA_KeepData
	
	topOffset += 1
	leftOffset += 80 + 15
	Button btn_close, pos={leftOffset, topOffset}, size={70, 22}, title="close"
	Button btn_close, font=$ksVM_defPanelFont, fSize=fontSize, fStyle=0
	Button btn_close, proc=btnFunc_VMREA_closeWin
	
	leftOffSet = 0
	topOffset = height + distance
	Display /W=(leftOffset, topOffset, width, height) /N=cv /HOST=$ksPanelName /FG=(FL,,,FB) /L=volt /B=cap gCv vs ramp
	ModifyGraph rgb=(0,0,0)
	ModifyGraph freePos(volt)={0,cap},freePos(cap)={0,volt}, lblPos=50
	ModifyGraph width={Aspect, 1},wbRGB=(bkgrdColor[0],bkgrdColor[1],bkgrdColor[2]),gbRGB=(bkgrdColor[0],bkgrdColor[1],bkgrdColor[2])
	SetAxis/A/N=1 volt
	SetAxis/A/N=0 cap
	topOffset += height
	SetActiveSubwindow ##
	
	
	tHeight = topOffset
	
	
	adjustPanelSize(ksPanelName, pixelHeight = topOffset, pixelWidth = tWidth)
	DoUpdate
	adjustPanelSize(ksPanelName, pixelHeight = topOffset + 1)
	
	STRUCT WMListboxAction lb
	lb.eventCode = 2
	lb.userdata = valStr
	Wave /T lb.listWave = listW
	Wave lb.selWave = selW
	
	lbFunc_VMREA_reanalyzeEpoch(lb)
	
	moveWindowOnScreen(ksPanelName)
	return 1
End

// *********************     P A N E L    F U N C T I O N    **********************

Function lbFunc_VMREA_reanalyzeEpoch(lb) : ListboxControl
	STRUCT WMListboxAction &lb
	
	switch (lb.eventCode)
		case 2:		// mouse up
		case 4:		// cell selection (mouse or arrow keys)
		case 5:		// cell selection plus shift key
			STRUCT VM_reanalyzeGUI guiValues
			StructGet /S guiValues, lb.userdata
			STRUCT VM_VGColorScale cs
			VoltGUI#loadCSPrefs(cs)
			
			Wave /T lWave = lb.listWave
			Wave sWave = lb.selWave
			
			Extract /FREE /INDX /O sWave, selection, sWave == 1
			sWave = 0
			sWave[selection[0]] = 1
			String epochName = lWave[selection[0]]
			guiValues.epoch = epochName
			
			Wave traces = VM_retrieveEpochData(epochName, "traces")
			Wave image = VM_retrieveEpochData(epochName, "image")
			Wave transient = VM_retrieveEpochData(epochName, "transient")
			Wave cv = VM_retrieveEpochData(epochName, "cv")

			Wave gTraces = VoltHelpers#getDataTraceWave()
			Wave gImage = VoltHelpers#getImageVoltammogram()
			Wave gTransient = VoltHelpers#getEpochTransient()
			Wave gCv = VoltHelpers#getCVTrace()
			Wave ramp = VM_getRampWave()
			
			Duplicate /O traces, gTraces
			Duplicate /O image, gImage
			Duplicate /O transient, gTransient
			Duplicate /O cv, gCv

			String theNote = note(cv)
			Variable cvPnt = NumberByKey(ksVM_CVPeakLocKey, note(cv))
			switch (numtype(cvPnt))
				case 0:
					guiValues.cvPnt = cvPnt
					break
				case 1:
				case 2:
					WaveStats /C=1/W/M=1/Q transient
					Wave M_Wavestats
					guiValues.cvPnt = x2pnt(transient, M_WaveStats[%maxLoc])
					theNote = ReplaceNumberByKey(ksVM_CVPeakLocKey, theNote, guiValues.cvPnt)
					Note /K gCv, theNote
			endswitch
			
			Variable cvAvgTr = NumberByKey(ksVM_AvgCVKey, theNote)
			switch (numtype(cvAvgTr))
				case 0:
					guiValues.avgCv = cvAvgTr - 1
					break
				case 1:
				case 2:
					guiValues.avgCv = 4
					theNote = ReplaceNumberByKey(ksVM_AvgCVKey, theNote, guiValues.avgCv)
					Note /K gCv, theNote
			endswitch
			
			
			theNote = note(transient)
//			Variable trLine = NumberByKey(ksVM_TransientLineKey, theNote)
//			if (numtype(trLine) != 0)
//				WaveStats /C=1/W/M=1/Q cv
//				Wave M_WaveStats
//				guiValues.transientPnt = x2pnt(cv, M_WaveStats[%maxLoc])
//			else
//				guiValues.transientPnt = trLine
//			endif
			
			Variable avgStPnt = NumberByKey(ksVM_AvgStartPointKey, theNote)
			Variable avgEdPnt = NumberByKey(ksVM_AvgEndPointKey, theNote)
			
			if (numtype(avgStPnt) != 2 && numtype(avgEdPnt) != 2)
				Variable difference = avgEdPnt - avgStPnt
				guiValues.avgTr = mod(difference, 2) == 1 ? difference - 1 : difference
				if (guiValues.avgTr == 0)
					guiValues.transientPnt = avgStPnt
				else
					guiValues.transientPnt = avgStPnt + (guiValues.avgTr / 2)
				endif
			else
				guiValues.avgTr = 0
			endif
			
			MatrixOp /FREE /O collector = maxVal(gImage)
			Variable upperLim = collector[0]
			ModifyImage /W=$lb.win#IMG $(NameOfWave(gImage)) ctab={-(upperLim * cs.PofUpper), upperLim, Rainbow, 0}
			
			updateGraphs(lb.win, guiValues, adjCsr = 1, setCsr = 1)
						
			SetActiveSubWindow #
			
			String valDisp = ""
			SetVariable sv_trLine, win=$lb.win, value=_NUM:guiValues.transientPnt
			sprintf valDisp, "%.2W1PV", ramp[guiValues.transientPnt]
			SetVariable sv_trLineS, win=$lb.win, value=_STR:valDisp

			SetVariable sv_cvLine, win=$lb.win, value=_NUM:guiValues.cvPnt
			sprintf valDisp, "%.1W1Ps", pnt2x(transient, guiValues.cvPnt)
			SetVariable sv_cvLineS, win=$lb.win, value=_STR:valDisp
			
			SetVariable sv_avgTrace, win=$lb.win, value=_NUM:guiValues.avgTr
			SetVariable sv_avgCV, win=$lb.win, value=_NUM:guiValues.avgCv
			
			StructPut /S guiValues, lb.userdata
			break
	endswitch
End

Function svFunc_VMREA_setValue(sv) : SetVariableControl
	STRUCT WMSetVariableAction &sv
	
	switch (sv.eventCode)
		case 1:
			// mouse up
		case 2:
			// enter key
			String usrData = GetUserData(sv.win, "lb_epochs", "")
			STRUCT VM_reanalyzeGUI guiValues
			StructGet /S guiValues, usrData
			
			strswitch (sv.ctrlName)
				case "sv_trLine":
					guiValues.transientPnt = sv.dval
					updateGraphs(sv.win, guiValues, setCsr = 1)
					break
				case "sv_cvLine":
					guiValues.cvPnt = sv.dval
					break
				case "sv_avgTrace":
					guiValues.avgTr = mod(sv.dval, 2) == 1 ? sv.dval - 1 : sv.dval
					SetVariable sv_avgTrace, win=$sv.win, value=_NUM:guiValues.avgTr
					
					updateGraphs(sv.win, guiValues, adjCsr = 0)
					break
				case "sv_avgCV":
					guiValues.avgCv = mod(sv.dval, 2) == 1 ? sv.dval - 1 : sv.dval
					SetVariable sv_avgCV, win=$sv.win, value=_NUM:guiValues.avgCv

					updateGraphs(sv.win, guiValues, adjCsr = 0)
					break
			endswitch
			
			StructPut /S guiValues, usrData
			Listbox lb_epochs, win=$sv.win, userdata=usrData
			break
	endswitch
End

Function btnFunc_VMREA_keepData(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch (bn.eventCode)
		case 2:			// mouse up
			String usrData = GetUserData(bn.win, "lb_epochs", "")
			STRUCT VM_reanalyzeGUI values
			StructGet /S values, usrData
			
			Wave image = VM_retrieveEpochData(values.epoch, "image")
			Wave transient = VM_retrieveEpochData(values.epoch, "transient")
			Wave cv = VM_retrieveEpochData(values.epoch, "cv")

			String theNote = RemoveByKey(ksVM_CalibrateKey, note(transient))
			
			Wave gImage = VoltHelpers#getImageVoltammogram()
			Wave gTransient = VoltHelpers#getEpochTransient(nums = DimSize(gImage, 0))
			Wave gCv = VoltHelpers#getCVTrace()
			
			Duplicate /O gImage, image
			Duplicate /O gTransient, transient
			Duplicate /O gCv, cv
			
			Note /K transient, theNote
			SetScale d 0, 0, "A", transient
			break
	endswitch
End

Function btnFunc_VMREA_autoPeak(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch (bn.eventCode)
		case 2:
			String usrData = GetUserData(bn.win, "lb_epochs", "")
			STRUCT VM_reanalyzeGUI values
			StructGet /S values, usrData
			
			Wave gImage = VoltHelpers#getImageVoltammogram()
			Wave gTransient = VoltHelpers#getEpochTransient(nums = DimSize(gImage, 0))
			Wave gCv = VoltHelpers#getCVTrace()
			
			WaveStats /C=1/W/M=1/Q gTransient
			Wave M_Wavestats
			values.cvPnt = x2pnt(gTransient, M_WaveStats[%maxLoc])

			WaveStats /C=1/W/M=1/Q gCv
			values.transientPnt = x2pnt(gCv, M_WaveStats[%maxLoc])

			updateGraphs(bn.win, values, setCsr = 1)
			
			break
	endswitch
	
End

Function btnFunc_VMREA_closeWin(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch (bn.eventCode)
		case 2:
			KillWindow $bn.win
			break
	endswitch
End

static Function updateGraphs(win, values, [adjCsr, setCsr])
	String win
	STRUCT VM_reanalyzeGUI &values
	Variable adjCsr, setCsr
	
	setCsr = ParamIsDefault(setCsr) || setCsr <= 0 ? 0 : 1
	adjCsr = ParamIsDefault(adjCsr) || adjCsr >= 1 ? 1 : 0
	
	Wave image = VoltHelpers#getImageVoltammogram()
	Variable cvSize = DimSize(image, 1)
	Variable trSize = DimSize(image, 0)

	Wave cv = VoltHelpers#getCVTrace()
	Wave transient = VoltHelpers#getEpochTransient(nums = trSize)
	Wave ramp = VM_getRampWave()
	
//	maybe needed for later additions
//	Wave traces = VoltHelpers#getDataTraceWave()

//	if (adjCsr)
//		DoUpdate
//	endif
	
	String theNote = note(cv)
	// first calculate and update the image graph
	Make /N=(cvSize) /O/FREE collector
	Make /N=(cvSize) /O/FREE trace
	Variable startRow = values.cvPnt - (values.avgCv / 2)
	Variable endRow = values.cvPnt + (values.avgCv / 2)
	Variable index
	
	Variable baseStartPnt = NumberByKey(ksVM_BaselineNum, note(image)) - 1
	if (numtype(baseStartPnt) == 0)
		startRow = startRow <= baseStartPnt ? baseStartPnt + 1 : startRow
		endRow = startRow <= baseStartPnt ? endRow + (baseStartPnt - startRow) + 1 : endRow
	endif
	
	for (index = startRow; index <= endRow; index += 1)
		trace = image[index][p]
		collector += trace
	endfor
	collector /= endRow - startRow + 1
	cv = collector[p]
	
	theNote = ReplaceNumberByKey(ksVM_AvgCVKey, theNote, values.avgCv + 1)
	
	if (!setCsr)
		WaveStats /C=1/W/M=1/Q cv
		Wave M_WaveStats
		values.transientPnt = x2pnt(cv, M_WaveStats[%maxLoc])
	endif
	
	theNote = ReplaceNumberByKey(ksVM_CVPeakLocKey, theNote, values.cvPnt)	
	Note /K cv, theNote

	VMREA_setCsrUpdateFlag(0)		// prevent hook function to update the graphs again because of cursor move
	Cursor /W=$win#IMG/I/P/H=1/L=0/S=1 E $(NameOfWave(image)) values.cvPnt, values.transientPnt
	
	SetDrawLayer /W=$win#IMG /K UserFront
	if (values.avgCv > 0)
		SetDrawEnv /W=$win#IMG xcoord= bottom, dash= 2, save
		DrawLine /W=$win#IMG (DimOffset(image, 0) + startRow * DimDelta(image, 0)), 0, (DimOffset(image, 0) + startRow * DimDelta(image, 0)), 1
		DrawLine /W=$win#IMG (DimOffset(image, 0) + endRow * DimDelta(image, 0)), 0, (DimOffset(image, 0) + endRow * DimDelta(image, 0)), 1
	endif

	Variable startCol = values.transientPnt - (values.avgTr / 2)
	Variable endCol = values.transientPnt + (values.avgTr / 2)
	Variable lowV = ramp[startCol]
	Variable lowVx = pnt2x(ramp, startCol)
	Variable transLine = ramp[values.transientPnt]
	Variable highV = ramp[endCol]
	Variable highVx = pnt2x(ramp, endCol)
	theNote = note(transient)
	theNote = ReplaceNumberByKey(ksVM_TransientLineKey, theNote, values.transientPnt)
	theNote = ReplaceNumberByKey(ksVM_AvgStartVoltKey, theNote, lowV)
	theNote = ReplaceNumberByKey(ksVM_AvgEndVoltKey, theNote, highV)	
	theNote = ReplaceNumberByKey(ksVM_AvgStartPointKey, theNote, startCol)
	theNote = ReplaceNumberByKey(ksVM_AvgEndPointKey, theNote, endCol)

	Redimension /N=(trSize) collector
	Redimension /N=(trSize) trace
	collector = 0
	trace = 0
	for (index = startCol; index <= endCol; index += 1)
		trace = image[p][index]
		collector += trace
	endfor
	collector /= endCol - startCol + 1
	transient = collector[p]
	Note /K transient, theNote

	if (values.avgTr > 0)
		SetDrawEnv /W=$win#IMG xcoord= prel, ycoord= left, dash= 3, save
		DrawLine /W=$win#IMG 0, lowVx, 1, lowVx
		DrawLine /W=$win#IMG 0, highVx, 1, highVx
	endif

	DoUpdate
	
	// now update the CV graph
	GetAxis /W=$win#CV/Q volt
	Variable axisMax = V_max
	SetDrawLayer /W=$win#CV /K UserBack
	SetDrawEnv /W=$win#CV xcoord = cap, ycoord= volt, save
	SetDrawEnv /W=$win#CV linefgc=(65535,65535,65535), fillbgc= (61166,61166,61166), fillfgc= (61166,61166,61166), save
	DrawRect /W=$win#CV lowV, 0, transLine, axisMax
	DrawRect /W=$win#CV transLine, 0, highV, axisMax
	
	String valDisp = ""
	SetVariable sv_trLine, win=$win, value=_NUM:values.transientPnt
	sprintf valDisp, "%.2W1PV", ramp[values.transientPnt]
	SetVariable sv_trLineS, win=$win, value=_STR:valDisp

//	SetVariable sv_cvLine, win=$win, value=_NUM:values.cvPnt
//	sprintf valDisp, "%.1W1Ps", pnt2x(transient, values.cvPnt)
//	SetVariable sv_cvLineS, win=$win, value=_STR:valDisp
	
//	SetVariable sv_avgTrace, win=$lb.win, value=_NUM:guiValues.avgTr
//	SetVariable sv_avgCV, win=$lb.win, value=_NUM:guiValues.avgCv


	return 1
End