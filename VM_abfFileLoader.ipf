#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma IgorVersion = 6.3
#pragma ModuleName = VMABFLoader
#pragma version = 1.3
#include "SoundBitesHelper", version >= 1


// by Roland Bock, May 2014
//
// created this file to make use of the XOP that Holger Taschenberger (Max Planck Gesellschaft) made
// available for reading pClamp 10 ABF files. This would avoid the need for the Bruxton extension.
// Since both of the XOPs are only working on Windows computers, I implemented conditional compiling 
// to clean up the menus.
//
// version 1.3 (OCT 2016): added an "auto load", "auto transient", "auto epoch" and "auto stats" function
//       to the "load next panel" to automate the data acquisition loading for different levels.
// version 1.1 (OCT 2016): added a panel to hold a button and display the name of the last loaded
//       abf file, to avoid interfering keyboard shortcuts with other applications, when Igor is 
//       not the top application.
// version 1 (MAY 2014)

// *********************************************************************
// ****************           C O N S T A N T S
// *********************************************************************

// global variable names for ABF loading function
static Strconstant ksTEps = "TotalEpisodes"
static Strconstant ksSInt = "SInterval"
static Strconstant ksTCh = "TotalChannels"
static Strconstant ksChNum = "ChannelNum"
static Strconstant ksChUnit = "ChannelUnit"
static Strconstant ksChNums = "ChannelNums"
static Strconstant ksEpSize = "EpisodeSize"
static Strconstant ksESt = "EpisodeStartTime"
static Strconstant ksStatus = "Status"
static Strconstant ksFilePath = "FilePath"
static Strconstant ksABFPackageName = "VM_ABFFileLoader"
static Strconstant ksVM_NextPanelName = "VM_LoadNextABFFilePanel"

static Strconstant ksVMSoundBites = "SoundBites"


static Strconstant ksVM_autoNext = "autoNext"
static Strconstant ksVM_autoTRFlag = "autoTransient"
static Strconstant ksVM_autoEpFlag = "autoEpoch"
static Strconstant ksVM_autoStatsFlag = "autoStats"
static Strconstant ksVM_loadInterval = "loadInterval"

static StrConstant ks_HTErrorMessage = "** ERROR %d (VM_loadABFIntoMatrixHT): %s\r"
static StrConstant ks_HTReadErrorMessage = "** ERROR reading data [channel: %g, episode: %g] (VM_loadABFIntoMatrixHT) (%d): %s\r"

// ***************    H O O K     F U N C T I O N S      *********************
// make sure the "load next abf file" panel is always closed at the end of the experiment
static Function IgorBeforeNewHook(igorApp)
	String igorApp

	cleanupPanel()
	return 0
End

static Function IgorBeforeQuitHook(unsavedExp, unsavedNotes, unsavedProcs)
	Variable unsavedExp, unsavedNotes, unsavedProcs
	
	cleanupPanel()
	return 0
End

Function NextABFFilePanelHook(wh)
	STRUCT WMWinHookStruct &wh
	
	Variable hookResult = 0
	
	switch(wh.eventCode)
		case 2:		// kill event
			SetWindow $(wh.winName), hook(cleanup)=$""
			cleanupPanel()
			break
	endswitch
	
	return hookResult
End

// *********************************************************************
// ****************                          M E N U
// *********************************************************************

Menu "Voltammetry", dynamic
	SubMenu "Load data"
		VM_menuLoadABF("normal"), VM_loadABFIntoMatrixBX()
		VM_menuLoadABF("next"), VM_loadABFIntoMatrixBX(next = 1)
		VM_menuLoadABF("overwrite"), VM_loadABFIntoMatrixBX(overwrite = 1)
		VM_menuLoadABF("calibration"), VM_loadABFIntoMatrixBX(calibration = 1)
//		VM_menuLoadABF("calibration overwrite"), VM_loadABFIntoMatrixBX(overwrite = 1, calibration = 1)
		VM_menuLoadABF("normal", xop = 2), VM_loadABFIntoMatrixHT()
		VM_menuLoadABF("overwrite", xop = 2), VM_loadABFIntoMatrixHT(overwrite = 1)
	End
End

Function /S VM_menuLoadABF(type, [xop])
	String type
	Variable xop
	
	if (ParamIsDefault(xop) || xop < 0)
		xop = 1				// default to Bruxton
	endif

	String text = ""
	
	#if Exists("ABFFileOpen")
	
	if (ABFXOPExists("Bruxton") && xop == 1)
		strswitch ( type )
			case "next":
				text = "[BX] load next ABF file .../S3"
				break			
			case "calibration overwrite":
				text = "[BX] ABF calibration files [overwrite] ..."
				break
			case "calibration":
				text = "[BX] ABF calibration files ..."
				break
			case "overwrite":
				text = "[BX] ABF data files [overwrite] .../2"
				break
			case "normal":
				text =  "[BX] ABF data files .../1"
				break
		endswitch
	endif

	#endif 
	
	#if Exists("bpc_AbfFileOpenDialog")
	
	if (ABFXOPExists("Bruxton") && xop == 2)
		strswitch ( type )
			case "normal":
				text = "-\r[HT] ABF data files .../4"
				break
			case "overwrite":
				text = "[HT] ABF data files [overwrite] .../5"
				break
		endswitch
	elseif (xop == 2)
		strswitch ( type )
			case "normal":
				text = "[HT] ABF data files .../1"
				break
			case "overwrite":
				text = "[HT] ABF data files [overwrite] .../2"
				break
		endswitch		
	endif
	
	#endif
	
	return text
End

// *********************************************************************
// ****************              S T A T I C     F U N C T I O N S 
// *********************************************************************

static Function ABFXOPExists(name)
	String name
	
	Variable exist
	String theOpList = "", theFnList = ""
	strswitch ( LowerStr(name) )
		case "ht":
			theFnList = FunctionList("bpc_Abf*", ";", "external")
			exist = strlen(theFnList) > 0
			break
		case "bruxton":
			theOpList = OperationList("ABF*", ";", "external")
			exist = strlen(theOpList) > 0
			break
		case "":
			theFnList = FunctionList("bpc_Abf*", ";", "external")
			theOpList = OperationList("ABF*", ";", "external")
			exist = strlen(theFnList) > 0 || strlen(theOpList) > 0
			break
	endswitch
	return exist
End

static Function /DF getABFFolder()
	DFREF cDF = GetDatafolderDFR()
	DFREF package = root:Packages:$ksABFPackageName
	if (DatafolderRefStatus(package) == 0)
		NewDatafolder /O/S root:Packages
		NewDatafolder /O/S $ksABFPackageName
		DFREF package = GetDatafolderDFR()
	endif
	SetDatafolder cDF
	return package
End

static Function /S getGlobalABFVar(name, [type])
	String name
	Variable type
	
	if (ParamIsDefault(type) || type < 0)
		type = 0
	else
		type = type > 1 ? 1 : round(type)
	endif
	DFREF package = getABFFolder()
	
	switch ( type )
		case 1:
			SVAR str = package:$name
			if (!SVAR_Exists(str))
				String /G package:$name
				SVAR str = package:$name
				str = ""
			endif
			break
		case 0:
		default:
			NVAR var = package:$name
			if (!NVAR_Exists(var))
				Variable /G package:$name
			endif
	endswitch
	
	return (GetDatafolder(1, package) + name)
End		

static Function cleanupPanel()
	NVAR autoNext = $(VoltHelpers#getNVarByName(ksVM_autoNext))	
	NVAR autoTransient = $(VoltHelpers#getNVarByName(ksVM_autoTRFlag))	
	NVAR autoEpoch = $(VoltHelpers#getNVarByName(ksVM_autoEpFlag))	
	NVAR autoStats = $(VoltHelpers#getNVarByName(ksVM_autoStatsFlag))	
	
	DoWindow /K $ksVM_NextPanelName
	KillVariables /Z autoNext, autoTransient, autoEpoch, autoStats
End

// **********************************************************************************
// ***********               M A I N    F U N C T I O N S         *******************
// **********************************************************************************

Function LoadNextABFFile(bs)
	STRUCT WMBackgroundStruct &bs
	
	Variable result = VM_loadABFIntoMatrixBX(next = 1, verbose = 0)

	if (result)
		NVAR autoTransient = $(VoltHelpers#getNVarByName(ksVM_autoTRFlag))	
		NVAR autoEpoch = $(VoltHelpers#getNVarByName(ksVM_autoEpFlag))
		NVAR autoStats = $(VoltHelpers#getNVarByName(ksVM_autoStatsFlag))

		if (autoTransient && ItemsInList(FunctionList("VM_makeTransient", ";", "KIND:2")) == 1)
			VM_makeTransient()
			// and select the first episode of the transient
			STRUCT WMButtonAction next
			next.win = ksVM_TransientGraphName
			next.eventCode = 2
			btnFunc_VM_nextTransient(next)
		endif

		if (autoEpoch && ItemsInList(WinList("VM_GUI.ipf", ";", "WIN:128")) == 1 && ItemsInList(WinList(ksVM_TransientGraphName, ";", "WIN:1")) == 1) 
			STRUCT WMButtonAction action
			action.win = ksVM_TransientGraphName
			action.eventCode = 2
			btnFunc_VM_keepAllAndDone(action)
		endif
		if (autoStats)
			String epochPanelName = VM_EpochControlPanel(move=0)
			DoWindow $epochPanelName
			if (V_flag == 1)
				Wave selWave = $GetUserData(epochPanelName, "", "epochSel")
				if (WaveExists(selWave))
					selWave = 1
					STRUCT WMButtonAction stats
					stats.win = epochPanelName
					stats.eventCode = 2
					stats.ctrlName = "btn_maxAndArea"
					btnFunc_VM_simpleStats(stats)
					stats.ctrlName = "btn_decayFits"
					btnFunc_VM_simpleStats(stats)
				endif
			endif
			DoWindow /F $ksVM_NextPanelName
		endif
	endif
	return result == 0
End



// this assumes that the ABF file only has the raw data episodes in it without any summarized episodes or analysis done in pClamp.
Function VM_loadABFIntoMatrixBX([overwrite, calibration, next, verbose])
	Variable overwrite, calibration, next, verbose
	
	overwrite = ParamIsDefault(overwrite) || overwrite < 1 ? 0 : 1
	calibration = ParamIsDefault(calibration) || calibration < 1 ? 0 : 1
	next = ParamIsDefault(next) || next < 1 ? 0 : 1
	verbose = ParamIsDefault(verbose) || verbose > 0 ? 1 : 0
	
	if (!ABFXOPExists("Bruxton"))
		print "** ERROR (VM_loadABFIntoMatrixBX): this function needs the Bruxton ABF XOP. Either it's missing or not properly installed. STOPPED!"
		return 0
	endif

	String fileList = ""
	SVAR lastABF = $(VoltHelpers#getSVarByName("lastABFFile"))

	if (next)
		LoadNextABFFilePanel(disable = 1)
		if (strlen(lastABF) > 0)
		 	PathInfo rbVMDataLoadPath
		 	String thePath = S_path
		 	if (V_flag && strlen(S_path) > 0)
		 		String lastFileName = ParseFilePath(3, lastABF,  ":",  0, 0)
				Variable nameLength = strlen(lastFileName)
		 		String lastFileNum = lastFileName[nameLength - 3, nameLength]
		 		String lastFileDate = lastFileName[0, nameLength - 4]
		 		String filePattern = lastFileDate + "%03d.abf"
				Variable nextNum = str2num(lastFileNum) + 1
		 		String newFileName = ""
		 		sprintf newFileName, filePattern, nextNum
		 		GetFileFolderInfo /P=rbVMDataLoadPath /Q/Z newFileName
		 		if (V_flag == 0 && V_isFile)
		 			fileList = thePath + newFileName + "\r"
				else
					fileList = SortList(VoltGUI#chooseFiles("abf"), "\r")				
		 		endif
		 	else
				fileList = SortList(VoltGUI#chooseFiles("abf"), "\r")
			endif
		 else
			fileList = SortList(VoltGUI#chooseFiles("abf"), "\r")
		endif
	else
		fileList = SortList(VoltGUI#chooseFiles("abf"), "\r")
	endif
	Variable filesToLoad = ItemsInList(fileList, "\r")
	
	if (filesToLoad == 0)
		if (verbose)
			print "-- no files selected to load, stopped."
		endif
		LoadNextABFFilePanel()		
		return 0 
	endif
	
	Execute /Q/Z "ModifyBrowser close"		// does not terminate execution if browser is not active
	
	Variable episode, channel, fileIndex, episodeInterval
	Variable rowOffset = 0, columnLength = 0, newColumnLength = 0
	Variable updateSteps = 5
	String fileName, cmd = "", dataWaveName = "", progressBar, theNote = ""
	
	DFREF cDF = GetDatafolderDFR()
	DFREF loadFolder = VoltHelpers#getDataLoadFolder()
	DFREF package = getABFFolder()
	
	NVAR status = $getGlobalABFVar(ksStatus)
	NVAR totalEpisodes = $getGlobalABFVar(ksTEps)
	NVAR totalChannels = $getGlobalABFVar(ksTCh)
	NVAR episodeSize= $getGlobalABFVar(ksEpSize)
	NVAR epStartTime = $getGlobalABFVar(ksESt)
	NVAR sInt = $getGlobalABFVar(ksSInt)
	NVAR channelNumber = $getGlobalABFVar(ksChNum)
	SVAR channelUnit = $getGlobalABFVar(ksChUnit, type=1)
	SVAR filePath = $getGlobalABFVar(ksFilePath, type=1)
	

	String experimentName = IgorInfo(1)
	if (cmpstr(experimentName, "Untitled") == 0)
		experimentName = ParseFilePath(3, StringFromList(0, fileList, "\r"), ":", 0, 0) + VoltGUI#getOrgzdExt()
	endif
	if (strsearch(experimentName, VoltGUI#getOrgzdExt(), 0) == -1)
		experimentName += VoltGUI#getOrgzdExt()
	endif
	SaveExperiment /P=home as (experimentName + ".pxp")

	print "- started to import", filesToLoad, "ABF files on", date(), "at", time()

	progressBar = VM_getProgressBarPanel(theTitle="Loading ABF File", theName="ProgressWaveLoad", theColor="blue")

	SetDatafolder package

	for (fileIndex = 0; fileIndex < filesToLoad; fileIndex += 1)
		filePath = StringFromList(fileIndex, fileList, "\r")
		fileName = ParseFilePath( 3, filePath, ":", 0, 0)
		
		status = 0
		totalEpisodes = 0
		totalChannels = 0
		episodeSize = 0
		epStartTime = 0
		channelNumber = 0
		
		sprintf cmd, "ABFFileOpen %s, status", ksFilePath
		Execute /Q cmd
		sprintf cmd, "ABFEpisodeGetCount %s, status", ksTEps
		Execute /Q cmd
		sprintf cmd, "ABFFileGetSampleInterval %s, status", ksSInt
		Execute /Q cmd
		Execute /Q "ABFEpisodeSet 1, status"
		sprintf cmd, "ABFEpisodeGetSampleCount %s, status", ksEpSize
		Execute /Q cmd
		sprintf cmd, "ABFChannelGetCount %s, status", ksTCh
		Execute /Q cmd

		if (totalEpisodes == 0)
			// the abf file has no recorded data
			printf "** WARNING: the file %s contains no data -> skipped!\r", fileName
			continue
		endif
		
		// remember the last loaded file name so that the "next" function works
		lastABF = fileName
		
		Make /O /D /N=(episodeSize) dataImport
		Make /O /N=(totalChannels) $ksChNums /WAVE=channelNums
		
		if ( totalEpisodes > 1000 )
			updateSteps = 50
		elseif ( totalEpisodes > 500 )
			updateSteps = 20
		elseif ( totalEpisodes > 100 )
			updateSteps = 10
		else
			updateSteps = 5
		endif

		overwrite = fileIndex > 0 ? 0 : overwrite			// if we load multiple files, make sure that the following file doesn't overwrite the first
		
		for (channel = 0; channel < totalChannels; channel += 1)
			sprintf cmd, "ABFChannelGetADC %g, %s, status", channel, ksChNum
			Execute cmd
			channelNums[channel] = channelNumber
			sprintf cmd, "ABFChannelGetUnits %g, %s, status", channel, ksChUnit
			Execute cmd
			
			dataWaveName = ksVM_dataPrefix + num2str(channelNumber)
			Wave data = loadFolder:$dataWaveName
			if (!WaveExists(data))
				SetDatafolder loadFolder
				String aList = WaveList(ksVM_dataPrefix + "*", ";", "")
				if (ItemsInList(aList) > 0)
					Wave otherChannel = loadFolder:$(StringFromList(0, aList))
					theNote = StringByKey("STATUS", note(otherChannel))
					if (cmpstr(theNote, "new") != 0)
						rowOffset = DimSize(otherChannel, 0)
					endif
					theNote = ""
				else
					rowOffset = 0
				endif
				Make /O /N=(rowOffset + totalEpisodes, episodeSize) loadFolder:$dataWaveName /WAVE=data
				Note /K data, ReplaceStringByKey("STATUS", theNote, "new")
				data = 0
				SetDatafolder package
			elseif ( overwrite )
				Redimension /N=(totalEpisodes, episodeSize) data
				data = 0
				rowOffset = 0
			else
				if (channel == 0)
					rowOffset = DimSize(data, 0)
					columnLength = DimSize(data, 1)
					
					if (columnLength != episodeSize)
						DoAlert /T="Wrong Episode Length" 1, "The episodes don't have the same size as the previously imported ones. This can create problems. Do you want to continue?"
						
						if (V_flag == 2)
							// No clicked
							Execute "ABFFileClose"
							return 0
						endif
						// resize the columns to the maxiumum data length
						newColumnLength = max(columnLength, episodeSize)
					else
						newColumnLength = columnLength
					endif
				endif
				
				Redimension /N=(rowOffset + totalEpisodes, newColumnLength) data
			endif
			
			SetScale d 0,0, channelUnit, data
			SetScale /P y 0, sInt, "s", data
		endfor
		
		VM_updateProgressBar(0, info = "importing " + num2str(totalEpisodes) + " episodes from file '" + fileName + "'", theName = progressBar)
		
		for ( episode = 1; episode <= totalEpisodes; episode += 1)
			sprintf cmd, "ABFEpisodeSet %g, status", episode
			Execute cmd
			if (episode == 1)
				sprintf cmd, "ABFEpisodeGetStart %s, status", ksESt
				Execute cmd
				episodeInterval = epStartTime
			elseif( episode == 2)
				sprintf cmd, "ABFEpisodeGetStart %s, status", ksESt
				Execute cmd
				episodeInterval = epStartTime - episodeInterval
			endif
				
			if (mod(episode, updateSteps) == 0)
				VM_updateProgressBar((episode/totalEpisodes), theName = progressBar)
			endif
			
			for (channel = 0; channel < totalChannels; channel += 1)
				sprintf cmd, "ABFEpisodeRead %g, 0, %g, dataImport, status", channel, episodeSize
				Execute cmd
				
				dataWaveName = ksVM_dataPrefix + num2str(channelNums[channel])
				Wave data = loadFolder:$dataWaveName
				data[rowOffset + episode - 1][, numpnts(dataImport) - 1] = dataImport[q]
				
				if (episode == 3)
					theNote = ReplaceStringByKey("STATUS", note(data), "filled")
					Note /K data, theNote
					SetScale /P x 0, episodeInterval, "s", data
				endif	
			endfor
		endfor
		
		VM_updateProgressBar(1, theName = progressBar)
		Execute "ABFFileClose"
		print "-- finished file", fileName, "at", time()
	endfor
	SetDatafolder cDF

	DoWindow /K $progressBar 		// close the progress bar window

	KillDatafolder /Z package			// get rid of the loading package
	SaveExperiment
	
	if (next)
		LoadNextABFFilePanel()
	endif
	
	VM_checkChannelAssign()
	
	if (calibration)
		String dataChannel = VM_getDataChannel()
		if (strlen(dataChannel) == 0)
			// ERROR
			DoAlert 0, "ERROR: can't identify the data channel. There can only be exactly one data channel. Please check your channel assignments!"
			VM_createChannelAssign("", refine=1)
		else
			SetDatafolder loadFolder
			String channelList = WaveList("*", ";", "DIMS:2")
			SetDatafolder cDF
			Variable numloadedChannels = ItemsInList(channelList)
			for (channel = 0; channel < numloadedChannels; channel += 1)
				Wave channelWave = loadFolder:$(StringFromList(channel, channelList))
				if (cmpstr(NameOfWave(channelWave), dataChannel) != 0)
					KillWaves /Z channelWave
				endif
			endfor
			Wave /T assign = VoltHelpers#getChannelAssignment()
			channel = 0
			do
				if (cmpstr(dataChannel, GetDimLabel(assign, 0, channel)) == 0)
					channel += 1
				else
					DeletePoints channel, 1, assign
				endif
			while (channel < DimSize(assign, 0))
				
		endif
	endif
	return 1
End

#if Exists("bpc_AbfFileOpenDialog")

Function VM_loadABFIntoMatrixHT([overwrite])
	Variable overwrite
	
	if (ParamIsDefault(overwrite) || overwrite < 0)
		overwrite = 0
	else
		overwrite = overwrite > 1 ? 1 : round(overwrite)
	endif
//	if (ParamIsDefault(calibration) || calibration < 0)
//		calibration = 0
//	else
//		calibration = calibration > 1 ? 1 : round(calibration)
//	endif
	
	if (!ABFXOPExists("ht"))
		print "** ERROR (VM_loadABFIntoMatrix): this function needs the ABF XOP by Holger Taschenberger. Either it's missing or not properly installed. STOPPED!"
		return 0
	endif

	String fileList = SortList(VoltGUI#chooseFiles("abf"), "\r")
	Variable filesToLoad = ItemsInList(fileList, "\r")
	
	if (filesToLoad == 0)
		print "-- no files selected to load, stopped."
		return 0
	endif
	
	fileList = ReplaceString(":", fileList, "\\")		// convert to windows path for the bpc_ commands
	
	Execute /Q/Z "ModifyBrowser close"		// does not terminate execution if browser is not active
	
	Variable episode, channel, fileIndex, episodeInterval
	Variable rowOffset = 0, columnLength = 0, newColumnLength = 0
	Variable updateSteps = 5
	String fileName, dataWaveName = "", progressBar, theNote = ""
	
	DFREF cDF = GetDatafolderDFR()
	DFREF loadFolder = VoltHelpers#getDataLoadFolder()
	DFREF package = getABFFolder()
	
	SVAR filePath = $getGlobalABFVar(ksFilePath, type=1)
	
	String experimentName = IgorInfo(1)
	if (cmpstr(experimentName, "Untitled") == 0)
		experimentName = ParseFilePath(3, StringFromList(0, fileList, "\r"), ":", 0, 0) + VoltGUI#getOrgzdExt()
	endif
	if (strsearch(experimentName, VoltGUI#getOrgzdExt(), 0) == -1)
		experimentName += VoltGUI#getOrgzdExt()
	endif
	SaveExperiment /P=home as (experimentName + ".pxp")

	print "- started to import", filesToLoad, "ABF files on", date(), "at", time()

	progressBar = VM_getProgressBarPanel(theTitle="Loading ABF File", theName="ProgressWaveLoad", theColor="blue")

	SetDatafolder package

	Variable totalChannels, totalEpisodes, episodeSize
	for (fileIndex = 0; fileIndex < filesToLoad; fileIndex += 1)
		filePath = StringFromList(fileIndex, fileList, "\r")
		fileName = bpc_AbfFileStripExt(bpc_AbfFileStripPathName(filePath))
		
		overwrite = fileIndex > 0 ? 0 : overwrite			// if we load multiple files, make sure that the following file doesn't overwrite the first

		if (!bpc_AbfIsAbfFile(filePath))
			printf "** WARNING (VM_loadABFIntoMatrixHT): the file %s is not an ABF file -> skipped!\r", fileName
			continue
		endif
		if (!bpc_AbfHasData(filePath))
			printf "** WARNING (VM_loadABFIntoMatrixHT): the file %s has no data -> skipped!", fileName
			continue
		endif
		
		if (!bpc_AbfChannelCount(filePath, totalChannels))
			printf "** ERROR %d (VM_loadABFIntoMatrixHT): %s", bpc_AbfLastErr(), bpc_AbfLastErrStr()
			continue
		endif

		if (!bpc_AbfEpisodeCount(filePath, totalEpisodes))
			printf "** ERROR %d (VM_loadABFIntoMatrixHT): %s", bpc_AbfLastErr(), bpc_AbfLastErrStr()
			continue
		endif
		
		if (!bpc_AbfSampleCount(filePath, 1, episodeSize))
			printf "** ERROR %d (VM_loadABFIntoMatrixHT): %s", bpc_AbfLastErr(), bpc_AbfLastErrStr()
			continue
		endif

		Make /O /D /N=(episodeSize) dataImport = NaN

		if (!bpc_AbfEpisodeRead(filePath, 0, 1, dataImport))
			printf ks_HTErrorMessage, bpc_AbfLastErr(), bpc_AbfLastErrStr()
			continue
		endif
		
		String channelUnit = WaveUnits(dataImport, 1)
		String timeUnit = WaveUnits(dataImport, 0)

		String dataInfo = note(dataImport)
		String channelNumbers = ReplaceString(",", StringByKey("nADCSamplingSeq", dataInfo, "="), ";")
		Variable samplingInterval = NumberByKey("fADCSequenceInterval", dataInfo, "=")
		
		// first prepare channel matrix waves to hold the amount of data we need to read
		for (channel = 0; channel < totalChannels; channel += 1)
			dataWaveName = ksVM_dataPrefix + StringFromList(channel, channelNumbers)
			Wave data = loadFolder:$dataWaveName
			if (!WaveExists(data))
				SetDatafolder loadFolder
				String aList = WaveList(ksVM_dataPrefix + "*", ";", "")
				if (ItemsInList(aList) > 0)
					Wave otherChannel = loadFolder:$(StringFromList(0, aList))
					theNote = StringByKey("STATUS", note(otherChannel))
					if (cmpstr(theNote, "new") != 0)
						rowOffset = DimSize(otherChannel, 0)
					endif
					theNote = ""
				else
					rowOffset = 0
				endif
				Make /O /N=(rowOffset + totalEpisodes, episodeSize) loadFolder:$dataWaveName /WAVE=data
				Note /K data, ReplaceStringByKey("STATUS", theNote, "new")
				data = 0
				SetDatafolder package
			elseif ( overwrite )
				Redimension /N=(totalEpisodes, episodeSize) data
				data = 0
				rowOffset = 0
			else
				if (channel == 0)
					rowOffset = DimSize(data, 0)
					columnLength = DimSize(data, 1)
					
					if (columnLength != episodeSize)
						DoAlert /T="Wrong Episode Length" 1, "The episodes don't have the same size as the previously imported ones. This can create problems. Do you want to continue?"
						
						if (V_flag == 2)
							// No clicked
							return 0
						endif
						// resize the columns to the maxiumum data length
						newColumnLength = max(columnLength, episodeSize)
					else
						newColumnLength = columnLength
					endif
				endif
				
				Redimension /N=(rowOffset + totalEpisodes, newColumnLength) data
			endif
			
			SetScale d 0,0, channelUnit, data
			SetScale /P y 0, samplingInterval, timeUnit, data
		endfor

		if ( totalEpisodes > 1000 )
			updateSteps = 50
		elseif ( totalEpisodes > 500 )
			updateSteps = 20
		elseif ( totalEpisodes > 100 )
			updateSteps = 10
		else
			updateSteps = 5
		endif

		VM_updateProgressBar(0, info = "importing " + num2str(totalEpisodes) + " episodes from file '" + fileName + "'", theName = progressBar)
		
		for ( episode = 1; episode <= totalEpisodes; episode += 1)
			episodeInterval = 0.1
			
			if (mod(episode, updateSteps) == 0)
				VM_updateProgressBar((episode/totalEpisodes), theName = progressBar)
			endif
			
			for (channel = 0; channel < totalChannels; channel += 1)
			
				if (!bpc_AbfEpisodeRead(filePath, channel, episode, dataImport))
					printf ks_HTReadErrorMessage, channel, episode, bpc_AbfLastErr(), bpc_AbfLastErrStr()
					continue
				endif
				
				dataWaveName = ksVM_dataPrefix + StringFromList(channel, channelNumbers)
				Wave data = loadFolder:$dataWaveName
				data[rowOffset + episode - 1][, numpnts(dataImport) - 1] = dataImport[q]
				
				if (episode == 3)
					theNote = ReplaceStringByKey("STATUS", note(data), "filled")
					Note /K data, theNote
					SetScale /P x 0, episodeInterval, "s", data
				endif	
			endfor
		endfor
		
		VM_updateProgressBar(1, theName = progressBar)
		print "-- finished file", fileName, "at", time()
	endfor
	SetDatafolder cDF

	DoWindow /K $progressBar 		// close the progress bar window

	KillDatafolder /Z package			// get rid of the loading package
	SaveExperiment
	
	VM_checkChannelAssign()
	
End

#endif


// created this panel soley to hold the "load next" button, since the keyboard shortcuts 
// required Igor to be the active appliction up front, which it often wasn't during recording;
// and instead activated the keyboard shortcut for the recording application.
static Function LoadNextABFFilePanel([disable])
	Variable disable
	
	disable = ParamIsDefault(disable) || disable < 1 ? 0 : 1

	String panelTitel = "Load Next ABF File"
	Variable doNew = 0
	DoWindow $ksVM_NextPanelName
	if (V_flag)
		if (WinType(ksVM_NextPanelName) != 7)
			KillWindow $ksVM_NextPanelName
			doNew = 1
		endif
	else
		doNew = 1
	endif
	
	SVAR lastABF = $(VoltHelpers#getSVarByName("lastABFFile"))
	NVAR autoNext = $(VoltHelpers#getNVarByName(ksVM_autoNext))
	NVAR autoTransient = $(VoltHelpers#getNVarByName(ksVM_autoTRFlag))	
	NVAR autoEpoch = $(VoltHelpers#getNVarByName(ksVM_autoEpFlag))	
	NVAR autoStats = $(VoltHelpers#getNVarByName(ksVM_autoStatsFlag))
	NVAR loadInterval = $(VoltHelpers#getNVarByName(ksVM_loadInterval))
	
	if (doNew)
//		SB_LoadSoundBite("cheer")
		NewPanel /K=1 /W=(10,10,220,130) /N=$ksVM_NextPanelName as panelTitel
		ModifyPanel /W=$ksVM_NextPanelName fixedSize=1, noEdit=1
		
		loadInterval = 120
		
		SetVariable sv_lastLoadedABFFile, win=$ksVM_NextPanelName, pos={22,5},size={180,20},bodyWidth=140
		SetVariable sv_lastLoadedABFFile, win=$ksVM_NextPanelName, title="last file "
		SetVariable sv_lastLoadedABFFile, win=$ksVM_NextPanelName, font=$ksVM_defPanelFont, fSize=12
		SetVariable sv_lastLoadedABFFile, win=$ksVM_NextPanelName, limits={-inf,inf,0},noedit= 1
		SetVariable sv_lastLoadedABFFile, win=$ksVM_NextPanelName, value=lastABF
		SetVariable sv_lastLoadedABFFile, win=$ksVM_NextPanelName, help={"Name of last loaded ABF file."}
		
		Button btn_loadNextABFFile, win=$ksVM_NextPanelName, pos={10,62}, size={90,40}, title="next ABF"
		Button btn_loadNextABFFile, win=$ksVM_NextPanelName, fSize=14, fStyle=1
		Button btn_loadNextABFFile, win=$ksVM_NextPanelName, help={"load the next abf file in sequence"}
		Button btn_loadNextABFFile, win=$ksVM_NextPanelName, proc=VMABFLoader#btn_loadNextABFFile
		if (!ABFXOPExists(""))
			Button btn_loadNextABFFile, win=$ksVM_NextPanelName, disable=2
		endif
		
		Checkbox cb_autoLoadNext, win=$ksVM_NextPanelName, pos={110, 26}, size={50,20}, title="auto load"
		Checkbox cb_autoLoadNext, win=$ksVM_NextPanelName, variable=autoNext
		Checkbox cb_autoLoadNext, win=$ksVM_NextPanelName, proc=cbx_autoLoadNext
		Checkbox cb_autoLoadNext, win=$ksVM_NextPanelName, disable=0
		
		SetVariable sv_abfFileInterval, win=$ksVM_NextPanelName, pos={125, 43}, size={80, 20}, bodyWidth=35
		SetVariable sv_abfFileInterval, win=$ksVM_NextPanelName, title = "interval (s)"
		SetVariable sv_abfFileInterval, win=$ksVM_NextPanelName, limits={-inf,inf,0}, value=loadInterval
		SetVariable sv_abfFileInterval, win=$ksVM_NextPanelName, help={"interval of next ABF file load in seconds"}

		if (!ABFXOPExists(""))
			Checkbox cb_autoLoadNext, win=$ksVM_NextPanelName, disable=1
			SetVariable sv_abfFileInterval, win=$ksVM_NextPanelName, disable=1
		endif
				
		Checkbox cb_autoTransient, win=$ksVM_NextPanelName, pos={110, 60}, size={50,20}, title="auto transient"
		Checkbox cb_autoTransient, win=$ksVM_NextPanelName, variable=autoTransient
		Checkbox cb_autoTransient, win=$ksVM_NextPanelName, proc=cbx_autoSetFlags
//		
//		Checkbox cb_autoEpoch, win=$ksVM_NextPanelName, pos={130, 77}, size={50,20}, title="auto epoch"
//		Checkbox cb_autoEpoch, win=$ksVM_NextPanelName, variable=autoEpoch, disable = 2
//		Checkbox cb_autoEpoch, win=$ksVM_NextPanelName, proc=cbx_autoSetFlags
//		
//		Checkbox cb_autoStats, win=$ksVM_NextPanelName, pos={130, 94}, size={50,20}, title="auto stats"
//		Checkbox cb_autoStats, win=$ksVM_NextPanelName, variable=autoStats, disable = 2
//		Checkbox cb_autoStats, win=$ksVM_NextPanelName, proc=cbx_autoSetFlags
//		
//		if (WhichListItem("VM_makeTransient", FunctionList("VM_makeTransient", ";", "KIND:2")) < 0)
//			Checkbox cb_autoTransient, win=$ksVM_NextPanelName, value=0, disable=0
//		endif	
		
		SetWindow $ksVM_NextPanelName, hook(cleanup)=NextABFFilePanelHook
		moveWindowOnScreen(ksVM_NextPanelName, location="right")
//		SB_play("cheer")
	endif
	
	if (ABFXOPExists(""))
		if (disable)
			Button btn_loadNextABFFile, win=$ksVM_NextPanelName, disable=2
		else
			Button btn_loadNextABFFile, win=$ksVM_NextPanelName, disable=0
		endif	
	endif
	return 1	
End

static Function btn_LoadNextABFFile(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch ( bn.eventCode )
		case 2:
			CtrlNamedBackground NextABFLoader, status
			if (NumberByKey("RUN", S_info) == 1)
				// if autoload is on and we have a background process, stop it
				CtrlNamedBackground NextABFLoader, stop
			endif
			NVAR autoNext = $(VoltHelpers#getNVarByName(ksVM_autoNext, value=0))
			
			STRUCT WMBackgroundStruct bs
			LoadNextABFFile(bs)
			
			break
	endswitch
	
	return 0
end

Function cbx_autoLoadNext(cb) : CheckboxControl
	STRUCT WMCheckboxAction &cb
	
	switch (cb.eventCode)
		case 2:		// mouse up
			NVAR loadInterval = $(VoltHelpers#getNVarByName(ksVM_loadInterval))
			
			CtrlNamedBackground NextABFLoader, period=(loadInterval * 60), proc=LoadNextABFFile
			CtrlNamedBackground NextABFLoader, status
			if (cb.checked)
				if (NumberByKey("RUN", S_info) == 0)
					CtrlNamedBackground NextABFLoader, start
				endif
			else
				if (NumberByKey("RUN", S_info) == 1)
					CtrlNamedBackground NextABFLoader, stop
				endif
			endif
			break
	endswitch
	return 0
End

Function cbx_autoSetFlags(cb) : CheckboxControl
	STRUCT WMCheckboxAction &cb
	
	switch (cb.eventCode)
		case 2:
			NVAR autoTransient = $(VoltHelpers#getNVarByName(ksVM_autoTRFlag))
			NVAR autoEpoch = $(VoltHelpers#getNVarByName(ksVM_autoEpFlag))
			NVAR autoStats = $(VoltHelpers#getNVarByName(ksVM_autoStatsFlag))

			strswitch (cb.ctrlName)
				case "cb_autoTransient":
					if (cb.checked)
//						Checkbox cb_autoEpoch, win=$ksVM_NextPanelName, disable=0
					else
						autoEpoch = 0
						autoStats = 0
//						Checkbox cb_autoEpoch, win=$ksVM_NextPanelName, disable=2
//						Checkbox cb_autoStats, win=$ksVM_NextPanelName, disable=2
					endif	
					break
				case "cb_autoEpoch":
					if (cb.checked)
						Checkbox cb_autoStats, win=$ksVM_NextPanelName, disable=0
						autoTransient = 1
					else
						Checkbox cb_autoStats, win=$ksVM_NextPanelName, disable=2
						autoStats = 0
					endif
					break
			endswitch
			break
	endswitch
	
	return 0
End