#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion = 6.22
#pragma version = 3.1
#pragma ModuleName = VoltHelpers
#include "RBMinionsVersionControl", version >= 2.4
#include "VM_Ramp", version >= 1
#include "VM_hookFunctions", version >= 2
#include "VM_GUI", version >= 2.4
#include "ListHelpers", version >= 4.2
#include "GraphHelpers", version >= 3.7
#include "MeanAndStDev", version >= 6.69

// by Roland Bock, January 2012
//
// version 3.1 (MAY 2017): various bug fixes
// version 2.8 (SEP 2015): created new button on CV trace graph during initial transient analysis to 
//								set the default dopamine range to 0.4 - 0.6 V for really small responses
// version 2.4 (JUN 2015): added helper function to calibrate transient traces
// version 2.3 (JUN 2015): added default calibration factor to preferences
// version 2.1 (APR 2013):
// version 1 (FEB 2013)
// ** version 0.1: JAN 2012

// Helper functions for the voltametry analysis

// ******************************************************
// **************                     C O N T S T A N T S 
// ******************************************************
Constant kVM_PrefsVersion = 5
Constant kVM_colorCatPrefsVersion = 1
Constant kVM_ExpFeatureVersion = 1
Constant kVM_ChannelPrefsVersion = 1
Constant kVM_AcqFrequency = 10					// Hz

#if defined(MACINTOSH)
Strconstant ksVM_defGraphFont = "Helvetica"
#else
Strconstant ksVM_defGraphFont = "Arial"
#endif

// ----------    Chemical Specific Constants    -------------
static Constant kDopamineStartVoltage = 0.5			// in V
static Constant kDopamineEndVoltage = 0.7			// in V
static Constant kEnkStartVoltage = 0.8				// in V
static Constant kEnkEndVoltage = 1.2					// in V

static Constant kMaxChannels = 16					// to initialize arrays in control/trigger structures
static Constant kBaselinePoints = 10					// number of points for the baseline
static Constant kTransientPoints = 60				// number of points for transient from start of peak
static Constant kCVPlotPoints = 2					// number of points left and right of the peak value to average for the CV plot

Constant kVM_scaleUpperFraction = 0.7755			// fraction of rainbow scale between 0 and max number
Constant kVM_scaleLofUFraction = 0.291667
Constant kVM_scaleUofLFraction = 3.42857
Constant kVM_defMaxColorScaleVG = 1.2e-08
Constant kVM_defMinColorScaleVG = -3.5e-09

Constant kVM_numCatPrefs = 10

Constant kVM_defCalibrationFactor = 30.41			// nM dopamine for 1 nA current

Strconstant ksPackagePrefsName = "RB Voltammetry Package"
Strconstant ksPrefsFileName = "VoltammetryPreferences.bin"

Strconstant ksVM_DefChannelAssignList = "AD0:control;AD1:data;AD2:control;AD3:other;"
Strconstant ksVM_DefChannelClassList = "AD0:laser;AD1:data;AD2:electrical;AD3:other;"

Strconstant ksVM_DefBinaryChoiceList = "Yes;No;"

Strconstant ksVM_restrictedDispValue = "restricted"

Strconstant ksVM_ErrorMsg = "** ERROR [Voltammetry] (%s): %s!\r"
Strconstant ksVM_WarnMsg = "-- WARNING [Voltammetry] (%s): %s.\r"

// folder names
static Strconstant ksPackageName = "Voltammetry"
static Strconstant ksInitalDataFolderName = "VMRawData"
static Strconstant ksEpochsFolderName = "VMEpochs"
static Strconstant ksErrorDisplay = "VMErrorDisplay"

// variable names
static Strconstant ksPeakPoint = "TRPeakPoint"
static Strconstant ksTRStartPoint = "TRStartPoint"
static Strconstant ksTREndPoint = "TREndPoint"
static Strconstant ksStimStart = "TRStimulus"
static Strconstant ksTRPeakVoltStart = "TRPeakVoltStart"
static Strconstant ksTRPeakVoltEnd = "TRPeakVoltEnd"
static Strconstant ksTRBaselinePoints = "TRBaselinePoints"
static Strconstant ksTRTransientPoints = "TRTransientPoints"
static Strconstant ksCurrentEpochClass = "currentEpochClass"
static Strconstant ksNoUpdateName = "noUpdateFlag"					// prevents the cursor hook functions from updating
static Strconstant ksKeepCVFront = "keepCVFrontFlag"
static Strconstant ksCVCsrToMax = "alignCVCsrFlag"
static Strconstant ksCalibFactorName = "calibrationFactor"
static Strconstant ksPrefsName = "CurrentTransientPrefs"

// wave names
static Strconstant ksChannelAssign = "ChannelAssign"
static Strconstant ksChannelClasses = "ChannelClasses"
static Strconstant ksEpochsName = "EpochLocations"
static Strconstant ksEpochsCategories = "EpochCategories"
static Strconstant ksEpochDataTraces = "EpochDataTraces"
static Strconstant ksEpochControlMatrix = "EpochControl"
static Strconstant ksEpochControlTrace = "EpochControlTrace"
static Strconstant ksEpochTransient = "EpochTransient"
static Strconstant ksVGCVName = "CVVoltammogram"
static StrConstant ksVGImageName = "ImageVoltammogram"
static StrConstant ksVGBaselineName = "VoltammogramBaseline"
static StrConstant ksVGDataTrace = "VGDataTrace"
static Strconstant ksCalibBaseline = "CalibrationBaselineTraces"
static Strconstant ksCalibStim = "CalibrationStimulationTraces"

// wave name additions
static Strconstant ksEpochTraces = "traces"
static Strconstant ksEpochVGimg = "img"
static Strconstant ksEpochVGcv = "cv"

// window names
static Strconstant ksEpochCatTableName = "EpochCatTable"
static Strconstant ksEpochCatTableTitle = "Epoch Categories Table"
static Strconstant ksEpochLocTableName = "EpochLocationTable"
static Strconstant ksEpochLocTableTitle = "Epoch Location Table"
static Strconstant ksVGWinBaseName = "VoltGram"
static Strconstant ksVGImageExtension = "img"
static Strconstant ksVGCVExtension = "cv"
static Strconstant ksChannelAssignTName = "ChannelTable"
static Strconstant ksControlGraphName = "TriggerTraces"

// list keys
Strconstant ksVM_ChannelLbl = "CHANNELLBL"
Strconstant ksVM_ChannelDesig = "CHDESIGNATION"
Strconstant ksVM_BaselineNum = "BASELINE"
Strconstant ksVM_StimPointinMatrix = "STIMPOINT"
Strconstant ksVM_AvgStartVoltKey = "AVGSTARTVOLT"
Strconstant ksVM_AvgEndVoltKey = "AVGENDVOLT"
Strconstant ksVM_AvgStartPointKey = "AVGSTARTPOINT"
Strconstant ksVM_AvgEndPointKey = "AVGENDPOINT"
Strconstant ksVM_DisplayKey = "DISPLAY"
Strconstant ksVM_AvgCVKey = "CVAVG"
Strconstant ksVM_CVPeakLocKey = "CVLOC"
Strconstant ksVM_TransientLineKey = "TRLINE"
Strconstant ksVM_CalibrateKey = "CALIBFACT"

// ******************************************************
// **********                      S T R U C T U R E S
// ******************************************************
static Structure CleanUpInfo
	DFREF wavePath
	String waveNames[20]
	String wavePrefixes[5]
	String windows[20]
	String variableNames[20]
	String stringNames[20]
	Variable numWaves
	Variable numPrefixes
	Variable numWindows
	Variable numVars
	Variable numStrs
EndStructure

Structure VoltammetryPreferences
	uint32 version

	STRUCT OxidationDetails oxDetails
	
	int16 baselinePoints
	int16 transientPoints
	int16 episodePoints
	int16 CVAvgPoints
	char type[100]
	double calibrationFactor
EndStructure

Structure ChannelDescription
	uint32 version
	char name[10]
	char type[20]
	char class[20]
EndStructure

Structure ChannelAssignment
	uint32 version
	STRUCT ChannelDescription channels[8]
	int16 numChannels
EndStructure

Structure VM_ExpFeatureControl
	uint32 version
	STRUCT VM_ExperimentalFeature features[10]
EndStructure

Structure VM_ExperimentalFeature
	uint32 version
	char feature[50]
	int16 enabled
	int16 warningEnabled
EndStructure

Structure VM_ControlChannels
	Wave /T assign					// assignment wave
	Wave controls					// indices of extracted control channels
	String labels[kMaxChannels]	// labels of the control channels
	Wave traces[kMaxChannels]		// wave of matrix waves of the control channels
	Variable numControls			// number of control channels
EndStructure

Structure VM_VGColorScale
	uint32 version
	int16 method					// 0 = automatic, 1 = manual
	double max						// maximum value
	double min						// minimum value
	double PofUpper				// lower scale percentage of the upper fraction
	double PofLower				// upper scale percentage of the lower fraction
EndStructure

Structure VM_CategoryColor
	uint32 version
	char name[31]
	char color[45]
EndStructure

Structure VM_CatColorPrefs
	uint32 version
	STRUCT VM_CategoryColor colorPref[kVM_numCatPrefs]
EndStructure

	
// ******************************************************
// **************                     S T A T I C S 
// ******************************************************

// ***********      S T R U C T U R E    F U N C T I O N S      *************
// **************        P R E F E R E N C E S           *****************
static Function updateVersion()
	VCheck#setVersion(ksPackageName, kVM_version)
End

static Function AfterCompiledHook()
	RBVC_checkVersion(ksPackageName, kVM_Version)
	
	if (VM_needsUpdate())
		if (VM_getFileVersion() <= 2.4)
			// with version 2.5 we switched to ramp value structures, so create the default
			//   one if it does not yet exists. Since until version 2.4 we only did dopamine, 
			//   we can set it to the default dopamine structure.
			STRUCT RampValues ramp
			VMRamps#LoadRamp(ramp, "dopamine")
			VMRamps#SaveRamp()
		endif
		VM_setVersion()
	endif
	return 0
End

static Function IgorBeforeNewHook(igorApplicationNameStr)
	String igorApplicationNameStr

	saveTransPrefs()
	return 0	
End

static Function IgorBeforeQuitHook(unsavedExp, unsavedNotebooks, unsavedProcedures)
	Variable unsavedExp, unsavedNotebooks, unsavedProcedures

	saveTransPrefs()	
	return 0	// Proceed with normal quit process
End

static Function BeforeExperimentSaveHook(refNum, fileName, path, type, creator, kind)
	Variable refNum
	String fileName, path, type, creator, kind
	
	VM_setVersion()
	return 0
End

static Function getPrefsRecordID(type)
	String type
	
	Variable id = 0
	strswitch (LowerStr(type))
		case "enkephalin":
			id = 1
			break
		case "dopamine":
		default:
			id = 0
	endswitch
	return id
End

static Function getTransPrefs(prefs)
	STRUCT VoltammetryPreferences &prefs
	
	DFREF path = getPackage()
	SVAR str = path:$ksPrefsName
	if (!SVAR_Exists(str))
		loadTransPrefs(prefs)
	else
		StructGet /S prefs, str	
	endif
End

static Function setTransPrefs(prefs)
	STRUCT VoltammetryPreferences &prefs
	
	DFREF path = getPackage()
	SVAR str = path:$ksPrefsName
	if (!SVAR_Exists(str))
		String /G path:$ksPrefsName = ""
		SVAR str = path:$ksPrefsName
	endif
	
	StructPut /S prefs, str	
End

static Function loadTransPrefs(prefs)
	STRUCT VoltammetryPreferences &prefs
	
	Variable recordID = VMRamps#getCurrentRecordID() - 1
		
	LoadPackagePreferences /MIS=1 ksPackagePrefsName, ksPrefsFileName, recordID, prefs
	if (V_flag != 0 || V_bytesRead == 0 || prefs.version != kVM_PrefsVersion)
		defaultPrefsStruct(prefs)
	endif
	setTransPrefs(prefs)
End

static Function saveTransPrefs([reset])
	Variable reset
	
	reset = ParamIsDefault(reset) || reset < 1 ? 0 : 1

	Variable recordID = VMRamps#getCurrentRecordID() - 1	
	STRUCT VoltammetryPreferences prefs
	getTransPrefs(prefs)
	
	if (reset)
		SavePackagePreferences /KILL ksPackagePrefsName, ksPrefsFileName, recordID, prefs	
	else
		SavePackagePreferences ksPackagePrefsName, ksPrefsFileName, recordID, prefs
	endif
End

static Function defaultPrefsStruct(prefs)
	STRUCT VoltammetryPreferences &prefs

	String type = VMRamps#getCurrentRampName()
		
	prefs.version = kVM_PrefsVersion
	
//	Variable startVoltage = 0, endVoltage = 0
//	Wave voltage = VM_getRampWave()
	
	STRUCT RampValues rv
	VMRamps#getRampStruct(rv)
	
//	if (loadResult && cmpstr(rv.name, type) == 0)
//		startVoltage = rv.oxDetails.oxiVoltage - rv.oxDetails.oxiRange
//		endVoltage = rv.oxDetails.oxiVoltage + rv.oxDetails.oxiRange
//	endif
	
	prefs.type = type
	prefs.baselinePoints = kBaselinePoints
	prefs.transientPoints = kTransientPoints
	
	prefs.oxDetails = rv.oxDetails
	
//	FindLevel /Q voltage, startVoltage
//	prefs.oxDetails.oxiStartTime = V_flag == 0 ? V_LevelX : NaN
//	prefs.oxDetails.oxiStartPoint = x2pnt(voltage, prefs.oxDetails.oxiStartTime)
//	prefs.oxDetails.oxiStartVoltage = startVoltage
//	
//	FindLevel /Q voltage, endVoltage
//	prefs.oxDetails.oxiEndTime = V_flag == 0 ? V_LevelX : NaN
//	prefs.oxDetails.oxiEndPoint = x2pnt(voltage, prefs.oxDetails.oxiEndTime)
//	prefs.oxDetails.oxiEndVoltage = endVoltage
//	
//	prefs.oxDetails.oxiRange = prefs.oxDetails.oxiEndVoltage - prefs.oxDetails.oxiStartVoltage

	prefs.CVAvgPoints = kCVPlotPoints	
	prefs.calibrationFactor = kVM_defCalibrationFactor
End
	
static Function loadColorCatPrefs(prefs)
	STRUCT VM_CatColorPrefs &prefs
	
	Variable recordID = 1
	
	LoadPackagePreferences /MIS=1 ksPackagePrefsName, ksPrefsFileName, recordID, prefs
	if (V_flag != 0 || V_bytesRead == 0)
		defaultColorCatPrefs(prefs)
	endif
	if (prefs.version != kVM_colorCatPrefsVersion)
		saveColorCatPrefs(prefs, reset=1)
		defaultColorCatPrefs(prefs)
	endif
End

static Function saveColorCatPrefs(prefs, [reset])
	STRUCT VM_CatColorPrefs &prefs
	Variable reset
	
	reset = ParamIsDefault(reset) || reset < 1 ? 0 : 1
	Variable recordID = 1
	
	if (!reset)
		SavePackagePreferences /FLSH=1 ksPackagePrefsName, ksPrefsFileName, recordID, prefs
	else
		SavePackagePreferences /FLSH=1 /KILL ksPackagePrefsName, ksPrefsFileName, recordID, prefs
	endif
End

static Function defaultColorCatPrefs(prefs)
	STRUCT VM_CatColorPrefs &prefs
	
	STRUCT VM_CategoryColor colorPref1
	STRUCT VM_CategoryColor colorPref2
	STRUCT VM_CategoryColor colorPref3
	
	colorPref1.version = kVM_colorCatPrefsVersion
	colorPref1.name = "electrical"
	colorPref1.color = "red"

	colorPref2.version = kVM_colorCatPrefsVersion
	colorPref2.name = "laser"
	colorPref2.color = "lightBlue"
	
	colorPref3.version = kVM_colorCatPrefsVersion
	colorPref3.name = "LED"
	colorPref3.color = "lightBlue"
	
	prefs.version = kVM_colorCatPrefsVersion
	prefs.colorPref[0] = colorPref1
	prefs.colorPref[1] = colorPref2
	prefs.colorPref[2] = colorPref3
	
	return 1
End	

// *****************             C L E A N U P             *****************

static Function getCleanUpInfo(info)
	STRUCT CleanUpInfo &info
	
	info.wavePath = getPackage()
	
	info.waveNames[0] = ksEpochDataTraces
	info.waveNames[1] = ksEpochTransient
	info.waveNames[2] = ksVGCVName
	info.waveNames[3] = ksVGImageName
	info.waveNames[4] = ksVGImageName + "_filtered"
	info.waveNames[5] = ksVGBaselineName
	info.waveNames[6] = ksVGDataTrace
	info.numWaves = 7
	
	info.wavePrefixes[0] = ksEpochControlTrace
	info.wavePrefixes[1] = ksEpochControlMatrix
	info.numPrefixes = 2
	
	info.windows[0] = ksVGWinBaseName + "_" + ksVGCVExtension
	info.windows[1] = ksVGWinBaseName + "_" + ksVGImageExtension
	info.windows[2] = ksControlGraphName
	info.windows[3] = ksChannelAssignTName
	info.numWindows = 4
	
	info.variableNames[0] = ksPeakPoint
	info.variableNames[1] = ksStimStart
	info.variableNames[2] = ksTRStartPoint
	info.variableNames[3] = ksTREndPoint
	info.variableNames[4] = ksTRBaselinePoints
	info.variableNames[5] = ksTRTransientPoints
	info.variableNames[6] = ksNoUpdateName
	info.variableNames[7] = ksKeepCVFront
	info.variableNames[8] = ksCVCsrToMax
	info.numVars = 9
	
	info.stringNames[0] = ksCurrentEpochClass
	info.numStrs = 1
End

static Function cleanUp()

	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()
	DFREF loadFolder = getDataLoadFolder()
	
	Wave epochs = VM_getEpochWave()
	
	STRUCT CleanUpInfo info
	getCleanUpInfo(info)

	print "    ---> trying to remove the raw data folder ..."
	KillDatafolder /Z loadFolder
	if (V_flag > 0)
		// some error occurred, most likely a wave is in a graph or table
		print "    ---> ... and failed."
		return 0
	endif
	print "    ---> ... and succeeded."
	
	epochs[][%rawDataExists] = 0
	
	Variable index
	for (index = 0; index < info.numWindows; index += 1)
		DoWindow $(info.windows[index])
		if (V_flag > 0)
			KillWindow $(info.windows[index])
		endif
	endfor
	print "    ---> closed windows."
	for (index = 0; index < info.numWaves; index += 1)
		KillWaves /Z info.wavePath:$(info.waveNames[index])
	endfor
	SetDatafolder package
	for (index = 0; index < info.numPrefixes; index += 1)
		Variable waves = 0
		String theList = WaveList(info.wavePrefixes[index] + "*", ";", "")
		for (waves = 0; waves < ItemsInList(theList); waves += 1)
			KillWaves /Z $(StringFromList(waves, theList))
		endfor
	endfor
	SetDatafolder cDF	
	print "    ---> killed helper waves."
	for (index = 0; index < info.numVars; index += 1)
		NVAR VartoKill = info.wavePath:$(info.variableNames[index])
		KillVariables /Z VartoKill
	endfor
	print "    ---> removed helper variables."
	for (index = 0; index < info.numStrs; index += 1)
		SVAR StrtoKill = info.wavePath:$(info.stringNames[index])
		KillStrings /Z StrtoKill
	endfor
	print "    ---> removed helper texts."
	return 1
End

// *****************  F O L D E R    G E T T E R S   *****************

static Function /DF getPackage()
	
	DFREF cDF = GetDatafolderDFR()
	DFREF path = root:Packages:$ksPackageName
	
	if (DatafolderRefStatus(path) == 0)
		SetDatafolder root:
		if (!DatafolderExists("Packages"))
			NewDatafolder /S Packages
			NewDatafolder /S $ksPackageName
		else
			SetDatafolder "Packages"
			NewDatafolder /S $ksPackageName
		endif
		DFREF path = GetDatafolderDFR()
	endif
	
	SetDatafolder cDF
	return path
End

ThreadSafe static Function /DF getDataLoadFolder()

	return makeFolder(ksInitalDataFolderName)
End

ThreadSafe static Function /DF getEpochFolder()
	
	return makeFolder(ksEpochsFolderName)
End

ThreadSafe static Function /DF getErrDisplayFolder()
	return makeFolder(ksErrorDisplay)
End

ThreadSafe static Function /DF makeFolder(theName)
	String theName
	
	DFREF cDF = GetDatafolderDFR()
	DFREF path = root:$theName
	
	if (DatafolderRefStatus(path) == 0)
		SetDatafolder root:
		NewDatafolder /S $theName
		path = GetDatafolderDFR()
		SetDatafolder cDF
	endif
	
	return path
End

// **************   V A R I A B L E    G E T T E R S   *****************

static Function /S getSVarByName(theName, [text])
	String theName, text
	
	DFREF path = getPackage()
	DFREF cDF = GetDatafolderDFR()
	
	SVAR theString = path:$theName
	if (!SVAR_Exists(theString))
		String /G path:$theName = ""
		SVAR theString = path:$theName
	endif
	if (!ParamIsDefault(text) && strlen(text) > 0)
		theString = text
	endif
	return (GetDatafolder(1, path) + theName)
end

static Function /S getNVarByName(theName, [value])
	String theName
	Variable value
	
	DFREF path = getPackage()
	DFREF cDF = GetDatafolderDFR()

	NVAR theVariable = path:$theName
	if (!NVAR_Exists(theVariable))
		Variable /G path:$theName
	endif
	NVAR theVariable = path:$theName
	if (!ParamIsDefault(value))
		theVariable = value
	endif
	return (GetDatafolder(1, path) + theName)
End

static Function /S getPeakPointVar()
		
	return getNVarByName(ksPeakPoint)
End

static Function /S getStimPoint()
	
	return getNVarByName(ksStimStart)
End
	
static Function /S getTransientStartPoint([init])
	Variable init
	
	init = ParamIsDefault(init) || init < 1 ? 0 : 1
	
	Variable value = 0
	
	if (!init)
		if (VM_rampStructExists())
			STRUCT RampValues rv
			VMRamps#getRampStruct(rv)
			value = rv.oxDetails.startPoint
		else
			STRUCT VoltammetryPreferences prefs
			getTransPrefs(prefs)
			value = prefs.oxDetails.startPoint
		endif
	endif
	return getNVarByName(ksTRStartPoint, value=value)
End

static Function /S getTransientEndPoint([init])
	Variable init
	
	init = ParamIsDefault(init) || init < 1 ? 0 : 1
	Variable value = 0
	
	if (!init)
		if (VM_rampStructExists())
			STRUCT RampValues rv
			VMRamps#getRampStruct(rv)
			value = rv.oxDetails.endPoint
		else
			STRUCT VoltammetryPreferences prefs
			getTransPrefs(prefs)
			value = prefs.oxDetails.endPoint
		endif
	endif
	return getNVarByName(ksTREndPoint, value=value)
End

static Function /S getBaselinePoints([init])
	Variable init
	
	init = ParamIsDefault(init) || init < 1 ? 0 : 1
		
	if (init)
		STRUCT VoltammetryPreferences prefs
		getTransPrefs(prefs)
		return getNVarByName(ksTRBaselinePoints, value=prefs.baselinePoints)
	else
		return getNVarByName(ksTRBaselinePoints)
	endif
End

static Function /S getTransientPoints([init])
	Variable init

	init = ParamIsDefault(init) || init < 1 ? 0 : 1

	Variable value = kTransientPoints
	
	if (!init)
		STRUCT VoltammetryPreferences prefs
		getTransPrefs(prefs)
		value = prefs.transientPoints
	endif
	
	return getNVarByName(ksTRTransientPoints, value=value)
End

static Function /S getCalibrationFactor()		
	String pathToVar = getNVarByName(ksCalibFactorName)
	NVAR factor = $pathToVar
	if (factor <= 0)
		STRUCT VoltammetryPreferences prefs
		getTransPrefs(prefs)
		factor = prefs.calibrationFactor
	endif
	return pathToVar
End
	


// *****************   W A V E    G E T T E R S   *****************

static Function /WAVE getDataTraceWave([rows, columns])
	Variable rows, columns
	
	if (ParamIsDefault(rows))
		rows = -1
	endif
	if (ParamIsDefault(columns))
		columns = -1
	endif
	return getGlobalPackageMatrix(ksEpochDataTraces, rows=rows, columns=columns)
End

static Function /WAVE getControlTraceWave([rows, columns, number, channel])
	Variable rows, columns, number
	String channel
	
	if (ParamIsDefault(rows))
		rows = -1
	endif
	if (ParamIsDefault(columns))
		columns = -1
	endif
	
	String ending = ""
	if (!ParamIsDefault(channel) && strlen(channel) > 0)
		ending = "_" + channel
	else
		if (!ParamIsDefault(number))
			if (number <= 0)
				number = 1
			else
				number = round(number)
			endif
			ending = "_" + num2str(number)
		endif
	endif
	
	return getGlobalPackageMatrix(ksEpochControlMatrix + ending, rows=rows, columns=columns)
End


static Function /WAVE getImageVoltammogram([rows, columns])
	Variable rows, columns

	if (ParamIsDefault(rows))
		rows = -1
	endif
	if (ParamIsDefault(columns))
		columns = -1
	endif
	return getGlobalPackageMatrix(ksVGImageName, rows=rows, columns=columns)
End

static Function /WAVE getCVTrace()
	
	return getGlobalPackageMatrix(ksVGCVName)
End


//! Get the epoch transient wave reference. It scales the wave according to nums. If 
// the nums is not given, the returned number of points depend on the calibration flag. 
// In calibration mode, the nums will be 2 times the baseline number of points, otherwise it 
// will be the number of baseline points plus the number of transient points. This function only
// creates a wave with the standard name and given number of points. It will NOT give the actual
// transient data. 
// @param nums:			number of points for the wave
// @param calibration:	flag to indicate calibration mode (default is 0, not calibration mode)
// @return a wave reference to the properly scaled epoch transient wave
//-
static Function /WAVE getEpochTransient([nums, calibration])
	Variable nums, calibration
	
	calibration = ParamIsDefault(calibration) || calibration < 1 ? 0 : 1
	
	DFREF path = getPackage()
	
	if (ParamIsDefault(nums) || nums < 1)
		NVAR baseline = $(getBaselinePoints())
		NVAR transient = $(getTransientPoints())
		if (NVAR_Exists(baseline) && NVAR_Exists(transient))
			if (calibration)
				nums = 2 * baseline
			else
				nums = baseline + transient
			endif
		else
			nums = 60
		endif
	endif
	Wave theEpoch = path:$ksEpochTransient
	if (!WaveExists(theEpoch))
		Make /N=(nums) path:$ksEpochTransient /WAVE=theEpoch
	elseif(nums != numpnts(theEpoch))
		Redimension /N=(nums) theEpoch
		theEpoch = NaN
	endif
	
	return theEpoch
End

//! General helper function to create a matrix with a given name and dimensions
// in the package folder. If the matrix already exists and the row and column 
// parameters are given, the matrix will be resized to the dimensions.
// @param theName:			the name of the matrix
// @param rows:				the number of rows of the matrix
// @param columns:			the number of columns of the matrix
// @return matrix in package folder with specific name and dimension
//-
static Function /WAVE getGlobalPackageMatrix(theName, [rows, columns])
	String theName
	Variable rows, columns
	
	DFREF path = getPackage()
	rows = ParamIsDefault(rows) || rows < 0 ? -1 : rows
	columns = ParamIsDefault(columns) || columns < 0 ? -1 : columns
	
	Wave matrix = path:$theName
	if (WaveExists(matrix))
		if (rows < 0 && columns < 0)
			return matrix
		endif
	endif
	rows = rows < 0 ? 60 : rows
	columns = columns < 0 ? 1000 : columns
	
	if (!WaveExists(matrix))
		Make /N=(rows, columns) path:$theName /WAVE=matrix
	else
		Redimension /N=(rows, columns) matrix
	endif
	return matrix
End

static Function /WAVE getGlobalPackageWave(theName, [length, asText])
	String theName
	Variable length, asText
	
	DFREF path = getPackage()
	
	length = ParamIsDefault(length) || length < 0 ? 0 : length
	asText = ParamIsDefault(asText) || asText < 1 ? 0 : 1
	
	if (asText)
		Wave /T tWave = path:$theName
		if (!WaveExists(tWave))
			Make /T/N=(length) path:$theName /WAVE=tWave
		endif
		return tWave
	else
		Wave theWave = path:$theName
		if (!WaveExists(theWave))
			Make /N=(length) path:$theName /WAVE=theWave
		endif
		return theWave
	endif
End

static Function /WAVE getDataMatrixByChannel(theChannel)
	String theChannel
	
	Wave channel
	Wave nullRef
	if (strlen(theChannel) == 0)
		return channel
	endif
	DFREF loadFolder = getDataLoadFolder()
	Wave channel = loadFolder:$theChannel
	if (!WaveExists(channel) || WaveDims(channel) < 2)
		return nullRef
	endif
	return channel
End
	

static Function /WAVE getFilteredImageVG([theImage])
	Wave theImage
	
	if (ParamIsDefault(theImage))
		Wave theImage = getImageVoltammogram()
	endif
	DFREF path = GetWavesDatafolderDFR(theImage)
	Variable rows = DimSize(theImage, 0) - 1
	Variable columns = DimSize(theImage, 1) - 1
	
	Duplicate /O theImage, path:$(NameOfWave(theImage) + "_filtered") /WAVE=filtered
//	ImageInterpolate /S={0, (rows/columns), rows, 0, 1, columns} /DEST=filtered bilinear theImage
	MatrixFilter median filtered
	
	return filtered
End

static Function /WAVE getImageVGFromGraph([theGraphName])
	String theGraphName
	
	if (ParamIsDefault(theGraphName) || strlen(theGraphName) == 0)
		theGraphName = ksVGWinBaseName + ksVGImageExtension
	endif
	
	Wave image
	DoWindow $theGraphName
	if (V_flag)
		Wave image = ImageNameToWaveRef(theGraphName, StringFromList(0, ImageNameList(theGraphName, ";")))
	endif
	
	return image
End

static Function /WAVE getChannelAssignment([show])
	Variable show
	
	DFREF cDF = GetDataFolderDFR()
	
	if (ParamIsDefault(show) || show < 0)
		show = 0
	else
		show = show > 1 ? 1 : round(show)
	endif

	DFREF package = getPackage()
	DFREF dataFolder
	Wave /T assignment = package:$ksChannelAssign
	
	if (!WaveExists(assignment))
		Make /N=0/T/O package:$ksChannelAssign /WAVE=assignment
	endif

	if (show)
		DoWindow /F $ksChannelAssignTName
		if (!V_flag)
			Edit /W=(5,44,337,175) /K=1 /N=$ksChannelAssignTName assignment.ld
		else
			// table exists, is this wave on it?
			Variable notDisplayed = ItemsInList(WaveList(ksChannelAssign, ";", "WIN:")) == 0
			if (notDisplayed)
				AppendToTable /W=$ksChannelAssignTName assignment.ld
			endif
		endif
	endif
	
	return assignment
End

static Function /WAVE getChannelClasses([show])
	Variable show
	
	if (ParamIsDefault(show) || show < 0)
		show = 0				// per default show nothing
	else
		show = show > 1 ? 1 : round(show)
	endif
	
	DFREF cDF = GetDataFolderDFR()
	DFREF package = getPackage()
	DFREF datafolder
	
	Wave /T classes = package:$ksChannelClasses
	
	if (!WaveExists(classes))
		Make /N=0/T/O package:$ksChannelClasses /WAVE=classes
	endif
	
	if (show)
		DoWindow /F $ksChannelAssignTName
		if (!V_flag)
			Edit /N=$ksChannelAssignTName classes.ld
		else
			AppendToTable /W=$ksChannelAssignTName classes
		endif
	endif
	
	return classes
End

// if using the Bruxtion DataAccess ABF file loader, the data wave names all will end with _AD0,
// which I would like to clear out.
static Function cleanNames([partToClean])
	String partToClean
	
	if (ParamIsDefault(partToClean) || strlen(partToClean)== 0)
		partToClean = "_AD0"
	endif
	
	String theWaveName, cleanName
	Variable location = 0
	DFREF folder = GetDatafolderDFR()
	Variable index
	do
		theWaveName = GetIndexedObjNameDFR(folder, 1, index)
		if (strlen(theWaveName) == 0)
			break
		endif
		location = strsearch(theWaveName, partToClean, 0)
		if (location >= 0)
			cleanName = theWaveName
			cleanName[location, location + strlen(partToClean)] = ""
			if (WaveExists($cleanName))
				print "--> can't clean wave '" + theWaveName + "': '" + cleanName + " already exists!"
			else
				Rename $theWaveName, $cleanName
			endif
		endif
		index += 1
	while(1)
End


//static Function /WAVE getCatColorPrefWave()
//	return getGlobalPackageWave(ksVMCatColorPref, length=kVM_numCatPrefs, asText=1)
//End


// *****************  N A M E    G E T T E R S   *****************

static Function /S getVGWinBaseName()
	
	return ksVGWinBaseName
end

static Function /S getVGImageExtension()

	return ksVGImageExtension
end

static Function /S getVGCVExtension()

	return ksVGCVExtension
end

// *****************   H E L P E R    F U N C T I O N S   *****************

//! Helper function to copy the raw data traces into a matrix for easier access. The start
// and end number are taken from the transient and determine the start and end of the 
// interesting epoch (provided through cursors in the user interface). The baseline will be noted
// in the wave notes so following functions can easily extract the baseline part
// @param channelWave:		matrix containing the data for the channel
// @param startNum:			index row for the first trace to extract
// @param endNum:				index for the last trace to extract
// @param baseline:			number of traces to be used for the baseline
// @return matrix containing the relevant rows given by startNum and endNum
//-
static Function /WAVE getChannelTraces(channelWave, startNum, endNum, baseline)
	Wave channelWave
	Variable startNum, endNum, baseline

	String errormsg = VM_getErrorStr("getChannelTraces")

	Wave nullRef
	if (!WaveExists(channelWave) || WaveDims(channelWave) < 2)
		printf errormsg, "channel wave does not exists or has wrong dimensions"
		return nullRef
	endif

	Variable rows = endNum - startNum
	Variable columns = DimSize(channelWave, 1)

	// basic check of input parameters, if they make sense
	if (endNum < startNum || endNum < 0 || startNum < 0)
		printf errormsg, "both startNum and endNum have to be positive integers AND endNum must be larger than startNum" 
		return nullRef
	else
		// just in case we have decimals, round them to the next integer.
		startNum = round(startNum)
		endNum = round(endNum)
	endif
	
	baseline = baseline < 0 ? 0 : round(baseline) 		// needs to be integer
	
	// get the basics and check if everything is alright
	DFREF cDF = GetDatafolderDFR()
	DFREF path = getPackage()
		
	String waveNote = ""
	Variable index = 0
	
	Wave /T channels = getChannelAssignment()
	String channelDesignation = channels[%$(NameOfWave(channelWave))]

	if (cmpstr(channelDesignation, "data") == 0)
		Wave traceMatrix = getDataTraceWave(rows=rows, columns=columns)
	else		
		Wave traceMatrix = getControlTraceWave(rows=rows, columns=columns, channel=NameOfWave(channelWave))
	endif
	waveNote = note(traceMatrix)
	waveNote = ReplaceStringByKey(ksVM_ChannelLbl, waveNote, NameOfWave(channelWave))
	waveNote = ReplaceStringByKey(ksVM_ChannelDesig, waveNote, channelDesignation)
	waveNote = ReplaceNumberByKey(ksVM_BaselineNum, waveNote, baseline)
	Note /K traceMatrix, waveNote

	Variable processors = ThreadProcessorCount
	if (processors > 1)
		MultiThread traceMatrix = channelWave[p + startNum][q]
	else
		traceMatrix = channelWave[p + startNum][q]
	endif
	
	SetScale /P x channelWave[startNum], DimDelta(channelWave, 0), WaveUnits(channelWave, 0), traceMatrix
	SetScale /P y 0, DimDelta(channelWave, 1), WaveUnits(channelWave, 1), traceMatrix
	SetScale d 0, 0, WaveUnits(channelWave, -1), traceMatrix
	return traceMatrix	
End	

// ******************************************************
// **************           G E N E R A L    F U N C T I O N S  
// ******************************************************
static Function /S getFeedbackStr(name, type)
	String name
	Variable type
	
	if (strlen(name) == 0)
		name = ">function name not given<"
	endif
	
	String result = ""
	
	switch(type)
		default:
		case 0:
			sprintf result, ksVM_ErrorMsg, name, "%s"
			break
		case 1:
			sprintf result, ksVM_WarnMsg, name, "%s"
			break
	endswitch
	return result
End

static Function /S filterHEKABySeries(series, list)
	Variable series
	String list
	
	String resultList = ""
	Variable index
	Variable numItems = ItemsInList(list)
	
	String prefix, year, month, day, aNum, ser, sweep, channel
	
	for (index = 0; index < numItems; index += 1)
		String name = StringFromList(index, list)

		String expr = "([[:alpha:]]+)_([[:digit:]]+)_([[:digit:]]+)_([[:digit:]]+)_([[:digit:]]+)_([[:digit:]]+)_([[:digit:]]+)_([[:digit:]]+).ibw"
		SplitString /E=expr name, prefix, year, month, day, aNum, ser, sweep, channel

		if (str2num(ser) == series)
			resultList = AddListItem(name, resultList, ";", Inf)
		endif
	endfor
	
	return SortList(resultList, ";", 16)
End
	
	

Function /S VM_getErrorStr(name)
	String name
	return getFeedbackStr(name, 0)
End

Function /S VM_getWarnStr(name)
	String name
	return getFeedbackStr(name, 1)
End

Function /S VM_getNoUpdateFlag()
	return getNVarByName(ksNoUpdateName)
End

Function /S VM_getKeepCVFrontFlag()
	return getNVarByName(ksKeepCVFront)
End
	
Function /S VM_getCVCsrAlignedFlag()
	return getNVarByName(ksCVCsrToMax)
End
	

Function /WAVE VM_getRawDataWave()
	
	Wave nullRef
	Wave /T channels = getChannelAssignment(show=0)
	Extract /FREE /INDX /O channels, labelIndx, cmpstr(channels, "data") == 0
	String theLabel
	if (numpnts(labelIndx) >= 1)
		Wave dataWave = getDataMatrixByChannel(GetDimLabel(channels, 0, labelIndx[0]))
	else
		return nullRef
	endif
	return dataWave
End

Function /WAVE VM_getRawControlWave([number, class])
	Variable number
	String class
	
	Wave nullRef
	Wave /T channels = getChannelAssignment(show=0)
	Extract /FREE /INDX /O channels, ctrlLabelIndex, cmpstr(channels, "control") == 0
	switch (numpnts(ctrlLabelIndex))
		case 0:
			return nullRef
			break
		case 1:
			return getDataMatrixByChannel(GetDimLabel(channels, 0, ctrlLabelIndex[0]))
			break
	endswitch

	// we only arrive here, if we have more than 1 control wave, so now we need to look at the 
	// input parameters to figure out which one we need to return
	
	Variable inputFlag = 0
	if (ParamIsDefault(number) || number < 0)
		number = 0
	else
		number = round(number)
		if (number > numpnts(ctrlLabelIndex))
			number = numpnts(ctrlLabelIndex) - 1
		endif
		return getDataMatrixByChannel(GetDimLabel(channels, 0, ctrlLabelIndex[number]))
	endif
	if (ParamIsDefault(class) || strlen(class) == 0)
		class = "electrical"
	else
		Wave /T classes = getChannelClasses()
		Extract /FREE /O /INDX classes, lblIndex, cmpstr(channels, class) == 0
		switch (numpnts(lblIndex))
			case 0:
				print "** ERROR (VM_getRawControlWave): no control channel of class '", class, "' exists. Returned first assigned control channel to be able to continue."
				return getDataMatrixByChannel(GetDimLabel(channels, 0, ctrlLabelIndex[0]))
				break
			case 1:
			default:
				return getDataMatrixByChannel(GetDimLabel(classes, 0, lblIndex[0]))
				break
		endswitch
	endif
	
	// we should only arrive here, if we have more than one control trace and neither a valid number
	// or a valid class, so just return the first control trace
	return getDataMatrixByChannel(GetDimLabel(channels, 0, ctrlLabelIndex[0]))
End	

Function /S VM_getAnalysisFunctions()
	return VM_getFunctionList(nice = 1, prefix=ksVM_AnalysisFuncPre)
End

Function /S VM_getControlFunctions()
	return VM_getFunctionList(nice = 1, prefix = ksVM_ControlFuncPre)
End

Function /S VM_getFunctionList([nice, prefix])
	String prefix
	Variable nice 
	
	if (ParamIsDefault(prefix) || strlen(prefix) == 0)
		prefix = ksVM_AnalysisFuncPre
	endif
	if (ParamIsDefault(nice) || nice < 0)
		nice = 0
	else
		nice = nice > 1? 1 : round(nice)
	endif
	
	String list = FunctionList(prefix + "_*", ";", "")
	if ( nice )
	 	list = ReplaceString(prefix + "_", list, "")
	 	list = ReplaceString("_", list, " ")
	 endif
	return list
End
	


Function /S VM_getCurrentEpochClass()
	
	DFREF cDF = GetDatafolderDFR()
	
	DFREF package = VoltHelpers#getPackage()
	SVAR currentClass = package:$ksCurrentEpochClass
	if (!SVAR_Exists(currentClass))
		String /G package:$ksCurrentEpochClass
		SVAR currentClass = package:$ksCurrentEpochClass
		currentClass = ""
	endif
	
	return GetDatafolder(1, package) + ksCurrentEpochClass
End

//! Determine the voltage delta increment of the ramp. It uses the difference
// of the 5th and 6th point after the ramp start to calculate the ramp delta.
// @param ramp:		wave reference to the ramp
// @return a single number showing the voltage delta of the ram
Function VM_rampDeltaY([ramp])
	Wave ramp
	
	if (ParamIsDefault(ramp) || !WaveExists(ramp))
		Wave ramp = VM_getRampWave()
	endif
	Variable rampStart = NumberByKey(ksVM_RampStartPKey, note(ramp))
	
	Variable deltaY = abs(ramp[rampStart + 6] - ramp[rampStart + 5])
	return deltaY
End

Function /WAVE VM_getTraceFromTransientLoc(location, channel, [transient])
	Variable location
	String channel
	Wave transient
	
	Wave nullRef
	DFREF cDF = GetDatafolderDFR()
	DFREF loadFolder = getDataLoadFolder()
	Wave cw = loadFolder:$(channel)
	
	if (!WaveExists(cw) || WaveDims(cw) < 2)
		return nullRef
	endif

	Make /N=(DimSize(cw, 1)) /O /FREE trace = cw[location][p]
	SetScale /P x 0, DimDelta(cw, 1), WaveUnits(cw, 1), trace
	return trace
End

Function /S VM_getPackagePrefsName()
	return ksPackagePrefsName
End

Function /S VM_getCVPlotName()
	return ksVGWinBaseName + "_" + ksVGCVExtension
End

Function /S VM_getImgVoltName()
	return ksVGWinBaseName + "_" + ksVGImageExtension
End

Function VM_resetPrefs2Default()
	STRUCT VoltammetryPreferences prefs
	STRUCT RampValues rv
	
	getTransPrefs(prefs)
	VMRamps#getRampStruct(rv)

	prefs.oxDetails = rv.oxDetails
	
	setTransPrefs(prefs)
		
	getTransientStartPoint()
	getTransientEndPoint()
	getTransientPoints()
	getBaselinePoints()
	
	print "--> reset variables to " + rv.name + " defaults on " + date()
End

Function VM_setDefCalibrationFactor(calibFactor)
	Variable calibFactor
	String type
	
	Variable result = 0
	String errormsg = ""
	sprintf errormsg, ksVM_ErrorMsg, "VM_setDefCalibrationFactor", "%s"
	
	STRUCT VoltammetryPreferences prefs
	getTransPrefs(prefs)
	
	if (calibFactor <= 0)
		printf errormsg, "the calibration factor has to be a postive number larger than 0"
		prefs.calibrationFactor = kVM_defCalibrationFactor
	else
		prefs.calibrationFactor = calibFactor
		print "-> set new default calibration factor to %g on", date(), time()
		result = 1
	endif
	
	setTransPrefs(prefs)
	return result
End

Function VM_checkChannelAssign([rawDataFolder, assign])
	String rawDataFolder, assign
	
	if (ParamIsDefault(rawDataFolder))
		rawDataFolder = ""
	endif
	
	Variable success = 1
	String channels = VM_getChannels(rawDataFolder = rawDataFolder)
	Wave /T assignment = getChannelAssignment()
	Wave /T classes = getChannelClasses()
	
	if (ItemsInList(channels) != numpnts(assignment))
		Redimension /N=(ItemsInList(channels)) assignment
		Redimension /N=(ItemsInList(channels)) classes
		if (ItemsInList(channels) == 1)
			assignment = "data"
			classes = "data"
			SetDimLabel 0, 0, $(StringFromList(0, channels)), assignment
			SetDimLabel 0, 0, $(StringFromList(0, channels)), classes
			success = 1
		else
			VM_createChannelAssign(channels)
			success = 1
		endif
	endif
	return success
End

Function /S VM_getDataChannel([rawDataFolder])
	String rawDataFolder
	
	if (ParamIsDefault(rawDataFolder))
		rawDataFolder = ""
	endif
	String channels = VM_getChannels(rawDataFolder = rawDataFolder)
	Wave /T assignment = getChannelAssignment()
	
	Extract /INDX /FREE /O assignment, dataIndex, cmpstr(assignment, "data") == 0
	
	if (numpnts(dataIndex) != 1)
		// something went wrong, we can only have one data channel 
		print "** ERROR (VM_getDataChannel): there needs to be one, and exactly one data channel assigned. Couldn't find it, so STOPPED!"
		return ""
	endif
	return StringFromList(dataIndex[0], channels)
End
	

Function /S VM_getChannels([rawDataFolder])
	String rawDataFolder
	// 

	DFREF cDF = GetDatafolderDFR()
	
	if (ParamIsDefault(rawDataFolder) || strlen(rawDataFolder) == 0)
		DFREF dataFolder = getDataLoadFolder()
	else
		DFREF dataFolder = $(rawDataFolder)
		if (DatafolderRefStatus(dataFolder) == 0)
			print "-- ERROR (getChannelAssignment): can't find folder '" + rawDataFolder + "' with the loaded data waves."
		endif
	endif
	
	SetDatafolder dataFolder
	String channelList = WaveList("AD*", ";", "DIMS:2")	
	SetDatafolder cDF
	
	return channelList
End

Function /WAVE VM_getEpochWave()
	
	DFREF cDF = GetDatafolderDFR()
	DFREF path = getPackage()
	
	Variable numDataColumns = 19
	
	Wave epochs = path:$ksEpochsName
	Variable relabel = 0	
	if (!WaveExists(epochs))
		Make /N=(0,numDataColumns) path:$ksEpochsName /WAVE=epochs
		relabel = 1
	else
		Variable columns = DimSize(epochs, 1)
		if (columns != numDataColumns)
			Redimension /N=(-1, numDataColumns) epochs
			relabel = 1
//			epochs = NaN
		endif
	endif
	
	if (relabel)
		SetDimLabel 1, 0, startpoint, epochs
		SetDimLabel 1, 1, endpoint, epochs
		SetDimLabel 1, 2, peakstartpoint, epochs
		SetDimLabel 1, 3, peakPoint, epochs
		SetDimLabel 1, 4, startx, epochs
		SetDimLabel 1, 5, endx, epochs
		SetDimLabel 1, 6, peakstartx, epochs
		SetDimLabel 1, 7, peakX, epochs
		SetDimLabel 1, 8, BLstartPnt, epochs
		SetDimLabel 1, 9, BLendPnt, epochs
		SetDimLabel 1, 10, BLValue, epochs
		SetDimLabel 1, 11, peakAvgStart, epochs
		SetDimLabel 1, 12, peakAvgEnd, epochs
		SetDimLabel 1, 13, peakAvgStartP, epochs
		SetDimLabel 1, 14, peakAvgEndP, epochs
		SetDimLabel 1, 15, stimulusPoint, epochs
		SetDimLabel 1, 16, stimulusX, epochs
		SetDimLabel 1, 17, rawDataExists, epochs
		SetDimLabel 1, 18, calibration, epochs
	endif
	
	return epochs
End // END VM_getTransientPeaks()

Function /WAVE VM_getEpochCatWave()

	DFREF cDF = GetDatafolderDFR()
	DFREF path = getPackage()
	
	Wave epochs = VM_getEpochWave()
	Wave /T epochCats = path:$ksEpochsCategories
	if (!WaveExists(epochCats))
		Make /N=0 /T path:$ksEpochsCategories /WAVE=epochCats
	endif
	
	Variable catLength = numpnts(epochCats)
	if (numpnts(epochCats) != DimSize(epochs, 0) && DimSize(epochs, 0) >= 0)
		Redimension /N=(DimSize(epochs, 0)) epochCats
		Variable index
		for (index = 0; index < DimSize(epochs, 0); index += 1)
			SetDimLabel 0, index, $(GetDimLabel(epochs, 0, index)), epochCats
		endfor
	endif
	return epochCats
End


Function /WAVE VM_getCalibDataTraces(bStart, bEnd, dStart, dEnd)
	Variable bStart, bEnd, dStart, dEnd
	
	DFREF path = getPackage()
	
	Variable bRows = bEnd - bStart
	Variable dRows = dEnd - dStart
	
	Wave rawData = VM_getRawDataWave()
	
	Wave tmp = getChannelTraces(rawData, bStart, bEnd, 0)
	Duplicate /O tmp, path:$ksCalibBaseline /WAVE=baseline
	
	Wave tmp = getChannelTraces(rawData, dStart, dEnd, 0)
	Duplicate /O tmp, path:$ksCalibStim /WAVE=stimulation
	
	Concatenate /O /NP=0 {baseline, stimulation}, path:$ksEpochDataTraces /WAVE=data
	
	String waveNote = note(data)
	waveNote = ReplaceStringByKey(ksVM_ChannelLbl, waveNote, NameOfWave(rawData))
	waveNote = ReplaceStringByKey(ksVM_ChannelDesig, waveNote, StringByKey(ksVM_ChannelDesig, note(stimulation)))
	waveNote = ReplaceNumberByKey(ksVM_BaselineNum, waveNote, bRows)
	Note /K data, waveNote
	
	return data
End


//! This function copies the single data traces into a 2 dimensional matrix
// with and returns it.
// @param theStart:		start index of the rows to be returned
// @param theEnd:			end index of the data rows to be returned
// @param baseline:		number of points at the beginning to be seen as baseline
// @return the data matrix containing the raw current traces for this epoch
//-
Function /WAVE VM_getDataTraces(theStart, theEnd, baseline)
	Variable theStart, theEnd, baseline
	
	Wave rawData = VM_getRawDataWave()
		
	return getChannelTraces(rawData, theStart, theEnd, baseline)
End // END VM_getDataTraces


// returns 
//		0		error or failure or no control channels 
//		1		success creating the control waves
Function VM_makeControlWaves(theStart, theEnd, baseline, controls)
	Variable theStart, theEnd, baseline
	STRUCT VM_ControlChannels &controls
	
	if (baseline < 0)
		print "** ERROR (VM_makeControlWaves): baseline is in points and can not be negative or fractional! STOPPED!"
		Wave /T controls.assign = $""
		Wave controls.controls = $""
		controls.labels[0] = ""
		Wave controls.traces[0] = $""
		controls.numControls = -1
		return 0
	else
		baseline = round(baseline)
	endif
		
	Variable numChannels = VM_checkControlWaves(chanStruct=controls)
	
	if (numChannels <= 0)
		// we have no control channels, so nothing more to do
		return 0
	endif
	
	Wave /T channels = controls.assign
	Wave controlChannels = controls.controls
	
	DFREF loadFolder = getDataLoadFolder()
	String channelName = ""	
	Variable index = 0
	do
		channelName = GetDimLabel(channels, 0, controlChannels[index])
		controls.labels[index] = channelName
		Wave controlWave = loadFolder:$channelName
		Wave controls.traces[index] = getChannelTraces(controlWave, theStart, theEnd, baseline)
		index += 1
	while(index < numpnts(controlChannels))
	return 1
End

// returns			number of assigned control channels
//				  0		if no control channel is assigned
//				-1		if assignment wave is missing
Function VM_checkControlWaves([chanStruct])
	STRUCT VM_ControlChannels &chanStruct
	
	if (VM_hasControls() == 0)
		return 0
	endif

	Wave /T channels = getChannelAssignment()
	Extract /FREE /INDX /O channels, controlChannels, cmpstr(channels, "control") == 0

	Variable index = 0
	if (!ParamIsDefault(chanStruct))
		Wave /T chanStruct.assign = channels
		Wave chanStruct.controls = controlChannels
		chanStruct.numControls = numpnts(controlChannels)
		if (chanStruct.numControls > 0)
			DFREF cDF = GetDatafolderDFR()
			DFREF path = getPackage()
			SetDatafolder path
			String controlList = WaveList(ksEpochControlMatrix + "_*", ";", "")
			SetDatafolder cDF
			for (index = 0; index < chanStruct.numControls; index += 1)
				String controlName = StringFromList(index, controlList)
				Wave chanStruct.traces[index] = path:$controlName
				chanStruct.labels[index] = ReplaceString(ksEpochControlMatrix + "_", controlName, "")
			endfor
		endif
	endif
	
	return numpnts(controlChannels)
End

Function VM_updateControlTraces(point)
	Variable point
	
	STRUCT VM_ControlChannels controls
	Variable numChannels = VM_checkControlWaves(chanStruct=controls)
	
	if (numChannels <= 0)
		return 0
	endif
	
	Wave /T channels = controls.assign
	Wave controlChan = controls.controls
		
	DFREF package = getPackage()
	
	String channel = ""
	Variable index = 0
	do
		channel = GetDimLabel(channels, 0, controlChan[index])
		Wave controlTrace = VM_getTraceFromTransientLoc(point, channel)
		Wave control = package:$(ksEpochControlTrace + "_" + channel)
		if (!WaveExists(control))
			Duplicate /O controlTrace, package:$(ksEpochControlTrace + "_" + channel) /WAVE=control
		else
			if(numpnts(controlTrace) != numpnts(control))
				Redimension /N=(numpnts(controlTrace)) control
			endif
			control = controlTrace[p]
		endif
		index += 1
	while (index < numpnts(controlChan))
	return 1
End

//! Creates the voltamogram as an image (2D matrix), by subtracting the baseline from all
// data rows.
// @param traceMatrix:		the data matrix with the original current traces
// @param blStart:			index row where the baseline starts (defaults to 0)
// @param blEnd:				index row where the baseline ends
// @return the wave reference to the voltammogram wave
//-
Function /WAVE VM_makeImageVoltammogram(traceMatrix, [blStart, blEnd])
	Wave traceMatrix
	Variable blStart, blEnd
	
	String errormsg = VM_getErrorStr("VM_makeImageVoltammogram")
	
	Wave image
	
	if (!WaveExists(traceMatrix))
		printf errormsg,  "the raw data doesn't exist"
		return image
	endif
	if (DimSize(traceMatrix, 1) == 0 || DimSize(traceMatrix, 2) > 0)
		// a one or higher dimensional matrix -> wrong input wave
		printf errormsg, "data input wave is wrong size"
		return image
	endif

	blStart = ParamIsDefault(blStart) || blStart < 0 ? 0 : blStart
	blEnd = ParamIsDefault(blEnd) || blEnd < 0 ? 0 : blEnd
		
	DFREF path = GetWavesDatafolderDFR(traceMatrix)
	
	Duplicate /O traceMatrix, path:$ksVGImageName /WAVE=image 
	Make /N=(DimSize(traceMatrix, 1)) /O path:$ksVGBaselineName /WAVE=baseline
	
// first calculate the baseline	
	Duplicate /O /FREE image, blMask
	blMask = 0
	blMask[blStart, blEnd][] = 1
	
	MatrixOp /O baseline = sumCols(image * blMask) / ((blEnd - blStart) + 1)
	Redimension /N=(DimSize(image, 1)) baseline
	SetScale /P x 0, DimDelta(image, 1), WaveUnits(image, 1), baseline
	SetScale d, 0, 0, "A", baseline
	
// second, subtract baseline (background) from all traces
	Variable index = 0
	do
		image[index][] -= baseline[q]
		index += 1
	while(index < DimSize(image, 0))

	return image
End // END VM_makeImageVoltammogram

//! Creates a mean, background subtracted current trace for a CV plot. If a mean is created,
// it will always be an uneven number, numPoints to the right and left of the peak location and the 
// acutal peak (2 * numPoints + 1). When using this function with calibration traces, peakLoc 
// indicates the beginning of the chemical traces and numPoints the total number of traces to average.
// @param voltammogram:		2D matrix of the background subtracted current traces
// @param peakLoc:				location of the peak 
// @param numPoints: 			number of points to the left and right to average (defaults to 0)
// @return a (mean) current trace for the CV plot
//-
Function /WAVE VM_makeCVCurves(voltammogram, peakLoc, [numPoints, calibration])
	Wave voltammogram
	Variable peakLoc, numPoints, calibration

	String errorMsg = VM_getErrorStr("VM_makeCVCurves")
	
	Wave CVTrace
	numPoints = ParamIsDefault(numPoints) || numPoints < 1 ? 0 : round(numPoints)
	calibration = ParamIsDefault(calibration) || calibration < 1 ? 0 : 1
	
	if (!WaveExists(voltammogram))
		printf errorMsg, "the voltammetry data does not exist"
		return CVTrace
	endif
	if (DimSize(voltammogram, 1) == 0 || DimSize(voltammogram, 2) > 0)
		// a one or higher dimensional matrix -> wrong input wave
		printf errorMsg, "the data input wave is the wrong size"
		return CVTrace
	endif
	if (calibration && !numPoints)
		printf errormsg, "numPoints can not be zero in calibration mode"
		return CVTrace
	endif
	
	Variable avgNums = 0
	
	DFREF path = GetWavesDatafolderDFR(voltammogram)
	Make /N=(DimSize(voltammogram, 1)) /O path:$ksVGCVName /WAVE=CVTrace
	CVTrace = 0
	
	if (calibration)
		avgNums = numPoints
		Duplicate /O/FREE voltammogram, pMask
		pMask = 0
		pMask[peakLoc,][] = 1
		
		MatrixOp /O CVTrace = sumCols(voltammogram * pMask) / avgNums
		Redimension /N=(DimSize(voltammogram, 1)) CVTrace
	else
		Variable index = peakLoc - numPoints
		do
			CVTrace += voltammogram[index][p]
			index += 1
		while (index <= peakLoc + numPoints)
		avgNums = 2 * numPoints + 1
		CVTrace /= avgNums						// average by number of points
	endif
		
	String theNote = note(CVTrace)
	theNote = ReplaceNumberByKey(ksVM_CVPeakLocKey, theNote, peakLoc)
	theNote = ReplaceNumberByKey(ksVM_AvgCVKey, theNote, avgNums)
	Note /K CVTrace, theNote
	SetScale d 0,0,"A", CVTrace
	SetScale /P x 0, DimDelta(voltammogram, 1), WaveUnits(voltammogram, 1), CVTrace
	return CVTrace
End // END VM_makeCVCurves


Function VM_keepData(theName, [calibration])
	String theName
	Variable calibration
	
	calibration = ParamIsDefault(calibration) || calibration < 1 ? 0 : 1
	
	Wave epochs = VM_getEpochWave()
	
	DFREF dest = getEpochFolder()
	Wave theEpoch = getEpochTransient(calibration = calibration)
	
	WaveStats /Q/C=1/W/M=1/Z theEpoch
	Wave M_WaveStats
	if (M_WaveStats[%numNaNs] == numpnts(theEpoch))
		// transient wave only contains NaNs, most likely we tried walked past the transient
		//   --> we don't want to keep these
		return 0		// failure to keep
	endif
	
	Duplicate /O theEpoch, dest:$(theName) /WAVE=epTransient
	String theNote = note(theEpoch)
	theNote = ReplaceNumberByKey(ksVM_AvgStartVoltKey, theNote, epochs[%$theName][%peakAvgStart])
	theNote = ReplaceNumberByKey(ksVM_AvgEndVoltKey, theNote, epochs[%$theName][%peakAvgEnd])
	theNote = ReplaceNumberByKey(ksVM_AvgStartPointKey, theNote, epochs[%$theName][%peakAvgStartP])
	theNote = ReplaceNumberByKey(ksVM_AvgEndPointKey, theNote, epochs[%$theName][%peakAvgEndP])
	Note /K epTransient, theNote
	
	Wave traceMatrix = getDataTraceWave()
	Duplicate /O traceMatrix, dest:$(theName + "_" + ksEpochTraces)
	Wave image = getImageVoltammogram()
	Duplicate /O image, dest:$(theName + "_" + ksEpochVGimg)
	Wave cv = getCVTrace()
	Duplicate /O cv, dest:$(theName + "_" + ksEpochVGcv)
	
//	STRUCT VM_ControlChannels controls
//	VM_checkControlWaves(chanStruct=controls)
//	if (controls.numControls > 0)
//		Variable index = 0
//		for (index = 0; index < controls.numControls; index += 1)
//			Wave control = controls.traces[index]
//			Duplicate /O control, dest:$(theName + "_" + controls.labels[index])
//		endfor
//	endif
	return 1		// success
End

Function VM_retrieveDataForTransient(theLabel, [plots, tGraph])
	String theLabel, tGraph
	Variable plots
	
	Variable withCsr = 0
	if (!ParamIsDefault(tGraph) && strlen(tGraph) > 0)
		withCsr = 1
	endif
	if (ParamIsDefault(plots) || plots < 0)
		plots = 0
	elseif (plots > 1)
		plots = 1
	else
		plots = round(plots)
	endif

	Wave epochs = VM_getEpochWave()
	Wave /T epochCat = VM_getEpochCatWave()
	Variable row = VM_getRowNumberFromLabel(theLabel, epochs)
	STRUCT VM_ControlChannels controls
	VM_checkControlWaves(chanStruct=controls)
	
	Wave theEpoch = VoltHelpers#getEpochTransient()
	Wave epochTrans = VM_retrieveEpochData(theLabel, "transient")
	Duplicate /O epochTrans, theEpoch
	theEpoch += epochs[row][%BLValue]

	Wave traceMatrix = VoltHelpers#getDataTraceWave()
	Wave epochtraceMatrix = VM_retrieveEpochdata(theLabel,"traces")
	Duplicate /O epochtraceMatrix, traceMatrix
	
	Wave image = VoltHelpers#getImageVoltammogram()
	Wave epochImage = VM_retrieveEpochdata(theLabel,"image")
	Duplicate /O epochImage, image
	
	Wave CVTrace = VoltHelpers#getCVTrace()
	Wave epochCVTrace = VM_retrieveEpochData(theLabel, "cv")
	Duplicate /O epochCVTrace, CVTrace

	NVAR cvInFront = $(VM_getKeepCVFrontFlag())

	if (plots)
		if (withCsr)
			VM_makeCVPlot(CVTrace, csrPnt1=epochs[row][%peakAvgStartP], csrPnt2=epochs[row][%peakAvgEndP], front=cvInFront, tGraph=tGraph)
		else
			VM_makeCVPlot(CVTrace, front=cvInFront)
		endif		
		STRUCT Point theStart
		theStart.v = epochs[row][%peakAvgStartP]
		theStart.h = epochs[row][%BLstartPnt]
		STRUCT Point theEnd
		theEnd.v = epochs[row][%peakAvgEndP]
		theEnd.h = epochs[row][%BLendPnt]
//		VM_showImageVoltammogram(filter=1, csr1=theStart, csr2=theEnd)
		VM_showImageVoltammogram(filter=1, restricted=1)
	endif

	Variable index = 0
	for (index = 0; index < controls.numControls; index += 1)
		String epochControlName = theLabel + "_" + controls.labels[index]
		DFREF dataPath = getEpochFolder()
		DFREF path = getPackage()
		Wave controlMatrix = dataPath:$epochControlName
		Wave controlM = getControlTraceWave(channel=controls.labels[index])
		if (WaveExists(controlMatrix))
			Duplicate /O controlMatrix, controlM
			Variable baseline = NumberByKey(ksVM_BaselineNum, note(controlMatrix))
			Make /N=(Dimsize(controlMatrix, 1)) /O path:$(ksEpochControlTrace + "_" + controls.labels[index]) /WAVE=controlTrace
			controlTrace = controlMatrix[baseline][p]
		else
			KillWaves /Z control
		endif		
	endfor
End

Function /WAVE VM_retrieveEpochData(epoch, type)
	String epoch, type
	
	DFREF path = getEpochFolder()
	Wave data
	type = LowerStr(type)
	strswitch(type)
		case "transient":
			Wave data = path:$(epoch)
			break
		case "traces":
			Wave data = path:$(epoch + "_" + ksEpochTraces)
			break
		case "image":
			Wave data = path:$(epoch + "_" + ksEpochVGimg)
			break
		case "cv":
			Wave data = path:$(epoch + "_" + ksEpochVGcv)
			break
		case "fit":
			Wave data = path:$(epoch + "_fit")
			break
	endswitch
	return data
End

Function VM_moveDataWaves(selector)
	String selector

	String theList = WaveList("*_" + selector + "*", ";", "")
	Variable index
	
	DFREF path = VoltHelpers#getDataLoadFolder()
	do
		MoveWave $(StringFromList(index, theList)), path
		index += 1
	while(index < ItemsInList(theList))
End

Function VM_getRowNumberFromLabel(theLabel, theWave)
	String theLabel
	Wave theWave
	
	String theLabels = makeLblListFromWave(theWave)
	return WhichListItem(theLabel, theLabels)
End


Function VM_getPointAdvance()
	
	Variable advance = -1
	Wave epochs = VM_getEpochWave()
	if (DimSize(epochs, 0) > 2)
		// at least 3 record events to determine point advance
		Variable diff1 = epochs[1][%stimulusPoint] - epochs[0][%stimulusPoint]
		Variable diff2 = epochs[2][%stimulusPoint] - epochs[1][%stimulusPoint]
		if (diff1 == diff2)
			advance = diff1
		else
			advance = floor((diff1 + diff2) / 2)
		endif
	endif
	return advance
End

Function VM_hasControls()
	Wave /T channels = getChannelAssignment()
	if (!WaveExists(channels))
		printf ksVM_ErrorMsg,  "VM_hasControls", "channel assignment wave is missing"
		return 0
	endif
	if (numpnts(channels) == 1)
		return 0 	// no control channels, the only channel must be data, so nothing more to do
	endif
	// we arrive here, when we have at least 2 channels, now extract the control channels
	Extract /FREE /INDX /O channels, controlChannels, cmpstr(channels, "control") == 0
	if (numpnts(controlChannels) == 0)
		// there are no assigned control channels, so nothing more to do
		return 0
	endif

	return numpnts(controlChannels)
End

//! Calibrates the traces in the given list with the calibration factor. All epochs that 
// already have a calibration indicator in their wave are ignored.
// @param transients:		a list of trace names of transients
//-
Function VM_calibrateTransients(transients)
	String transients
	
	String errormsg = VM_getErrorStr("VM_calibrateTransients") 
	String warnmsg = VM_getWarnStr("VM_calibrateTransients")

	Variable numTransients = ItemsInList(transients)
	if (numTransients == 0)
		printf errormsg, "need a list of transient names to calibrate"
		return 0
	endif
	
	NVAR factor = $(getCalibrationFactor())
	DFREF epochFolder = getEpochFolder()
	Variable index
	for (index = 0; index < numTransients; index += 1)
		String epoch = StringFromList(index, transients)
		Wave current = epochFolder:$epoch
		if (!WaveExists(current))
			printf warnmsg, "did not find wave " + epoch + ", not calibrated"
			continue
		endif
		
		String theNote = note(current)
		Variable isCalibrated = numtype(NumberByKey(ksVM_CalibrateKey, theNote)) == 0
		
		if (isCalibrated)
			printf warnmsg, "the wave " + epoch + " is already calibrated - skipped"
			continue
		else	
			current *= factor
			SetScale d 0, 0, "M", current
			Note /K current, ReplaceNumberByKey(ksVM_CalibrateKey, theNote, factor)
		endif
	endfor
	return 1
End


// *********************   T A B L E S   &    G R A P H S    *********************

Function VM_showChannelAssignment()
	String channels = VM_getChannels()
	if (strlen(channels) == 0 || ItemsInList(channels) == 0)
		DoAlert 0, "No channels available!"
		return 0
	else
		getChannelAssignment(show=1)
		getChannelClasses(show=1)
	endif
	return 1
End

Function VM_showEpochCategories()

	DoWindow $ksEpochCatTableName
	switch (V_flag)
		case 0:
			// window doesn't exist, or is hidden
			Wave /T epochsCat = VM_getEpochCatWave()
			Edit /W=(5,44,260,251) /K=1 /N=$(ksEpochCatTableName) epochsCat.ld as ksEpochCatTableTitle
			break
		case 2:
			DoWindow /HIDE=0 $ksEpochCatTableName
			break
	endswitch
End // END VM_showEpochCategories

Function VM_showEpochLocations()

	DoWindow /F $ksEpochLocTableName
	switch(V_flag)
		case 0:
			Wave /T epochs = VM_getEpochWave()
			Edit /K=1 /N=$(ksEpochLocTableName) epochs.ld as ksEpochLocTableTitle
			break
		case 2:
			DoWindow /HIDE=0 $ksEpochLocTableName
			break
	endswitch
End
	
//! Creates a voltammogram as a 2 dimensional color plot.
// @param image:				2D matrix with the background subracted current traces, defaults to the
//										automatically created one with the VM_makeImageVoltammogram function.
// @param theGraphName:	name of the graph to be created, defaults to the standard name
// @param filter:				flag to indicate to filter (smooth) the image or not (defaults to 0)
// @param csr1:				Point structure containing the location of the first cursor
// @param csr2:				Point structure containing the location of the second cursor
// @param restricted:		flag to determine the shown range (only ramp or the full trace), defaults
//										to show the full trace (0)
// @return the name of the graph with the voltammogram
//-
Function /S VM_showImageVoltammogram([image, theGraphName, filter, csr1, csr2, restricted])
	Wave image
	String theGraphName
	Variable filter, restricted
	STRUCT Point &csr1, &csr2
	
	String errormsg = VM_getErrorStr("VM_showImageVoltammogram")
	
	if (ParamIsDefault(theGraphName) || strlen(theGraphName) == 0) 
		theGraphName = VM_getImgVoltName()
	endif
	String theGraphTitle = ReplaceString("_", theGraphName, " ")

	if (ParamIsDefault(image) || !WaveExists(image))
		Wave image = getImageVoltammogram()
	endif

	filter = ParamIsDefault(filter) || filter < 1 ? 0 : 1
	restricted = ParamIsDefault(restricted) || restricted < 1 ? 0 : 1 
	
	Wave ramp = VM_getRampWave()

	if (restricted)
		if (WaveExists(image))
			Wave ramp = VM_getRampWave(numbers=DimSize(image, 1))
		endif
		Variable rampxStart = pnt2x(ramp, NumberByKey(ksVM_RampStartPKey, note(ramp)))
		Variable rampxEnd = pnt2x(ramp, NumberByKey(ksVM_RampEndPKey, note(ramp)))
	endif
		
	DFREF path = getPackage()
	Wave filtered = getFilteredImageVG(theImage=image)
	
	if (filter)
		Wave image = filtered
	endif
	
	String theNote = ""
	Variable displayScale = 0
	Wave dispWave = VM_getVoltgrmDispWave("display")
	Wave pointWave = VM_getVoltgrmDispWave("points")
	displayScale = WaveExists(dispWave) && WaveExists(pointWave)
	
	DoWindow $theGraphName
	STRUCT VM_VGColorScale prefs
	VoltGUI#loadCSPrefs(prefs)
	
	switch (V_flag)
		case 0:
			if (displayScale)
				Display /W=(60,60,382,280) /K=1/N=$theGraphName as theGraphTitle
				AppendImage image
				ModifyGraph margin(left)=40
				ModifyGraph userticks(left)={pointWave,dispWave}
				Label left "V"
				ModifyGraph tick(bottom)=3, mirror=2, noLabel(bottom)=2
			else
				Display /W=(60,60, 280,280) /K=1/N=$theGraphName as theGraphTitle
				AppendImage image
				ModifyGraph margin(left)=14
				ModifyGraph tick=3,noLabel=2
			endif
			ModifyGraph margin(bottom)=14,margin(top)=14,margin(right)=90			
			ModifyGraph standoff=0, width={Aspect,1}
			ModifyGraph gfont=$ksVM_defGraphFont, gfsize=kVM_defFSize
			SetWindow $theGraphName, hook(cScaleAdjust)=WH_setVGColorScale
			ColorScale /W=$theGraphName /C/N=theCScale/F=0/A=RC/X=5.00/Y=0.00/E=2 image=$(NameOfWave(image)), frame=0.00, heightPct=100
		case 1:
//			ModifyImage /W=$theGraphName $(NameOfWave(image)) ctab={-3.5e-09,1.2e-08,Rainbow,0}
			if (prefs.method)
				// 1 = manual setting
				ModifyImage /W=$theGraphName $(NameOfWave(image)) ctab={prefs.min,prefs.max,Rainbow,0}
			else
				if (cmpstr((NameOfWave(image))[0,2], "ep_") == 0)
					DFREF loc = GetWavesDatafolderDFR(image)
					if (filter)
						Wave cv = loc:$(ReplaceString("_img_filtered", NameOfWave(image), "_cv"))
					else
						Wave cv = loc:$(ReplaceString("_img", NameOfWave(image), "_cv"))
					endif
				elseif (strsearch(NameOfWave(image), ksVGImageName, 0) >= 0)
					Wave cv = path:$ksVGCVName
				else
					DFREF loc = GetWavesDatafolderDFR(image)
					Variable uScr = strsearch(NameOfWave(image), "_img", 0)
					if (uScr < 0)
						printf errormsg, "can't locate the corresponding CV trace. STOPPED!"
						return ""
					endif
					if (filter)
						Wave cv = loc:$(ReplaceString("_img_filtered", NameOfWave(image), "_cv"))
					else
						Wave cv = loc:$(ReplaceString("_img", NameOfWave(image), "_cv"))
					endif
				endif
				
				Variable theMax, theMin
				if (restricted)
					theMax = WaveMax(cv, rampxStart, rampxEnd)
					theMin = WaveMin(cv, rampxStart, rampxEnd)
				else
					theMax = WaveMax(cv)
					theMin = WaveMin(cv)
				endif

				Variable newMin = -(theMax * prefs.PofUpper)
				ModifyImage /W=$theGraphName $(NameOfWave(image)) ctab={newMin, theMax, Rainbow, 0}
			endif
			break
		case 2:
			DoWindow /HIDE=0 $theGraphName
	endswitch

	if (restricted)
		SetAxis /W=$theGraphName left, rampxStart, rampxEnd
		Note /K image, ReplaceStringByKey(ksVM_DisplayKey, note(image), ksVM_restrictedDispValue)
	endif
	
	if (!ParamIsDefault(csr1) || !ParamIsDefault(csr2))
		SetWindow $theGraphName hook(baselines)=WH_adjustVGFromCursor
		if (!ParamIsDefault(csr1))
			Cursor /I /H=1 /L=1 /P /S=1 /W=$theGraphName E $(NameOfWave(image)) csr1.h, csr1.v
		endif
		if (!ParamIsDefault(csr2))
			Cursor /I /H=1 /L=1 /P /S=1 /W=$theGraphName F $(NameOfWave(image)) csr2.h, csr2.v		
		endif
	endif
	return theGraphName
End


// This function makes the CV Plot
//
// required parameters
// theCap	
//
// optional parameters
// theVoltage
// theName
// rampPoints
// csrPnt1
// csrPnt2
// csr
// front			0 = (default), treat the CV window as normal window
//					1 = keep CV window always in front
//
// toMax			0 = (default), keep cursors as they are
//					1 = align cursors to the maximum value
//					2 = align cursors to the fitted maximum value
// tGraph			
// add				
// 
Function /S VM_makeCVPlot(theCap, [theVoltage, theName, rampPoints, csrPnt1, csrPnt2, csr, front, toMax, tGraph, straight, colorize, add])
	Wave theCap, theVoltage
	String theName, tGraph
	Variable rampPoints, csrPnt1, csrPnt2, csr, front, toMax, add, straight, colorize
	

	String errormsg = ""
	sprintf errormsg, ksVM_ErrorMsg, "VM_makeCVPlot", "%s"
	
	colorize = ParamIsDefault(colorize) || colorize < 1 ? 0 : 1
	straight = ParamIsDefault(straight) || straight < 1 ? 0 : 1
	add = ParamIsDefault(add) || add < 1 ? 0 : 1
	
	if (ParamIsDefault(toMax) || toMax <= 0)
		toMax = 0
	else
		switch (round(toMax))
			case 1:
				toMax = 1
				break
			case 2:
				toMax = 2
				break
			default:
				toMax = 0
		endswitch
	endif
	
	front = ParamIsDefault(front) || front < 1 ? 0 : 1
	csr = ParamIsDefault(csr) || csr < 1 ? 0 : 1

	if (!ParamIsDefault(csrPnt1) || !ParamIsDefault(csrPnt2))
		csr=1
	endif
	if (csr && (ParamIsDefault(tGraph) || strlen(tGraph) == 0))
		// we need a name for the transient graph if we display the cursors
		printf errormsg, "The transient graph is missing. Can't continue"
		DoAlert 0, "ERROR! The transient graph name is missing and needed for the cursor interaction. Either add the name or remove the cursor display."
		return ""
	endif
	if (ParamIsDefault(theName) || strlen(theName) == 0)
		theName = VM_getCVPlotName()
	endif
	String theTitle = ReplaceString("_", theName, " ")
	if (!straight)
		if (ParamIsDefault(theVoltage))
			if (ParamIsDefault(rampPoints) || rampPoints < 0)
				Wave theVoltage = VM_getRampWave()
			else
				Wave theVoltage = VM_getRampWave(numbers=round(rampPoints))
			endif
		endif
		if (!WaveExists(theVoltage))
			straight = 1
		endif
	endif
	
	if (front)
		DoWindow /F $theName
	else
		DoWindow $theName
	endif
	String status = ""
	switch (V_flag)
		case 0:			
			Display /W=(370,59,670,349) /K=1 /N=$theName as theTitle
			add = 1
		case 1:
			if (!add)
				break
			endif			
			if (!straight)
				AppendtoGraph /W=$theName /L=current /B=voltage theCap vs theVoltage
				// make sure we exclude any artifact from the display, which might skew the displayed scale
				Variable rampStartP = NumberByKey(ksVM_RampStartPKey, note(theVoltage))
				Variable rampEndP = NumberByKey(ksVM_RampEndPKey, note(theVoltage))
				Variable low = WaveMin(theVoltage, pnt2x(theVoltage, rampStartP), pnt2x(theVoltage, rampEndP))
				Variable high = WaveMax(theVoltage,  pnt2x(theVoltage, rampStartP), pnt2x(theVoltage, rampEndP))
				Variable offset = ((high -low) / (rampEndP - rampStartP)) / 2
				low = low < 0 ? low + offset : low - offset
				if (numtype(rampStartP) != 2 && numtype(rampEndP) != 2)
					SetAxis /N=1 voltage low, high
				else
					SetAxis /A/N=1 voltage
				endif	
				SetAxis /A=2/N=1 current
				ModifyGraph /W=$theName freePos(current)={0,voltage},freePos(voltage)={0,current}
				Label voltage "voltage (\\U)"
				Label current "current (\\U)"
				ModifyGraph /W=$theName lblPos=50, width={Aspect,1}
			else	
				AppendToGraph /W=$theName theCap
				SetAxis /A/N=1 left
				SetAxis /A/N=1 bottom
				Label bottom "time (\\U)"
				Label left "current (\\U)"
				ModifyGraph /W=$theName width={Aspect,2}
			endif
			status = "new"
			SetWindow $theName userdata(status)= status
			ModifyGraph /W=$theName mode($(NameOfWave(theCap)))=0, rgb($(NameOfWave(theCap)))=(0,0,0)
			ModifyGraph /W=$theName standoff=0
			ModifyGraph /W=$theName gfont=$ksVM_defGraphFont, gfsize=kVM_defFSize

			break
		case 2:
			DoWindow /HIDE=0 $theName
			break
	endswitch

	SetWindow $theName hook(baselines)=WH_adjustVGFromCursor
	
	if (csr)
		SetWindow $theName userdata(transientGraph)=tGraph
		STRUCT VoltammetryPreferences prefs
		getTransPrefs(prefs)
		if(ParamIsDefault(csrPnt1) || ParamIsDefault(csrPnt2))
			if (ParamIsDefault(csrPnt1))
				csrPnt1 = prefs.oxDetails.startPoint
			endif
			if (ParamIsDefault(csrPnt2))
				csrPnt2 = prefs.oxDetails.endPoint
			endif
		endif
		if (toMax > 0)
			Variable csrDiff = csrPnt2 - csrPnt1
			Variable rest = mod(abs(csrDiff), 2)
			
			Variable maxLocX, maxLoc
			if (!straight)
				Variable rampMaxP = NumberByKey(ksVM_RampMaxPKey, note(theVoltage))
				WaveStats /Q/C=0/W/M=1/R=[rampStartP, rampMaxP] theCap
			else
				WaveStats /Q/C=0/W/M=1 theCap
			endif

			Wave M_WaveStats
			maxLocX = WaveDims(M_WaveStats) == 1 ? M_WaveStats[%maxLoc] : M_WaveStats[%maxLoc][0]
			maxLoc = x2pnt(theCap, maxLocX)
			
			switch (toMax)
				case 2:
					rampStartP = NumberByKey(ksVM_RampStartPKey, note(theVoltage))
					rampEndP = NumberByKey(ksVM_RampEndPKey, note(theVoltage))

					Variable PRange = rampEndP - rampStartP
					PRange = PRange / 16
					Make /N=4 /O /FREE coefs = {0,0,0,0}
					Make /N=(numpnts(theCap)) /O /FREE fitValues
					CurveFit /N/NTHR=0/Q/W=2 gauss, kwCWave=coefs theCap[maxLoc - PRange, maxLoc + PRange] /D=fitValues
				
					maxLoc = round(x2pnt(theCap, coefs[2]))
				case 1:
					if (rest)
						csrPnt1 = maxLoc - floor(csrDiff / 2)
						csrPnt2 = maxLoc + ceil(csrDiff / 2)
					else
						csrPnt1 = maxLoc - (csrDiff / 2)
						csrPnt2 = maxLoc + (csrDiff / 2)
					endif
					break
			endswitch
		endif
		
		SetWindow $theName hook(refineTrans)=WH_refineTransientFromCV
				
		if (toMax)
			SetWindow $theName userdata(update)= "no"
			Cursor /H=2 /S=2 /L=1 /C=(0, 0, 65535) /P /W=$theName G $(NameOfWave(theCap)) csrPnt1
			SetWindow $theName userdata(update)= "yes"
			Cursor /H=2 /S=2 /L=1 /C=(65535, 0, 0) /P /W=$theName H $(NameOfWave(theCap)) csrPnt2			
		else
			SetWindow $theName userdata(update)= "no"
			Cursor /H=2 /S=2 /L=1 /C=(0, 0, 65535) /P /W=$theName G $(NameOfWave(theCap)) csrPnt1
			Cursor /H=2 /S=2 /L=1 /C=(65535, 0, 0) /P /W=$theName H $(NameOfWave(theCap)) csrPnt2
			SetWindow $theName userdata(update)= "yes"
		endif
		
		ControlBar /W=$theName 25
//		SetVariable svar_distanceInVolt, win=$theName
		SetVariable svar_distanceInVolt, win=$theName, pos={10, 5}, size={80, 20}, title="distance"
		SetVariable svar_distanceInVolt, win=$theName, fSize=kVM_defFSize, font=$ksVM_defPanelFont
		SetVariable svar_distanceInVolt, win=$theName, limits={-inf, inf, 0},format="%1.3g"
		SetVariable svar_distanceInVolt, win=$theName, value=_NUM:prefs.oxDetails.range
		SetVariable svar_distanceInVolt, win=$theName, proc=svarFunc_VM_setCursorDistance
		
		Button btn_resetToDistance, win=$theName, pos={100, 2}, size={35, 20}, title="set"
		Button btn_resetToDistance, win=$theName, fSize=kVM_defFSize, font=$ksVM_defPanelFont
		Button btn_resetToDistance, win=$theName, proc=btnFunc_VM_resetCursorDistance
		
		Button btn_setToDefLoc, win=$theName, pos={140, 2}, size={100, 20}
		Button btn_setToDefLoc, win=$theName, font=$ksVM_defPanelFont, fsize=kVM_defFSize
		Button btn_setToDefLoc, win=$theName, title="set to def range"
		Button btn_setToDefLoc, win=$theName, proc=btnFunc_VM_set2DefLoc
		
		Button btn_showCT, win=$theName, pos={245, 2}, size={30,20}
		Button btn_showCT, win=$theName, fsize=kVM_defFSize, title="CT"
		Button btn_showCT, win=$theName, proc=btnFunc_VM_showCT
//		SetWindow $theName hook(equiDistance)=WH_equiDistantCursors	
	endif
	
	if (colorize)
		VM_highlightCVTracePhases(theName)
	endif
	return theName
End


Function VM_showControlTraces()
	
	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()

	DoWindow /F $ksControlGraphName
	if (!V_flag)
		//  create the graph
		Display /W=(705, 60, 1080, 320) /K=1/N=$ksControlGraphName as "Trigger Traces"
		ModifyGraph standoff=0
	endif

	String traceList = TraceNameList(ksControlGraphName, ";", 1)
	SetDatafolder package
	String controlList = WaveList(ksEpochControlTrace + "_*", ";", "")
	SetDatafolder cDF
	
	Variable position = strlen(ksEpochControlTrace + "_")
	Variable index = 0, colorNum = 0
	colorNum = ItemsInList(traceList)
	String legendText = ""
	for (index = 0; index < ItemsInList(traceList); index += 1)
		legendText += "\\s(" + StringFromList(index, traceList) + ") " + (StringFromList(index, traceList))[position, Inf] + "\r"
	endfor
	index = 0
	do
		Wave data = package:$(StringFromList(index, controlList))
		String theLabel = (NameOfWave(data))[position, Inf]
		if (WhichListItem(NameOfWave(data), traceList) < 0)
			// is not in the list, so add it
			Wave color = $(getColorValueWave())
			AppendToGraph /W=$ksControlGraphName /C=(color[colorNum][0], color[colorNum][1], color[colorNum][2]) data
			legendText += "\\s(" + NameOfWave(data) + ") " + theLabel + "\r"
			colorNum += 1
		endif
		index += 1
	while (index < ItemsInList(controlList))
	legendText = RemoveEnding(legendText, "\r")
	Legend /C/N=theLegend/A=LT/X=2.00/J legendText
	SetAxis /A/N=1 left
	SetAxis /A/N=1 bottom
End

Function svarFunc_VM_setCursorDistance(sv) : SetVariableControl
	STRUCT WMSetVariableAction &sv
	
	switch(sv.eventCode)
		case 2:			// enter key hit
			String gCsrInfo = CsrInfo(G, sv.win)
			Variable gCsrExists = strlen(gCsrInfo) > 0
			Variable hCsrExists = strlen(CsrInfo(G, sv.win)) > 0
			
			if (!gCsrExists)
				DoAlert 0, "Need the G cursor on the trace to position the H Cursor!"
				return 0
			endif
			
			STRUCT VoltammetryPreferences prefs
			VoltHelpers#getTransPrefs(prefs)
			
			Wave cv = TraceNameToWaveRef(sv.win, StringByKey("TNAME", gCsrInfo))
			Wave ramp = VM_getRampWave()
			Variable maxLoc = NumberByKey(ksVM_RampMaxPKey, note(ramp))
			Variable voltDelta = VM_rampDeltaY(ramp=ramp)			
			Variable numPoints = sv.dval / voltDelta
			
			Variable newHLoc = pcsr(G, sv.win) + numPoints
			if (newHLoc < maxLoc)
				prefs.oxDetails.endPoint = newHLoc
				prefs.oxDetails.endTime = pnt2x(ramp, prefs.oxDetails.endPoint)
				prefs.oxDetails.endVoltage = ramp[prefs.oxDetails.endPoint]
				prefs.oxDetails.range = prefs.oxDetails.endVoltage - prefs.oxDetails.startVoltage
				setTransPrefs(prefs)
				Cursor /H=2 /L=1 /P /W=$sv.win H $(NameOfWave(cv)) prefs.oxDetails.endPoint
			else
				DoAlert 0, "New end location will be outside the rising edge of the ramp! Not set!"
				return 0	
			endif
			
			break
	endswitch
End

Function btnFunc_VM_resetCursorDistance(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch(bs.eventCode)
		case 2:
			STRUCT WMSetVariableAction var
			var.eventCode = 2
			var.win = bs.win
			
			ControlInfo /W=$bs.win svar_distanceInVolt
			var.dval = V_value
			
			svarFunc_VM_setCursorDistance(var)
			break
	endswitch
	
End

Function btnFunc_VM_set2DefLoc(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch(bs.eventCode)
		case 2:
			String gCsrInfo = CsrInfo(G, bs.win)
			Variable gCsrExists = strlen(gCsrInfo) > 0
			
			if (!gCsrExists)
//				String list = TraceNameList(bs.win, ";", 0)
//				if (ItemsInList(list) != 1)
					return 0
//				endif
//				Cursor /W=$bs.win G $(StringFromList(0, list))			
			endif
			
			
			Wave ramp = VM_getRampWave()
			String theNote = note(ramp)
			Variable maxPoint = NumberByKey(ksVM_RampMaxPKey, theNote)
			
			FindLevel /EDGE=1 /P /Q /R=[0, maxPoint] ramp, 0.4
			Variable xPoint = V_flag == 1 ? 0 : V_LevelX
			Cursor /H=2 /L=1 /P /W=$bs.win G $(StringByKey("TNAME", gCsrInfo)) xPoint
			
			STRUCT WMSetVariableAction var
			var.eventCode = 2
			var.win = bs.win			
			var.dval = 0.2
			
			svarFunc_VM_setCursorDistance(var)
			break
	endswitch

End

Function btnFunc_VM_showCT(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch (bs.eventCode)
		case 2:			// mouse button up
			Wave cv = TraceNameToWaveRef(bs.win, StringFromList(0, TraceNameList(bs.win, ";", 0)))
			VM_makeCVPlot(cv, theName = VM_getCVPlotName() + "_straight", straight=1)
			
			break
	endswitch
End



Function /S VM_showMeanCV(meanName, errorName, [theName])
	String meanName, errorName, theName
	
	String graphName = ""
	String theTitle = ""
		
	if (ParamIsDefault(theName) || strlen(theName) == 0)
		theName = ""
		theTitle = ""
	else
		theTitle = theName
	endif

	if (strlen(meanName) == 0 || strlen(errorName) == 0)		
		if (strlen(meanName) == 0)
			SVAR lastMN = $(SiST_getLastMeanName())
			if (SVAR_Exists(lastMN))
				meanName = lastMN
			endif
		endif
		if (strlen(errorName) == 0)
			SVAR lastMN = $(SiST_getLastMeanName())
			if (SVAR_Exists(lastMN))
				if (cmpstr(meanName, lastMN) == 0)
					// only suggest the last error wave name, when the last mean wave was choosen
					SVAR errType = $(SiST_getLastErrType())
					if (SVAR_Exists(errType))
						strswitch ( errType )
							case "SD + SEM":
							case "SD":
								SVAR lastEN = $(SiST_getLastSDName())
								break
							case "SEM":
								SVAR lastEN = $(SiST_getLastSEMName())
								break
						endswitch
						if (SVAR_Exists(lastEN))
							errorName = lastEN
						endif
					endif
				endif
			endif
		endif
		
		Prompt meanName, "Please enter the name of the mean wave:"
		Prompt errorName, "Please enter the name of the error wave:"
		Prompt theName, "Please choose a graph", popup, "_new_;" + WinList("*", ";", "WIN:1")
		
		DoPrompt "Choose CV Mean to Plot", meanName, errorName, theName
		
		if (V_flag)
			// user canceled
			return graphName
		endif
		
	endif
	
	if (cmpstr(theName, "_new_") == 0)
		theName = UniqueName("MeanCVPlot", 6, 0)
		theTitle = "Mean CV " + theName[10, strlen(theName) - 1]
	endif
	
	Wave meanW = $meanName
	Wave errorW = $errorName

	if (!WaveExists(meanW) || !WaveExists(errorW))
		printf "** ERROR (VM_showMeanCV): waves '%s' and/or '%s' don't exists -> STOPPED!", meanName, errorName
		return graphName
	endif
	
	DFREF errDispF = getErrDisplayFolder()
	Duplicate /O errorW, errDispF:$(errorName + "_hi") /WAVE=hi, errDispF:$(errorName + "_lo") /WAVE=lo
	FastOp hi = meanW + errorW
	FastOp lo = meanW - errorW

	String wunits = WaveUnits(meanW, 1)
	if (cmpstr(wunits[0, 3], "mean") == 0)
		SetScale d 0,0, wunits[5, strlen(wunits) - 1], meanW, hi, lo
	endif
	
	DoWindow /F $theName
	if (V_flag == 0)
		graphName = VM_makeCVPlot(hi, theName=theName)
		DoWindow /T $graphName, theTitle
	else
		graphName = VM_makeCVPlot(hi, theName=theName, add=1)
	endif
	
	String traceName = NameOfWave(hi)
	ModifyGraph /W=$graphName mode($traceName)=7, rgb($traceName)=(52428,52428,52428)
	ModifyGraph /W=$graphName hbFill($traceName)=2, toMode($traceName)=1
	VM_makeCVPlot(lo, theName=graphName, add=1)
	ModifyGraph rgb($(NameOfWave(lo)))=(52428,52428,52428)
	VM_makeCVPlot(meanW, theName=graphName, add=1)
	
	Variable index
	String udata = GetUserData(graphName, "", "")
	String meanList = ""
	if (strlen(udata) > 0)
		meanList = StringByKey("MEANS", udata)
		if (strlen(meanList) > 0)
			for (index = 0; index < ItemsInList(meanList, ","); index += 1)
				ReorderTraces $meanName, {$(StringFromList(index, meanList, ","))}
			endfor
		endif
	endif
	
	meanList = AddListItem(meanName, meanList, ",", Inf)
	udata = ReplaceStringByKey("MEANS", udata, meanList)
	SetWindow $graphName userdata=udata
	
	return graphName
End

Function VM_deleteArtifactsFromData(distance, length)
	Variable distance, length

	Wave data = VM_getRawDataWave()
	if (!WaveExists(data))
		return 0
	endif
	
	Variable numCol = DimSize(data, 1)
	Variable numRows = DimSize(data, 0)
	
	Variable acqFreq = DimDelta(data, 0)
	Variable numJumpPoints = distance / acqFreq
	Variable numArtifactPnts = length / acqFreq
	
	DFREF folder = VoltHelpers#getPackage()
	Wave expTransient = folder:$"Transient"
	
	String diffName = NameOfWave(expTransient) + "_DIF"
	Duplicate /O expTransient, folder:$diffName /WAVE=diff	
	Differentiate diff
	
	Extract /FREE /INDX /O diff, levels, diff < -7.5e-8
	Variable numLevels = numpnts(levels)
	if (numLevels == 0)
		// no levels found, nothing more to do
		return -1
	endif
	
	Duplicate /FREE levels, levels_DIF	
	Differentiate /METH=1 levels_DIF
	Extract /FREE /INDX /O levels_DIF, levelIndx, levels_DIF >= numJumpPoints

	numLevels = numpnts(levelIndx)
	if (numLevels == 0)
		// something went wrong
		return -1
	endif	
	
	Make /FREE /N=(numLevels) levelLoc = levels[levelIndx[p]]
	
	Variable index = 0
	for (index = numLevels - 1; index >= 0; index -= 1)
		Variable endP = levelLoc[index] + 3
		Variable startP = (endP - numArtifactPnts) + 1
		startP = startP < 0 ? 0 : startP
		
		if (index == numLevels - 1)
			if ((startP + numJumpPoints) < numpnts(expTransient) - 1)
				DeletePoints /M=0 (startP + numJumpPoints), numpnts(expTransient) - (startP + numJumpPoints), data
			endif
		endif
		
		DeletePoints /M=0 startP, numArtifactPnts, data
	
	endfor 
End

