#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion = 6.22
#pragma version = 2.7
#pragma ModuleName = VoltAnalysis
#include "VM_Helpers", version >= 2.4
#include "VM_GUI", version >= 2.7
#include "VM_Ramp", version >= 1.5
#include "WindowHelper", version >= 2.6

// by Roland Bock, January 2012
// - version 2.7: OCT 2018 - minor bug fixes
// - version 2.5: APR 2017 - fixed bug in cleanup procedure
// - version 2.2: JUN 2015 - changed the get simple stats wave functions with an option to return it
//					with the number of entries in the provided list.
// - version 2.1: APR 2014
// - version 2: FEB 2013
// ** version 1: FEB 2013

// ***************************************************************
// *************               S T A T I C    C O N S T A N T S 
// ***************************************************************
Constant kVM_FitPreferenceVersion = 1
static Constant kPrefAnalysisVersion = 1
static Strconstant ksAnalysisPrefsFileName = "AnalysisPreferences.bin"

static Strconstant ksTransientName = "Transient"
static Strconstant ksTransientDiffName = "Transient_DIF"
static Strconstant ksTransientSmoothName = "Transient_smooth"
static Strconstant ksTransientSearchName = "Transient_search"
static Strconstant ksTransientColorName = "TransientColor"
static Strconstant ksTransientControlName = "ControlTrace"
static Strconstant ksStimLocName = "StimLocFromControl"
static Strconstant ksMasterControlName = "MasterControlTrace"
static Strconstant ksMasterStimLocName = "MasterStimulusLocations"
static Strconstant ksMasterClassName = "MasterClasses"
static Strconstant ksPeakStartSName = "peakStartSearch"
static Strconstant ksPeakStartIName = "peakStartIndexes"
static Strconstant ksXForStimLoc = "XForStimLoc" 
static Strconstant ksSimpleFitValues = "VM_SimpleFitValues"
static Strconstant ksSimpleStatsMax = "VM_MaxValues"
static Strconstant ksSimpleStatsArea = "VM_AreaValues"
static Strconstant ksSimpleTaus = "VM_SimpleTaus"
static StrConstant ksVMCatColorPref = "VMCategoryColorPreference"
static StrConstant ksVM_peakLevel = "VM_PeakLevel"
static StrConstant ksVM_artifactSize = "VM_ArtifactSize"

static Strconstant ksStatusKey = "STATUS"

static Strconstant ksSnapshotF = "VMSnapshot"
static Strconstant ksEpochsAnalysisName = "VMAnalysis"

Strconstant ksSimpleFitStartP = "VM_SimpleFitStartPoints"

Strconstant ksSimpleTausPlot = "VM_TauPlot"
Strconstant ksSimpleMaxPlot = "VM_MaxPlot"

Strconstant ksVM_TransientGraphName = "TransientGraph"

Strconstant ksVM_transEpochKey = "EPOCHS"
Strconstant ksVM_Description = "DESCRIPTION"

// function name prefixes
Strconstant ksVM_AnalysisFuncPre = "VMfTPeak"
Strconstant ksVM_ControlFuncPre = "VMfStim"


// ***************************************************************
// *************                     S T R U C T U R E S
// ***************************************************************
Structure VMGUI_analysisFunction
	int16 version
	char analysisFunc[32]				// function names can only be 32 characters long
	char analysisDisp[32]
	char controlFunc[32]
	char controlDisp[32]
EndStructure

Structure VM_fitPreferences
	uint32 version
	char fitName[80]
	int32 startP
	int32 endP
	double threshold
	int16 repeats
	STRUCT VM_FitMasterGuess guess
EndStructure

Structure VM_FitMasterGuess
	uint32 version
	double params[10]
	int16 numParams
EndStructure

static Structure CleanUpInfo
	DFREF wavePath
	String waveNames[10]
	String graphNames[5]
	String wavePrefixes[5]
	Variable numWaves
	Variable numGraphs
	Variable numPrefixes
EndStructure

//! This structure collects the peak information about the transient. //-
Structure VMPeakInfo
	Variable peakPoint			//< point of the transient with a peak
	Variable peakStartPoint 	//< start of the above peak
	Variable episodeEndPoint
	Variable stimPoint			//< point when the stimulation for the peak occurs
	Variable hasControl			//< flag to show if there are any stim traces
	Variable ignoreControl		//< flag to ignore the control trace
	Variable advance				//< if regular stimulation can be determined, advance by that number
	Variable baseline				//< number of baseline points (from peak start point backwards)
	Variable transientPoints	//< number of points for the transient (from peak start point)
	Variable episodeLengthPnts		//< number of points for the whole episode (used in calibration mode)
	Variable peakExists			//< flag if a peak has been found or not
	Variable maxPointLimit		//< in case the stimulus wasn't found, how far am I allowed to search
	Variable calibration		//< flag to indiciate calibration mode (needs 2 cursors for baseline and peak)
	
	STRUCT VMFoundPeakVals foundVals	//< structure containing the peak values
	
	String class					//< define the substance, i.e. dopamine, serotonin, etc
	FUNCREF VM_findPeakPrototypeFunc findPeak	//< function reference to the peak finding function
	FUNCREF VM_findStimPrototypeFunc findStim	//< function reference to the stimulation finding function
EndStructure

//! This structure collects the actual found peak values. //-
Structure VMFoundPeakVals
	Variable posPeakVal
	Variable posPeakLoc
	Variable posPeakPoint
	Variable posPeakWidth
	Variable negPeakVal
	Variable negPeakLoc
	Variable negPeakPoint
	Variable negPeakWidth
	Variable peakStart
	Variable peakStartPoint
	Variable peakEnd
	Variable peakEndPoint
EndStructure
// ****************************************************
// *****************              M E N U E S            ****************
// ****************************************************
Menu "Voltammetry"
	SubMenu "Utilities"
		"Calculate baseline drift profile", VM_calcBaselineDriftProfile()
		"Calculate calibration factor from graph", VM_calcCalibFactorFromGraph()
	End
	SubMenu "Adjustments"
		SubMenu "Reset preferences"
			"Default fit preferences", VM_resetFitPreferences()
			"Default analysis functions", VM_resetAnalysisPrefs()
		End
	End
End

// ***************************************************************
// *************               S T A T I C    G E T T E R S  
// ***************************************************************
static Function loadAnalysisPrefs(prefs)
	STRUCT VMGUI_analysisFunction &prefs
	
	Variable recordID = getRecordID("analysis")
	LoadPackagePreferences /MIS=1 VM_getPackagePrefsName(), ksAnalysisPrefsFileName, recordID, prefs
	if (V_flag != 0 || V_bytesRead == 0)
		defaultAnalysisStruct(prefs)
	endif
	if (prefs.version != kPrefAnalysisVersion)
		defaultAnalysisStruct(prefs)
	endif
	saveAnalysisPrefs(prefs)
End

static Function saveAnalysisPrefs(prefs, [reset])
	STRUCT VMGUI_analysisFunction &prefs
	Variable reset
	
	Variable recordID = getRecordID("analysis")
	if (ParamIsDefault(reset) || reset < 0)
		reset = 0
	else
		reset = reset > 1? 1 : round(reset)
	endif
	
	if (reset)
		SavePackagePreferences /KILL /FLSH=1 VM_getPackagePrefsName(), ksAnalysisPrefsFileName, recordID, prefs	
	else
		SavePackagePreferences /FLSH=1 VM_getPackagePrefsName(), ksAnalysisPrefsFileName, recordID, prefs
	endif
End

static Function defaultAnalysisStruct(prefs)	
	STRUCT VMGUI_analysisFunction &prefs
	
	prefs.version = kPrefAnalysisVersion
	prefs.analysisFunc = "VMfTPeak_AL_MA_continuous"
	prefs.analysisDisp = ReplaceString("_", ReplaceString(ksVM_AnalysisFuncPre + "_", prefs.analysisFunc, ""), " ")
	prefs.controlFunc = "VMfStim_AL_single"
	prefs.controlDisp = ReplaceString("_", ReplaceString(ksVM_ControlFuncPre + "_", prefs.controlFunc, ""), " ")
End

static Function getCleanUpInfo(info)
	STRUCT CleanUpInfo &info
	
	info.wavePath = VoltHelpers#getPackage()
	
	info.waveNames[0] = ksTransientName
	info.waveNames[1] = ksTransientColorName
	info.waveNames[2] = ksMasterControlName
	info.waveNames[3] = ksMasterStimLocName
	info.waveNames[4] = ksMasterClassName
	info.waveNames[5] = ksTransientDiffName
	info.waveNames[6] = ksTransientSmoothName
	info.waveNames[7] = ksTransientSearchName
	info.numWaves = 8
	
	info.wavePrefixes[0] = ksTransientControlName
	info.wavePrefixes[1] = ksStimLocName
	info.numPrefixes = 2
	
	info.graphNames[0] = ksVM_TransientGraphName
	info.numGraphs = 1
End

static Function loadFitPrefs(fit)
	STRUCT VM_fitPreferences &fit

	Variable recordID = getRecordID("fit")
	LoadPackagePreferences /MIS=1 VM_getPackagePrefsName(), ksAnalysisPrefsFileName, recordID, fit
	if (V_flag != 0 || V_bytesRead == 0)
		defaultFitPrefs(fit)
	endif

End

static Function defaultFitPrefs(fit)
	STRUCT VM_fitPreferences &fit

	fit.version = kVM_FitPreferenceVersion
	fit.fitName = "ExpDecay"
	fit.startP = -1
	fit.endP = -1
	fit.threshold = 3.5e-19
	fit.repeats = 1
	
	STRUCT VM_FitMasterGuess guess
	guess.version = kVM_FitPreferenceVersion
	guess.params[0] = 0
	guess.params[1] = 0.1
	guess.params[2] = 1
	guess.numParams = 3
	
	fit.guess = guess
End


static Function saveFitPrefs(fit, [reset])
	STRUCT VM_fitPreferences &fit
	Variable reset
	
	Variable recordID = getRecordID("fit")
	if (ParamIsDefault(reset) || reset < 0)
		reset = 0
	else
		reset = reset > 1? 1 : round(reset)
	endif
	
	if (reset)
		SavePackagePreferences /KILL /FLSH=1 VM_getPackagePrefsName(), ksAnalysisPrefsFileName, recordID, fit	
	else
		SavePackagePreferences /FLSH=1 VM_getPackagePrefsName(), ksAnalysisPrefsFileName, recordID, fit
	endif
End


static Function getRecordID(name)
	String name
	
	name = LowerStr(name)
	Variable id = 0
	strswitch( name )
		case "analysis":
			id = 0
			break
		case "fit":
			id = 1
			break
	endswitch
	return id
End


static Function cleanUp()
	
	STRUCT CleanUpInfo info
	getCleanUpInfo(info)
	
	DFREF location = info.wavePath
	Variable index = 0
	print "---> trying to clean analysis leftovers."
	for (index = 0; index < info.numGraphs; index += 1)
		DoWindow $(info.graphNames[index])
		if (V_flag > 0)
			KillWindow $(info.graphNames[index])
		endif
	endfor
	index = 0
	print "    ---> closed windows."
	DFREF cDF = GetDatafolderDFR()
	DFREF keepFolder = getSnapshotFolder()

	SetDatafolder keepFolder
	String theList = WaveList(info.waveNames[index] + "_*", ";", "")
	Variable number = ItemsInList(theList)
	SetDatafolder cDF

	do		
		Wave tmp = location:$(info.waveNames[index])
		if (WaveExists(tmp))
			Duplicate /O tmp, keepFolder:$(info.waveNames[index] + "_" + num2str(number))
			KillWaves /Z tmp
		endif
		index += 1
	while (index < 5)
	do
		KillWaves /Z location:$(info.waveNames[index])
		index += 1
	while (index < info.numWaves)		

	index = 0	
	SetDatafolder location
	do
		theList = WaveList(info.wavePrefixes[index] + "_*", ";", "")
		Variable numItems = ItemsInList(theList)
		Variable waves = 0
		for (waves = 0; waves < numItems; waves += 1)
			KillWaves $(StringFromList(waves, theList))
		endfor
		index += 1
	while (index < info.numPrefixes)	
	SetDatafolder cDF
	
	print "    ---> killed waves."
	return 1
End

// This function checks if there is a transient graph and returns the name if it exists, otherwise
// it returns an empty string.
static Function /S getTRGraph()
	
	DoWindow $ksVM_TransientGraphName
	
	if (V_flag)
		return ksVM_TransientGraphName
	else
		return ""
	endif
End

static Function /WAVE getAnalysisTransient()
	
	DFREF path = VoltHelpers#getPackage()
	Wave transient = path:$ksTransientName
	if (!WaveExists(transient))
		Make /N=0 /O path:$ksTransientName /WAVE=transient
	endif
	return transient
End

static Function /WAVE getTransientColorWave()

	DFREF path = VoltHelpers#getPackage()
	Wave color = path:$ksTransientColorName
	if (!WaveExists(color))
		Make /N=0 /O path:$ksTransientColorName /WAVE=color
	endif
	return color
End
	 

static Function /WAVE getPeakStartDiffWave()

	return VoltHelpers#getGlobalPackageWave(ksPeakStartSName)
End

static Function /WAVE getPeakIndexWave()
	
	return VoltHelpers#getGlobalPackageWave(ksPeakStartIName)	
End

static Function /DF getSnapshotFolder()

	DFREF cDF = GetDatafolderDFR()
	SetDatafolder root:
	DFREF path = $ksSnapshotF
	if (DataFolderRefStatus(path) == 0)
		NewDatafolder $ksSnapshotF
		DFREF path = $ksSnapshotF
	endif
	SetDatafolder cDF
	return path
End


static Function /S getNamedWaveList(thePrefix)
	String thePrefix

	DFREF cDF = GetDatafolderDFR()
	DFREF package = VoltHelpers#getPackage()
	
	SetDatafolder package
	String theList = WaveList(thePrefix + "_*", ";", "")
	SetDatafolder cDF
	
	return theList
End


static Function /WAVE getNamedPackageWave(theName, [text])
	String theName
	Variable text
	
	if (ParamIsDefault(text))
		text = 0
	endif
	
	DFREF cDF
	DFREF path = VoltHelpers#getPackage()
	
	if (text)
		Wave /T textWave = path:$theName
		if (!WaveExists(textWave))
			Make /T /N=0 path:$theName /WAVE=textWave
			Note /K textWave, ReplaceStringByKey(ksStatusKey, "", "fresh")
		endif
		return textWave
	else
		Wave theWave = path:$(theName)
		if (!WaveExists(theWave))
			Make /N=0 path:$theName /WAVE=theWave
			Note /K theWave, ReplaceStringByKey(ksStatusKey, "", "fresh")
		endif
		return theWave
	endif
End


static Function /DF getAnalysisFolder()

	return VoltHelpers#makeFolder(ksEpochsAnalysisName)
End

static Function /WAVE loadColorCatPreferences()

	STRUCT VM_CatColorPrefs prefs
	STRUCT VM_CategoryColor colorPrefs
	
	VoltHelpers#loadColorCatPrefs(prefs)
	Wave /T catColors = getCatColorPrefWave()
	
	Variable index = 0
	do
		colorPrefs = prefs.colorPref[index]
		SetDimLabel 0, index, $(colorPrefs.name), catColors
		catColors[index] = colorPrefs.color
		index += 1
	while(index < kVM_numCatPrefs && strlen(colorPrefs.name) > 0)
	return catColors
End

static Function saveColorCatPreferences()

	STRUCT VM_CatColorPrefs prefs

	VoltHelpers#loadColorCatPrefs(prefs)
	Wave /T catColors = getCatColorPrefWave()
	
	Variable index = 0
	do
		STRUCT VM_CategoryColor colorPrefs
		colorPrefs.version = kVM_colorCatPrefsVersion
		colorPrefs.name = GetDimLabel(catColors, 0, index) 
		colorPrefs.color = catColors[index]
		
		prefs.colorPref[index] = colorPrefs
		index += 1
	while (index < kVM_numCatPrefs && strlen(colorPrefs.name) > 0)
	
	VoltHelpers#saveColorCatPrefs(prefs)
	return 1
End



static Function /WAVE getCatColorPrefWave()
	Wave /T catColors = getNamedPackageWave(ksVMCatColorPref, text=1)
	
	Variable fresh = cmpstr(StringByKey(ksStatusKey, note(catColors)), "fresh") == 0
	
	if (fresh)
		Redimension /N=(kVM_numCatPrefs) catColors
		Note /K catColors, ReplaceStringByKey(ksStatusKey, note(catColors), "established")
		loadColorCatPreferences()
	endif
	return catColors
End

static Function /S getPeakLevel([value])
	Variable value
	
	if (ParamIsDefault(value))
		return VoltHelpers#getNVarByName(ksVM_peakLevel)
	else
		return VoltHelpers#getNVarByName(ksVM_peakLevel, value=value)
	endif
End

static Function /S getArtifactSize([value])
	Variable value
	
	if (ParamIsDefault(value))
		return VoltHelpers#getNVarByName(ksVM_artifactSize)
	else
		return VoltHelpers#getNVarByName(ksVM_artifactSize, value=value)
	endif
End

// ****************     H E L P E R     F U N C T I O N S     ********************


Function /S VM_getControlTraceList()
	return getNamedWaveList(ksTransientControlName)
End

Function /S VM_getStimLocTraceList()
	return getNamedWaveList(ksStimLocName)
End

Function /WAVE VM_getMasterControlWave()
	return getNamedPackageWave(ksMasterControlName)
End

Function /WAVE VM_getMasterStimLocWave()
	return getNamedPackageWave(ksMasterStimLocName)
End

Function /WAVE VM_getMasterClassWave()
	return getNamedPackageWave(ksMasterClassName, text=1)
End

Function VM_resetAnalysisPrefs()
	STRUCT VMGUI_analysisFunction prefs
	saveAnalysisPrefs(prefs, reset=1)
	defaultAnalysisStruct(prefs)
	saveAnalysisPrefs(prefs)
	return 0
End

Function VM_resetFitPreferences()
	STRUCT VM_fitPreferences fit
	saveFitPrefs(fit, reset=1)
	defaultFitPrefs(fit)
	saveFitPrefs(fit)
	return 0
End

Function /WAVE VM_getSimpleFitValueWave([labelList])
	String labelList
	
	if (ParamIsDefault(labelList) || strlen(labelList) == 0)
//		Wave /T epochCats = VM_getEpochCatWave()
		labelList = makeLblListFromStatus(VM_getEpochCatWave())
	endif
	Variable adjust = 0, numLabels = ItemsInList(labelList)
	DFREF cDF = GetDatafolderDFR()
	DFREF path = VoltHelpers#getPackage()
	Wave fitValues = path:$ksSimpleFitValues
	if (!WaveExists(fitValues))
//		Make /N=(numpnts(epochCats), 1) path:$ksSimpleFitValues /WAVE=fitValues
		Make /N=(numLabels, 1) path:$ksSimpleFitValues /WAVE=fitValues
		adjust = 1
	endif
	if (DimSize(fitValues, 0) != numLabels)
//	if (DimSize(fitValues, 0) != numpnts(epochCats))
//		Redimension /N=(numpnts(epochCats), -1) fitValues
		Redimension /N=(numLabels, -1) fitValues
		adjust = 1
	endif
	if (!adjust)
		String currentLabels = makeLblListFromWave(fitValues)
		if (sameItems(currentLabels, labelList))
			return fitValues
		endif
		adjust = 1
	endif
	
	Variable index = 0
	for (index = 0; index < numLabels; index += 1)
//	for (index = 0; index < numpnts(epochCats); index += 1)
//		SetDimLabel 0, index, $GetDimLabel(epochCats, 0, index), fitValues
		SetDimLabel 0, index, $StringFromList(index, labelList), fitValues
	endfor
	return fitValues
End

Function /WAVE VM_getSimpleMaxWave([list])
	String list
	if (ParamIsDefault(list))
		list = ""
	endif
	return getSimpleSummaryWave(ksSimpleStatsMax, list=list)
End

Function /WAVE VM_getSimpleAreaWave([list])
	String list
	if (ParamIsDefault(list))
		list = ""
	endif
	return getSimpleSummaryWave(ksSimpleStatsArea, list=list)
End

static Function /WAVE getSimpleSummaryWave(name, [list])
	String name, list
	
	Wave /T epochCats = VM_getEpochCatWave()
	Variable numRows, fromList = 0, adjust = 0
	if (ParamIsDefault(list) || strlen(list) == 0)
		numRows = numpnts(epochCats)
	else
		numRows = ItemsInList(list)
		adjust = 1
		fromList = 1
	endif
	
	DFREF cDF = GetDatafolderDFR()
	DFREF path = VoltHelpers#getPackage()
	Wave summary = path:$name
	if (!WaveExists(summary) || numpnts(summary) != numRows)
		Make /O/N=(numRows) path:$name /WAVE=summary
		summary = NaN
		adjust = 1
	endif
	if (!adjust)
		return summary
	endif
	
	Variable index = 0, done = 0
#if IgorVersion() >= 8.00
	if (!fromList)
		CopyDimLabels /ROWS=0 epochCats, summary
		done = 1
	endif
#endif
	if (!done)
		for (index = 0; index < numRows; index += 1)
			if (fromList)
				SetDimLabel 0, index, $StringFromList(index, list), summary
			else
				SetDimLabel 0, index, $GetDimLabel(epochCats, 0, index), summary
			endif
		endfor
	endif
	return summary
End	

Function /WAVE VM_getSimpleTaus(fitValues, [plot, colorCat])
	Wave fitValues
	Variable plot, colorCat
	
	String errormsg = VM_getErrorStr("VM_getSimpleTaus")
	
	Wave taus
	if (!WaveExists(fitValues))
		printf errormsg, "fit value wave does not exists"
		return taus 
	endif
	Variable rows = DimSize(fitValues, 0)
	Variable columns = DimSize(fitValues, 1)
	
	plot = ParamIsDefault(plot) || plot < 1 ? 0 : 1
	colorCat = ParamIsDefault(colorCat) || colorCat < 1 ? 0 : 1
	
	DFREF package = VoltHelpers#getPackage()
	Wave taus = package:$ksSimpleTaus
	if (!WaveExists(taus))
		Make /N=(rows) package:$ksSimpleTaus /WAVE=taus
	else
		Redimension /N=(rows) taus
	endif
	if (columns <= 0)
		Redimension /N=0 taus
		return taus
	endif
	
	taus = fitValues[p][%tau]
	SetScale d 0, 0, "s", taus
	
	Variable row
	for (row = 0; row < rows; row += 1)
		SetDimLabel 0, row, $(GetDimLabel(fitValues, 0, row)), taus
	endfor
	
	if (plot)
		String graphName = ""
		if (colorCat)
			Variable index = 0
			for (index = 0; index < numpnts(taus); index += 1)
				String lbl = GetDimLabel(taus, 0, index)
				if (cmpstr(lbl[strlen(lbl) - 4, strlen(lbl) - 1], "_dif") == 0)
					SetDimLabel 0, index, $(lbl[0, strlen(lbl) - 5]), taus
				endif
			endfor
			Wave /T categories = VM_getEpochCatWave()
			DFREF sourceFolder = GetWavesDatafolderDFR(taus)
			Wave colorRef = $(getColorValueWave())
			Wave /T prefColors = getCatColorPrefWave()
			Wave colorIndex
			if (WaveExists(prefColors))
				Duplicate /O taus, sourceFolder:$((NameOfWave(taus))[0,25] + "_cIdx") /WAVE=colorIndex
				colorIndex = 0
				
				colorIndex = FindDimLabel(colorRef, 0, prefColors[%$(categories[%$(GetDimLabel(taus, 0, p))])])
				colorIndex = colorIndex[p] < 0 ? 0 : colorIndex[p]
			else
				colorCat = 0
			endif
		endif
		if (colorCat)
			graphName = VM_simpleGraphPlot(taus, ksSimpleTausPlot, title="Simple Taus", colorCat=colorIndex)
		else
			graphName = VM_simpleGraphPlot(taus, ksSimpleTausPlot, title="Simple Taus")
		endif

		Label /W=$graphName left "\\Z14\\F'Symbol't \\F'Arial'\\Z12(\\U)"		
	endif
	
	return taus
End

Function /S VM_plotSimpleStats(theWave, type, [colorCat])
	Wave theWave
	String type
	Variable colorCat
	
	colorCat = ParamIsDefault(colorCat) || colorCat < 1 ? 0 : 1
	
	if (colorCat)
		Wave /T categories = VM_getEpochCatWave()
		DFREF sourceFolder = GetWavesDatafolderDFR(theWave)
		Wave colorRef = $(getColorValueWave())
		Wave /T prefColors = getCatColorPrefWave()
		Wave colorIndex
		if (WaveExists(prefColors))
			Duplicate /O theWave, sourceFolder:$((NameOfWave(theWave))[0,25] + "_cIdx") /WAVE=colorIndex
			colorIndex = 0
			Make /FREE /N=(numpnts(colorIndex)) /T colorTranslate
			
			Variable index
			for (index = 0; index < numpnts(colorTranslate); index += 1)
				if (FindDimLabel(prefColors, 0, categories[%$(GetDimLabel(theWave, 0, index))]) >= 0)
					colorTranslate[index] = prefColors[%$(categories[%$(GetDimLabel(theWave, 0, index))])]
				else
					colorTranslate[index] = "black"
				endif
			endfor
			
			colorIndex = FindDimLabel(colorRef, 0, colorTranslate[p])			
		else
			colorCat = 0
		endif
	endif
	
	String theName = ""
	strswitch (LowerStr(type))
		case "max":
		default:
			if (colorCat)
				theName = VM_simpleGraphPlot(theWave, ksSimpleMaxPlot, title="Simple Max", marker=16, colorCat=colorIndex)	
			else
				theName = VM_simpleGraphPlot(theWave, ksSimpleMaxPlot, title="Simple Max", marker=16)
			endif
			Label /W=$theName, left "amplitude (\\U)"
	endswitch
	return theName
End

// ****************     T H R E A D     F U N C T I O N S     ********************

ThreadSafe static Function calcMeanWorker(raw, summary, s, e, stp, etp)
	Wave raw, summary
	Variable stp, etp, s, e
	
	Make /N=(etp-stp) /O /FREE tmp
	do
		tmp = raw[s][p + stp]
		summary[s] = mean(tmp)
		s += 1
	while (s < e)
	return 0
End

ThreadSafe static Function calcMaxWorker(raw, summary, s, e)
	Wave raw, summary
	Variable s, e
	
	Make /N=(DimSize(raw, 1)) /O /FREE tmp
	do
		tmp = raw[s][p]
		summary[s] = WaveMax(tmp)
		s += 1
	while (s < e)
	return 0
End

// Analysis functions for voltametric current traces


// ***************************************************************
// *************               A N A L Y S I S      F U N C T I O N S  
// ***************************************************************


Function /WAVE VM_makeTransient([startTime, endTime, sweepDelta])
	Variable startTime, endTime, sweepDelta

	DFREF loadFolder = VoltHelpers#getDataLoadFolder()
	DFREF package = VoltHelpers#getPackage()
	Wave transient
	
	Wave /T channels = VoltHelpers#getChannelAssignment(show=0)
	Extract /FREE /INDX /O channels, ctrlLabelIndex, cmpstr(channels, "control") == 0
	Variable numControls = numpnts(ctrlLabelIndex)
	
	DFREF cDF = GetDatafolderDFR()
	Variable show, controlCounter
	
	String progressBar = VM_getProgressBarPanel()
	VM_updateProgressbar(0, info="creating transient", theName=progressBar)
	
	VM_makeSingleTransient(VM_getRawDataWave(), show=1)
	
	VM_updateProgressbar((1/(1+numControls)), theName=progressBar)
		
	if (numControls > 0)
		for (controlCounter = 0; controlCounter < numControls; controlCounter += 1)
			VM_updateProgressbar(((1+controlCounter)/(1+numControls)), info="creating control trace for channel " + GetDimLabel(channels, 0, ctrlLabelIndex[controlCounter]), theName=progressBar)	
			VM_makeControlTrace(VM_getRawControlWave(number=controlCounter),  show=1)
			VM_updateProgressbar(((1+controlCounter + 1)/(1+numControls)), theName=progressBar)	
		endfor
		VM_updateProgressbar(1, theName=progressBar)
	endif
	
	KillWindow $progressBar
	return transient
End

// dataWave 	Matrix, (2D wave) of raw data
// mStartP		start point to calculate the mean
// mEndP		end point to calculate the mean
// tStartP		start point in the transient to recalculate the mean, only used with the refine key
// tEndP		end point of the transient to recalculate the mean, only used with the refine key
// sweepDelta	time in seconds between two data points
// type			transient type, i. e. dopamine etc
// add			add to the transient new points
// show			show the transient in a graph
// refine		refine the transient (recalculate) either all or just a section of it

Function /WAVE VM_makeSingleTransient(dataWave, [mStartP, mEndP, tStartP, tEndP, add, show, refine])
	Wave dataWave
	Variable mStartP, mEndP, tStartP, tEndP, add, show, refine
	
	String errormsg = VM_getErrorStr("VM_makeSingleTransient")
	Variable numPoints = 40
	DFREF path = VoltHelpers#getPackage()
	Wave transient
	Wave transientColor
	
	if (!WaveExists(dataWave) || WaveDims(dataWave) < 2)
		printf errormsg, "raw data wave either does not exists or has not enough dismensions. Make sure a data channel is assigned!"
		return transient
	endif
		
	Wave /T channelAssignments = VoltHelpers#getChannelAssignment(show=0)
	
	STRUCT OxidationDetails od
	STRUCT RampValues rv
	STRUCT VoltammetryPreferences prefs

	VoltHelpers#getTransPrefs(prefs)
	
	if (VM_rampStructExists())
		VMRamps#getRampStruct(rv)
		od = rv.oxDetails
		prefs.type = rv.name
	else
		od = prefs.oxDetails
	endif
		
	Extract /FREE /INDX /O channelAssignments, dataLabelIndex, cmpstr(channelAssignments, "data") == 0
	String theChannel = GetDimLabel(channelAssignments, 0, dataLabelIndex[0])
	if (cmpstr(theChannel, NameOfWave(dataWave)) != 0)
		printf errormsg, "the supplied data wave '", NameOfWave(dataWave), "' is not assigned as data"
		return transient
	endif

	refine = ParamIsDefault(refine) || refine < 1 ? 0 : 1

	if (ParamIsDefault(tStartP) || tStartP < 0)
		tStartP = 0
	else
		tStartP = tStartP >= DimSize(dataWave, 0) ? DimSize(dataWave, 0) - 1 : round(tStartP)
	endif
	if (ParamIsDefault(tEndP) || tEndP < 0)
		tEndP = DimSize(dataWave, 0)
	else
		tEndP = tEndP < 0 ? 0 : round(tEndP)
	endif
	if (tStartP > tEndP)
		variable newStart = tEndP
		tEndP = tStartP
		tStartP = newStart
	endif

	DFREF cDF = GetDatafolderDFR()

	NVAR theStart = $(VoltHelpers#getTransientStartPoint())
	if (ParamIsDefault(mStartP) || mStartP < 0)
		mStartP = theStart
	else
		theStart = round(mStartP)
	endif
	od.startPoint = mStartP


	NVAR theEnd = $(VoltHelpers#getTransientEndPoint())
	if (ParamIsDefault(mEndP) || mEndP < 0)
		mEndP = theEnd
	else
		theEnd = round(mEndP)
	endif
	od.endPoint = mEndP
	
	Wave ramp = VM_getRampWave()

	if (od.startPoint == od.endPoint)
		// something went wrong here, so use default values
		od.voltage = str2num(StringFromList(0, StringByKey(prefs.type, ksVM_SubstanceOxidations), ",")) / 1000
		od.range = NumberByKey(prefs.type, ksVM_SubOxidationRanges) / 1000
		od.startVoltage = od.voltage - od.range/2
		od.endVoltage = od.startVoltage + od.range
		
		FindLevel /EDGE=1 /P/Q ramp, od.startVoltage
		if (V_flag)
			// is 1 when level was NOT found
			return transient
		endif
		od.startPoint = V_LevelX
		FindLevel /EDGE=1 /P/Q ramp, od.endVoltage
		if (V_flag)
			// is 1 when level was NOT found
			return transient
		endif
		od.endPoint = V_LevelX		
	endif
	
	od.startTime = pnt2x(ramp,od.startPoint)
	od.endTime = pnt2x(ramp, od.endPoint)
	od.startVoltage = ramp[od.startPoint]
	od.endVoltage = ramp[od.endPoint]	
	prefs.oxDetails = od
	VoltHelpers#setTransPrefs(prefs)

	Variable length = DimSize(dataWave, 0)
	if (!refine)
		if (ParamIsDefault(add) || add == 0)
			Make /N=(length) /O path:$ksTransientName /WAVE=transient
			transient = 0
			Make /N=(length) /O path:$ksTransientColorName /WAVE=transientColor
			transientColor = 0
			add = 0
		else
			add = 1
			Wave transient = path:$ksTransientName
			Wave transientColor = path:$ksTransientColorName
			if (!WaveExists(transient))
				Make /N=(length) /O path:$ksTransientName /WAVE=transient
				Make /N=(length) /O path:$ksTransientColorName /WAVE=transientColor
				add = 0
			else
				tStartP = numpnts(transient)
				tEndP = length
				Redimension /N=(length) transient
				if (!WaveExists(transientColor))
					Make /N=(numpnts(transient)) path:$ksTransientColorName /WAVE=transientColor
				else
					Redimension /N=(numpnts(transient)) transientColor
				endif
			endif
		endif
		SetScale /P x 0, DimDelta(dataWave, 0), WaveUnits(dataWave, 0), path:$ksTransientName
	else
		Wave transient = path:$ksTransientName
		Wave transientColor = path:$ksTransientColorName		
	endif
	Variable refNum, microseconds

	refNum = startMSTimer	
	
	Variable numItems = tEndP - tStartP
	Variable processors = ThreadProcessorCount
	if (processors > 1 && numItems >= 1000)
		Variable size = floor(numItems / processors)
		Variable rest = mod(numItems, processors)
		Variable thread = 0
		Variable tgID = ThreadGroupCreate(processors)
				
		do
			if (thread == processors - 1)
				ThreadStart tgID, thread, calcMeanWorker(dataWave, transient, (size * thread) + tStartP, ((size * (thread + 1)) + rest) + tStartP, od.startPoint, od.endPoint)
			else
				ThreadStart tgID, thread, calcMeanWorker(dataWave, transient, (size * thread) + tStartP, (size * (thread + 1)) + tStartP, od.startPoint, od.endPoint)
			endif
			thread += 1
		while (thread < processors)
				
		do
			Variable tgs = ThreadGroupWait(tgID, 10)
		while (tgs != 0)
		Variable dummy = ThreadGroupRelease(tgID)
	else
		calcMeanWorker(dataWave, transient, tStartP, tEndP, od.startPoint, od.endPoint)
	 endif

	microseconds = stopMSTimer(refNum)
	 
	String theNote = note(transient)
	
	// after work, some user feedback
	if (add)
		print " --> added", numItems, "traces to transient on " + date(), time(), ", needed", microSecondsToTimeStr(microseconds)
	elseif (refine)
		print " --> refined", numItems, "points of the transient from point", tStartP, ", averaging points", mStartP, "-", mEndP, "on", date(), time(), ", needing", microSecondsToTimeStr(microseconds)	
	else
		print " --> created new transient of", numItems, "traces on " + date(), time(), ", needed", microSecondsToTimeStr(microseconds)
	endif
	
	SetScale d 0,0, "A", transient
	theNote = ReplaceStringByKey(ksStatusKey, theNote, "new")
	Note /K transient, theNote
	
	if (ParamIsDefault(show) || show == 1)
		VM_makeTransientGraph(theTransient = transient)
	else
		// don't show, DoUpdate has no effect if the graph doesn't exists
		DoUpdate /W=$(ksVM_TransientGraphName)
	endif
	return transient
End // END VM_makeTransient

Function /WAVE VM_makeControlTrace(dataWave, [add, show])
	Wave dataWave
	Variable add, show
	
	Wave nullRef
	if (!WaveExists(dataWave) || WaveDims(dataWave) < 2)
		print "** ERROR (VM_makeControlTrace): raw data wave either does not exists or has not enough dimensions. STOPPED!"
		return nullRef
	endif
	DFREF cDF = GetDatafolderDFR()
	DFREF path = VoltHelpers#getPackage()
	Wave control
	
	if (ParamIsDefault(add) || add < 0)
		add = 0
	else
		add = add > 1 ? 1 : round(add)
	endif
	if (ParamIsDefault(show) || show < 0)
		show = 0
	else
		show = show > 1 ? 1 : round(show)
	endif
	
	String controlName = ksTransientControlName + "_" + NameOfWave(dataWave)
	Variable length = DimSize(dataWave, 0)

	Variable addStart = 0
	if (add)
		Wave control = path:$controlName
		if (WaveExists(control))
			addStart = numpnts(control)
			Redimension /N=(length) control
		else
			Make /N=(length) /O path:$controlName /WAVE=control		
		endif
	else
		Make /N=(length) /O path:$controlName /WAVE=control
	endif
	
	Variable refNum, microseconds
	refNum = startMSTimer	
	Variable numItems = length - addStart
	Variable processors = ThreadProcessorCount

	ImageThreshold /M=0/T=4 dataWave
	Wave M_ImageThresh
	ImageTransform sumAllRows M_ImageThresh
	Wave W_sumRows
	if (processors > 1)
		Multithread control = W_sumRows[p] > 0 ? 5 : 0
	else
		control = W_sumRows[p] > 0 ? 5 : 0
	endif
	SetScale /P x 0, DimDelta(dataWave, 0), WaveUnits(dataWave, 0), control
	KillWaves /Z W_sumRows, M_ImageThresh

	microseconds = stopMSTimer(refNum)
	

	String theNote = note(control)
	theNote = ReplaceStringByKey(ksVM_Description, theNote, "maximum of trigger traces")
	theNote = ReplaceStringBykey(ksStatusKey, theNote, "new")
	Note /K control, theNote

	Extract /INDX control, path:$(ksStimLocName + "_" + NameOfWave(dataWave)), control == 5
	Wave locs = path:$(ksStimLocName + "_" + NameOfWave(dataWave))
	theNote = note(locs)
	theNote = ReplaceStringByKey(ksVM_Description, theNote, "location of the 5 V triggers in the control trace")
	theNote = ReplaceStringByKey(ksStatusKey, theNote, "new")
	Note /K locs, theNote
	
	if (add)
		print " ---> added", numItems, "traces to control trace of channel " + NameOfWave(dataWave), "needing", microSecondsToTimeStr(microseconds)
	else
		print " ---> created control trace of channel " + NameOfWave(dataWave), "from", numItems, "traces, needed ", microSecondsToTimeStr(microseconds)
	endif
	
	Wave transient = getAnalysisTransient()
	SetScale /P x 0, deltax(transient), WaveUnits(transient, 0), control
	
	if (show)
		String theGraph = getTRGraph()
		if (strlen(theGraph) == 0)
			return control
		endif
		VM_addControlsToTransient(theGraph)
	endif
	
	return control
End // END VM_makecControlTrace


//! This function tries to locate the transient peak, using the peak find function
// choosen on the panel and stored in the VMPeakInfo structure.
// @param transient:		transient wave, defaults to default transient wave
// @param peakInfo:			VMPeakInfo structure containing the relevant values to find the 
//									the next peak in the transient trace
// @param startPoint:		give the start point for the search, defaults to 0
// @param backward:			a flag 0 (default) or 1 to indicate if to search backward
// @return result of the find peak function, the start point of the peak
//-
Function VM_findTransientPeak([transient, peakInfo, startPoint, backward])
	Wave transient
	Variable startPoint, backward
	STRUCT VMPeakInfo &peakInfo
	
	DFREF cDF = GetDatafolderDFR()
	DFREF path = VoltHelpers#getPackage()

	if (ParamIsDefault(transient))
		Wave transient = path:$ksTransientName
	endif
	if (!WaveExists(transient))
		print "** ERROR (VM_findTransientPeak): can't find the transient wave. STOPPED!"
		return -1
	endif

	backward = ParamIsDefault(backward) || backward < 1 ? 0 : 1

	if (ParamIsDefault(startPoint) || startPoint < 0)
		if (!backward)
			startPoint = 0
		else
			startPoint = numpnts(transient)
		endif
	else
		startPoint = round(startPoint)
	endif

	if (backward)
		if (startPoint <= 0)
			peakInfo.peakExists = 0
			return -1
		endif
	else
		if (startPoint >= numpnts(transient))
			peakInfo.peakExists = 0
			return -1
		endif
	endif
	
	return peakInfo.findPeak(peakInfo, transient=transient, start=startPoint, backward=backward)		
End

Function VMfTPeak_AL_MA_episodic(peakInfo, [transient, start, backward])
//Function VMfTPeak_AL_MA_continuous(peakInfo, [transient, start, backward])
	STRUCT VMPeakInfo &peakInfo
	Wave transient
	Variable start, backward
	
	String errormsg = VM_getErrorStr("VMfTPeak_AL_MA_episodic")
	
	DFREF cDF = GetDatafolderDFR()
	DFREF path = VoltHelpers#getPackage()

	start = ParamIsDefault(start) || start < 0 ? (backward ? numpnts(transient) : 0) : start

	if (ParamIsDefault(transient) || !WaveExists(transient))
		Wave transient = path:$ksTransientName
	endif
	if (!WaveExists(transient))
		printf errormsg, "can not find the transient wave. STOPPED!"
		return -1
	endif
	
	if (ParamIsDefault(start) || start < 0)
		if (!backward)
			start = 0
		else
			start = numpnts(transient)
		endif
	else
		start = round(start)
	endif
	
	Variable peakStart = -1
	
	String status = StringByKey(ksStatusKey, note(transient))
	Variable available = 1
	
	SetDatafolder path
	
	if (cmpstr(status, "current") == 0)
		Wave diff = $ksTransientDiffName
		Wave theSmooth = $ksTransientSmoothName
		Wave theSearch = $ksTransientSearchName
		if (!WaveExists(diff) || !WaveExists(theSmooth) || !WaveExists(theSearch))
			available = 0
		endif
	endif
	
	if (cmpstr(status, "") == 0 || cmpstr(status, "new") == 0 || !available)
		Note /K transient, ReplaceStringByKey(ksStatusKey, note(transient), "current")
		Differentiate /METH=2 transient /D=$ksTransientDiffName
		Wave diff = $ksTransientDiffName

		Duplicate /O transient, $ksTransientSmoothName /WAVE=theSmooth, $ksTransientSearchName /WAVE=theSearch
		Smooth /E=1 numpnts(transient) > 9000 ? 4500 : round(numpnts(transient) / 2), theSmooth
		theSearch -= theSmooth
	endif
	
	Variable level
	if (!peakInfo.hasControl)
		WaveStats /Q/C=1/W /R=[10, numpnts(theSearch) -11] theSearch
		Wave M_WaveStats
		level = (M_WaveStats[%max] - M_WaveStats[%avg]) / 10
	endif
	SetDatafolder cDF
	
	Variable theEnd = numpnts(transient)
	if (backward)
		theEnd = 0
	endif
	
	NVAR peakPoint = $(VoltHelpers#getPeakPointVar())

	if (peakInfo.hasControl && !peakInfo.ignoreControl)
		Variable stimLoc = peakInfo.findStim(peakInfo, start=start, backward=backward)
		if (stimLoc >= 0)
			peakInfo.stimPoint = stimLoc
			peakInfo.peakStartPoint = stimLoc
			peakInfo.episodeEndPoint = stimLoc + peakInfo.transientPoints
			
			WaveStats /Q/C=1/W/R=[peakInfo.stimPoint - peakInfo.baseline, peakInfo.episodeEndPoint] transient
			Wave M_WaveStats
			
			peakInfo.peakPoint = x2pnt(transient, M_WaveStats[%maxLoc])
			peakPoint = peakInfo.peakPoint
			peakInfo.peakExists = 1
		else
			peakInfo.peakExists = 0
			peakInfo.stimPoint = stimLoc
			peakInfo.peakStartPoint = peakInfo.stimPoint
			peakInfo.episodeEndPoint = stimLoc + peakInfo.transientPoints
			peakInfo.peakPoint = -1
			peakPoint = peakInfo.peakPoint
		endif
			
	else
		FindPeak /Q/M=(level) /R=[start, theEnd] /P theSearch
		if (!V_flag) 			// Peak was found
			peakInfo.peakPoint = round(V_PeakLoc)
			peakPoint = peakInfo.peakPoint
			FindLevel /Q/P /R=[peakInfo.peakPoint - 1, min(start, theEnd)] diff, 0
			if (!V_flag)			// a level crossing to zero was found, so set this to start of peak
				peakInfo.peakStartPoint = round(V_LevelX)
			else
				peakInfo.peakStartPoint = peakInfo.peakPoint
			endif
			peakInfo.episodeEndPoint = peakInfo.peakStartPoint + peakInfo.transientPoints
			peakInfo.class = ""
			peakInfo.peakExists = 1
		endif
	endif

	return peakInfo.peakStartPoint
End

//! The main worker function to use with a continous acquistion, i.e. when a control/trigger
// trace is missing. This function extracts the data for the actual transient.
// @param peakInfo:			VMPeakInfo structure to be filled
// @param transient:		transient wave (defaults to the displayed transient wave)
// @param start:				start point for the peak search
// @param backward:			a flag either 0 (default) or 1 to search backwards
// @returns a filled VMPeakInfo structure and as a single value the point of the stimulation
// 			which is the where the peak should start.
//-
Function VMfTPeak_AL_MA_continuous(peakInfo, [transient, start, backward])
	STRUCT VMPeakInfo &peakInfo
	Wave transient
	Variable start, backward
	
	String errormsg = VM_getErrorStr("VMfTPeak_AL_MA_continuous")
	
	DFREF cDF = GetDatafolderDFR()
	DFREF path = VoltHelpers#getPackage()

	backward = ParamIsDefault(backward) || backward < 1 ? 0 : 1
	
	if (ParamIsDefault(transient) || !WaveExists(transient))
		Wave transient = path:$ksTransientName
	endif
	if (!WaveExists(transient))
		printf errormsg, "can not find the transient wave. STOPPED!"
		return -1
	endif
	
	start = ParamIsDefault(start) || start < 0 ? (backward ? numpnts(transient) : 0) : start
		
	Variable peakStart = -1
	
	String status = StringByKey(ksStatusKey, note(transient))
	Variable available = 1
	
	NVAR artifact = $(getArtifactSize())
	Variable theEnd = numpnts(transient) - artifact
	NVAR level = $(getPeakLevel())
	
	if (cmpstr(status, "current") == 0)
		Wave diff = path:$ksTransientDiffName
		Wave theSmooth = path:$ksTransientSmoothName
		Wave theSearch = path:$ksTransientSearchName
		if (!WaveExists(diff) || !WaveExists(theSmooth) || !WaveExists(theSearch))
			available = 0
		endif
	endif
	
	SetDatafolder path 
	
	if (cmpstr(status, "") == 0 || cmpstr(status, "new") == 0 || !available)
		Note /K transient, ReplaceStringByKey(ksStatusKey, note(transient), "current")
		Differentiate /METH=2 transient /D=$ksTransientDiffName
		Wave diff = $ksTransientDiffName

		Variable smoothFactor = numpnts(transient) > 9000 ? 4500 : round(numpnts(transient) / 2)
		Duplicate /O transient, $ksTransientSmoothName /WAVE=theSmooth, $ksTransientSearchName /WAVE=theSearch
		Smooth /E=1 smoothFactor, theSmooth
		theSearch -= theSmooth
		Smooth /E=1 smoothFactor, diff
		
		FindLevel /Q/R=[0, numpnts(diff) / 2] /P/EDGE=1 diff, 0
		if (V_flag == 0)
			 artifact = V_LevelX
		endif
		
		theEnd = numpnts(transient) - artifact
		start = start < artifact ? artifact : start			// adjust search start for artifact
		WaveStats /Q/R=[artifact, theEnd] /C=1 /W /M=1 diff		// avoid potential artifacts
		Wave M_WaveStats
		level = 0.5 * M_WaveStats[%max]
	endif
	
	SetDatafolder cDF

	STRUCT VMFoundPeakVals vals
	vals = peakInfo.foundVals

 	FindPeak /Q/R=[start, theEnd] /P /M=(level) diff
	
	NVAR peakPoint = $(VoltHelpers#getPeakPointVar())	
	
	if (V_flag == 0)
		// peak was found
		vals.posPeakVal = V_PeakVal
		vals.posPeakPoint = round(V_PeakLoc)
		vals.posPeakLoc = pnt2x(diff, V_PeakLoc)
		vals.posPeakWidth = V_PeakWidth
		
		// now determine the actual peak location
		FindPeak /Q/R=[vals.posPeakPoint, theEnd] /M=(level) /P theSearch
		if (V_flag == 0)
			peakInfo.peakPoint = round(V_PeakLoc)
			peakPoint = peakInfo.peakPoint
		endif
		
		FindPeak /Q/R=[vals.posPeakPoint, theEnd] /M=(-level) /P /N diff
		if (V_flag == 0)
			// negative peak was found - we have an end
			vals.negPeakVal = V_PeakVal
			vals.negPeakPoint = round(V_PeakLoc)
			vals.negPeakLoc = pnt2x(diff, V_PeakLoc)
			vals.negPeakWidth = V_PeakWidth
			
			WaveStats /Q/R=[vals.posPeakPoint - vals.posPeakWidth, vals.posPeakPoint] /C=1/W/M=1 theSearch
			Wave M_WaveStats
			vals.peakStart = M_WaveStats[%minLoc]
			vals.peakStartPoint = x2pnt(theSearch, vals.peakStart)
			
			WaveStats /Q/R=[vals.negPeakPoint - vals.negPeakWidth, vals.negPeakPoint] /C=1/W/M=1 theSearch
			vals.peakEnd = M_WaveStats[%maxLoc]
			vals.peakEndPoint = x2pnt(theSearch, vals.peakEnd)
			
			peakInfo.peakStartPoint = vals.peakStartPoint
			peakInfo.episodeEndPoint = vals.peakEndPoint
			peakInfo.advance = peakInfo.episodeEndPoint - peakInfo.peakStartPoint
			peakInfo.class = ""
			peakInfo.peakExists = 1
			
			peakInfo.foundVals = vals
		else
			// no negative peak means no end found
			peakInfo.peakExists = 0
		endif
	else
		// no positive peak found -> nothing more to do
		peakInfo.peakExists = 0
	endif
	
	return peakInfo.peakStartPoint
End

Function VMfStim_AL_single(peakInfo, [start, backward])
	STRUCT VMPeakInfo &peakInfo
	Variable start, backward
	
	String errormsg = VM_getErrorStr("VMfStim_AL_single")
	
	if (ParamIsDefault(backward) || backward < 0)
		backward = 0
	else
		backward = backward > 1 ? 1 : round(backward)
	endif	
	
	DFREF path = VoltHelpers#getPackage()
	
	NVAR stimulus = $(VoltHelpers#getStimPoint())	
	Wave /T channels = VoltHelpers#getChannelAssignment(show=0)
	if (numpnts(channels) < 1)
		print "** ERROR (VM_findStimulusPoint): no channels are assigned, nothing more for me to do!"
		stimulus = NaN
		return NaN
	elseif (numpnts(channels) == 1)
		// we only have a data channel and no control channel
		peakInfo.stimPoint = -1
		stimulus = -1
		return peakInfo.stimPoint
	endif

	String channelName = ""
	
	// we have multiple channels, since we only allow one data channel, the rest must be 
	// control channels, so we assume that they are here to give us the precise location of the stimulus
	// and thus to determine latencies, we need to determine the stimulus. If there is no stimulus, 
	// this can not be a valid peak
	Extract /FREE /INDX /O channels, controlChannels, cmpstr(channels, "control") == 0
	if (numpnts(controlChannels) == 0)
		printf errormsg, "no control channels designated! Using determined peak start point."
		peakInfo.stimPoint = -1
		return peakInfo.stimPoint
	endif
	
	Wave /T classes = VoltHelpers#getChannelClasses()
	
	Wave masterControl = VM_getMasterControlWave()
	Wave masterStim = VM_getMasterStimLocWave()
	Wave /T masterClasses = VM_getMasterClassWave()

	String controlList = VM_getControlTraceList()
	String locList = VM_getStimLocTraceList()

	Wave control = path:$(StringFromList(0, controlList))
	String status = StringByKey(ksStatusKey, note(control))
	
	strswitch (status)
		case "current":
			// nothing more to do, since we are current, so just use it as is
			break
		case "new":
			masterControl = 0
			Redimension /N=0 masterStim
			Redimension /N=0 masterClasses
			Variable index
			for (index = 0; index < ItemsInList(controlList); index += 1)
				String theName = StringFromList(index, controlList)
				String theChannel = theName[strsearch(theName, "_", 0) + 1, Inf]
				Wave control = path:$(theName)
				Note /K control, ReplaceStringByKey(ksStatusKey, note(control), "current")
				Wave stimLoc = path:$(StringFromList(index, locList))
				Note /K control, ReplaceStringByKey(ksStatusKey, note(stimLoc), "current")
				if (numpnts(masterControl) < numpnts(control))
					Redimension /N=(numpnts(control)) masterControl
				endif
				if (numpnts(stimLoc) > 0)
					masterControl += control
					
					Concatenate /NP {stimLoc}, masterStim
					Variable newStart = numpnts(masterClasses)
					Redimension /N=(numpnts(masterStim)) masterClasses
					masterClasses[newStart,] = classes[%$theChannel]
				endif
			endfor
			
			Sort masterStim, masterStim, masterClasses
			Note /K masterControl, ReplaceStringByKey(ksStatusKey, note(masterControl), "current")
			Note /K masterStim, ReplaceStringByKey(ksStatusKey, note(masterStim), "current")
			Note /K masterClasses, ReplaceStringByKey(ksStatusKey, note(masterClasses), "current")
			break
	endswitch
	
	if (numpnts(masterStim) == 0)
		// we have no stimuli in the master trace, so the following analysis won't work
		print "** ERROR (VMfStim_AL_single): although there are designated control traces, no stimulus was found!\r              Please remove the control channel assignment and try again."
		return -1
	endif
	
	if (ParamIsDefault(start) || start < 0)
		if (backward)
			// we start from the last point
			peakInfo.stimPoint = masterStim[numpnts(masterStim) - 1]
			stimulus = peakInfo.stimPoint
			peakInfo.class = masterClasses[numpnts(masterClasses) - 1]
		else
			peakInfo.stimPoint = masterStim[0]
			stimulus = peakInfo.stimPoint
			peakInfo.class = masterClasses[0]
		endif
		// no need to go any further, just stop here
		return peakInfo.stimPoint
	endif
	
	if (start <= masterStim[0])
		peakInfo.stimPoint = masterStim[0]
		stimulus = peakInfo.stimPoint
		peakInfo.class = masterClasses[0]
		return peakInfo.stimPoint
	endif
	if (start > masterStim[numpnts(masterStim) - 1])
		peakInfo.stimPoint = -1
		stimulus = peakInfo.stimPoint
		peakInfo.class = ""
		return peakInfo.stimPoint
	endif
								
	FindLevel /Q/P masterStim, start
	if (V_flag)
		// no level found
		peakInfo.stimPoint = -1
		peakInfo.peakExists = 0
		stimulus = -1
		peakInfo.class = ""
		return peakInfo.stimPoint
	else
		// level was found
		if (backward)
			peakInfo.stimPoint = masterStim[floor(V_LevelX)]
			stimulus = peakInfo.stimPoint
			peakInfo.class = masterClasses[floor(V_LevelX)]
		else
			peakInfo.stimPoint = masterStim[ceil(V_levelX)]
			peakInfo.class = masterClasses[ceil(V_levelX)]
			stimulus = peakInfo.stimPoint
		endif
	endif
	return peakInfo.stimPoint
End




//Function VMfStim_AL_multiple(peakInfo, [start, backward])
//	STRUCT VMPeakInfo &peakInfo
//	Variable start, backward
//	
//	if (ParamIsDefault(backward) || backward < 0)
//		backward = 0
//	else
//		backward = backward > 1 ? 1 : round(backward)
//	endif	
//
//	return peakInfo.stimPoint
//End


// start is in points
//Function VMfindTPeak_AL_MA_STDv1(peakInfo, [transient, start, backward])
//	STRUCT VMPeakInfo &peakInfo
//	Wave transient
//	Variable start, backward
//			
//	DFREF cDF = GetDatafolderDFR()
//	DFREF path = VoltHelpers#getPackage()
//
//	if (ParamIsDefault(backward) || backward < 0)
//		backward = 0
//	elseif (backward > 1)
//		backward = 1
//	else
//		backward = round(backward)
//	endif
//	
//	if (ParamIsDefault(transient) || !WaveExists(transient))
//		Wave transient = path:$ksTransientName
//	endif
//	if (!WaveExists(transient))
//		print "** ERROR (VM_findTransientPeak): can't find the transient wave. STOPPED!"
//		return -1
//	endif
//	
//	if (ParamIsDefault(start) || start < 0)
//		if (!backward)
//			start = 0
//		else
//			start = numpnts(transient)
//		endif
//	else
//		start = round(start)
//	endif
//	
//	Variable peakStart = -1
//	
//	String status = StringByKey(ksStatusKey, note(transient))
//	Variable available = 1
//	
//	SetDatafolder path
//	
//	if (cmpstr(status, "current") == 0)
//		Wave diff = $ksTransientDiffName
//		Wave theSmooth = $ksTransientSmoothName
//		Wave theSearch = $ksTransientSearchName
//		if (!WaveExists(diff) || !WaveExists(theSmooth) || !WaveExists(theSearch))
//			available = 0
//		endif
//	endif
//	
//	if (cmpstr(status, "") == 0 || cmpstr(status, "new") == 0 || !available)
//		Note /K transient, ReplaceStringByKey(ksStatusKey, note(transient), "current")
//		Differentiate /METH=2 transient /D=$ksTransientDiffName
//		Wave diff = $ksTransientDiffName
//
//		Duplicate /O transient, $ksTransientSmoothName /WAVE=theSmooth, $ksTransientSearchName /WAVE=theSearch
//		Smooth /E=1 numpnts(transient) > 9000 ? 4500 : round(numpnts(transient) / 2), theSmooth
//		theSearch -= theSmooth
//	endif
//		
//	WaveStats /Q/C=1/W /R=[10, numpnts(theSearch) -11] theSearch
//	Wave M_WaveStats
//	Variable level = (M_WaveStats[%max] - M_WaveStats[%avg]) / 10
//	SetDatafolder cDF
//	
//	Variable theEnd = numpnts(transient)
//	if (backward)
//		theEnd = 0
//	endif
//	
//	NVAR peakPoint = $(VoltHelpers#getPeakPointVar())
//
//	Variable again = 0
//	if (peakInfo.advance <= 0)
//		do
//			FindPeak /Q/M=(level) /R=[start, theEnd] /P theSearch
//			if (!V_flag) 			// Peak was found
//				peakInfo.peakPoint = round(V_PeakLoc)
//				peakPoint = peakInfo.peakPoint
//				FindLevel /Q/P /R=[peakInfo.peakPoint - 1, min(start, theEnd)] diff, 0
//				if (!V_flag)			// a level crossing to zero was found, so set this to start of peak
//					peakInfo.peakStartPoint = round(V_LevelX)
//				else
//					peakInfo.peakStartPoint = peakInfo.peakPoint
//				endif
//				peakInfo.peakExists = 1
//			endif
//			
//			if (peakInfo.hasControl)
//				again = peakInfo.findStim(peakInfo) < 0		// no stimulus found, so no peak
//				if (again)
//					peakInfo.peakExists = 0				
//					if (backward)
//						start = peakInfo.peakStartPoint - peakInfo.baseline
//						again = start > theEnd
//					else
//						start = peakInfo.peakStartPoint + peakInfo.transientPoints
//						again = start < theEnd
//					endif
//					peakInfo.peakStartPoint = -1
//				endif
//				peakInfo.peakStartPoint = peakInfo.stimPoint
//			endif
//		while(again)
//	else
//		// we have a positive advance value, so start the search by that number of points from the given start
//		// for the peak
//		Variable peakValue = 0
//		if (backward)
//			peakInfo.peakStartPoint -= peakInfo.advance
//			start = peakInfo.peakStartPoint + peakInfo.transientPoints
//			if (start <= 0)
//				start = 0
//				peakInfo.peakExists = 0
//				return -1
//			endif
//			theEnd = peakInfo.peakStartPoint - peakInfo.baseline
//			if (theEnd <= 0)
//				peakInfo.peakExists = 0
//				return -1
//			endif
//		else
//			peakInfo.peakStartPoint += peakInfo.advance
//			start = peakInfo.peakStartPoint - peakInfo.baseline
//			if (start >= numpnts(theSearch))
//				peakInfo.peakExists = 0
//				return -1
//			endif
//			theEnd = peakInfo.peakStartPoint + peakInfo.transientPoints
//			if (theEnd >= numpnts(theSearch))
//				theEnd = numpnts(theSearch) - 1
//			endif
//		endif
//
//		if (peakInfo.hasControl)
//			again = 0
//			do
//				again = peakInfo.findStim(peakInfo) <= 0
//				if (again)
//					if (backward)
//						peakInfo.peakStartPoint -= peakInfo.advance
//						start = peakInfo.peakStartPoint + peakInfo.transientPoints
//						if (start <= 0)
//							start = 0
//							peakInfo.peakExists = 0
//							return -1
//						endif
//						theEnd = peakInfo.peakStartPoint - peakInfo.baseline
//						if (theEnd <= 0)
//							peakInfo.peakExists = 0
//							return -1
//						endif
//					else
//						peakInfo.peakStartPoint += peakInfo.advance
//						start = peakInfo.peakStartPoint - peakInfo.baseline
//						if (start >= numpnts(theSearch))
//							peakInfo.peakExists = 0
//							return -1
//						endif
//						theEnd = peakInfo.peakStartPoint + peakInfo.transientPoints
//						if (theEnd >= numpnts(theSearch))
//							theEnd = numpnts(theSearch) - 1
//						endif
//					endif
//				endif
//			while(again)
//			
//			if (peakInfo.stimPoint <= 0)
//				peakInfo.peakExists = 0
//			else
//				FindPeak /Q /R=[peakInfo.stimPoint, theEnd] /P theSearch
//				if (!V_flag)
//					peakInfo.peakPoint = round(V_PeakLoc)
//					peakPoint = peakInfo.peakPoint
//					peakInfo.peakStartPoint = peakInfo.stimPoint
//				else
//					Make /FREE /N=(abs(start-theEnd)) /O searchwave = theSearch[p + min(start, theEnd)]
//					peakValue = WaveMax(searchWave)
//					Extract /FREE /INDX /O searchWave, searchLocations, searchWave == peakValue
//					Extract /FREE /O searchLocations, searchStim, searchLocations[p] + min(start,theEnd) > peakInfo.stimPoint
//					peakInfo.peakPoint = searchStim[0] + min(start, theEnd)
//					peakPoint = peakInfo.peakPoint
//					peakInfo.peakStartPoint = peakInfo.stimPoint
//				endif
//				peakInfo.peakExists = 1
//			endif
//		else
//			// Variable peakValue = WaveMax(transient, pnt2x(transient, start), pnt2x(transient, theEnd))
//			FindPeak /Q /R=[start, theEnd] /P theSearch
//			if (!V_flag)
//				peakInfo.peakPoint = round(V_PeakLoc)
//				peakPoint = peakInfo.peakPoint
//				FindLevel /Q/P /R=[peakInfo.peakPoint - 1, min(start, theEnd)] diff, 0
//				if (!V_flag)			// a level crossing to zero was found, so set this to start of peak
//					peakInfo.peakStartPoint = round(V_LevelX)
//				else
//					peakInfo.peakStartPoint = peakInfo.peakPoint
//				endif
//			else
//				// findPeak didn't find a peak
//				Make /FREE /N=(abs(start-theEnd)) /O searchwave = theSearch[p + min(start, theEnd)]
//				peakValue = WaveMax(searchWave)
//				Extract /FREE /INDX /O searchWave, searchLocations, searchWave == peakValue
//				peakInfo.peakPoint = searchLocations[0] + min(start, theEnd)
//				peakInfo.peakStartPoint = peakInfo.peakPoint
//				peakPoint = peakInfo.peakPoint
//				peakInfo.stimPoint = -1
//			endif
//			peakInfo.peakExists = 1
//		endif
//	endif	
//	
//	return peakInfo.peakStartPoint
//End // END VM_findTransientPeak


//Function VMfindStim_AL_STDv1(peakInfo, [start, backward])
//	STRUCT VMPeakInfo &peakInfo
//	Variable start, backward
//	
//	Variable searchStart = peakInfo.peakStartPoint - peakInfo.baseline
//	Variable searchEnd = peakInfo.peakStartPoint + peakInfo.transientPoints
//			
//	NVAR stimulus = $(VoltHelpers#getStimPoint())	
//	Wave /T channels = VoltHelpers#getChannelAssignment(show=0)
//	if (numpnts(channels) < 1)
//		print "** ERROR (VM_findStimulusPoint): no channels are assigned, nothing more for me to do!"
//		stimulus = NaN
//		return NaN
//	elseif (numpnts(channels) == 1)
//		// we only have a data channel and no control channel, so just accept the peak start point as stimulus point
//		peakInfo.stimPoint = -1
//		stimulus = -1
//		return peakInfo.peakStartPoint
//	endif
//
//	String channelName = ""
//	peakInfo.stimPoint = peakInfo.peakStartPoint
//	
//	// we have multiple channels, since we only allow one data channel, the rest must be 
//	// control channels, so we assume that they are here to give us the precise location of the stimulus
//	// and thus to determine latencies, we need to determine the stimulus. If there is no stimulus, 
//	// this can not be a valid peak
//	
//	Extract /FREE /INDX /O channels, controlChannels, cmpstr(channels, "control") == 0
//	if (numpnts(controlChannels) == 0)
//		print "** WARNING (VM_findStimulusPointAL): no control channels designated! Using determined peak start point."
//		peakInfo.stimPoint = -1
//		return peakInfo.peakStartPoint
//	elseif (numpnts(controlChannels) == 1)
//		// what to do if only one control channel?
//	elseif (numpnts(controlChannels) == 2)
//		Variable two = numpnts(controlChannels) == 2
//		Variable counter = 0
//		do
//			channelName = GetDimLabel(channels, 0, controlChannels[0])
//			Wave controlMatrix1 = VoltHelpers#getChannelTraces(searchStart, searchEnd, peakInfo.baseline, channelName)
//			if (two)
//				channelName = GetDimLabel(channels, 0, controlChannels[1])
//				Wave controlMatrix2 = VoltHelpers#getChannelTraces(searchStart, searchEnd, peakInfo.baseline, channelName)
//			endif
//						
//			Make /FREE /N=(DimSize(controlMatrix1, 1)) control1
//			if (two)
//				Make /FREE /N=(DimSize(controlMatrix2, 1)) control2
//			endif
//						
//			control1 = controlMatrix1[peakInfo.baseline][p]
//			if (two)
//				control2 = controlMatrix2[peakInfo.baseline][p]
//			endif
//			Variable stimLevel = WaveMax(control1)
//			if (two)
//				stimLevel += WaveMax(control2)
//			endif
//			stimLevel = round(stimLevel)
//			if (stimLevel < 5)
//				Variable index = 0
//				do
//					control1 = controlMatrix1[index][p]
//					if (two)
//						control2 = controlMatrix2[index][p]
//					endif
//					stimLevel = WaveMax(control1)
//					if (two)
//						stimLevel += WaveMax(control2)
//					endif
//					stimLevel = round(stimLevel)
//					if (stimLevel >= 5)
//						peakInfo.stimPoint = searchStart + index
//					else
//						peakInfo.stimPoint = -1
//						stimulus = -1
//					endif
//					index += 1
//				while (stimLevel < 5 && index <= DimSize(controlMatrix1, 0))
//				searchStart -= peakInfo.maxPointLimit
//				searchEnd += peakInfo.maxPointLimit
//				counter += 1
//			else
//				peakInfo.stimPoint = peakInfo.peakStartPoint
//			endif
//		while(stimLevel < 5 && counter < 2)
//		stimulus = peakInfo.stimPoint
//	else
//		print "** ERROR (VMfindStim_AL_standard): only 2 control channels are allowed, but found more! STOPPED!"
//		stimulus = -1
//		return stimulus
//	endif
//	
//	SVAR currentClass = $(VoltHelpers#getCurrentEpochClass())
//	
//	Wave /T classes = VoltHelpers#getChannelClasses()
//	if (!two)
//		currentClass = classes[%$(GetDimLabel(channels, 0, controlChannels[0]))]
//	else
//		if (round(WaveMax(control1)) >= 5)
//			currentClass = classes[%$(GetDimLabel(channels, 0, controlChannels[0]))]
//		elseif (round(WaveMax(control2)) >= 5)
//			currentClass = classes[%$(GetDimLabel(channels, 0, controlChannels[1]))]
//		else
//			currentClass = ""
//		endif
//	endif
//	peakInfo.class = currentClass
//	
//	return peakInfo.stimPoint
//End

Function VM_findPeakPrototypeFunc(peakInfo, [transient, start, backward])
	STRUCT VMPeakInfo &peakInfo
	Wave transient
	Variable start, backward
	
	Variable peakStartPoint = NaN 
	return peakStartPoint
End

Function VM_findStimPrototypeFunc(peakInfo, [start, backward])
	STRUCT VMPeakInfo &peakInfo
	Variable start, backward
	
	// return NaN, if you can't determine the stimulus point
	Variable stimulusPoint = NaN
	return stimulusPoint
End

Function /S VM_differentiateTraces(theList, [show, type])
	String theList, type
	Variable show
	
	String resultList = ""
	if (ParamIsDefault(show) || show > 1)
		show = 1
	else
		show = show < 0 ? 0 : round(show)
	endif
	if (strlen(theList) == 0)
		return resultList
	endif
	if (ParamIsDefault(type) || strlen(type) == 0)
		type = "min"
	else
		strswitch ( type )
			case "max":
				type = "max"
				break
			case "min":
			default:
				type = "min"
		endswitch
	endif
	
	
	DFREF cDF = GetDatafolderDFR()
	DFREF analysis = getAnalysisFolder()
	DFREF package = VoltHelpers#getPackage()
	String graphName = ""
	
	Variable index = 0, items = ItemsInList(theList)
	
	if (show)
		Display
		graphName = S_name
	endif
	
	Wave startP = SiSt_getSimpleFitStartPoints(name=ksSimpleFitStartP)
	if (numpnts(startP) != items)
		Redimension /N=(items) startP
	endif
	
	for (index = 0; index < items; index += 1)
		String theName = StringFromList(index, theList)
		String destName = ParseFilePath(0, theName, ":", 1, 0) + "_dif"
		Wave data = $theName
		Differentiate data /D=analysis:$destName
		if (show)
			AppendToGraph /W=$graphName analysis:$destName
		endif
		resultList = AddListItem(GetDatafolder(1, analysis) + destName, resultList, ";", Inf)
		
		WaveStats /Q /M=1 /C=1 /W analysis:$destName
		Wave M_WaveStats
		
		strswitch ( type )
			case "max":
				startP[index] = x2pnt(data, M_WaveStats[%maxLoc]) + 1
				break
			case "min":
			default:
				startP[index] = x2pnt(data, M_WaveStats[%minLoc]) + 1
		endswitch
		SetDimLabel 0, index, $destName, startP
	endfor
	
	return resultList
End


Function VM_calcBaselineDriftProfile()
	
	Variable baselineEpisode
	Prompt baselineEpisode, "Enter start baseline episode"
	Variable drugEpisode
	Prompt drugEpisode, "Enter start drug episode"
	Variable defEpisodeAvg = 3
	Prompt defEpisodeAvg, "Enter number of episodes to average"
	
	DoPrompt "Drift Calc Parameters", baselineEpisode, drugEpisode, defEpisodeAvg
	if (V_flag == 1)
		// user clicked cancel
		return 0
	endif
	
	defEpisodeAvg = defEpisodeAvg < 1 ? 1 : round(defEpisodeAvg)
	
	VM_getBaselineChangeProfile(baselineEpisode, drugEpisode, num=defEpisodeAvg)
	return 1
End
	

//! Calculates the profile of the baseline value change 
// @param baseEpisode:			number of the episode for the basis
// @param drugEpisode:			number of the episode for the drug application
// @return the name of the wave containing the drift profile
//-
Function /S VM_getBaselineChangeProfile(baseEpisode, drugEpisode, [num])
	Variable baseEpisode, drugEpisode, num
	
	if (ParamIsDefault(num) || num < 1)
		num = 3
	else
		num = round(num)
	endif
	
	String driftProfileName = "Drift_Profile"
	
	Variable numComparisons = 2
	DFREF folder = VoltHelpers#getEpochFolder()
	
	String namePattern = "ep_%d_traces"
	String baseName = "", drugName = "", name = ""
	Variable index, episodes, eps, traces
	
	Make /N=(numComparisons)/O/WAVE/FREE baseAverages
	for (episodes = 0; episodes < numComparisons; episodes += 1)
		switch (episodes)
			case 0:
				eps = baseEpisode
				Make /N=0/O/FREE Drift_avgBaseline
				SetScale d 0,0,"A", Drift_avgBaseline
				baseAverages[0] = Drift_avgBaseline
				break
			case 1:
				eps = drugEpisode
				Make /N=0/O/FREE Drift_avgDrugline
				SetScale d 0,0,"A", Drift_avgDrugline
				baseAverages[1] = Drift_avgDrugline
				break
		endswitch
		
		Wave collector = baseAverages[episodes]
		for (index = eps; index < (eps + num); index += 1)
			sprintf name, namePattern, index
			Wave data = folder:$name
			
			if (!WaveExists(data))
				continue
			endif
			
			Variable length = DimSize(data, 1)
			Make /N=(length)/O/FREE average = 0
			Variable numBaseline = NumberByKey("BASELINE", note(data))
			for (traces = 0; traces < numBaseline; traces += 1)
				average += data[traces][p]
			endfor
			average /= numBaseline
			
			if (index == eps)
				Redimension /N=(length) collector
				collector = 0				
			endif
			collector += average
		endfor
		collector /= num
	endfor
	
	Make /N=(length)/O $driftProfileName /WAVE=driftProfile
	SetScale d 0, 0, WaveUnits(collector, 1), driftProfile
	Wave baseLine = baseAverages[0]
	driftProfile = collector - baseLine
	
	VM_makeCVPlot(driftProfile, theName="Drift_Profile_Graph")
	
	return driftProfileName
End

//! Calculates the calibration factor (Molarity per Ampere) for the carbon fiber from
// the entered graph. It defaults to take the traces from the top graph and uses 1 micro molar
// as the dose concentration.
// @param dose:			applied dose in molar for the traces (defaults to 1 micro molar; 1e-6)
// @param numDigits:	determines precision of the returned factor (defaults to 2 decimal points)
// @param theGraph:		name of the graph with the CV traces (defaults to the top graph)
// @return the calibration factor in Molarity per Ampere
// -
Function VM_calcCalibrationFactor(list, [dose, numDigits])
	String list
	Variable dose, numDigits
	
	String errormsg = VM_getErrorStr("VM_calcCalibrationFactor")
	
	dose = ParamIsDefault(dose) || dose <= 0 ? 1e-6 : dose
	numDigits = ParamIsDefault(numDigits) || numDigits < 0 ? 2 : round(numDigits)
	
	if (strlen(list) == 0 || ItemsInList(list) == 0)
		print errormsg, "the list of waves is empty, nothing more for me to do"
		return 0
	endif
	
	Variable numTraces = ItemsInList(list)
	
	Wave ramp = VMRamps#getCurrentRamp()
	String rampInfo = note(ramp)
	Variable startx = pnt2x(ramp, NumberByKey(ksVM_RampStartPKey, rampInfo))
	Variable endx = pnt2x(ramp, NumberByKey(ksVM_RampEndPKey, rampInfo))
	
	Make /N=(numTraces) /FREE maxes
	
	maxes = WaveMax($(StringFromList(p, list)), startx, endx)
	
	Variable maxMeans = mean(maxes)
	Variable factor = dose / maxMeans
	
	String converter = ""
	sprintf converter, "%.*f\r", numDigits, factor
	
	return str2num(converter)
End



// *********************************************************
// ***********                       G R A P H S 
// *********************************************************

Function /S VM_makeTransientGraph([theTransient, panel])
	Wave theTransient
	Variable panel
	
	String errormsg = VM_getErrorStr("VM_makeTransientGraph")
	
	DFREF cDF = GetDatafolderDFR()
	DFREF path = VoltHelpers#getPackage()
	
	if (ParamIsDefault(theTransient))
		Wave theTransient = path:$ksTransientName
		if (!WaveExists(theTransient))
			printf errormsg, "no transient wave found"
			return ""
		endif
	endif
	
	panel = ParamIsDefault(panel) || panel >= 1 ? 1 : 0
	
	DoWindow /F $ksVM_TransientGraphName
		
	if (!V_flag)
		Display /K=1/N=$ksVM_TransientGraphName theTransient as ksVM_TransientGraphName
		SetAxis /A/N=1 left
		SetAxis /A/N=0 bottom
		ModifyGraph standoff=0,  gfont=$ksVM_defGraphFont, gfsize=kVM_defFSize
		SetWindow $ksVM_TransientGraphName hook(adjust)=WH_adjustTransientDataFromCsr
		if (panel)
			VM_makeGraphCntrlPanel(ksVM_TransientGraphName)
		endif
		resizeWindowToScreen(ksVM_TransientGraphName, screenFraction=0.75, ratio=3)
		moveWindowOnScreen(ksVM_TransientGraphName)
		
		Wave colorWave = getTransientColorWave()
		if (WaveExists(colorWave))
			SetDatafolder path
			Wave colorIndex = $(getColorValueWave())
			ModifyGraph zColor($(NameOfWave(theTransient)))={colorWave, *, *, cindexRGB, 0, colorIndex}
			SetDatafolder cDF
		endif
	endif
	
	return ksVM_TransientGraphName
End

Function /S VM_addControlsToTransient(theGraph)
	String theGraph
	
	if (strlen(theGraph) == 0)
		return ""
	else
		DoWindow $theGraph
		if (V_flag == 0)
			// window does not exist
			return ""
		endif
	endif
	
	DFREF cDF = GetDatafolderDFR()
	DFREF path = VoltHelpers#getPackage()
	
	String theList = VM_getControlTraceList()
	
	String traceList = TraceNameList(theGraph, ";", 0)
	Variable index
	for (index = 0; index < ItemsInList(theList); index += 1)
		if (WhichListItem(StringFromList(index, theList), traceList) >= 0)
			// trace is already on the list, so remove from list
			theList = RemoveListItem(index, theList)
		endif
	endfor
	if (ItemsInList(theList) == 0)
		return theGraph
	endif
	
	Wave /T classes = VoltHelpers#getChannelClasses(show=0)
	ModifyGraph /W=$theGraph axisEnab(left)={0.10,1}
	
	for (index = 0; index < ItemsInList(theList); index += 1)
		String theTrace = StringFromList(index, theList)
		strswitch (classes[%$(theTrace[strsearch(theTrace, "_", 0) + 1, Inf])])
			case "laser":
			case "light":
				AppendToGraph /W=$theGraph /L=controls /C=(0,0,65535) path:$(theTrace)
				break
			case "electrical":
			default:
				AppendToGraph /W=$theGraph /L=controls /C=(0,0,0) path:$(theTrace)			
		endswitch
	endfor
	ModifyGraph /W=$theGraph axisEnab(controls)={0, 0.08}, standoff=0
	ModifyGraph /W=$theGraph freePos(controls)={0,bottom},nticks(controls)=2
	SetAxis /N=1/E=1 controls 0,5
	
	return theGraph
End

Function /S VM_simpleGraphPlot(theWave, theName, [title, add, marker, colorCat])
	Wave theWave, colorCat
	String theName, title
	Variable add, marker
	
	String errormsg = VM_getErrorStr("VM_simpleGraphPlot")
	Variable makeColor = 0
	
	if (!WaveExists(theWave))
		printf errormsg,  "can't display a wave that doesn't exists!"
		return ""
	endif
	if (ParamIsDefault(title) || strlen(title) == 0)
		title = "Simple Stats Plot"
	endif
	if (ParamIsDefault(marker) || marker <= 0)
		marker = 19
	endif
	if (ParamIsDefault(add) || add <= 0)
		add = 0
	else
		add = add > 1 ? 1 : round(add)
	endif
	if (!ParamIsDefault(colorCat) && WaveExists(colorCat) && numpnts(colorCat) == numpnts(theWave))
		Wave colorRefWave = $(getColorValueWave())
		makeColor = 1
	endif
	
	
	Variable update = 0
	DoWindow /F $theName
	if (V_flag)
		String traces = TraceNameList(theName, ";", 1)
		Variable item = WhichListItem(NameOfWave(theWave), traces)
		if (item >= 0)
			Wave otherWave = TraceNameToWaveRef(theName, StringFromList(item, traces))
			if (!WaveRefsEqual(theWave, otherWave))
				if (!add)
					DoWindow /K $theName
					Display /N=$theName as title
					theName = S_name
				endif
				update = 1
			endif				
		else
			if (!add)
				DoWindow /K $theName
				Display /N=$theName as title
				theName = S_name
			endif
			update = 1
		endif
	else
		Display /N=$theName as title
		theName = S_name
		update = 1
	endif
	
	if (update)
		AppendToGraph /W=$theName theWave
		ModifyGraph /W=$theName rgb($NameOfWave(theWave))=(0,0,0), standoff=0, mode=4, marker=marker
		ModifyGraph /W=$theName gfont="Arial", gfSize=12
		SetAxis/A/N=1/E=1 left
		SetAxis/A/N=1/E=1 bottom
		if (makeColor)
			ModifyGraph /W=$theName zColor($NameOfWave(theWave))={colorCat, *, *, cindexRGB, 0, colorRefWave}
		endif
	endif
	
	return theName
End

//! Calculates the calibration factor from the CV traces on the top graph. The peak values will be highlighted
// in the graph and the calibration factor added as an annotation.
// @param theGraph:		name of the graph containing the CV traces (defaults to top)
// @param dose:			concentration of the calibration dose (defaults to 1 micro Molar)
// @param verbose:		flag to provide user feedback or not (default to provide feedback in the history window)
// @returns the calibration factor in nM / nA
Function VM_calcCalibFactorFromGraph([theGraph, dose, verbose])
	String theGraph
	Variable dose, verbose
	
	dose = ParamIsDefault(dose) || dose <= 0 ? 1e-6 : dose
	verbose = ParamIsDefault(verbose) || verbose < 1 ? 1 : 0
	
	if (ParamIsDefault(theGraph))
		theGraph = StringFromList(0, WinList("*", ";", "WIN:1"))
		if (strlen(theGraph) == 0)
			return 0
		endif
	else
		DoWindow $theGraph
		if (V_flag == 0)
			return 0
		endif
	endif
	
	String list = TraceNameList(theGraph, ";", 1)
	Variable numTraces = ItemsInList(list)
	if (numTraces == 0)
		return 0
	endif
	
	Variable add = 1
	String peakPost = "_P_peaks", peakXPost = "_X_peaks"
	String results = ""
	
	Variable index
	for (index = 0; index < numTraces; index += 1)
		String trace = StringFromList(index, list)
		if (cmpstr(trace, theGraph + peakPost) == 0 || cmpstr(trace, theGraph + peakXPost) == 0)
			// exclude the peak display traces in case we recalculate from the same graph
			add = 0
			continue
		endif
	
		results = AddListItem(GetWavesDatafolder(TraceNameToWaveRef(theGraph, trace), 2), results, ";", index)
	endfor
	
	Variable factor = VM_calcCalibrationFactor(results, dose = dose)
	
	numTraces = ItemsInList(results)
	
	DFREF package = VoltHelpers#getPackage()
	String peaksName = theGraph + peakPost
	String peakLocName = theGraph + peakXPost
	
	Make /N=(numTraces) /O package:$peaksName /WAVE=peaks, package:$peakLocName /WAVE=peaksX
	
	Wave ramp = VMRamps#getCurrentRamp()
	String rampInfo = note(ramp)
	Variable startx = pnt2x(ramp, NumberByKey(ksVM_RampStartPKey, rampInfo))
	Variable endx = pnt2x(ramp, NumberByKey(ksVM_RampEndPKey, rampInfo))
	
	for (index = 0; index < numpnts(peaks); index += 1)
		WaveStats /W/C=1/M=1/Q/R=(startx, endx) $(StringFromList(index, results))
		Wave M_WaveStats
		peaks[index] = M_WaveStats[%max]
		peaksX[index] = M_WaveStats[%maxLoc]
	endfor
	
	peaksX = ramp[x2pnt(ramp, peaksX[p])]
	
	GetAxis /W=$theGraph /Q current
	Variable currentInUse = V_flag == 0
	GetAxis /W=$theGraph /Q voltage
	Variable voltageInUse = V_flag == 0	
	
	if (add)
		if (currentInUse && voltageInUse)
			AppendToGraph /W=$theGraph /L=current /B=voltage peaks vs peaksX
			TextBox /W=$theGraph/C/N=calibFactor/F=2/A=RT/X=0.00/Y=0.00 "nM/na"
		else
			AppendToGraph peaks vs peaksX
			TextBox /W=$theGraph/C/N=calibFactor/F=2/A=RT/X=0.00/Y=0.00 "nM/nA"
		endif
		ModifyGraph /W=$theGraph mode($peaksName)=3, marker($peaksName)=0, rgb($peaksName)=(65535,0,0)
	endif
	
	TextBox /W=$theGraph/C/N=calibFactor num2str(factor) + " nM / nA"

	if (verbose)
		printf "Calculating calibration factor from %s on %s %s\r", ReplaceString(";", RemoveEnding(RemoveFromList((theGraph + peakPost), list)), ", "), time(), date()
		printf " -factor: %g nM per nA\r", factor 
	endif
	
	return factor
End
