#pragma rtGlobals=1					// Use modern global access method.
#pragma ModuleName = VMA_Calib		// Used to avoid name conflicts with other packages
#pragma version = 0.4
#include "mafPC2"
#include "mafITC"
#include "VM_Ramp"
#include "VM_mafPCBridge"
#include "ListHelpers"

// written by Roland Bock
// This file is modeled after the DemoDataAcq Example that ships with Igor. It should replace the 
// the general acquisition software for a specialized calibration task.

static StrConstant ksVM_mafPC_DopaminePattern = "PATTYPE:Interval;NUMROWS:4;SAMPINT:10;CONTROL:DA1,;SAVECHAN:AD1,;TIME0:0.32;AMP0_1:-400;ABS0_1:1;TIME1:4;AMP1_1:RAMP;ABS1_1:1;RAMPSTART1_1:-400;RAMPEND1_1:1200;TIME2:4;AMP2_1:RAMP;ABS2_1:1;RAMPSTART2_1:1200;RAMPEND2_1:-400;TIME3:91.68;AMP3_1:-400;ABS3_1:1;"
static StrConstant ksVM_mafPC_SerotoninPattern = "PATTYPE:Interval;NUMROWS:5;SAMPINT:10;CONTROL:DA1,;SAVECHAN:AD1,;TIME0:0.32;AMP0_1:200;ABS0_1:1;TIME1:0.8;AMP1_1:RAMP;ABS1_1:1;RAMPSTART1_1:200;RAMPEND1_1:1000;TIME2:1.1;AMP2_1:RAMP;ABS2_1:1;RAMPSTART2_1:1000;RAMPEND2_1:-100;TIME3:0.3;AMP3_1:RAMP;ABS3_1:1;RAMPSTART3_1:-100;RAMPEND3_1:200;TIME4:97.48;AMP4_1:200;ABS4_1:1;"
static StrConstant ksVM_mafPC_chunkPattern = "PATTYPE:Event;NUMROWS:2;SAMPINT:10;CONTROL:DA1,;SAVECHAN:AD1,;TIME0:0;EVENT0:TRAIN;PATTERN0:dopamine;REP0:600;DEPENDENCY:dopamine,;TIME1:59999;EVENT1:FINISH;"
static Strconstant ksChunkPat = "PlayChunk_1min"					//< pattern name in mafPC to play a long set

static StrConstant ksPackageName = "Calibration"								//< package folder name
static StrConstant ksPanelName = "VMCalibrationPanel"						//< panel name
static StrConstant ksPanelTitle = "Calibration Acquisition Panel"		//< panel title

static StrConstant ksVigorName = "template_current"
static StrConstant ksVigorPPath = "data"

static Strconstant ksCalibDataName = "CalibrationData"						//< default name of the data wave
static Strconstant ksCalibTransName = "CalibrationTransient"			//< default name of the transient wave
static Strconstant ksDataMask = "CalibDataMask"								//< default name of the mask to calculate the transient from data
static Strconstant ksTransientChunk = "CalibTransientChunk"
static Strconstant ksRunCounter = "runNumber"
static Strconstant ksCurrPattern = "currentPattern"
static Strconstant ksPrevBaseName = "previousBaseName"

static Strconstant ksHistoryGraph = "AcquiredTransientGraph"			//< default name of transient graph
static Strconstant ksHistoryGraphTitle = "Acquired Transient"			//< default title of transient graph

static Strconstant ksCalBaseName = "calib"							//< needed for mafPC

static Strconstant ksOut = "CONTROL"									//< mafPC pattern keyword for output channels
static Strconstant ksIn = "SAVECHAN"									//< mafPC pattern keyword for input channels

static Strconstant ksROff = "ROWOFFSET"
static Strconstant ksCTYPE = "SUBSTANCE"
//static Strconstant ksRStartP = "RANGESTART"
//static Strconstant ksREndP = "RANGEEND"

// *********************     S T A T I C    H E L P E R S       *********************************

//! This function returns the package datafolder and creates it if it does not exists.  
// @return: package datafolder reference
//-
static Function /DF getPackage()
	DFREF cDF = GetDatafolderDFR()
	SetDataFolder VoltHelpers#getPackage()
	NewDatafolder /O/S $ksPackageName
	DFREF path = GetDatafolderDFR()
	SetDatafolder cDF
	return path
End

//! This helper function creates a global variable or string in the package folder and returns
// the string path to it that can be used to reference it in a function
// @param name:	name of the global variable
// @param type:	type of the global variable (0 for variable, 1 for string)
// @param value:	initial numeric value of the global variable (only used with type 0)
// @param str:		initial string value of the global string (only used with type 1)
// @return: a string representation of the path to the created global variable/string
//-
static Function /S getGobal(name, type, [value, str])
	String name, str
	Variable type, value
	
	type = type < 1 ? 0 : 1
	
	if (strlen(name) == 0)
		return ""
	endif
	if (ParamIsDefault(str))
		str = ""
	endif
	
	value = ParamIsDefault(value) ? 0 : value
	
	DFREF dfr = getPackage()
	
	switch(type)
		case 0:
			NVAR var = dfr:$name
			if (!NVAR_Exists(var))
				Variable /G dfr:$name = value
			endif
			break
		case 1:
			SVAR strg = dfr:$name
			if (!SVAR_Exists(strg))
				String /G dfr:$name = str
			endif
			break
	endswitch
	
	return (GetDatafolder(1, dfr) + name)
End
	

static Function initPackageData()
	DFREF dfr = getPackage()
	Variable/G dfr:running = 0
	Variable/G dfr:paused = 0
	Variable/G dfr:$ksRunCounter = 0
	
	DFREF mafPc = root:maf:
	SVAR chunk = mafPC:$ksChunkPat
	if (!SVAR_Exists(chunk))
		String /G mafPC:$ksChunkPat = ksVM_mafPC_chunkPattern
		SVAR chunk = mafPC:$ksChunkPat
	endif
	SVAR dopamine = mafPC:dopamine
	if (!SVAR_Exists(dopamine))
		String /G mafPC:dopamine = ksVM_mafPC_DopaminePattern
		SVAR dopamine = mafPC:dopamine
	endif
	SVAR serotonin = mafPC:serotonin
	if (!SVAR_Exists(serotonin))
		String /G mafPC:serotonin = ksVM_mafPC_SerotoninPattern
	endif
	
	// now make sure we use the same in- and output channels for the chunk pattern
	chunk = ReplaceStringByKey(ksOut, chunk, StringByKey(ksOut, dopamine))
	chunk = ReplaceStringByKey(ksIn, chunk, StringByKey(ksIn, dopamine))
	
	// ... and make sure the patterns have been compiled.
	mafPC_Compile("dopamine", "")
	mafPC_Compile("serotonin", "")
	mafPC_Compile(ksChunkPat, "")
	analyzeRamp("dopamine")
	analyzeRamp("serotonin")
End

static Function needsInit()
	DFREF dfr = getPackage()

	NVAR running = dfr:running
	NVAR paused = dfr:paused
	
	SVAR chunkPat = root:maf:$ksChunkPat
	SVAR dopamine = root:maf:dopamine
	SVAR serotonin = root:maf:serotonin

	return (!NVAR_Exists(running) || !NVAR_Exists(paused) || !SVAR_Exists(chunkPat) || !SVAR_Exists(dopamine) || !SVAR_Exists(serotonin))
End

static Function /S getRunCounter()
	DFREF pack = getPackage()
	NVAR counter = pack:$ksRunCounter
	if (!NVAR_Exists(counter))
		Variable /G pack:$ksRunCounter = 0
	endif
	return GetDatafolder(1, pack) + ksRunCounter
End

static Function ControlPanelExists()
	DoWindow $ksPanelName
	return V_flag != 0
End

static Function SetStatusMessage(statusMessage)
	String statusMessage		// Message to display in control panel
	
	if (ControlPanelExists())
		SetVariable StatusMessage, win=$ksPanelName, value= _STR:statusMessage
	endif
End

static Function DisableSettings()		// Disables settings that can not be changed while acquisition is running
	if (ControlPanelExists())
		SetVariable SecondsBetweenRuns, win=$ksPanelName, disable=2
		SetVariable maxTime, win=$ksPanelName, disable=2
	endif
End

static Function EnableSettings()		// Enables settings disabled by DisableSettings
	if (ControlPanelExists())
		SetVariable SecondsBetweenRuns, win=$ksPanelName, disable=0
		SetVariable maxTime, win=$ksPanelName, disable=0
	endif
End

static Function GetSecondsBetweenRuns()
	if (!ControlPanelExists())
		return 3
	endif
	
	ControlInfo /W=$ksPanelName SecondsBetweenRuns
	return V_Value
End

static Function SetSecondsBetweenRuns(interval)
	Variable interval
	
	if (!ControlPanelExists())
		return 0
	endif
	interval = interval <= 0 ? 61 : interval
	
	SetVariable SecondsBetweenRuns, win=$ksPanelName, value=_NUM:interval
	return 1
End

static Function GetMaxRuns()
	if (!ControlPanelExists())
		return 5
	endif
	
	ControlInfo /W=$ksPanelName MaxRuns
	return V_Value
End

static Function SetMaxRuns(value)
	Variable value
	
	if (!ControlPanelExists())
		return 0
	endif
	
	value = value < 1 ? 1 : ceil(value)
	
	SetVariable MaxRuns, win=$ksPanelName, value=_NUM:value
	return 1
End

static Function GetRunNumber()
	if (!ControlPanelExists())
		return 0
	endif
	
	NVAR runCounter = $(getRunCounter())

//	ControlInfo /W=$ksPanelName RunNumber
//	return V_Value
	return runCounter
End

static Function SetRunNumber(runNumber)
	Variable runNumber
	
	if (!ControlPanelExists())
		return 0
	endif
	
	NVAR runCounter = $(getRunCounter())
	runCounter = runNumber
	
	return runNumber
End

static Function isTemplate()
	return (cmpstr(IgorInfo(1), ksVigorName) == 0 || strsearch(IgorInfo(1), "template_", 0) >= 0)
end

static Function /S getCurrentPattern()
	DFREF dfr = getPackage()
	SVAR pattern = dfr:$ksCurrPattern
	if (!SVAR_Exists(pattern))
		String /G dfr:$ksCurrPattern = ""
	endif
	return (GetDatafolder(1, dfr) + ksCurrPattern)
end

static Function setCurrentPattern(name)
	String name
	
	SVAR pattern = $(getCurrentPattern())
	pattern = name
End

// *******************                  P A N E L               *************************************

/// The panel creation function. 
static Function CreatePanel()
	// /K=3 means hide the window when the close box is clicked.
	// The panel must not be killed during acquisition.
	// Call DemoDataAcq#KillPackage() to kill everything and start fresh.

	DFREF dfr = getPackage()
	Variable interval = 61	
	Variable initialRunTime = 5 				// in min
	
	Variable maxRunValue = ceil((initialRunTime * 60) / interval)
	initialRunTime = (maxRunValue * interval) / 60
	
	NewPanel /N=$ksPanelName /W=(145,86,603,217) /K=1 as ksPanelTitle
	SetWindow $ksPanelName, hook(winPrefs)=WH_setPrefWinLocation

	SetVariable SecondsBetweenRuns,pos={15,15},size={200,20},title="Seconds between runs: "
	SetVariable SecondsBetweenRuns,help={"Approximate seconds between runs. Resolution is 1/60th of a second."}
	SetVariable SecondsBetweenRuns,fSize=12,limits={0,inf,0.1},value=_NUM:interval
	SetVariable SecondsBetweenRuns, proc= VMA_Calib#svFunc_changeBetweenRuns

	SetVariable maxTime, pos={230, 15}, size={180,20}, title="Maximum time (min): ", bodyWidth = 50
	SetVariable maxTime, help={"Enter the maximum time in minutes the calibration should run."}
	SetVariable maxTime, fSize=12, limits={0,15,0},value=_NUM:initialRunTime
	SetVariable maxTime, proc=VMA_Calib#svFunc_changeTime

	SetVariable MaxRuns, pos={230,40},size={180,19},title="Maximum runs: ", bodyWidth=50
	SetVariable MaxRuns, help={"Number of runs before acquisition automatically stops."}
	SetVariable MaxRuns, fSize = 12,limits={0,9000,0},value=_NUM:maxRunValue,noedit=1
	SetVariable MaxRuns, noedit = 1, frame = 0

	Button StartStopButton,pos={15,40},size={80,20},proc=VMA_Calib#StartStopProc,title="Start"
	Button StartStopButton, fColor=(0, 65535, 0), userdata(lousy)="0"

	ValDisplay RunNumber,pos={165,42},size={85,17},title="Run: ",fSize=12,bodyWidth=100
	ValDisplay RunNumber,limits={0,maxRunValue,0},barmisc={0,0}, highColor=(3,52428,1)
	ValDisplay RunNumber,value=#getRunCounter(), frame=0

	Button PauseResumeButton,pos={15,65},size={80,20},disable=1,proc=VMA_Calib#PauseResumeProc,title="Pause"
	Button PauseResumeButton, disable=1		// Hide button
		
	Button NewTemplate, pos={353, 93},size={80,20},title="New"
	Button NewTemplate, proc=VMA_Calib#btnFunc_newTemplate,disable=2
	
	PopupMenu Substance, pos={110,65},size={100,20},title="Substance"
	PopupMenu Substance, mode = 1, fSize = 12, popvalue="dopamine", userdata(lousy)="0"
	PopupMenu Substance, value=makeListFromWave("root:maf:mafPC_patterns")
	PopupMenu Substance, proc=VMA_Calib#puFunc_setRamp
	
	Checkbox minMode, pos={350,67}, size={20,20}, title="chunks "
	Checkbox minMode, mode=1, value=1, side=1, fSize=12
	Checkbox minMode, proc=VMA_Calib#chbx_changeAcqMode

	Checkbox lousyMode, pos={375,67}, size={20,20}, title="lousy"
	Checkbox lousyMode, mode=1, value=0, fSize=12
	Checkbox lousyMode, proc=VMA_Calib#chbx_changeAcqMode
	

	String availablePatterns = makeListFromWave("root:maf:mafPC_patterns")
	setCurrentPattern(StringFromList(0, availablePatterns))

	SetVariable StatusMessage,pos={21,93},size={280,19},title=" ",fSize=12,frame=0, noedit=1
	SetVariable StatusMessage,value= _STR:"Click Start to start calibration"
	
	STRUCT Rect size
	Variable locIndex = VoltaMafPCBridge#getWinPrefs(size, ksPanelName)
	if (locIndex > 0)
		MoveWindow /W=$ksPanelName size.left, size.top, size.right, size.bottom
	endif

End

// *********************           P A N E L    F U N C T I O N S           ************************

static Function chbx_changeAcqMode(cb) : CheckboxControl
	STRUCT WMCheckboxAction &cb
	
	switch (cb.eventCode)
		case 2:
			Variable genCheckVal = 0, newSeconds = 0
			
			strswitch (cb.ctrlName)
				case "minMode":
					genCheckVal = 1
					newSeconds = 61
					Button StartStopButton, win=$cb.win, userdata(lousy)="0"
					PopupMenu Substance, win=$cb.win, userdata(lousy)="0"
					break
				case "lousyMode":
					genCheckVal = 2
					newSeconds = 0.1
					Button StartStopButton, win=$cb.win, userdata(lousy)="1"
					PopupMenu Substance, win=$cb.win, userdata(lousy)="1"
					break
				endswitch

				Checkbox minMode, win=$cb.win, value= genCheckVal == 1
				Checkbox lousyMode, win=$cb.win, value= genCheckVal == 2

				SetVariable SecondsBetweenRuns, win=$cb.win, value=_NUM:newSeconds
				
				ControlInfo /W=$cb.win maxTime
				Variable maxRunValue = ceil((V_value * 60) / newSeconds)
				Variable newTime = (newSeconds * maxRunValue) / 60

				SetVariable MaxRuns, win=$cb.win, value=_NUM:maxRunValue
				ValDisplay RunNumber, win=$cb.win, limits={0,maxRunValue,0}
				SetVariable MaxTime, win=$cb.win, value=_NUM:newTime
				
			break
		case -1:
			break
	endswitch
	
End


static Function svFunc_changeTime(sv) : SetVariableControl
	STRUCT WMSetVariableAction &sv
	
	switch(sv.eventCode)
		case 2:
			DFREF dfr = getPackage()
			Variable secondsBetweenRuns = GetSecondsBetweenRuns()
			Variable maxRunValue = ceil((sv.dval * 60) / secondsBetweenRuns)
			Variable newTime = (secondsBetweenRuns * maxRunValue) / 60
			SetVariable MaxRuns, win=$sv.win, value=_NUM:maxRunValue
			ValDisplay RunNumber, win=$sv.win, limits={0,maxRunValue,0}
			SetVariable MaxTime, win=$sv.win, value=_NUM:newTime
			break
	endswitch

End

static Function svFunc_changeBetweenRuns(sv) : SetVariableControl
	STRUCT WMSetVariableAction &sv
	
	switch(sv.eventCode)
		case 1:
		case 2:
			ControlInfo /W=$sv.win maxTime
			Variable maxRunValue = ceil((V_value * 60) / sv.dval)
			Variable newValue = (sv.dval * maxRunValue) / 60
			SetVariable MaxRuns, win=$sv.win, value=_NUM:maxRunValue
			ValDisplay RunNumber, win=$sv.win, limits={0,maxRunValue,0}
			SetVariable MaxTime, win=$sv.win, value=_NUM:newValue		
			break
	endswitch	

End


static Function btnFunc_NewTemplate(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch (bn.eventCode)
		case 2:
		       if (isTemplate())
		       	// need to save as copy first
		       	DoIgorMenu "File", "Save Experiment As"
		       endif
		       String fileFilters = "Igor Experiments (*.pxp):.pxp;"
	       	Variable refNum
	       	String savePath = SpecialDirPath("Igor Pro User Files", 0, 0, 0) + ksVigorPPath + ":"
//	       	Execute /P "NEWEXPERIMENT"
//	       	Open /A /F=fileFilters refNum as savePath + ksVigorName
			break
	endswitch
End

static Function puFunc_setRamp(pu) : PopupMenuControl
	STRUCT WMPopupAction &pu
	
	switch (pu.eventCode)
		case 2:
			Variable lousy = str2num(GetUserData(pu.win, pu.ctrlName, "lousy")) == 1
			mafPC_Compile(pu.popstr, "")
			setCurrentPattern(pu.popstr)
			VMRamps#setCurrentRampName(pu.popstr)
			SVAR rampData = root:maf:$pu.popstr
			if (!SVAR_Exists(rampData))
				// if we end up here something is really wrong
				return 0
			endif
			Wave ramp = analyzeRamp(pu.popstr)
			Duplicate /O ramp, root:tmp /WAVE=tmp
			tmp *= 1e-3							// values are in mV, convert to V
			String theNote = note(tmp)
			Variable pnt = NumberByKey(ksVM_RampEndPKey, theNote)
			Redimension /N=(pnt + 1) tmp
			pnt = NumberByKey(ksVM_RampStartPKey, theNote)
			if (pnt > 0)
				DeletePoints 0, pnt, tmp
				Variable maxPnt = NumberByKey(ksVM_RampMaxPKey, theNote)
				theNote = ReplaceNumberByKey(ksVM_RampMaxPKey, theNote, maxPnt - pnt)
			endif
			theNote = ReplaceNumberByKey(ksVM_RampStartPKey, theNote, 0)
			theNote = ReplaceNumberByKey(ksVM_RampEndPKey, theNote, numpnts(tmp) - 1)
			Note /K tmp, theNote
			VMRamps#setCurrentRamp(tmp)
			KillWaves /Z tmp
			
			if (lousy)
				// needs to be implemented
			else
				// update the chunk pattern with the ramp name
				SVAR chunk = root:maf:$ksChunkPat
				if (!SVAR_Exists(chunk))
					String /G root:maf:$ksChunkPat = ksVM_mafPC_chunkPattern
					SVAR chunk = root:maf:$ksChunkPat
				endif
				
				chunk = ReplaceStringByKey(ksOut, chunk, StringByKey(ksOut, rampData))
				chunk = ReplaceStringByKey(ksIn, chunk, StringByKey(ksIn, rampData))
				chunk = ReplaceStringByKey("PATTERN0", chunk, pu.popstr)
			endif
			
			break
	endswitch
End

static Function DisplayPanel()
	if (ControlPanelExists())					// Panel already exist?
		DoWindow/F $ksPanelName			// Bring panel to front
	else
		CreatePanel()
	endif
End

// this function exists exclusively for development purposes to quickly kill and redraw the panel
static Function UpdatePanel()
	if (ControlPanelExists())
		DoWindow /K $ksPanelName
	endif
	CreatePanel()
End


static Function InitPackage()
	// Create globals if needed
	DFREF dfr = getPackage()		// Get reference to package data folder
	Variable n = needsInit()
	if (needsInit())							// Data folder does not exist?
		InitPackageData()
	endif

	// TODO: - make sure to kill the general acquisition panel to avoid confusion
	// TODO: - precompile all patterns in calibration set
	
	DisplayPanel()							// Create panel if it does not exist.
End

// KillPackage()
// Stops acquisition and recreates panel and private data from scratch.
// This is mainly of use during development.
static Function KillPackage()
	DataAcqStop()
	DoWindow/K $ksPanelName
	DFREF dfr = getPackage()
	Variable status = DataFolderRefStatus(dfr)
	if (status == 1)							// Data folder exists?
		KillDataFolder dfr
	endif
End

static Function DataAcqTask(s)				// This is the function that will be called periodically by the background task
	STRUCT WMBackgroundStruct &s
		
	SVAR pattern = $(getCurrentPattern())
	
	Variable runNumber = GetRunNumber()	
	
	runCalibration(pattern)
	runNumber += 1
	SetRunNumber(runNumber)
		
	Variable maxRuns = GetMaxRuns()
	if (runNumber >= maxRuns)
		String statusMessage
		sprintf statusMessage, "%d runs completed. Acquisition stopped.", maxRuns
		SetStatusMessage(statusMessage)
		DataAcqStop()
	endif
	
	return 0	// Continue background task
End

static Function LousyDataAcqTask(s)				// This is the function that will be called periodically by the background task
	STRUCT WMBackgroundStruct &s
		
	SVAR pattern = $(getCurrentPattern())
	
	Variable runNumber = GetRunNumber()	
	
	runCalibration_lt(pattern, runNumber)
	runNumber += 1
	SetRunNumber(runNumber)
		
	Variable maxRuns = GetMaxRuns()
	if (runNumber >= maxRuns)
		String statusMessage
		sprintf statusMessage, "%d runs completed. Acquisition stopped.", maxRuns
		SetStatusMessage(statusMessage)
		DataAcqStop()
	endif
	
	return 0	// Continue background task
End

static Function StartBackgroundTask([lousy])
	Variable lousy
	
	lousy = ParamIsDefault(lousy) || lousy < 1 ? 0 : 1
	
	Variable secondsBetweenRuns = GetSecondsBetweenRuns()
	Variable periodInTicks = 60 * secondsBetweenRuns
	
	if (!lousy)
		CtrlNamedBackground $ksPackageName, proc=VMA_Calib#DataAcqTask, period=periodInTicks, start
	else
		CtrlNamedBackground $ksPackageName, proc=VMA_Calib#LousyDataAcqTask, period=periodInTicks, start
	endif
End

static Function StopBackgroundTask()
	CtrlNamedBackground $ksPackageName, stop
End

static Function DataAcqStart([add, lousy])
	Variable add, lousy
	
	add = ParamIsDefault(add) || add < 1 ? 0 : 1
	lousy = ParamIsDefault(lousy) || lousy < 1 ? 0 : 1
	
	DFREF dfr = getPackage()

	SetRunNumber(0)
	
	NVAR running = dfr:running
	running = 1
	
	NVAR paused = dfr:paused
	paused = 0
	
	Button StartStopButton, win=$ksPanelName, title="Stop", fColor=(65535,0,0)
	SetStatusMessage("Calibration running")
	Button PauseResumeButton, win=$ksPanelName, title="Pause", disable=0		// Unhide button

	DisableSettings()	// Can't change while acquisition is running
	
	Variable maxRuns = GetMaxRuns()
	SVAR pattern = $(getCurrentPattern())
	
	SVAR pat = root:maf:$pattern

	SVAR lP = root:maf:$ksChunkPat
	lP = ReplaceStringByKey("PATTERN0", lP, pattern)
	Variable numReps = NumberByKey("REP0", lP)
	mafPC_Compile(ksChunkPat, "")
	Wave ramp = analyzeRamp(pattern)			// reanalyze the new wave and recreate the wave note

	String substanceInfo = note(ramp)
	Variable rightPnts = NumberByKey(ksVM_RampEndPKey, substanceInfo)
	Variable leftPnts = NumberByKey(ksVM_RampStartPKey, substanceInfo)
	Variable rangeStart = NumberByKey(ksVM_RangeStartP, substanceInfo)
	Variable rangeEnd = NumberByKey(ksVM_RangeEndP, substanceInfo)
	
	if (!lousy)
		Wave collector = setupCollectionWave(0, 0, pattern, add=add)
		Wave mask = setupDataMask(rightPnts - leftPnts + 1, numReps, rangeStart-leftPnts, rangeEnd-leftPnts)
		Wave transientChunk = getTransChunkWave(length = numReps)
	else
		Wave collector = setupCollectionWave(maxRuns, numpnts(ramp), pattern, add = add)
	endif
	
	displayHistWave()
	
//	StartBackgroundTask(lousy = lousy)
	 runCalibration(pattern)
	 return 1
End

static Function DataAcqStop()
	DFREF dfr = getPackage()

	NVAR running = dfr:running
	running = 0
	
	NVAR paused = dfr:paused
	paused = 0

	Button StartStopButton, win=$ksPanelName, title="Start", fColor=(0,65535,0)
	
	Button PauseResumeButton, win=$ksPanelName, title="Pause", disable=1		// Hide button

	EnableSettings()		// Can change while acquisition is stopped

	StopBackgroundTask()
	
	Variable runs = GetRunNumber()
	Wave collector = getCollectionWave()
	Variable rOff = NumberByKey(ksROFF, note(collector))
	Redimension /N=(rOff + runs, -1) collector
End

static Function StartStopProc(bn) : ButtonControl
	STRUCT WMButtonAction &bn

	switch (bn.eventCode)
		case 2:
			DFREF dfr = getPackage()
			NVAR running = dfr:running

			if (running)
				DataAcqStop()
				Variable runNumber = GetRunNumber()
				String statusMessage
				sprintf statusMessage, "Acquisition stopped after %d runs.", runNumber
				SetStatusMessage(statusMessage)
				
				DoAlert /T="Save Data" 1, "Do you want to save the data?"
				if (V_flag == 1)
					 btnFunc_finalize(bn)
				endif
			//		VM_organizeDataWaves(feedback=1)
			else
				Variable lousy = str2num(GetUserData(bn.win, bn.ctrlName, "lousy"))
				Wave collector = getCollectionWave()
				
				if (WaveExists(collector))
					DoAlert /T="Overwrite Existing Data" 1, "Calibration data already exists! Do you want to overwrite?"
					if (V_flag == 1)
					
					// TODO: make reset dependent on pattern type
					
						resetAcqValues()
						DataAcqStart(lousy=lousy)
					else
						DataAcqStart(add = 1, lousy=lousy)
					endif
				else
					DataAcqStart(lousy=lousy)
				endif
			endif
		break
	endswitch
End


//static Function DoDataAcq(pattern)
//	String pattern
//	runCalibration(patternName)
//	DoUpdate	
//End

static Function DataAcqResume()
	DFREF dfr = getPackage()
	
	NVAR paused = dfr:paused
	paused = 0

	DisableSettings()		// Can't change while acquisition is running
	
	Button PauseResumeButton, win=$ksPanelName, title="Pause"
	SetStatusMessage("Resuming acquisition")

	StartBackgroundTask()
End

static Function DataAcqPause()
	DFREF dfr = getPackage()
	
	NVAR paused = dfr:paused
	paused = 1
	
	EnableSettings()		// Can change while acquisition is paused

	Button PauseResumeButton, win=$ksPanelName, title="Resume"
	SetStatusMessage("Acquisition paused")

	StopBackgroundTask()
End

static Function PauseResumeProc(ctrlName) : ButtonControl
	String ctrlName

	DFREF dfr = getPackage()
	NVAR paused = dfr:paused
	if (paused)
		DataAcqResume()
	else
		DataAcqPause()
	endif
End

static Function btnFunc_finalize(bn) : ButtonControl
	STRUCT WMButtonAction &bn

	switch (bn.eventCode)
		case 2:			// button up
//			final()
			DoIgorMenu "File", "Save Experiment As"
			break
	endswitch
End


static Function resetAcqValues([pattern])
	String pattern
	
	if (ParamIsDefault(pattern) || strlen(pattern) == 0)
		pattern = "dopamine"
	endif
	
	NVAR counter = root:maf:mafITC_count
	counter = 0
	Variable set_hp1 = -400, current_hp1 = 0
	current_hp1 = mafITC_GetHP(1)

	if (current_hp1 != set_hp1)
		mafITC_SetHP(1, set_hp1)
	endif
End


// ***************************************************************************************************
// **********                     M A F    I N T E R A C T I O N S                ********************
// ***************************************************************************************************

static Function /WAVE analyzeRamp(name)
	String name
	
	Wave ramp
	
	SVAR rampInfo = root:maf:$name
	if (!SVAR_Exists(rampInfo))
		return ramp
	endif
	
	String control = StringFromList(0, ReplaceString(",", StringByKey("CONTROL", rampInfo), ";"))
	
	Wave ramp = root:maf:$(name + "_" + control)
	if (!WaveExists(ramp))
		mafPC_Compile(name, "")
		Wave ramp = root:maf:$(name + "_" + control)
	endif

	String theNote = note(ramp)
	theNote = ReplaceStringByKey(ksVM_RampKey, theNote, name)
	theNote = ReplaceStringByKey(ksVM_RampStruct, theNote, "no")
	
	Variable startX = NumberByKey("TIME0", rampInfo) * 1e-3		// given in ms
	Variable holding = NumberByKey("AMP0_1", rampInfo)				// given in mV
	
	Variable startP = x2pnt(ramp, startX)
	theNote = ReplaceNumberByKey(ksVM_RampStartPKey, theNote, startP)	
	
	Extract /FREE /INDX /O ramp, holdIndx, ramp == holding
	if (numpnts(holdIndx) == 0)
		// something went wrong here, no holdings found
		return ramp
	endif
	Extract /FREE /O holdIndx, changes, holdIndx[p] - holdIndx[p-1] > 1
	if (numpnts(changes) == 0)
		// only the whole wave consists of the same holding value -> not a ramp
		return ramp
	endif
	
	Variable endP = changes[numpnts(changes) - 1]
	Variable endX = pnt2x(ramp, endP)
	theNote = ReplaceNumberByKey(ksVM_RampEndPKey, theNote, endP)	
	
	WaveStats /C=1/W/M=1/R=[startP, endP]/Q/Z ramp
	Wave M_WaveStats
	
	theNote = ReplaceNumberByKey(ksVM_RampMaxPKey, theNote, x2pnt(ramp, M_WaveStats[%maxLoc]))	
	
	Variable oxyVolt = str2num(StringFromList(0, StringByKey(name, ksVM_SubstanceOxidations), ","))
	Variable oxyRange = str2num(StringFromList(1, StringByKey(name, ksVM_SubstanceOxidations), ","))
	if (numtype(oxyVolt) > 0)
		oxyVolt = 600						// default to dopamine
		oxyRange = 100
	endif
	
	FindLevel /P /EDGE=1/Q ramp, oxyVolt - oxyRange
	if (V_flag == 0)
		theNote = ReplaceNumberByKey(ksVM_RangeStartP, theNote, ceil(V_LevelX))
	endif
	FindLevel /P /EDGE=1/Q ramp, oxyVolt + oxyRange
	if (V_flag == 0)
		theNote = ReplaceNumberByKey(ksVM_RangeEndP, theNote, floor(V_LevelX))
	endif
	
	Note /K ramp, theNote
	return ramp
End



// ***************************************************************************************************
// ***************      B A C K G R O U N D     A C Q U S I T I O N    W O R K E R S      ************
// ***************************************************************************************************


static Function /WAVE getCollectionWave()
	DFREF loadFolder = VoltHelpers#getDataLoadFolder()
	Wave collector = loadFolder:$ksCalibDataName
	return collector
End

static Function /WAVE getTransDispWave()
	DFREF package = getPackage()
	Wave dispWave = package:$ksCalibTransName
	return dispWave
End

static Function /WAVE getTransChunkWave([length])
	Variable length
	DFREF package = getPackage()
	Wave chunk = package:$ksTransientChunk
	if (!WaveExists(chunk))
		length = ParamIsDefault(length) || length <= 0 ? 0 : round(length)
		Make /N=(length) package:$ksTransientChunk /WAVE=chunk
	else
		if (!ParamIsDefault(length) && length != numpnts(chunk))
			Redimension /N=(length) chunk
		endif
	endif
	
	return chunk
End

static Function /WAVE getDataMask()
	DFREF package = getPackage()
	Wave mask = package:$ksDataMask
	return mask
End

static Function /WAVE setupCollectionWave(rows, columns, type, [add])
	Variable rows, columns, add
	String type
	
	add = ParamIsDefault(add) || add < 1 ? 0 : 1
	
	DFREF loadFolder = VoltHelpers#getDataLoadFolder()
	Wave collector = loadFolder:$ksCalibDataName
	Variable rOffSet = 0, new = 0
		
	if (!WaveExists(collector) || !add)
		Make /N=(rows, columns)/O loadFolder:$ksCalibDataName /WAVE=collector
		SetScale d 0, 0, "A", collector
		SetScale /P x 0, 0.1, "dat", collector
		add = 0
		new = 1
	endif
	
	if (add)
		rOffSet = DimSize(collector, 0)
		if (columns == 0)
			columns = DimSize(collector, 1)
		elseif (columns != DimSize(collector, 1))
			DoAlert /T="Ramp Size Mismatch" 0, "You are trying to add to existing data with a different ramp size. Please save experiment first and clear existing data!"
			return $""
		endif
	endif
	
	if (!new && rows > 0)
		Redimension /N=(rows + rOffSet, columns) collector
	endif	

	String theNote = note(collector)
	String extType = StringByKey(ksCTYPE, theNote)
	if (strlen(extType) > 0)
		if (WhichListItem(type, extType, ",") < 0)
			extType = AddListItem(type, extType, ",", Inf)
		endif
	else
		extType = type
	endif

	theNote = ReplaceStringByKey(ksCTYPE, theNote, extType)
	theNote = ReplaceNumberByKey(ksROFF, theNote, rOffset)

	Note /K collector, theNote
	
	setupTransientDispWave(rows, type, add = add)
	
	return collector
End

static Function /WAVE setupTransientDispWave(rows, type, [add])
	Variable rows, add
	String type
	
	add = ParamIsDefault(add) || add < 1 ? 0 : 1
	rows = rows < 0 ? 0 : round(rows)
	
	Variable rOffset = 0
	DFREF package = getPackage()
	Wave dispWave = package:$ksCalibTransName
	if (!WaveExists(dispWave) || !add)
		Make /N=(rows)/O package:$ksCalibTransName /WAVE=dispWave
		SetScale d 0, 0, "A", dispWave
		SetScale /P x 0, 0.1, "dat", dispWave
		add = 0
	endif
	
	if (add)
		rOffset = numpnts(dispWave)
		Redimension /N=(rOffset + rows) dispWave
	endif
	
	String theNote = note(dispWave)
	String extType = StringByKey(ksCTYPE, theNote)
	if (strlen(extType) > 0)
		if (WhichListItem(type, extType, ",") < 0)
			extType = AddListItem(type, extType, ",", Inf)
		endif
	else
		extType = type
	endif

	theNote = ReplaceStringByKey(ksCTYPE, theNote, extType)
	theNote = ReplaceNumberByKey(ksROFF, theNote, rOffset)

	Note /K dispWave, theNote		
	return dispWave
End

static Function /WAVE setupDataMask(rows, columns, rangeStart, rangeEnd)
	Variable rows, columns, rangeStart, rangeEnd
	
	rows = rows < 0 ? 0 : round(rows)
	columns = columns < 0 ? 0 : round(columns)
	rangeStart = rangeStart < 0 ? 0 : round(rangeStart)
	rangeEnd = rangeStart < 0 || rangeEnd < rangeStart ? rangeStart : round(rangeEnd)
	
	DFREF package = getPackage()
	Make /N=(rows, columns) /O package:$ksDataMask /WAVE=mask
	mask = 0
	mask[rangeStart, rangeEnd][] = 1
	
	return mask
End


static Function runCalibration_lt(patternName, index)
	String patternName
	Variable index
	
	Wave collector = getCollectionWave()
	if (!WaveExists(collector))
		return -1
	endif

	if (index < 0 || index > DimSize(collector, 0))
		// this is the index into the data matrix and needs to be an positive integer less than 
		//       the row numbers of the data matrix
		return -1
	endif
	
	String parameterlist = ""
	
	String fPName = "root:maf:" + patternName
	SVAR currPat = $(fPName)
	
	// assume precompiled patterns (should not be too many)
	
	mafITC_Stim(fPName, bits2Seq (StringByKey ("CONTROL", currPat)), bits2seq (StringByKey ("SAVECHAN", currPat)), "PATTERN:" + patternName + SelectString (strlen (parameterlist) > 0, "", ";" + parameterlist), 1)
	
	NVAR mafITC_count = root:maf:mafITC_count
	
	String cCh = StringByKey("CONTROL", currPat)
	cCh = cCh[2, strlen(cCh) - 1]
	String sCh = StringByKey("SAVECHAN", currPat)
	sCh = sCh[2, strlen(sCh) - 1]

	// TODO: if counter is 0 move pattern name and ramp to analysis folder

	if (mafITC_isNI())
		Wave rampTrace = $("W_NIout" + cCh)
	elseif (mafITC_isITC())
		Wave rampTrace = $"W_ITCout"
	endif
	
	Wave data = $(mafITC_lastWave(str2num(sCh)))
		
	Variable offset = NumberByKey(ksROFF, note(collector))
	offset = numtype(offset) == 2 ? 0 : offset

	collector[offset + index][] = data[q]
	mafITC_count -= 1	// overwrite previous wave to prevent overfilling and long wait times because of inefficient
							//   wave list operations						
	return 1
End

static Function runCalibration(patternName)
	String patternName
	
	Wave collector = getCollectionWave()
	if (!WaveExists(collector))
		return -1
	endif
	Variable first = numpnts(collector) == 0

	String parameterList = ""
	String fPName = "root:maf:" + patternName

	String cPName = "root:maf:" + ksChunkPat
	SVAR currPat = $(cPName)				// assuming that this pattern exists and has been compiled

	mafITC_Stim(cPName, bits2Seq (StringByKey ("CONTROL", currPat)), bits2seq (StringByKey ("SAVECHAN", currPat)), "PATTERN:" + patternName + SelectString (strlen (parameterlist) > 0, "", ";" + parameterlist), 1)
	
	NVAR mafITC_count = root:maf:mafITC_count
	
	String cCh = StringFromList(0, StringByKey("CONTROL", currPat), ",")
	cCh = cCh[2, strlen(cCh) - 1]
	String sCh = StringFromList(0, StringByKey("SAVECHAN", currPat), ",")
	sCh = sCh[2, strlen(sCh) - 1]

	if (mafITC_isNI())
		Wave rampTrace = $("W_NIout" + cCh)
	elseif (mafITC_isITC())
		Wave rampTrace = $"W_ITCout"
	endif
	
	Wave data = $(mafITC_lastWave(str2num(sCh)))
	
//	SVAR oPat = $(fPName)
	Wave patWave = analyzeRamp(patternName)
	String theNote = note(patWave)
	Variable rightPnts = NumberByKey(ksVM_RampEndPKey, theNote)
	Variable leftPnts = NumberByKey(ksVM_RampStartPKey, theNote)
	Variable range = rightPnts - leftPnts

	Variable patLength = numpnts(patWave)
	Variable numTraces = NumberByKey("REP0", currPat)

	Redimension /N=(patLength, numTraces) data
	Redimension /N=(rightPnts + 1, -1) data
	if (leftPnts > 0)
		DeletePoints /M=0 0, leftPnts, data
	endif
	
	Wave transient = getTransDispWave()
	Wave transientChunk = getTransChunkWave()
	Wave mask = getDataMask()
	
	MatrixOp /O transientChunk = sumCols(data * mask)/range	
	
	Concatenate /NP=0 {transientChunk}, transient
	
	MatrixTranspose data	
	if (first)
		Concatenate /O {data}, collector
	else
		Concatenate /NP=0 {data}, collector
	endif
	Note /K collector, ReplaceNumberByKey(ksROFF, note(collector), DimSize(collector, 0))
	
	mafITC_count -= 1	// overwrite previous wave to prevent overfilling and long wait times because of inefficient
							//   wave list operations						
	return 0
End


//! This display function creates a graph for the transient wave to monitor the 
//  progression of the acquisition. 
//  @param: wave that contains the transient. If not provided the default display wave 
//          will be used.
//  @return: returns the name of the graph
//-
static Function /S displayHistWave([history])
	Wave history
	
	if (ParamIsDefault(history) || !WaveExists(history))
		Wave history = getTransDispWave()
	endif
	
	DoWindow $ksHistoryGraph
	switch (V_flag)
		case 2:
			DoWindow /HIDE=0 $ksHistoryGraph
		case 1:
			return ksHistoryGraph
			break
	endswitch
	
	// we only get here, if the window does not exists
	
	STRUCT Rect size
	
	Display /N=$ksHistoryGraph history as ksHistoryGraphTitle
	SetWindow $ksHistoryGraph hook(winPrefs)=WH_setPrefWinLocation
	ModifyGraph dateInfo(bottom)={1,2,0}, standoff=0, rgb=(26112, 26112, 26112)
	SetAxis /A/E=0 left
	SetAxis /A/E=1 bottom
	Label bottom "time"
	Label left "current (\\U)"
	
	Variable locIndex = VoltaMafPCBridge#getWinPrefs(size, ksHistoryGraph)
	if (locIndex > 0)
		MoveWindow /W=$ksHistoryGraph size.left, size.top, size.right, size.bottom
	endif
	
	return ksHistoryGraph
End

// The following hook procedures make sure that the state of this package is consistent
// with reality when an experiment is opened, killed and when Igor quits.

// This is called by Igor when any file is opened, including the experiment file containing these procedures.
// When an experiment is opened, background tasks are stopped. Here we make sure that these procedures
// are in-sync with the fact that the background task is stopped. This is needed if the experiment was saved
// while the acquisition was running.
static Function AfterFileOpenHook(refNum, file, pathName, type, creator, kind)
	Variable refNum,kind
	String file, pathName, type, creator
	
	if (kind==1 || kind==2)				// Experiment just opened?
		DFREF dfr = getPackage()
		Variable status = DataFolderRefStatus(dfr)
		if (status == 1)
			NVAR running = dfr:running
			if (running)
				// DoAlert 0, "Experiment was saved with acquisition running. It is now stopped."	// For debugging only
				DataAcqStop()
				SetStatusMessage("Click Start to start acquisition")
			endif
		endif
	endif
	
	return 0	// Igor ignores this
End

// This function makes sure acquisition is stopped when the current experiment is killed.
static Function IgorBeforeNewHook(igorApplicationNameStr)
	String igorApplicationNameStr

	DFREF dfr = getPackage()
	Variable status = DataFolderRefStatus(dfr)
	if (status == 1)
		NVAR running = dfr:running
		if (running)
			DoAlert 0, "Experiment is being killed. Acquisition is running but will now be stopped."
			DataAcqStop()
		endif
	endif
	
	return 0	// Igor ignores this
End

//! This function adds a hook when Igor is quiting to make sure the acquisition is 
// stopped when Igor is quitting.
// @param unsavedExp:			number to show that the experiment is unsaved
// @param unsavedNotebooks:		number to show that there are unsaved Notebooks open
// @param unsavedProcedures:	number to show that there are unsaved procedures open
// @return:	either 0 to continue with other quit hook functions or 1 to ignore other 
//          hook functions.
//-
static Function IgorBeforeQuitHook(unsavedExp, unsavedNotebooks, unsavedProcedures)
	Variable unsavedExp, unsavedNotebooks, unsavedProcedures

	DFREF dfr = getPackage()
	Variable status = DataFolderRefStatus(dfr)
	if (status == 1)
		NVAR running = dfr:running
		if (running)
			DoAlert 0, "Igor is quitting. Acquisition is running but will now be stopped."
			DataAcqStop()
			SetStatusMessage("Click Start to start acquisition")
		endif
	endif
	
	return 0	// Proceed with normal quit process
End
