#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 0.6
#pragma IgorVersion = 6.3
// #pragma IndependentModule = VM_mafPCBridge
#pragma ModuleName = VoltaMafPCBridge
#include "SoundBitesHelper", version >= 1
#include "GraphHelpers"
#include "VM_GUI"
#include "mafPC2"

// written by Roland Bock
// version 0.5: March 2017
// version 0.3: June 2015 
// version 0.1: July 2013

// This set of functions adds functionality to the mafPC package from Matthew Xu-Friedman, 
// University of Buffalo, for electrophysiology to enable it to be used for fast-cycling voltammetry
// and amperometry measurements.


// ************************       C O N S T A N T S         *******************************************

static StrConstant ks_bridgePanelName = "VoltaBridgePanel"
static StrConstant ks_bridgePanelTitle = "Voltammetry Bridge Panel"
static StrConstant ks_packageName = "VoltaMAFBridge"
static Strconstant ks_utilFolderName = "VMUtilityData"

static Strconstant ks_GoNoGoPanelName = "GoNoGo_Panel"
static Strconstant ks_stopRunTargetWinName = "VM_StopRunTargetWindow"

static Strconstant ks_defProtLoc = "Voltammetry:Stim Sets"
static Strconstant ks_PrefsFileName = "AcquisitionPreferences.bin"
// static Strconstant ks_WinPrefsName = "WindowLocationPreferences.bin"

static Strconstant ks_WinPrefsStr = "WindowPreferences"

static StrConstant ks_StimNameTable = "stimulusNameTable"
static StrConstant ks_StimNameTableSel = "stimulusNameTableSel"
static StrConstant ks_recordFlag = "recording"
static StrConstant ks_recordText = "recordString"
static StrConstant ks_pauseFlag = "paused"
static Strconstant ks_runStopRequest = "runStopRequest"
static Strconstant ks_stopRunPanelExists = "stopRunPanelExists"

static StrConstant ks_btnAction = "ACTION"


static Constant kNumWinPrefs = 50

static Constant k_defaultInterval = 120							// default interval 120 s = 2 minutes
static Constant k_defaultRepeats = 1							// default stimulation repetitions 

static Constant kPrefsVersion = 1


// **************************     H O O K    F U N C T I O N S     *********************************************

// The following hook procedures make sure that the state of this package is consistent
// with reality when an experiment is opened, killed and when Igor quits.

// This is called by Igor when any file is opened, including the experiment file containing these procedures.
// When an experiment is opened, background tasks are stopped. Here we make sure that these procedures
// are in-sync with the fact that the background task is stopped. This is needed if the experiment was saved
// while the acquisition was running.
static Function AfterFileOpenHook(refNum, file, pathName, type, creator, kind)
	Variable refNum,kind
	String file, pathName, type, creator
	
	if (kind == 1 || kind == 2)				// Experiment just opened?
		NVAR recording = $(getRecFlag())
		if (recording == 1)
			// DoAlert 0, "Experiment was saved with acquisition running. It is now stopped."	// For debugging only
			DataAcq_stop()
		endif
	endif
	
	return 0	// Igor ignores this
End

// This function makes sure acquisition is stopped when the current experiment is killed.
static Function IgorBeforeNewHook(igorApplicationNameStr)
	String igorApplicationNameStr

	NVAR recording = $(getRecFlag())
	if (recording == 1)
		DoAlert 0, "Experiment is being killed. Acquisition is running but will now be stopped."
		DataAcq_stop()
	endif
	
	SVAR prefs = $getWinPrefsStr()
	STRUCT VM_WinPrefs windows
	if (SVAR_Exists(prefs))
		StructGet /S windows, prefs
		saveWinPrefs(windows)
	endif
	
	return 0	// Igor ignores this
End

// This function makes sure acquisition is stopped when Igor is quitting.
static Function IgorBeforeQuitHook(unsavedExp, unsavedNotebooks, unsavedProcedures)
	Variable unsavedExp, unsavedNotebooks, unsavedProcedures

	NVAR recording = $(getRecFlag())
	if (recording == 1)
		DoAlert 0, "Igor is quitting. Acquisition is running but will now be stopped."
		DataAcq_stop()
	endif
	
	SVAR prefs = $getWinPrefsStr()
	STRUCT VM_WinPrefs windows
	if (SVAR_Exists(prefs))
		StructGet /S windows, prefs
		saveWinPrefs(windows)
	endif

	return 0	// Proceed with normal quit process
End

static Function IgorStartOrNewHook(igorApplicationNameStr)
	String igorApplicationNameStr
	
	STRUCT VM_WinPrefs windows
	loadWinPrefs(windows)
	init()
End

// **************************    M E N U E    *********************************

Menu "Voltammetry", dynamic
	Submenu "Data Acquisition"
		"(VM with mafPC", createVPanel()
		"-"
		"(Load protocol ...", VM_loadProtocol()
		"(Choose protocols folder ...", VoltaMafPCBridge#setProtPath()
		"-"
		"Open STOP-run target window", VM_stopRunTargetWin()
		CalibrationMenuItem(1), /Q, VMA_Calib#InitPackage()
		CalibrationMenuItem(2), /Q, VoltaMafPCBridge#calibMode()
	End
End

Function /S CalibrationMenuItem(item)
	Variable item
	
	String menuStr = ""
	Variable isLoaded = strlen(WinList("VMA_Calibration.ipf", ";", "WIN:128")) > 0
	
	switch (item)
		case 1:
			if (isLoaded)
				menuStr = "-\rCalibration"
			endif
			break
		case 2:
		default:
			if (!isLoaded)
				menuStr = "-\rEnter Calibration Mode"
			else
				menuStr = "-\rRemove Calibration Mode"
			endif
	endswitch
	
	return menuStr
End


static Function calibMode()
	Variable isLoaded = strlen(WinList("VMA_Calibration.ipf", ";", "WIN:128")) > 0

	if (!isLoaded)
		Execute/P/Q/Z "INSERTINCLUDE \"VMA_Calibration\""
	else
		Execute/P/Q/Z "DELETEINCLUDE \"VMA_Calibration\""	
	endif	
	
	Execute/P/Q/Z "COMPILEPROCEDURES "
End

// *********************            S T R U C T U R E S       ******************************


Structure VM_AcqPrefs
	int16 version
	char protPath[400]		// path to the protocol preferences
EndStructure

Structure VM_transientPrefs
	Variable lowerLimit
	Variable upperLimit
	Variable preRamp
EndStructure

Structure VM_WinPrefs
	int16 numWins
	STRUCT VM_WinLocation locations[kNumWinPrefs]
EndStructure

Structure VM_WinLocation
	STRUCT Rect size
	char name[33]
EndStructure

// **************************    H E L P E R    F U N C T I O N S   *******************************

//! This function gets the package folder, and creates it if it doesn't already exists.
// @return: a datafolder reference to the package folder.
//!
static Function /DF getPackage()
	DFREF cDF = GetDatafolderDFR()
	DFREF path = VoltHelpers#getPackage():$ks_packageName
	
	if (DatafolderRefStatus(path) == 0)
		SetDatafolder VoltHelpers#getPackage()
		NewDatafolder /S $ks_packageName
		DFREF path = GetDatafolderDFR()
	endif
	
	SetDatafolder cDF
	return path
End

//! This function gets the data folder for the utility data. This folder contains all other data
// that has been aquired but is not directly needed to generate the voltamograms.
// @return: a datafolder reference to the utility package folder
//!
static Function /DF getUtilityDataFolder()
	DFREF cDF = GetDatafolderDFR()
	NewDatafolder /O/S $ks_utilFolderName
	DFREF path = GetDatafolderDFR()	
	SetDatafolder cDF
	return path
End

//! A utility function that tests if the mafPC bridge panel exists or not.
// @return: 1 for true or 0 for false depending if the panel exists or not
//!
static Function panelExists()
	DoWindow $ks_bridgePanelName
	return V_flag != 0
End

static Function /S getFlagVar(name, [type])
	String name
	Variable type
	
	if (ParamIsDefault(type) || type < 0)
		type = 0
	else
		type = type > 1 ? 1 : round(type)
	endif
	
	
	DFREF path = getPackage()
	DFREF cDF = GetDatafolderDFR()

	switch ( type )
		case 0:			// default, checking for global variable
			NVAR flag = path:$name
			if (!NVAR_Exists(flag))
				Variable /G path:$name
			endif
			break
		case 1:			// check for string variable
			SVAR aName = path:$name
			if (!SVAR_Exists(aName))
				String /G path:$name
			endif
			break
		default:
			print "** ERROR (getFlagVar): can't determine if to look for a string or variable. FAILED!"
			return ""
	endswitch
	return GetDatafolder(1, path) + name
End

static Function defaultPrefs(prefs)
	STRUCT VM_AcqPrefs &prefs
	
	prefs.version = kPrefsVersion
	prefs.protPath = SpecialDirPath("Documents", 0, 0, 0) + ks_defProtLoc
	String path = prefs.protPath
	
	GetFileFolderInfo /Q /Z path
	if (V_flag == 0 && V_isFolder == 1)
		prefs.protPath = path
	else
		Variable depths = ItemsInList(ks_defProtLoc, ":")
		Variable index = 0
		path = SpecialDirPath("Documents", 0,0,0)
		for (index = 0; index < depths; index += 1)
			path += StringFromList(index, ks_defProtLoc, ":") + ":"
			GetFileFolderInfo /Q /Z path
			if (V_flag != 0 || V_isFolder == 0)
				NewPath /C/O/Q/Z tmp, path
				if (V_flag == 0)
					prefs.protPath = path
				else
					// could not create the folder 
				endif
			endif
		endfor
		KillPath /Z tmp
	endif
	
	return 1
End

static Function loadPrefs(prefs)
	STRUCT VM_AcqPrefs &prefs
	
	Variable recordID = 0
	String name = VM_getPackagePrefsName()
	
	LoadPackagePreferences /MIS=1 name, ks_PrefsFileName, recordID, prefs
	if (V_flag != 0 || V_bytesRead == 0)
		defaultPrefs(prefs)
	endif
	if (prefs.version != kPrefsVersion)
		savePrefs(prefs, reset=1)
		defaultPrefs(prefs)
	endif
End


static Function savePrefs(prefs, [reset])
	STRUCT VM_AcqPrefs &prefs
	Variable reset
	
	String name = VM_getPackagePrefsName()
	
	Variable recordID = 0
	reset = ParamIsDefault(reset) || reset < 1 ? 0 : 1
	
	if (reset)
		SavePackagePreferences /KILL name, ks_PrefsFileName, recordID, prefs
	else
		SavePackagePreferences name, ks_PrefsFileName, recordID, prefs
	endif
	return 1
End


static Function loadWinPrefs(prefs)
	STRUCT VM_WinPrefs &prefs
	
	Variable recordID = 1
	String name = VM_getPackagePrefsName()
	
	LoadPackagePreferences /MIS=1 name, ks_PrefsFileName, recordID, prefs
//	if (V_flag != 0 || V_bytesRead == 0)
//		defaultWinPrefs(prefs)
//	endif

	SVAR winStruct = getPackage():$ks_WinPrefsStr
	if (!SVAR_Exists(winStruct))
		String /G getPackage():$ks_winPrefsStr
		SVAR winStruct = getPackage():$ks_WinPrefsStr
	endif
	
	StructPut /S prefs, winStruct
End

static Function saveWinPrefs(prefs)
	STRUCT VM_WinPrefs &prefs
	
	Variable recordID = 1
	String name = VM_getPackagePrefsName()

	SavePackagePreferences name, ks_PrefsFileName, recordID, prefs
End

static Function getWinPrefs(size, name)
	STRUCT Rect &size
	String name
	
	STRUCT VM_WinPrefs windows

	SVAR winStr = getPackage():$ks_WinPrefsStr
	if (!SVAR_Exists(winStr))
		loadWinPrefs(windows)
		SVAR winStr = $(getFlagVar(ks_WinPrefsStr, type=1))
	endif
	
	StructGet /S windows, winStr
	
	Variable index = 0
	do
		index += 1
	while (index < kNumWinPrefs && cmpstr(name, windows.locations[index].name) != 0)
	
	if (index == kNumWinPrefs)
		// no window with the name found, so no adjustment necessary
		return -1
	endif
	
	size = windows.locations[index].size
	return index
End	

static Function setWinPrefs(size, name)
	STRUCT Rect &size
	String name
	
	STRUCT Rect old
	Variable index = getWinPrefs(old, name)
	SVAR winStr = getPackage():$ks_WinPrefsStr

	STRUCT VM_WinPrefs windows
	StructGet /S windows, winStr

	if (index < 0)
		// no preferences for this window exist
		if (windows.numWins + 1 <= kNumWinPrefs)
			// still space
			windows.numWins += 1
			index = windows.numWins - 1
			windows.locations[index].name = name
			windows.locations[index].size = size
		else
			// ran out of space, how to deal with this?
		endif
	else
		if (cmpstr(name, windows.locations[index].name) == 0)
			windows.locations[index].size = size
		endif
	endif
	
	StructPut /S windows, winStr
End		

static Function /S getWinPrefsStr()
	return GetDatafolder(1, getPackage()) + ks_WinPrefsStr
End

static Function setProtPath([pathToFolder, useDefault])
	String pathToFolder
	Variable useDefault
	
	useDefault = ParamIsDefault(useDefault) || useDefault < 1 ? 0 : 1
	
	STRUCT VM_AcqPrefs prefs
	loadPrefs(prefs)
	String path = prefs.protPath
	
	PathInfo VMStimulusProtocols
	if (!V_flag)
		NewPath /Q VMStimulusProtocols, path
		PathInfo /S VMStimulusProtocols
	endif
	
	if ((ParamIsDefault(pathToFolder) || strlen(pathToFolder) == 0) && !useDefault)
		NewPath /O/Q/M="Set the folder to the stimulus protocols" VMStimulusProtocols
	else
		NewPath /O/Q/M="Set the folder to the stimulus protocols" VMStimulusProtocols, path
	endif
	if (V_flag > 0)
		print "-> stimulus path not changed."
		return 0
	endif
	PathInfo VMStimulusProtocols
	prefs.protPath = S_path
	savePrefs(prefs)
	return 1
End


static Function isRecording()
	NVAR recFlag = $(getRecFlag())
	return recFlag == 1
End

static Function isStopped()
	NVAR recFlag = $(getRecFlag())
	return recFlag == -1
End

static Function isPaused()
	NVAR pauseFlag = $(getPauseFlag())
	return pauseFlag == 1
End

static Function /S getRecFlag()
	return getFlagVar(ks_recordFlag)
End

static Function /S getPauseFlag()
	return getFlagVar(ks_pauseFlag)
End

static Function /S getRecStr()
	return getFlagVar(ks_RecordText, type = 1)
End

static Function setRecFlag(type)
	String type
	
	NVAR recFlag = $(getRecFlag())
	SVAR recText = $(getRecStr())
	strswitch (type)
		case "recording":
		case "rec":
		case "RECORDING":
			recFlag = 1
			recText = "recording"
			break
		case "free":
		case "FREE":
			recFlag = 0
			recText = ""
			break
		case "stop":
		case "stopped":
		case "STOP":
		default:
			recFlag = -1
			recText = ""
	endswitch
End

static Function setPauseFlag(type)
	String type
	
	NVAR flag = $(getPauseFlag())
	SVAR recText = $(getRecStr())	
	strswitch (type)
		case "paused":
		case "pause":
		case "PAUSED":
		case "PAUSE":
			flag = 1
			recText = ""
			break
		case "resume":
		case "RESUME":
		case "continue":
		case "CONTINUE":
		case "cont":
		case "not":
			flag = 0
			NVAR rec = $(getRecFlag())
			if (rec < 0)
				// stopped
				recText = ""
			else
				recText = "recording"
			endif
			break
		default:
			print "** ERROR (setPauseFlag): didn't recognize command \"" + type + "\". Should only be 'pause' or 'resume'!"
			flag = -1
			recText = ""
	endswitch
	return flag
End
			
static Function /WAVE getStimulusTable()
	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()
	WAVE /T stimTable = package:$ks_StimNameTable
	if (!WaveExists(stimTable))
		Make /N=(1,3) /T package:$ks_StimNameTable /WAVE=stimTable
		Make /N=(1,3) /O package:$ks_StimNameTableSel /WAVE=stimTableSel
	endif
	return stimTable
End

static Function /WAVE getStimulusTableSelW()
	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()
	WAVE /T stimTable = package:$ks_StimNameTable
	WAVE stimTableSel = package:$ks_StimNameTableSel
	if (!WaveExists(stimTable))
		Make /N=(1,3) /T package:$ks_StimNameTable /WAVE=stimTable
		Make /N=(1,3) /O package:$ks_StimNameTableSel /WAVE=stimTableSel
	elseif (!WaveExists(stimTableSel))
		Make /N=(DimSize(stimTable, 0), DimSize(stimTable, 1)) package:$ks_StimNameTableSel /WAVE=stimTableSel
	else
		Variable rows = DimSize(stimTable, 0)
		Variable columns = DimSize(stimTable, 1)
		if (DimSize(stimTableSel, 0) != rows || DimSize(stimTableSel, 1) != columns)
			Redimension /N=(rows, columns) stimTableSel
		endif
	endif
	return stimTableSel
End

static Function initializeStimTable()
	Wave stimTable = getStimulusTable()
	Wave stimTableSel = getStimulusTableSelW()
	
	// fill with necessary meat
	
	
End

static Function killPackage()

End

static Function getIntervalInSec()
	if (!panelExists())
		return k_defaultInterval
	endif
	ControlInfo /W=$ks_bridgePanelName sv_interval
	return V_Value
End

static Function setMafPCPath4Stim([path])
	String path
	
	STRUCT VM_AcqPrefs prefs
	loadPrefs(prefs)
	
	if (ParamIsDefault(path) || strlen(path) == 0)
		path = prefs.protPath
	endif
	
	GetFileFolderInfo /Q /Z path
	prefs.protPath = path

	if (V_flag == 1 || V_isFolder == 0)
		// doesn't exist, so make it.
		NewPath /C /O/Q/Z mafPCFolder, path
		if (V_flag == 1)
			// could not create the path
		else
			prefs.protPath = path
		endif
	endif
	
	savePrefs(prefs)
	
End

static Function init()
	DFREF package = getPackage()
	setRecFlag("stop")
	setPauseFlag("not")
	initializeStimTable()
	setMafPCPath4Stim()
	soundInit()
End

static Function soundInit()
	SB_LoadSoundBite("acqStopRequested")
	SB_LoadSoundBite("acqStopCleared")
End	

// *****************************     B A C K G R O U N D    D A T A     A C Q U I S I T I O N    ******************************
static Function startBackgroundAcq(interval)
	Variable interval
	
	Variable intervalInTicks = 60 * interval
	CtrlNamedBackground $(ks_PackageName + "Acq"), proc = VoltaMafBridge#DataAcqTask, period = intervalInTicks, start
End

static Function stopBackgroundAcq()
	CtrlNamedBackground $(ks_PackageName + "Acq"), stop
End

static Function DataAcq_start(interval)
	Variable interval
	
	setRecFlag("recording")
	setPauseFlag("not")
	startBackgroundAcq(interval)
	disableSettings("start")
	setStatusMessage("Click \"STOP\" or \"pause\" to halt acqisition.")
	return 1
End

static Function DataAcq_stop()
	setRecFlag("stop")
	setPauseFlag("not")
	stopBackgroundAcq()
	enableSettings("stop")
	setStatusMessage("Click \"Start\" to start acquistion.")
	return 1	
End

static Function DataAcq_pause()
	setRecFlag("free")
	setPauseFlag("paused")
	stopBackgroundAcq()
	enableSettings("")
	setStatusMessage("Click \"resume\" to continue or \"STOP\" to halt acquisition.")
	return 1
End

static Function DataAcq_resume(interval)
	Variable interval
	
	setRecFlag("recording")
	setPauseFlag("resume")
	startBackgroundAcq(interval)
	disableSettings("start")
	setStatusMessage("Click \"STOP\" or \"pause\" to halt acquisition.")
	return 1
End

static Function DataAcq_Task(s)
	STRUCT WMBackgroundStruct &s
	
	Variable currentStimNum = getCurrentStim() + 1
	setStimNumber(currentStimNum)
	
	Variable error = DataAcquisition(currentStimNum, getStimMethod(), 1)
	
	Variable upperLim = getMaxRepeats()
	if (currentStimNum >= upperLim || error)
		String statusMessage
		if (error)
			statusMessage = "Method not recognized. Acquisition stopped."
		else
			sprintf statusMessage, "%d stimulations completed. Acquisition stopped.", upperLim
		endif
		setStatusMessage(statusMessage)
		DataAcq_stop()
	endif
	
	return 0				// continue background task
End

static Function DataAcquisition(number, method, stimulusSet)
	Variable number, stimulusSet
	String method
	
	VoltaMafPCBridge#setRecFlag("recording")
	
	strswitch (method)
		case "FSCV":
		case "fscv":
			break
		case "Amperometry":
		case "amperometry":
			break
		default:
			return 1
	endswitch 
	
	
	VoltaMafPCBridge#setRecFlag("free")
	return 0
End


// ************************************************************************************************************
// *************                V I S U A L    F E E D B A C K                      ****************************
// ************************************************************************************************************
// ************************************************************************************************************


Function VM_visualFeedBackPanel([cName])
	String cName
	
	if (ParamIsDefault(cName) || strlen(cName) == 0)
		cName = "green"
	endif

	Variable locIndex = -1
	STRUCT Rect size
	
	DoWindow $ks_GoNoGoPanelName
	if (!V_flag)
		NewPanel /K=1/N=$ks_GoNoGoPanelName /W=(10,10,154,154) as "Go NoGo Phase"
		SetWindow $ks_GoNoGoPanelName hook(winPrefs)=WH_setPrefWinLocation
		SetWindow $ks_GoNoGoPanelName hook(overlap)=WH_checkToStop
//		ModifyPanel /W=$ks_GoNoGoPanelName width=144, height=144
		ModifyPanel /W=$ks_GoNoGoPanelName fixedSize=1, noEdit=1
		MoveWindow /W=$ks_GoNoGoPanelName 787.5,44.75,931.5,188.75
	
//		locIndex = VoltaMafPCBridge#getWinPrefs(size, ks_GoNoGoPanelName)
//		if (locIndex >= 0)
//			MoveWindow /W=$ks_GoNoGoPanelName size.left, size.top, size.right, size.bottom
//		endif
	endif
	
	Wave color = getWaveRGBByName(cName)
	ModifyPanel /W=$ks_GoNoGoPanelName cbRGB=(color[0],color[1],color[2])
	DoUpdate /W=$ks_GoNoGoPanelName
	
	return 1
End


Function VM_stopRunTargetWin()
	Variable locIndex = -1
	STRUCT Rect size
	DoWindow $ks_stopRunTargetWinName
	if (!V_flag)
		// window does not exists
		soundInit()
		NewPanel /K=1/N=$ks_stopRunTargetWinName /W=(10,10,130,130) as "STOP RUN"
		SetWindow $ks_stopRunTargetWinName hook(winPrefs)=WH_setPrefWinLocation
		ModifyPanel /W=$ks_stopRunTargetWinName fixedSize=1, noEdit=1
		TitleBox description win=$ks_stopRunTargetWinName, pos={15,50},size={110,80},fixedSize=0
		TitleBox description win=$ks_stopRunTargetWinName, frame=0
		TitleBox description win=$ks_stopRunTargetWinName, font=$ksVM_defPanelFont, fSize=14
		TitleBox description win=$ks_stopRunTargetWinName, title="\JCMove 'Go NoGo' window\r\f01here\f00\rto \K(65535,0,0)\f01stop\f00\K(0,0,0) acquistion"
		
		locIndex = VoltaMafPCBridge#getWinPrefs(size, ks_stopRunTargetWinName)
		if (locIndex >= 0)
			MoveWindow /W=$ks_stopRunTargetWinName size.left, size.top, size.right, size.bottom
		endif
	endif
	return 1
End

// ***********************************************************************************************************************************
// *******************************      Voltammetry Bridge Panel and Panel Functions       **********************************
// ***********************************************************************************************************************************

// ********************       P A N E L   G E T T E R    A N D    S E T T E R S   ****************************************

static Function /S getStimMethod()
	if (!panelExists())
		return ""
	endif
	
	ControlInfo /W=$ks_bridgePanelName pop_method
	return S_Value
End

static Function getCurrentStim()
	if (!panelExists())
		return 0
	endif
	
	ControlInfo /W=$ks_bridgePanelName vd_currentStimulationRun
	return V_Value
End

static Function setStimNumber(number)
	Variable number
	if (!panelExists())
		return 0
	endif
	
	number = round(number)		// make sure we only have integers
	
	ValDisplay vd_currentStimulationRun, win=$ks_bridgePanelName, value= _NUM:number
	return 1
 End

static Function getMaxRepeats()
	if (!panelExists())
		return 1
	endif
	
	ControlInfo /W=$ks_bridgePanelName sv_repeats
	return V_Value
End

static Function setStatusMessage(message)
	String message
	
	if (!panelExists())
		return 0
	endif
	TitleBox tb_NumRunsStopped, win=$ks_bridgePanelName, title=message	
	return 1
End

static Function enableSettings(type)
	String type
	
	if (!panelExists())
		return 0
	endif
	
	SetVariable sv_repeats, win=$ks_bridgePanelName, disable=0
	SetVariable sv_interval, win=$ks_bridgePanelName, disable=0
	strswitch(type)
		case "stop":
			Button btn_pauseResume, win=$ks_bridgePanelName, disable=2
			break
		default:
			Button btn_pauseResume, win=$ks_bridgePanelName, disable=0
	endswitch
	return 0
End

static Function disableSettings(type)
	String type
	
	if (!panelExists())
		return 0
	endif
	
	SetVariable sv_repeats, win=$ks_bridgePanelName, disable=2
	SetVariable sv_interval, win=$ks_bridgePanelName, disable=2
	strswitch(type)
		case "start":
			Button btn_pauseResume, win=$ks_bridgePanelName, disable=0
			break
		default:		
			Button btn_pauseResume, win=$ks_bridgePanelName, disable=2
	endswitch
	return 0
End

static Function /S createVPanel()

	init()
	NVAR recording = $(getRecFlag())
	SVAR recText = $(getRecStr())
	String actionString = ""
	
	DoWindow /F $ks_bridgePanelName
	if (V_flag != 0)
		// window exists, brought it to front or it was hidden, so do nothing more 
		return ks_bridgePanelName
	endif
	
	NewPanel /N=$ks_bridgePanelName /W=(63,194,499,425) as ks_bridgePanelTitle

	actionString = ReplaceStringByKey(ks_btnAction, actionString, "start")
	Button btn_StartStop,pos={10,16},size={60,25},title="Start"
	Button btn_StartStop,userdata=actionString,fStyle=0,fSize=14
	Button btn_StartStop, proc=btnFunc_StartStop
	Button btn_StartStop, help={"start recording"}
	
	actionString = ReplaceStringByKey(ks_btnAction, actionString, "pause")
	Button btn_pauseResume,pos={76,19},size={51,21},disable=2,title="pause"
	Button btn_pauseResume,help={"pause recording"}, userdata=actionString
	Button btn_pauseResume, proc = btnFunc_PauseResume

	ValDisplay vd_recordLED,pos={134,17},size={25,25},frame=4
	ValDisplay vd_recordLED,limits={-1,1,0},barmisc={0,0},mode= 2,highColor=(65280,0,0),lowColor=(21760,21760,21760),zeroColor=(0,52224,0)
	ValDisplay vd_recordLED,value= #getRecFlag()
	TitleBox tb_recordingStatus,pos={167,23},size={53,13},frame=0, variable=recText
	TitleBox tb_recordingStatus,fStyle=1,fColor=(65280,0,0)
	
	SetVariable sv_interval,pos={249,6},size={171,16},bodyWidth=80,title="acquisition interval"
	SetVariable sv_interval,limits={1,inf,1},value= _NUM:k_defaultInterval
	SetVariable sv_repeats,pos={301,26},size={119,16},bodyWidth=80,title="repeats"
	SetVariable sv_repeats,help={"maximum number of repetitions"}
	SetVariable sv_repeats,limits={1,inf,1},value= _NUM:k_defaultRepeats
	ValDisplay vd_currentStimulationRun,pos={328,47},size={80,14},title="current:"
	ValDisplay vd_currentStimulationRun,limits={0,0,0},barmisc={0,1000}
	ValDisplay vd_currentStimulationRun,value= _NUM:0
	TitleBox tb_NumRunsStopped,pos={13,48},size={154,13},title="Click \"Start\" to start acquisition."
	TitleBox tb_NumRunsStopped,frame=0
	
	PopupMenu pop_method,pos={282,77},size={128,21},bodyWidth=90,title="method"
	PopupMenu pop_method,help={"choose a between fast cycling voltammetry or amperometry measurements"}
	PopupMenu pop_method,mode=2,popvalue="FSCV",value= #"\"FSCV;Amperometry;\""
	
	Button btn_addStimRow,pos={14,113},size={50,20},title="add"
	Button btn_addStimRow,help={"add a row to the stimulust table for the loop"}
	Button btn_deleteStimRow,pos={68,113},size={50,20},title="delete"
	Button btn_deleteStimRow,help={"add a row to the stimulust table for the loop"}

	ListBox lb_stimulusTable,pos={12,139},size={406,83},frame=2
	ListBox lb_stimulusTable,listWave=root:Packages:Voltammetry:VoltaMAFBridge:stimulusNameTable
	ListBox lb_stimulusTable,selWave=root:Packages:Voltammetry:VoltaMAFBridge:stimulusNameTableSel
	ListBox lb_stimulusTable,mode= 6
	
	SetDrawLayer UserBack
	SetDrawEnv linefgc= (17408,17408,17408)
	DrawLine 7,69,415,69
	SetDrawEnv linefgc= (30464,30464,30464)
	DrawLine 8,70,416,70
	SetDrawEnv linefgc= (30464,30464,30464)
	DrawLine 8,104,416,104
	SetDrawEnv linefgc= (17408,17408,17408)
	DrawLine 7,103,415,103
	
	return ks_bridgePanelName
End

Function btnFunc_StartStop(ba) : ButtonControl
	STRUCT WMButtonAction &ba

	switch( ba.eventCode )
		case 2: // mouse up
			String actionString = ""
			strswitch (StringByKey(ks_btnAction, ba.userData))
				case "start":
					actionString = ReplaceStringByKey(ks_btnAction, actionString, "stop")
					Button $ba.ctrlName win=$ba.win, title="STOP", fStyle=1, userdata=actionString
					
					ControlInfo /W=$ks_bridgePanelName sv_interval
					DataAcq_start(V_Value)
					break
				case "stop":
					actionString = ReplaceStringByKey(ks_btnAction, actionString, "start")
					Button $ba.ctrlName win=$ba.win, title="Start", fStyle=0, userdata=actionString
					actionString = ReplaceStringByKey(ks_btnAction, actionString, "pause")
					Button btn_pauseResume win=$ba.win, title="pause", fStyle=0, userdata=actionString
					
					DataAcq_stop()
					break
			endswitch
			break
		case -1: // control being killed
			break
	endswitch
	return 0
End

Function btnFunc_PauseResume(ba) : ButtonControl
	STRUCT WMButtonAction &ba
	
	switch (ba.eventCode)
		case 2: // mouse up
			String actionString = ""
			strswitch (StringByKey(ks_btnAction, ba.userData))
				case "pause":
					actionString = ReplaceStringByKey(ks_btnAction, actionString, "resume")
					Button $ba.ctrlName win=$ba.win, title="resume", userdata=actionString
					
					DataAcq_pause()				
					break
				case "resume":
					actionString = ReplaceStringByKey(ks_btnAction, actionString, "pause")
					Button $ba.ctrlName win=$ba.win, title="pause", userdata=actionString
					
					ControlInfo /W=$ks_bridgePanelName sv_interval				
					DataAcq_resume(V_value)
					break
			endswitch
			break
		case -1: // killed
			break
	endswitch
	return 0
End


// **************************     G E N E R A L   F U N C T I O N S     *********************************************

Function VM_mafBridgeIsLoaded()
	return 1		// just an identify function to look for if determining if this file is loaded
End

Function VM_loadProtocol([protName])
	String protName
	
	String errormsg = VM_getErrorStr("VM_loadProtocol")
	
	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()
	Variable ask = 0
	
	if (ParamIsDefault(protName) || strlen(protName) == 0)
		protName = ""
		ask = 1
	endif
	
	PathInfo VMStimulusProtocols
	if (V_flag == 0)
		setProtPath(useDefault=1)
	endif
	
	String protList = IndexedFile(VMStimulusProtocols, -1, ".pxp")
	Variable numProts = ItemsInList(protList)
	protList = SortList(protList, ";", 16)
	String protDisplay = ReplaceString(".pxp", protList, "")
	Prompt protName, "Enter a protocol name", popup, protDisplay
	
	if (ask)
		DoPrompt "Choose protocol", protName
		if (V_flag == 1)
			print "-> user canceled protocol load on ", date(), time()
			return 0
		endif
		
	else
		Variable isThere = WhichListItem(protName, protDisplay) >= 0
		if (!isThere)
			printf errormsg, "the named protocol " + protName + " does not yet exist"
			return 0
		endif
	endif
	
	SetDatafolder package
	LoadData /P=VMStimulusProtocols/O/Q protName + ".pxp"
	SetDatafolder cDF
End


// *************************    H O O K    F U N K T I O N S      **************************************

Function WH_setPrefWinLocation(wh)
	STRUCT WMWinHookStruct &wh
	
	Variable hookResult = 0
	strswitch (wh.eventName)
		case "resize":
		case "moved":
			STRUCT Rect size
			GetWindow $wh.winName wsize
			size.top = V_top
			size.left = V_left
			size.bottom = V_bottom
			size.right = V_right
			VoltaMafPCBridge#setWinPrefs(size, wh.winName)			
			break
		case "kill":
			SVAR prefs = $VoltaMafPCBridge#getWinPrefsStr()
			STRUCT VM_WinPrefs windows
			if (SVAR_Exists(prefs))
				StructGet /S windows, prefs
				VoltaMafPCBridge#saveWinPrefs(windows)
			endif
			SetWindow $wh.winName, hook(winPrefs) =  $""
			break
	endswitch
	return hookResult
End

Function WH_checkToStop(wh)
	STRUCT WMWinHookStruct &wh
	
	Variable hookResult = 0
	strswitch (wh.eventName)
		case "moved":
			DoWindow $ks_stopRunTargetWinName
			switch (V_flag)
				case 2:
					// window is hidden, doesn't work for us, so unhide it and continue with case 1
					DoWindow /HIDE=0 $ks_stopRunTargetWinName
				case 1:
					// we have window, figure out the location / dimension
					Variable overlap = 0
					
					STRUCT Rect target 
					STRUCT Rect local
					GetWindow $ks_stopRunTargetWinName wsize
					target.top = V_top
					target.bottom = V_bottom
					target.left = V_left
					target.right = V_right
					GetWindow $wh.winName wsize
					local.top = V_top
					local.bottom = V_bottom
					local.left = V_left
					local.right = V_right
									
					Variable bottomIn = local.bottom >= target.top && local.bottom <= target.bottom
					Variable topIn = local.top >= target.top && local.top <= target.bottom
					Variable leftIn = local.left >= target.left && local.left <= target.right
					Variable rightIn = local.right >= target.left && local.right <= target.right
					Variable isIn = local.top <= target.top && local.left <= target.left && local.bottom >= target.bottom && local.right >= target.right
					
					overlap = (topIn && leftIn) || (topIn && rightIn) || (bottomIn && leftIn) || (bottomIn && rightIn) || isIn
					
					NVAR stopRequest = $(getFlagVar(ks_runStopRequest))
					if (stopRequest != overlap)
						if (overlap)
							print "---| user requested acquisition stop on", time(), date()
							SB_play("acqStopRequested")
						else
							print "---> acquisition stop cleared on", time(), date()
							SB_play("acqStopCleared")
						endif
					endif
					stopRequest = overlap
					
					break
				case 0:
					// stop window doesn't exist, nothing more to do
					break
			endswitch
			break
	endswitch
	
	return hookResult
End
