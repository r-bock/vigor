# Vigor #

## What is it? ##

**Very much a work in progress!**

This collection of [Igor Pro](http://www.wavemetrics.com) scripts creates an analysis package for Voltammetry recordings (Voltammetry in Igor Pro). With the help of plugins, it can load pClamp (.abf) files and arranges the individual traces for easy analysis. Fast-scan cyclic voltammetry (FSCV) is a chemoelectrical method to identify and measure mostly monoamines like dopamine or serotonin in the brain. 

## Dependencies ##
The analysis part of this package depends on a few functions that are shared with other packages I wrote. The commonly shared functions have been bundled into their own package which is available from the [common functions](https://bitbucket.org/r-bock/common-igor-functions/src/master/) repository. You need to download and install those within the Igor Pro _User Procedure_ folder to make this package work. 

## Installation ##
Open your Igor Pro *User Procedures* folder either by navigating to the *Wavemetrics* folder from your user homes *Document* folder or by starting Igor Pro and clicking on the "Show Igor Pro User Files" in the "Help" menu. Make a new folder and call it "RB Minions" (or any other name you prefer) and open it.

If you are on Mac OS and prefer to use the command line (replace the \<version\> with the actual version number of your Igor Pro installation, e. g. 6, 7 or 8):

```shell
cd ~/Documents/Wavemetrics/Igor Pro <version> User Files/User Procedures/
mkdir RB\ Minions
cd RB\ Minions

git clone https://bitbucket.org/r-bock/vigor.git
```

or use a graphical version control application like [SourceTree](https://www.sourcetreeapp.com/) to install the files in above mentioned location.

For a new installation, create shortcuts or aliases (depending on the platform you are working with) for the _Voltammetry Package Loader.ipf_ file and move it into the **Igor Procedures** folder on the same level as the **User Procedures** folder.

**Important!** You will also **need** the [common igor functions](https://bitbucket.org/r-bock/common-igor-functions/src/master/) repository for this to function. Please follow the link and the installation instructions over there. 

Restart your Igor Pro application. To open the Voltammetry package, navigate to the **Analysis** menu and chose the **Load Voltammetry Package** in the 'Packages > RB Minions' submenu.

## Who to talk to? ##
Please direct comments and issues to 

* [Roland Bock](mailto:rolandbock@gmail.com?subject=[VIGOR]:%20Repository%20Question) 

