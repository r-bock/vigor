#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion = 6.22
#pragma version = 2.5
#include "GraphHelpers", version >= 3.9
#include "VM_Helpers", version >= 2.4

// created by Roland Bock, Feburary 2013
//
// version 2: FEB 2013
// version 1: FEB 2013
//
// I created this file to collect all the hook functions for easier editing and debugging.

static StrConstant ks_lastClickTime = "VM_HookLastClickTime"
static Constant k_DoubleClickTime = 25

static Function getLastClick()
	DFREF package = VoltHelpers#getPackage()
	NVAR lastclick = package:$ks_lastClickTime
	if (!NVAR_Exists(lastClick))
		Variable /G package:$ks_lastClickTime = -1
		NVAR lastclick = package:$ks_lastClickTime
	endif
	return lastclick
End

static Function setLastClick(number)
	Variable number
	
	DFREF package = VoltHelpers#getPackage()
	NVAR lastclick = package:$ks_lastClickTime
	if (!NVAR_exists(lastclick))
		getLastClick()
		NVAR lastclick = package:$ks_lastClickTime
	endif
	lastclick = number
	return lastclick
End

static Function isDoubleClick(number)
	Variable number
	
	DFREF package = VoltHelpers#getPackage()
	Variable lastclick = getLastClick()
	Variable diff = number - lastClick
	setLastClick(number)
	return (diff < k_DoubleClickTime)
End

static Function AfterFileOpenHook(refNum, file, pathName, type, creator, kind)
	Variable refNum, kind
	String file, pathName, type, creator
	
	if (VM_needsUpdate())
		VM_redisplayEpochPanel()
	else
		VM_EpochControlPanel()
	endif
	return 0
End	

//Function WH_equiDistantCursors(wh)
//	STRUCT WMWinHookStruct &wh
//	
//	Variable hookResult = 0
//	strswitch(wh.eventName)
//		case "cursormoved":
//			STRUCT VoltammetryPreferences prefs
//			VoltHelpers#loadPrefs(prefs)
//			strswitch(wh.cursorName)
//				case "G":
//					SetVariable svar_distanceInVolt, win=$wh.winName, value=_NUM:prefs.oxiVoltageDistance
//					STRUCT WMSetVariableAction var
//					var.win = wh.winName
//					var.eventCode = 2
//					var.dval = prefs.oxiVoltageDistance
//					svarFunc_VM_setCursorDistance(var)			
//					break
//				case "H":
//					String info = CsrInfo(G, wh.winName)
//					Variable gCsrExists = strlen(info) > 0
//					if (gCsrExists)
//						Wave theVoltage = CsrXWaveRef(H, wh.winName)
//						Variable theEnd = pnt2x(theVoltage, pcsr(H, wh.winName))
//						prefs.oxiEndTime = theEnd
//						prefs.oxiEndVoltage = theVoltage[pcsr(H, wh.winName)]
//						prefs.oxiVoltageDistance = prefs.oxiEndVoltage - theVoltage[pcsr(G, wh.winName)]
//
//						VoltHelpers#savePrefs(prefs)
//					endif
//				endswitch
//			break
//		case "kill":
//			SetWindow $wh.winName hook(equiDistance)=$""
//			break
//	endswitch
//	return hookResult
//End

Function WH_refineTransientFromCV(wh)
	STRUCT WMWinHookStruct &wh
	
	Variable hookResult = 0
	strswitch ( wh.eventName )
		case "cursormoved":
//			if (cmpstr(GetUserData(wh.winName, "", "status"), "new") == 0)
//				return hookResult
//			endif
			if (cmpstr(GetUserData(wh.winName, "", "update"), "no") == 0)
				return hookResult
			endif
			String transientGraph = GetUserData(wh.winName, "", "transientGraph")
			DoWindow $transientGraph
			if (V_flag == 0)
				return hookResult
			endif
			
			STRUCT VoltammetryPreferences prefs
			VoltHelpers#getTransPrefs(prefs)
			Variable start = 0
			Variable cCsrExists = strlen(CsrInfo(C, transientGraph)) > 0
			if (cCsrExists)
				start = pcsr(C, transientGraph) - prefs.baselinePoints
				start = start < 0 ? 0 : start
			endif
			
			Variable gExists = strlen(CsrInfo(G, wh.winName)) > 0
			Variable hExists = strlen(CsrInfo(H, wh.winName)) > 0
			
			strswitch (wh.cursorName)
				case "G":
//					SetVariable svar_distanceInVolt, win=$wh.winName, value=_NUM:prefs.oxiVoltageDistance
//					STRUCT WMSetVariableAction var
//					var.win = wh.winName
//					var.eventCode = 2
//					var.dval = prefs.oxiVoltageDistance
//					svarFunc_VM_setCursorDistance(var)			
					break
				case "H":
					if (gExists)
						VM_makeSingleTransient(VM_getRawDataWave(), mStartP=pcsr(G, wh.winName), mEndP=pcsr(H, wh.winName), tStartP=start, show=0, refine=1)
					endif
//					SetWindow $wh.winName userdata(status)= "refined"
					break
			endswitch
			
			NVAR front = $VM_getKeepCVFrontFlag()
			if (front)
				DoWindow /F $wh.winName
			endif
			break
		case "kill":
			// remove window hook
			SetWindow $wh.winName hook(refineTrans)=$""
			break

	endswitch
	return hookResult
End

Function WH_adjustVGFromCursor(wh)
	STRUCT WMWinHookStruct &wh
	
	Variable hookResult = 0
	strswitch(wh.eventName)
		case "cursormoved":
			NVAR noUpdate = $(VM_getNoUpdateFlag())
			if (noUpdate)
				return hookResult
			endif
			Variable uSc = strsearch(wh.winName, "_", Inf, 1)
			if (uSc < 0)
				// attached to wrong window
				return 0
			endif
			String graphType = (wh.winName)[uSc + 1, strlen(wh.winName)]
			String graphBaseName = (wh.winName)[0, uSc]
			String iGraph = graphBaseName + VoltHelpers#getVGImageExtension()
			String cvGraph = graphBaseName + VoltHelpers#getVGCVExtension()
			
			Variable gCsrExists = 0, hCsrExists = 0, eCsrExists = 0, fCsrExists = 0
			
			DoWindow $cvGraph
			if (V_flag)
				gCsrExists = strlen(CsrInfo(G, cvGraph)) > 0
				hCsrExists = strlen(CsrInfo(H, cvGraph)) > 0
			endif
			DoWindow $iGraph
			if (V_flag)
				eCsrExists = strlen(CsrInfo(E, iGraph)) > 0
				fCsrExists = strlen(CsrInfo(F, iGraph)) > 0
			endif
			
			Wave image = VoltHelpers#getImageVGFromGraph(theGraphName=iGraph)
			Wave cv = VoltHelpers#getCVTrace()
			
			STRUCT VoltammetryPreferences prefs
			VoltHelpers#getTransPrefs(prefs)
			STRUCT OxidationDetails ox
			ox = prefs.oxDetails
			
			String info = ""
			
			strswitch(graphType)
				case "cv":
					strswitch(wh.cursorName)
						case "G":
							NVAR theStart = $(VoltHelpers#getTransientStartPoint())
							Wave theVoltage = CsrXWaveRef(G, wh.winName)
							theStart = pcsr(G, wh.winName)
							ox.startPoint = theStart
							ox.startTime = pnt2x(theVoltage, ox.startPoint)
							ox.startVoltage = theVoltage[pcsr(G, wh.winName)]
							if (eCsrExists)
								Cursor /I /P /W=$(iGraph) E $(NameOfWave(image)) pcsr(E, iGraph),  pcsr(G, wh.winName)
							endif
							break
						case "H":
							NVAR theEnd = $(VoltHelpers#getTransientEndPoint())
							Wave theVoltage = CsrXWaveRef(H, wh.winName)
							theEnd = pcsr(H, wh.winName)
							ox.endPoint = theEnd
							ox.endTime = pnt2x(theVoltage, ox.endPoint)
							ox.endVoltage = theVoltage[pcsr(H, wh.winName)]
							ox.range = ox.endVoltage - ox.startVoltage
							if (fCsrExists)
								Cursor /I /P /W=$(iGraph) F $(NameOfWave(image)) pcsr(F, iGraph),  pcsr(H, wh.winName)
							endif
							break
					endswitch
					break
				case "img":
					Wave theVoltage = VM_getRampWave()
					strswitch(wh.cursorName)
						case "E":
							NVAR theStart = $(VoltHelpers#getTransientStartPoint())
							if (gCsrExists)
								theStart = qcsr(E, iGraph)
								Cursor /P /W=$(cvGraph) G $(NameOfWave(cv)) qcsr(E, (iGraph))
							else
								theStart = qcsr(E, iGraph)
							endif
							ox.startPoint = theStart
							ox.startTime = pnt2x(theVoltage, ox.startPoint)
							ox.startVoltage = qcsr(E, iGraph)
							break
						case "F":
							NVAR theEnd = $(VoltHelpers#getTransientEndPoint())
							if (hCsrExists)
								theEnd = qcsr(F, iGraph)
								Cursor /P /W=$(cvGraph) H $(NameOfWave(cv)) qcsr(F, (iGraph))
							else
								theStart = qcsr(F, iGraph)
							endif
							ox.endPoint = theEnd
							ox.endTime = pnt2x(theVoltage, ox.endPoint)
							ox.endVoltage = qcsr(F, iGraph)
							break
					endswitch
					break
			endswitch
			
			prefs.oxDetails = ox
			VoltHelpers#setTransPrefs(prefs)
			hookResult = 0
			break
		case "kill":
			// remove window hook
			SetWindow $wh.winName hook(baselines)=$""
			break
	endswitch
	return hookResult
End

Function WH_adjustTransientDataFromCsr(wh)
	STRUCT WMWinHookStruct &wh
	
	Variable hookResult = 0
	strswitch(wh.eventName)
		case "cursormoved":
			NVAR cFlag = $(VoltGUI#getCursorFlag())
			if (cFlag == kVM_Recorded)	
				// stop here, only set the cursors.
				return 1
			endif
			
			NVAR baseline = $(VoltHelpers#getBaselinePoints())
			NVAR peakDuration = $(VoltHelpers#getTransientPoints())
			
			
			Wave epochLocations = VM_getEpochWave()
			Make /N=(DimSize(epochLocations, 0)) /FREE peakstartpoints = epochLocations[p][%peakstartpoint]
			
			Variable cExists = strlen(CsrInfo(C, wh.winName)) > 0
			Variable dExists = strlen(CsrInfo(D, wh.winName)) > 0
			Variable startPoint, endPoint, recorded = 0
			Wave transient
			strswitch(wh.cursorName)
				case "C":
					startPoint = pcsr(C, wh.winName) - baseline
					Wave transient = CsrWaveRef(C, wh.winName)
					if (dExists)
						endPoint = pcsr(D, wh.winName)
						if (endPoint < startPoint)
							endPoint = startPoint + peakDuration
							if (endPoint >= numpnts(transient))
								endPoint = numpnts(transient) -1
							endif
						endif
					else
						endPoint = startPoint + peakDuration
					endif
					break
				case "D":
					endPoint = pcsr(D, wh.winName)
					if (cExists)
						startPoint = pcsr(C, wh.winName) - baseline
						Wave transient = CsrWaveRef(C, wh.winName)
						if (endPoint < startPoint)
							startPoint = endPoint - peakDuration
							if (startPoint < 0)
								startPoint = 0
							endif
						endif
					else
						// we can't do anything without a start point, so stop here
						return 0
					endif
					break
			endswitch
			if (!WaveExists(transient))
				// nothing more to do
				return 0
			endif
			
			Wave traceMatrix = VM_getDataTraces(startPoint, endPoint, baseline)
//			STRUCT VM_ControlChannels channels
//			VM_makeControlWaves(startPoint, endPoint, baseline, chanStruct=channels)
			Wave image = VM_makeImageVoltammogram(traceMatrix, blEnd=baseline)
			NVAR thePeakLoc = $(VoltHelpers#getPeakPointVar())
			String CVName = VM_makeCVPlot(VM_makeCVCurves(image, thePeakLoc - startPoint, numPoints=2))
			SetWindow $CVName userdata(transient) = GetWavesDatafolder(transient, 2)
			SetWindow $CVName userdata(transientGraph) = wh.winName
			Wave epochTransient = VoltHelpers#getEpochTransient(nums=endPoint - startPoint)
			epochTransient = transient[p + startPoint]
			SetScale d 0,0,"A", epochTransient
			NVAR stimulusPoint = $(VoltHelpers#getStimPoint())
			if (stimulusPoint >= 0)
				SetScale /P x -baseline * deltax(transient), deltax(transient), WaveUnits(transient, 0), epochTransient
			else
				SetScale /P x 0, deltax(transient), "s", epochTransient
			endif
			
			if (cmpstr(wh.cursorName, "C") == 0)
				// update the control traces from the matrix to match the current cursor position
				VM_updateControlTraces(pcsr(C, wh.winName))
			endif

			Variable uSc = strsearch(CVName, "_", Inf, 1)
			if (uSc < 0)
				// attached to wrong window
				return 0
			endif
			String graphBaseName = (CVName)[0, uSc]
			String iGraph = graphBaseName + VoltHelpers#getVGImageExtension()
			
			DoWindow $iGraph
			if (V_flag)
				Variable eExists = strlen(CsrInfo(E, iGraph)) > 0
				Variable fExists = strlen(CsrInfo(F, iGraph)) > 0
				
				if (eExists || fExists)				// only update if at least one cursor is on the graph
					STRUCT Point csr1
					STRUCT Point csr2
					STRUCT VoltammetryPreferences prefs
					VoltHelpers#getTransPrefs(prefs)
					
					if (eExists && fExists)
						csr1.v = qcsr(E, iGraph)
						csr2.v = qcsr(F, iGraph)
					else
						csr1.v = prefs.oxDetails.startPoint
						csr2.v = prefs.oxDetails.endPoint
					endif
					csr1.h = 0
					csr2.h = baseline
		
					VM_showImageVoltammogram(filter=1, csr1=csr1, csr2=csr2)
				endif
			endif
			hookResult = 0
			break
	endswitch
	
	return hookResult
End

Function WH_adjustCalibrationDataFromCsr(wh)
	STRUCT WMWinHookStruct &wh
	
	Variable hookResult = 0
	strswitch(wh.eventName)
		case "cursormoved":
			NVAR cFlag = $(VoltGUI#getCursorFlag())
			if (cFlag == kVM_Recorded)	
				// stop here, only set the cursors.
				return 1
			endif
			
			Wave cValues = $(getRGBByName("okker"))
			
			Variable baseline = str2num(GetUserData(wh.winName, "", "baseline"))
			NVAR peakDuration = $(VoltHelpers#getTransientPoints())

			Variable cCsrExists = strlen(CsrInfo(C, wh.winName)) > 0		// need to check existance, because a kill
			Variable dCsrExists = strlen(CsrInfo(D, wh.winName)) > 0		//   command triggers a cursormoved event
			Variable eCsrExists = strlen(CsrInfo(E, wh.winName)) > 0
			Variable fCsrExists = strlen(CsrInfo(F, wh.winName)) > 0

			Variable point = 0
			Wave transient
			
			strswitch(wh.cursorName)
				case "C":
					if (cCsrExists)
						Wave transient = CsrWaveRef(C, wh.winName)
						point = pcsr(C, wh.winName) - baseline
						Cursor /W=$(wh.winName) /P /H=2 /C=(cValues[0], cValues[1], cValues[2]) E transient, point
					endif
					break			
				case "E":
					if (eCsrExists)
						Wave transient = CsrWaveRef(E, wh.winName)
						point = pcsr(E, wh.winName) + baseline
						Cursor /W=$(wh.winName) /P /C=(cValues[0], cValues[1], cValues[2]) C transient, point
					endif
					break
				case "D":
					if (dCsrExists)
						getRGBByName("blue")
						Wave transient = CsrWaveRef(D, wh.winName)
						point = pcsr(D, wh.winName) + baseline
						Cursor /W=$(wh.winName) /P /H=2 /C=(cValues[0], cValues[1], cValues[2]) F transient, point
					endif
					break			
				case "F":
					if (fCsrExists)
						getRGBByName("blue")
						Wave transient = CsrWaveRef(F, wh.winName)
						point = pcsr(F, wh.winName) - baseline
						Cursor /W=$(wh.winName) /P /C=(cValues[0], cValues[1], cValues[2]) D transient, point
					endif				
					break
			endswitch


			Wave calibData = VM_getCalibDataTraces(pcsr(E, wh.winName), pcsr(C, wh.winName), pcsr(D, wh.winName), pcsr(F, wh.winName))
			Wave image = VM_makeImageVoltammogram(calibData, blEnd=baseline-1)


//			String CVName = VM_makeCVPlot(VM_makeCVCurves(image, peakInfo.peakPoint - pcsr(C, theHostName) + baseline, numPoints=2), csr=1, front=cvInFront, toMax=cvCsrToMax, tGraph=theHostName)
			NVAR thePeakLoc = $(VoltHelpers#getPeakPointVar())
			String CVName = VM_makeCVPlot(VM_makeCVCurves(image, baseline, numPoints = baseline, calibration = 1))
			SetWindow $CVName userdata(transient) = GetWavesDatafolder(transient, 2)
			SetWindow $CVName userdata(transientGraph) = wh.winName

			VM_showImageVoltammogram(filter=1, restricted = 1)

			hookResult = 0
			break
	
	endswitch
	
	return hookResult
End




Function WH_killXDisplayWave(wh)
	STRUCT WMWinHookStruct &wh
	
	Variable hookResult = 0
	strswitch ( wh.eventName )
		case "kill":
			DFREF package = VoltHelpers#getPackage()
			Wave xDisp = package:$ksVM_xTransientDisp 
			Execute /P/Q/Z "KillWaves /Z " +  GetWavesDataFolder(xDisp, 2)
			hookResult = 1
			break
	endswitch
	return hookResult
End

Function WH_killChannelAssignPanel(wh)
	STRUCT WMWinHookStruct &wh
	
	Variable hookResult = 0
	strswitch( wh.eventName )
		case "killVote":
			Wave assignSel = $GetUserData(wh.winName, "", "assignsel")
			if (!VM_hasDataAssigned(assignSel))
				hookResult = 2
				DoAlert 0, "You need to assign one data channel!"
			else
				STRUCT WMButtonAction bs
				bs.win = wh.winName
				bs.eventCode = 2
				btnFunc_VM_doneChanAssignCheck(bs)
			endif
			break
	endswitch
	return hookResult
End

Function WH_setVGColorScale(wh)
	STRUCT WMWinHookStruct &wh
	
	Variable hookResult = 0
	strswitch (wh.eventName )
		case "MouseDown":
			if (isDoubleClick(wh.ticks))
				VM_setVGColorScaleP(wh.winName)
				
				
			endif
			break
	
	endswitch
End

Function WH_adjustRampDesigner(wh)
	STRUCT WMWinHookStruct &wh
	
	Variable hookResult = 0
	strswitch(wh.eventName)
		case "resize":
//		case "moved":
			STRUCT RampDesignerWin guiVal
			String str = GetUserData(wh.winName, "", "")
			StructGet /S guiVal, str

			guiVal.winWidth = wh.winRect.right - wh.winRect.left
			guiVal.winHeight = wh.winRect.bottom - wh.winRect.top

			Variable move = 0
			if (guiVal.winWidth < guiVal.minWinWidth)
				guiVal.winWidth = guiVal.minWinWidth
				move = 1
			endif
			if (guiVal.winHeight < guiVal.minWinHeight)
				guiVal.winHeight = guiVal.minWinHeight
				move = 1
			endif
			if (move)
				Variable /C winSize = moveWindowOnScreen(wh.winName, location="center", minWidth=guiVal.winWidth, minHeight=guiVal.winHeight)
				guiVal.winHeight = imag(winSize)
				guiVal.winWidth = real(winSize)
			endif	

			StructPut /S guiVal, str
			SetWindow $wh.winName, userdata = str
			
			VMRamps#createCntrls4RampDesigner(panelName = wh.winName, resize = 1)
			break
		case "kill":
			SetWindow $wh.winName, hook(winChange) = $""
			break
	endswitch
	return hookResult
End

