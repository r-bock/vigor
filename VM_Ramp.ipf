#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion = 6.22
#pragma version = 1.6
#pragma ModuleName = VMRamps
#include "VM_Helpers"
#include "VM_hookFunctions"

// by Roland Bock, January 2013
//    version 1.6: MAY 2017 - added a function to create a color coded wave for the CV traces to highlight the 
//									oxidation and reduction phase of the CV trace, extended this to the ramp trace;
//                        added 2 menu items to apply colorization to top graph
//    version 1.5: APR 2017 - added the analyzeRamp function that extracts some basic features of a given ramp
//									that has not been designed with the ramp designer.
//    version 1.4: MAR 2017 - the ramp creator can now take multiple peaks also going into the negative
//                          -  still need to fix some display issues during resizing
//    version 1.3: FEB 2016 - modified rampCreator function to automatically determine the direction
//                            of the voltage change based on the next voltage value.
//    version 1.2: MAR 2016 - changed ramp designer to look for either the oxidation or reduction voltage
//                            enabling the potential use for serotonin detection
// ** version 1: FEB 2013
//
// The ramp creation seems to become more complicated, so that it required the creation of an 
// extra file


#if Exists("PanelResolution") != 3
static Function PanelResolution(wName) 		// compatibiltiy with Igor 7
	String wName
	return 72
End
#endif

// ********************         C O N S T A N T S       *******************************

static Constant kMaxRamps = 20

static StrConstant ksRDGName = "DesignerGraph"
static StrConstant ksDesignerPanelName = "RampDesignerCtrlPanel"
static StrConstant ksDesignerPanelTitle = "Ramp Designer"

static StrConstant ksRampFolder = "Ramps"
static StrConstant ksRampFile = "Ramps.bin"
static StrConstant ksRampStr = "CurrentRampStructure"
static StrConstant ksRampName = "CurrentRampName"
static StrConstant ksModRampStr = "ModifiedRampStructure"
// static StrConstant ksRampPrefsFile = "Ramps.bin"
static StrConstant ksRIndexStr = "RampIndex"
static StrConstant ksRampList = "RampList"
static StrConstant ksRampKeyList = "RampKeyList"


static Strconstant ksVoltageDisplay = "Ramp_DisplayScale"
static Strconstant ksVoltageDisplayPoints = "Ramp_DisplayPoints"
static Strconstant ksRampColorWave = "Ramp_colorIndex"

// default values
StrConstant ksVM_SubstanceOxidations = "dopamine:600,100;serotonin:600,100;norepinephrine:600,100;" //< substance oxydation values in mV
StrConstant ksVM_SubOxidationRanges = "dopamine:200;serotonin:200;norepinephrine:200;" //< search range of the oxidation values in mV

// wave names
StrConstant ksVM_VoltageRamp = "CVRamp"

// list keys
Strconstant ksVM_RampKey = "RAMP"						//< name for the ramp (ie. dopamine)
Strconstant ksVM_RampStartPKey = "STARTPOINT"		//< start point of the ramp
Strconstant ksVM_RampEndPKey = "ENDPOINT"			//< end point of the ramp
Strconstant ksVM_RampMaxPKey = "MAXPOINT"			//< point of the maximum
Strconstant ksVM_RampMinPKey = "MINPOINT"			//< point of the minimum, if it exists
Strconstant ksVM_RampStruct = "FROMSTRUCT"			//< indicator if the ramp has been created from the structure
Strconstant ksVM_RangeStartP = "RANGESTART"			//< start point of the voltage range where to look for the data
Strconstant ksVM_RangeEndP = "RANGEEND"				//< end point for the voltage range


// ********************     S T R U C T U R E S       ***********************

Structure RampDesignerWin
	uint32 winHeight
	uint32 winWidth
	uint32 ctrlHeight
	uint32 ctrlWidth
	uint32 minCtrlHeight
	uint32 minCtrlWidth
	uint32 minWinHeight
	uint32 minWinWidth

	STRUCT RampValues ramp
	
	char traceName[32]
	char panelName[32]
	char graphName[32]
EndStructure

Structure RampIndex
	int32 numRamps
	int32 nextID
	STRUCT RampRecord ramp[kMaxRamps]
EndStructure

Structure RampRecord
	int32	recordID
	char name[100]
EndStructure

Structure RampValues
	uint32 id
	uint32 numPoints
	double xScale

	STRUCT OxidationDetails oxDetails
	
	int16 lastGroupNum
	double startV[6]
	double holdV[6]
	double VRate[6]
	
	char name[100]
EndStructure

//! Structure storing the values for the oxidation points for the particular substance
//-
Structure OxidationDetails
	int16 oxidation					//< looking for oxidation point (default: 1) or reduction (0)
	double voltage						//< voltage at the oxidation point
	double range						//< indicate the range around the oxidation point for average
	double startVoltage
	double endVoltage
	int32 startPoint
	int32 endPoint
	double startTime
	double endTime
EndStructure

static Structure RampScalingGroup
	Variable number
	String groupName
	String voltage
	String voltLbl
	String holdVolt
	String holdLbl
	String voltRate
	String voltRateLbl
	String btnAName
	String btnDName
EndStructure

// *******************      H O O K    F U N C T I O N S        ************************

//

// This function makes sure acquisition is stopped when Igor is quitting.
static Function IgorBeforeQuitHook(unsavedExp, unsavedNotebooks, unsavedProcedures)
	Variable unsavedExp, unsavedNotebooks, unsavedProcedures

//	saveRampIndex()
	
	return 0	// Proceed with normal quit process
End

static Function BeforeExperimentSaveHook(refNum, file, path, type, creator, kind)
	Variable refNum
	String file, path, type, creator, kind
	
//	saveRampIndex()
	
	return 0
End

// *******************      M E N U    E N T R I E S         ************************

Menu "Voltammetry"
	SubMenu "Ramps"
		"Current Ramp", VMRamps#VM_showCurrentRamp()
		"Ramp Designer", VM_makeRDCtrlPanel()
		SubMenu "Ramp Preferences"
			"Show Ramp File", VMRamps#getRampPrefLoc(show=1)
		End
		"-"
		"Color CV Trace", VM_highlightCVTracePhases("")
		"Show colored Ramp", VM_highlightCVTracePhases(VMRamps#VM_showCurrentRamp())
	End
End


// ********************     S T A T I C     F U N C T I O N S   ***********************

static Function /DF getRampDF()
	
	DFREF cDF = GetDatafolderDFR()
	DFREF package = VoltHelpers#getPackage()
	NewDataFolder /O/S package:$ksRampFolder
	DFREF ramps = GetDatafolderDFR()
	SetDatafolder cDF
	return ramps
End
	 
static Function /S getRampPrefLoc([show])
	Variable show
	
	show = ParamIsDefault(show) || show < 1 ? 0 : 1
	
	String fullPath = SpecialDirPath("Packages", 0, 0, 0)
	fullPath += VM_getPackagePrefsName() + ":"
	
	PathInfo VM_preferencesPath
	if (!V_flag)
		NewPath /O/Q/Z VM_preferencesPath, fullPath
	endif
	
	if (show)
		PathInfo /SHOW VM_preferencesPath
	endif
	
	return fullPath
End

static Function SaveRamp([reset])
	Variable reset
	
	reset = ParamIsDefault(reset) || reset < 1 ? 0 : 1
	
	Variable recordID = getCurrentRecordID()
	if (recordID < 1)
		// something went wrong, don't save
		return 0
	endif
	
	STRUCT RampValues rv
	
	if (reset)	
		SavePackagePreferences /KILL VM_getPackagePrefsName(), ksRampFile, recordID, rv
	else		
		saveRampIndex()
		getRampStruct(rv)
	
		SavePackagePreferences VM_getPackagePrefsName(), ksRampFile, recordID, rv
	endif
	return 1	
End

static Function LoadRamp(rv, name, [noSave])
	STRUCT RampValues &rv
	String name
	Variable noSave
	
	noSave = ParamIsDefault(noSave) || noSave < 1 ? 0 : 1
	
	STRUCT RampIndex ri
	SVAR riStr = getRampDF():$ksRIndexStr
	if (!SVAR_Exists(riStr))
		loadRampIndex(ri)
	else
		StructGet /S ri, riStr
	endif
	
	STRUCT RampRecord rr
	
	Variable index = 0
	for (index = 0; index < ri.numRamps; index += 1)
		if (cmpstr(name, ri.ramp[index].name) == 0)
			rr = ri.ramp[index]
			break
		endif
	endfor
	
	if (rr.recordID > 0)
		// found it
		LoadPackagePreferences /MIS=1 VM_getPackagePrefsName(), ksRampFile, rr.recordID, rv
		if (V_flag != 0 || V_bytesRead == 0)
			SaveRamp(reset=1)
			defaultDopamineStruct(rv)
		else
			if (noSave)
				setCurrentRampName(rv.name)
			endif
		endif
	else
		// did not find it
		rv.lastGroupNum = 1
		VM_makeRDCtrlPanel()
	endif
	
	if (!noSave)
		saveRampStruct(rv)
	endif
End

static Function /S getRampIndexStr()
	DFREF structFolder = getRampDF()
	SVAR riStr = structFolder:$ksRIndexStr
	if (!SVAR_Exists(riStr))
		String /G structFolder:$ksRIndexStr = ""
	endif
	return GetDatafolder(1, structFolder) + ksRIndexStr
End	
			
static Function loadRampIndex(ri)
	STRUCT RampIndex &ri

	LoadPackagePreferences /MIS=1 VM_getPackagePrefsName(), ksRampFile, 0, ri
	if (V_flag != 0 || V_bytesRead == 0 || ri.numRamps == 0)
		// check for correct load
		defaultRampIndex(ri)
	endif
	
//	DFREF structFolder = getRampDF()
	SVAR riStr = $(getRampIndexStr())
//	SVAR riStr = structFolder:$ksRIndexStr
//	if (!SVAR_Exists(riStr))
//		String /G structFolder:$ksRIndexStr = ""
//		SVAR riStr = structFolder:$ksRIndexStr
//	endif
	
	StructPut /S ri, riStr
	updateRampList(ri)
End	

static Function saveRampIndex()
	DFREF structFolder = getRampDF()
	SVAR riStr = structFolder:$ksRIndexStr
	if (strlen(riStr) == 0)
		return 0
	endif

	STRUCT RampIndex ri
	StructGet /S ri, riStr
	
	SavePackagePreferences VM_getPackagePrefsName(), ksRampFile, 0, ri
End

static Function defaultRampIndex(ri)
	STRUCT RampIndex &ri
	defaultRampRecord(ri.ramp[0])
	ri.numRamps = 1
	ri.nextID = 2
End

static Function defaultRampRecord(rr)
	STRUCT RampRecord &rr
	rr.recordID = 1
	rr.name = "dopamine"
End

static Function getNextRecordID(ri)
	STRUCT RampIndex &ri
	
	Make /N=(kMaxRamps) /FREE existingIDs
	existingIDs = ri.ramp[p].recordID
	Variable highest = WaveMax(existingIDs)
	if (highest >= kMaxRamps)
		// we used all record ids, now check if we deleted some
		existingIDs = existingIDs[p] == 0 ? kMaxRamps + 1 : existingIDs[p]
		Sort existingIDs, existingIDs
		Extract /FREE existingIDs, missingIDs, existingIDs != p + 1
	else
		return highest + 1
	endif

	if (numpnts(missingIDs) == 0)
		return -1				// we have no missing IDs 
	else
		return missingIDs[0]	// next available ID is in the first location
	endif
	
End

static Function addRampToIndex(ramp)
	STRUCT RampValues &ramp
	
	STRUCT RampIndex ri
	SVAR riStr = $(getRampIndexStr())
	if (strlen(riStr) == 0)
		loadRampIndex(ri)
	else
		StructGet /S ri, riStr
	endif
	
	STRUCT RampRecord rr	
	rr.name = ramp.name
	rr.recordID = ri.nextID
	
	ramp.id = rr.recordID
	
	ri.ramp[ri.numRamps] = rr
	ri.numRamps += 1
	ri.nextID = getNextRecordID(ri)			
	
	StructPut /S ri, riStr
	updateRampList(ri)
End

static Function deleteRampFromIndex(name)
	String name
	
End

static Function getCurrentRecordID([name])
	String name
	
	if (ParamIsDefault(name) || strlen(name) == 0)
		name = getCurrentRampName()
	endif
	
	STRUCT RampIndex ri
	SVAR riStr = getRampDF():$ksRIndexStr
	if (!SVAR_Exists(riStr))
		loadRampIndex(ri)
	else
		StructGet /S ri, riStr
	endif
	
	STRUCT RampRecord rr
	Variable index = 0
	for (index = 0; index < ri.numRamps; index += 1)
		if (cmpstr(name, ri.ramp[index].name) == 0)
			return ri.ramp[index].recordID
		endif
	endfor
	return -1
End

static Function getRampStruct(rv)
	STRUCT RampValues &rv
	
	String name = getCurrentRampName()
	DFREF structFolder = getRampDF()
	SVAR rampStr = structFolder:$ksRampStr
	if (!SVAR_Exists(rampStr) || strlen(rampStr) == 0)
		LoadRamp(rv, name)
	else
		StructGet /S rv, rampStr	
	endif	
End


static Function saveRampStruct(rv)
	STRUCT RampValues &rv
	
	DFREF structFolder = getRampDF()
	SVAR rampStr = structFolder:$ksRampStr
	if (!SVAR_Exists(rampStr))
		String /G structFolder:$ksRampStr = ""
		SVAR rampStr = structFolder:$ksRampStr
	endif
	
	StructPut /S rv, rampStr
	setCurrentRampName(rv.name)

	// now make sure we have the oxidation details also
	//    in our preferences
	STRUCT VoltammetryPreferences prefs
	VoltHelpers#getTransPrefs(prefs)
	prefs.oxDetails = rv.oxDetails
	VoltHelpers#setTransPrefs(prefs)
End

static Function /S getRampListString()
	return GetDatafolder(1, getRampDF()) + ksRampList
End

static Function /S getRampKeyListString()
	return GetDatafolder(1, getRampDF()) + ksRampKeyList
End

static Function updateRampIndex()
	STRUCT RampIndex ri
	loadRampIndex(ri)
End

static Function /S updateRampList(ri)
	STRUCT RampIndex &ri
	
	DFREF path = getRampDF()
	String /G path:$ksRampKeyList = ""
	SVAR keyList = path:$ksRampKeyList
	String /G path:$ksRampList = ""
	SVAR rampList = path:$ksRampList
	
	STRUCT RampRecord rr
	
	Variable index = 0
	for (index = 0; index < ri.numRamps; index += 1)
		rr = ri.ramp[index]
		keyList = ReplaceNumberByKey(rr.name, keyList, rr.recordID)
		rampList = AddListItem(rr.name, rampList)
	endfor
	
	rampList = SortList(rampList)
	return rampList
End


static Function /S getCurrentRampName()
	DFREF structFolder = getRampDF()
	SVAR rampName = structFolder:$ksRampName
	if (!SVAR_Exists(rampName))
		String /G structFolder:$ksRampName = "dopamine"		// default to dopamine
		SVAR rampName = structFolder:$ksRampName
	endif
	return rampName
End

static Function setCurrentRampName(name)
	String name
	
	String errormsg = VM_getErrorStr("setCurrentRampName [VMRamps]")
	if (strlen(name) == 0)
		printf errormsg, "the ramp name can not be empty"
		return 0
	endif
	
	DFREF structFolder = getRampDF()
	SVAR rampName = structFolder:$ksRampName
	if (!SVAR_Exists(rampName))
		String /G structFolder:$ksRampName = name
		SVAR rampName = structFolder:$ksRampName
	else
		rampName = name
	endif
	
	return 1
End	


static Function /WAVE getCurrentRamp()
	DFREF package = VoltHelpers#getPackage()
	Wave currRamp = package:$ksVM_VoltageRamp	
	return currRamp
End

static Function setCurrentRamp(ramp, [rNote])
	Wave ramp
	String rNote
	
	if (!WaveExists(ramp))
		return 0
	endif

	DFREF package = VoltHelpers#getPackage()
	Duplicate /O ramp, package:$ksVM_VoltageRamp /WAVE=nRamp
	if (!ParamIsDefault(rNote) && strlen(rNote) > 0)
		Note /K nRamp, rNote
	endif
	return 1
End

//! This function analyzes the ramp for some features and sets specific values needed
// for analysis.
// @param ramp: 		ramp wave reference
// @return a string with the set values that can be added as a wave note
//-
static Function /S analyzeRamp(ramp, [type])
	Wave ramp
	String type
	
	String theNote = ""
	if (!WaveExists(ramp))
		return theNote
	endif
	
	if (mean(ramp) == ramp[0])
		// we just have a flat line, no ramp
		return theNote
	endif
	
	if (ParamIsDefault(type) || strlen(type) == 0)
		type = "dopamine"
	endif
	
	theNote = note(ramp)
	theNote = ReplaceStringByKey(ksVM_RampKey, theNote, type)
	theNote = ReplaceStringByKey(ksVM_RampStruct, theNote, "no")
	
	Variable startP = 0, endP = 0
	
	Variable startValue = ramp[0]
	Extract /FREE /INDX /O ramp, holdIndx, ramp == startValue
	if (numpnts(holdIndx) > 0 && numpnts(holdIndx) < 3)
		// we only have max 2 points, defining start and end of the ramp
		startP = holdIndx[0]
		if (numpnts(holdIndx) == 1)
			endP = numpnts(ramp) - 1
		else
			endP = holdIndx[numpnts(holdIndx) - 1]
		endif
	else
		// we have hold periods or multiple crossings
		Differentiate /METH=1 /EP=1 holdIndx /D=diff 
		Extract /FREE /INDX /O diff, changes, diff > 1
		
		if (numpnts(changes) == 0)
			// we only have one continuous holding potential at the beginning
			startP = holdIndx[numpnts(diff)]
			endP = numpnts(ramp) - 1
		else
			startP = holdIndx[changes[0]]
			Extract /FREE /INDX /O diff, changes, diff > 20
			if (numpnts(changes) == 0)
				endP = numpnts(ramp) -1
			else
				endP = holdIndx[changes[0] + 1]
				endP = endP >= numpnts(ramp) ? numpnts(ramp) - 1 : endP
			endif
		endif
		
		KillWaves /Z diff
	endif
	
	theNote = ReplaceNumberByKey(ksVM_RampStartPKey, theNote, startP)
	theNote = ReplaceNumberByKey(ksVM_RampEndPKey, theNote, endP)
	
	WaveStats /C=1/W/M=1/R=[startP, endP]/Q/Z ramp
	Wave M_WaveStats
	
	theNote = ReplaceNumberByKey(ksVM_RampMaxPKey, theNote, x2pnt(ramp, M_WaveStats[%maxLoc]))
	
	if (M_WaveStats[%min] < startValue)
		theNote = ReplaceNumberBykey(ksVM_RampMinPKey, theNote, x2pnt(ramp, M_WaveStats[%minLoc]))
	endif
	
	Variable oxyVolt = str2num(StringFromList(0, StringByKey(type, ksVM_SubstanceOxidations), ","))
	Variable oxyRange = str2num(StringFromList(1, StringByKey(type, ksVM_SubstanceOxidations), ","))
	if (numtype(oxyVolt) > 0)
		oxyVolt = 0.6
		oxyRange = 0.1
	else
		oxyVolt *= 1e-3			// voltage and range are given in mV, convert to V
		oxyRange *= 1e-3	
	endif
	
	FindLevel /P /EDGE=1/Q ramp, oxyVolt - oxyRange
	if (V_flag == 0)
		// level crossing was found
		theNote = ReplaceNumberByKey(ksVM_RangeStartP, theNote, ceil(V_LevelX))
	endif
	FindLevel /P /EDGE=1/Q ramp, oxyVolt + oxyRange
	if (V_flag == 0)
		theNote = ReplaceNumberByKey(ksVM_RangeEndP, theNote, floor(V_LevelX))
	endif
		
	return theNote
End

static Function getRampScaleGroup(rsg, number)
	STRUCT RampScalingGroup &rsg
	Variable number

	rsg.number = number
	rsg.groupName = "voltGroup_" + num2str(number)
	rsg.voltage = "voltage_" + num2str(number)
	rsg.voltLbl = "voltageLbl_" + num2str(number)
	rsg.holdVolt = "holdVoltage_" + num2str(number)
	rsg.holdLbl = "holdVoltLbl_" + num2str(number)
	rsg.voltRate = "voltRate_" + num2str(number)
	rsg.voltRateLbl = "voltRateLbl_" + num2str(number)
	rsg.btnAName = "addVoltage_" + num2str(number)
	rsg.btnDName = "delVoltage_" + num2str(number)
End

// ************************************************************************************
// *******            R A M P    C R E A T I O N     F U N C T I O N 
// ************************************************************************************

static Function rampCreator(ramp, rv)
	Wave ramp
	STRUCT RampValues &rv
	
	if (!WaveExists(ramp))
		return 0
	endif
	
	STRUCT OxidationDetails od 
	od = rv.oxDetails
	
	String theNote = ""
	
	Variable index = 0, startP = 0, endP = 0, numHoldPnts = 0, up = 1
	
	Redimension /N=(rv.numPoints) ramp
	SetScale /P x 0, rv.xScale, "s", ramp
	SetScale d 0, 0, "V", ramp

	theNote = ReplaceStringByKey(ksVM_RampKey, theNote, rv.name)
	theNote = ReplaceStringByKey(ksVM_RampStruct, theNote, "yes")
	
	for (index = 0; index <= rv.lastGroupNum; index += 1)
		Variable changeFactor = rv.VRate[index] * rv.xScale
		numHoldPnts = round(rv.holdV[index] / rv.xScale)
		if (numHoldPnts > rv.numPoints)
			return -1			// holding time mismatch with wave length
		endif
		
		if (index == 0)
			if (rv.holdV[index] > 0)
				ramp[0, numHoldPnts - 1] = rv.startV[index]
				startP = numHoldPnts  - 1
			else
				ramp[startP] = rv.startV[index]
			endif
			
			theNote = ReplaceNumberByKey(ksVM_RampStartPKey, theNote, startP)
		else
			FindLevel /Q/P/R=[startP,] ramp, rv.startV[index]
			if (!V_flag)
				// level was found
				startP = round(V_LevelX)
				if (rv.holdV[index] > 0)
					endP = startP + numHoldPnts
					ramp[startP + 1, endP] = rv.startV[index]
					startP = endP
				endif
			else
				// level was not found
				return -2
			endif
		endif

		up = index == rv.lastGroupNum ? rv.startV[0] > rv.startV[index] : rv.startV[index] < rv.startV[index + 1]
		if (up)
			ramp[startP + 1,] = ramp[p - 1] + changeFactor
		else
			ramp[startP + 1,] = ramp[p - 1] - changeFactor
		endif
				
		if (index == rv.lastGroupNum)
			FindLevel /Q/P/R=[startP,] ramp, rv.startV[0]
			if (V_flag)
				return -1
			endif
			startP = round(V_LevelX)
			ramp[startP + 1,] = rv.startV[0]
			theNote = ReplaceNumberByKey(ksVM_RampEndPKey, theNote, startP)
		endif
	endfor

	WaveStats /C=1/W/Q/M=1 ramp
	Wave M_WaveStats
	Variable maxLoc = M_WaveStats[%maxRowLoc] 			// point of the maximum
	theNote = ReplaceNumberbyKey(ksVM_RampMaxPKey, theNote, maxLoc)

	Variable minLoc = 0
	if (M_WaveStats[%min] < ramp[0])
		minLoc = M_WaveStats[%minRowLoc]
		theNote = ReplaceNumberbyKey(ksVM_RampMinPKey, theNote, minLoc)	
	endif
	
	Note /K ramp, theNote
	
	// now set the oxidation details.
	if (od.oxidation)
		// we are looking for the oxidation peak, assuming that the ramp will follow through the oxidation
		//    point from low voltage to high voltage
		od.startVoltage = od.voltage - (od.range / 2)
		od.endVoltage = od.startVoltage + od.range
	else
		// we are looking for the reduction peak, assuming the voltage will go from high to low through the
		//    reduction voltage
		od.startVoltage = od.voltage + (od.range / 2)
		od.endVoltage = od.startVoltage - od.range
	endif
	
	if (od.oxidation)
		// find the level for the oxidation point (- increasing voltage)
		FindLevel /Q/P/EDGE=1 ramp, od.startVoltage
	else
		// otherwise find the level for the reduction point (- decreasing voltage)
		FindLevel /Q/P/EDGE=2 ramp, od.startVoltage
	endif
	
	if (!V_flag)
		// level was found
		od.startPoint = V_LevelX
		od.startTime = pnt2x(ramp, od.startPoint)
	endif
	
	if (od.oxidation)
		FindLevel /Q/P/EDGE=1/R=[od.startPoint, numpnts(ramp)-1] ramp, od.endVoltage
	else
		FindLevel /Q/P/EDGE=2/R=[od.startPoint, numpnts(ramp)-1] ramp, od.endVoltage
	endif
	
	if (!V_flag)
		// level was found
		od.endPoint = V_LevelX
		od.endTime = pnt2x(ramp, od.endPoint)
	endif
	
	rv.oxDetails = od
	return 1
End

static Function makeDisplayScaleFromRamp()
	STRUCT RampValues rv
	Variable loadResult = getRampStruct(rv)
	DFREF package = VoltHelpers#getPackage()
	
	Make /N=0 /T /O package:$ksVoltageDisplay /WAVE=theDisplay
	Make /N=0 /O package:$ksVoltageDisplayPoints /WAVE=thePoints

	if (!loadResult)
		// we don't have a current ramp structure
		return 0
	endif
	
	Variable index = 0, startX = 0, voltDiff = 0, xDist = 0
	for (index = 0; index <= rv.lastGroupNum; index += 1)
		Variable changeFactor = rv.VRate[index] * rv.xScale
		theDisplay[numpnts(theDisplay)] = {num2str(rv.startV[index])}
		
		if (index > 0)
			voltDiff = abs(rv.startV[index] - rv.startV[index - 1])
			xDist = voltDiff / rv.VRate[index - 1]
			startX += xDist
		endif

		thePoints[numpnts(thePoints)] = {startX}			
			
		if (rv.holdV[index] > 0)
			startX += rv.holdV[index]
			theDisplay[numpnts(theDisplay)] = {num2str(rv.startV[index])}
			thePoints[numpnts(thePoints)] = {startX}			
		endif
	endfor
	
	// now set the return ramp of the last group
	voltDiff = rv.startV[rv.lastGroupNum] - rv.startV[0]
	xDist = voltDiff / rv.VRate[rv.lastGroupNum]
	startX += xDist
	theDisplay[numpnts(theDisplay)] = {num2str(rv.startV[0])}
	thePoints[numpnts(thePoints)] = {startX}
	
	Variable lastX = rv.numPoints * rv.xScale
	if (startX < lastX)
		theDisplay[numpnts(theDisplay)] = {num2str(rv.startV[0])}
		thePoints[numpnts(thePoints)] = {lastX - rv.xScale}
	endif
End

static Function /WAVE makeColorCVWaveFromRamp([colorList])
	String colorList
	
	String oxColor = "lightBlue"
	String reduxColor = "lightRed"

	if (!ParamIsDefault(colorList) && ItemsInList(colorList) >= 2)
		oxColor = StringFromList(0, colorList)
		reduxColor = StringFromList(1, colorList)
	endif

	Wave ramp = VM_getRampWave()
	String rampInfo = note(ramp)
	Variable rampStart = NumberByKey(ksVM_RampStartPKey, rampInfo)
	Variable rampEnd = NumberByKey(ksVM_RampEndPKey, rampInfo)
	Variable rampMax = NumberByKey(ksVM_RampMaxPKey, rampInfo)
	Variable rampMin = NumberByKey(ksVM_RampMinPKey, rampInfo)
	
	if (numtype(rampMin) == 0 && rampMin < rampEnd)
		rampEnd = rampMin
	endif
		
	DFREF package = VoltHelpers#getPackage()
	Duplicate /O ramp, package:$ksRampColorWave /WAVE=colorIndex
	colorIndex = 0
	colorIndex[rampStart, rampMax] = getColorNameIndex(oxColor)
	colorIndex[rampMax + 1, rampEnd] = getColorNameIndex(reduxColor)
	
	Wave colorWave = $(getColorValueWave())
	
	return colorIndex
End

static Function labelMeasurePoint(guiVal)
	STRUCT RampDesignerWin &guiVal
	
	String name = "measurePoint"
	String graphName = guiVal.panelName + "#" + guiVal.graphName
	Wave ramp = TraceNameToWaveRef(graphName, guiVal.traceName)
	if (!WaveExists(ramp))
		return 0
	endif

	STRUCT OxidationDetails ox
	ox = guiVal.ramp.oxDetails
	
	Variable mVal = ox.startTime + ((ox.endTime - ox.startTime) / 2)
	
	String type = "oxidation"
	if (!ox.oxidation)
		type = "reduction"
	endif

	String lblTextPattern = "\\Zr075\\f01%s\\f00\rvolt.: %.2W1PV\rtime: %g ms"
	String lblText = 	""
	
	sprintf lblText, lblTextPattern, type, ox.voltage, mVal 
	
	String annotations = AnnotationList(graphName)

	if (WhichListItem(name, annotations) < 0)
		// annotation does not exist
		Tag /W=$graphName/C/N=$name/L=2/F=2/X=10/Y=-45/P=1/TL={sharp=0.35}/G=(26214,26214,26214) $(guiVal.traceName), mVal, lblText
	else
		Tag /W=$graphName/C/N=$name $(guiVal.traceName), mVal, lblText
	endif
	
	SetDrawLayer /W=$graphName /K UserBack
	SetDrawEnv /W=$graphName xcoord=bottom, ycoord= prel, linefgc=(26205,52428,1), fillfgc=(26205,52428,1), fillpat=4
	DrawRect /W=$graphName ox.startTime, 0, ox.endTime, 1
	
	SetDrawLayer /W=$graphName UserFront
	
End

static Function defaultDopamineStruct(rv, [short])
	STRUCT RampValues &rv
	Variable short
	
	short = ParamIsDefault(short) || short >= 2000 ? 0 : 1

	rv.id = 1
	rv.name = "dopamine"
	rv.numPoints = short ? 1000 : 2000
	rv.xScale = 1e-5
	rv.lastGroupNum = 1
	rv.startV[0] = -0.4
	rv.holdV[0] = 0.00032
	rv.VRate[0] = 400
	rv.startV[1] = 1.2
	rv.holdV[1] = 0
	rv.VRate[1] = 400
	
	STRUCT OxidationDetails ox
	
	ox.voltage = 0.6
	ox.range = 0.2
	ox.oxidation = 1
	ox.startVoltage = ox.voltage - (ox.range / 2)
	ox.endVoltage = ox.startVoltage + ox.range
	
	rv.oxDetails = ox
End
	

Function /WAVE VMRamp_dopamine_std(theName, numbers, acqFreq)
	String theName
	Variable numbers, acqFreq
	
	Make /N=(numbers) /D /O $theName /WAVE=ramp
	SetScale d 0, 0, "V", ramp
	SetScale /P x 0, acqFreq, "s", ramp
	
	String theNote = ReplaceStringByKey(ksVM_RampKey, "", "dopamine")
	
	switch(numbers)
		case 1000:
			ramp = p < x2pnt(ramp, 0.00016) ? -0.4 : ramp[p-1] + 0.004
			ramp = p > x2pnt(ramp, 0.00415) ? ramp[p-1] - 0.004 : ramp[p]
			ramp = p > x2pnt(ramp, 0.00815) ? -0.4 : ramp[p]
			
			theNote = ReplaceNumberByKey(ksVM_RampStartPKey, theNote, x2pnt(ramp, 0.00015))
			theNote = ReplaceNumberByKey(ksVM_RampEndPKey, theNote, x2pnt(ramp, 0.00815))
			theNote = ReplaceNumberByKey(ksVM_RampMaxPKey, theNote, x2pnt(ramp, 0.00415))
			break
		case 2000:
		default:	
			ramp = p < x2pnt(ramp, 0.00032) ? -0.4 :  ramp[p-1] + 0.004
			ramp = p > x2pnt(ramp, 0.00431) ? ramp[p-1] - 0.004 : ramp[p]
			ramp = p > x2pnt(ramp, 0.00831) ? -0.4 : ramp[p]

			theNote = ReplaceNumberByKey(ksVM_RampStartPKey, theNote, x2pnt(ramp, 0.00031))
			theNote = ReplaceNumberByKey(ksVM_RampEndPKey, theNote, x2pnt(ramp, 0.00831))
			theNote = ReplaceNumberByKey(ksVM_RampMaxPKey, theNote, x2pnt(ramp, 0.00431))
	endswitch
	Note /K ramp, theNote
	
	return ramp
End	


// ************************************************************************************
// *******                            H E L P E R S
// ************************************************************************************

// just a quick convenience check if we actually have a ramp structure (stored locally in 
// a string.
Function VM_rampStructExists()
	DFREF structFolder = getRampDF()
	SVAR rampStr = structFolder:$ksRampStr
	if (SVAR_Exists(rampStr) && strlen(rampStr) > 0)
		return 1
	endif
	return 0
End


// Create the used ramp wave
// --> this need some work since the ramp might be changed
Function /WAVE VM_getRampWave([numbers, acqFreq])
	Variable numbers, acqFreq
	
	DFREF path = VoltHelpers#getPackage()
	Wave ramp = path:$ksVM_VoltageRamp
		
	// check for data trace
	Wave data = VM_getRawDataWave()
	if (ParamIsDefault(numbers) || numbers < 0)
		numbers = WaveExists(data) ? DimSize(data, 1) : 1000
	else
		numbers = round(numbers)
	endif
	if (ParamIsDefault(acqFreq) || acqFreq <= 0)
		acqFreq = WaveExists(data) ? DimDelta(data, 1) : 1e-5
	endif
		
	if (WaveExists(ramp))
		if (!WaveExists(data))
			return ramp
		endif
		String rampType = StringByKey(ksVM_RampKey, note(ramp))
		String type = getCurrentRampName()
		if ((cmpstr(rampType, type) == 0) && (numpnts(ramp) == numbers) || strlen(rampType) == 0)
			// no significant change, so just return the wave here
			return ramp
		endif
	endif
		
	STRUCT RampValues rv
	defaultDopamineStruct(rv, short= numbers < 2000)
	Make /N=(numbers)/O path:$ksVM_VoltageRamp /WAVE=ramp
	
	rampCreator(ramp, rv)
	setCurrentRampName(rv.name)
	
	return ramp 
End // END getRampWave

Function /WAVE VM_getVoltgrmDispWave(type)
	String type
	
	DFREF package = VoltHelpers#getPackage()
	Wave /T disp = package:$ksVoltageDisplay
	Wave points = package:$ksVoltageDisplayPoints
	
	if (!WaveExists(disp) || !WaveExists(points))
		makeDisplayScaleFromRamp()
		Wave /T disp = package:$ksVoltageDisplay
		Wave points = package:$ksVoltageDisplayPoints
	endif
	
	strswitch( type )
		case "display":
			return disp
			break
		case "points":
			return points
			break
	endswitch
End

Function VM_highlightCVTracePhases(graphName, [traceName, colorList])
	String graphName, traceName, colorList
	
	String errormsg = VM_getErrorStr("VM_highlightCVTracePhases")
	
	if (strlen(graphName) == 0)
		graphName = StringFromList(0, WinList("*", ";", "WIN:1"))
	endif
	
	if (ParamIsDefault(colorList) || strlen(colorList) == 0)
		colorList = "blue;red"
	endif
	
	DoWindow /F $graphName
	if (V_Flag == 0)
		// no window of the name exists, so just stop here
		return 0
	endif

	String traceNames = TraceNameList(graphName, ";", 1)

	if (!ParamIsDefault(traceName) && strlen(traceName) > 0)
		// if an empty string is provide, assume to process all traces on the graph
		if (WhichListItem(traceName, traceNames) < 0)
			// given trace name is not on the graph, stop here
			printf errormsg, "the given trace name '" + traceName + "' is not on the graph '" + graphName + "'"
			return 0
		else
			traceNames = traceName
		endif
	endif

	Variable numTraces = ItemsInList(traceNames)
	Variable index
	
	Wave highlightWave = makeColorCVWaveFromRamp(colorList=colorList)
	Wave colorWave = $(getColorValueWave())
	
	for (index = 0; index < numTraces; index += 1)
		traceName = StringFromList(index, traceNames)
		Wave trace = TraceNameToWaveRef(graphName, traceName)
		
		if (numpnts(trace) == numpnts(highlightWave))
			// only highlight if waves are the same size
			ModifyGraph zColor($traceName)={highlightWave,*,*,cindexRGB,0,colorWave}
		else
			printf errormsg, "the highlight wave mismatches in size with the trace"
		endif
	endfor
End


Function VM_adjustTransientToShift(transient)
	Wave transient

	Wave current = VM_getRawDataWave()
	Wave ramp = getCurrentRamp()
	
	String rampInfo = note(ramp)
	Variable maxPoint = NumberByKey(ksVM_RampMaxPKey, rampInfo)
	
	STRUCT VoltammetryPreferences prefs
	VoltHelpers#getTransPrefs(prefs)
	
	STRUCT OxidationDetails oxd
	oxd = prefs.oxDetails
	
	Variable index
	String type = getCurrentRampName()
	
	strswitch (type)
		case "dopamine":
			Variable fullRange = WaveMax(ramp) - WaveMin(ramp)
			Variable lowerPortion = 0.85/fullRange					// -0.4 & 0.45
			Variable pnts2search = floor(maxPoint * lowerPortion)
			
//			Duplicate /O/FREE current, avgMask
			Duplicate /O current, avgMask //, avgMask2
			avgMask = 0
			avgMask[2,6][] = 1
			MatrixOp /O avgReference = sumCols(current * avgMask) / 5
			Redimension /N=(DimSize(current, 1)) avgReference			
			
			WaveStats /C=1/W/Q/R=[0, pnts2search] avgReference
			Wave M_WaveStats
			Variable refAmplitude = M_WaveStats[%max] - M_WaveStats[%min]
			
			Duplicate /O current, avgMask
			Redimension /N=(-1, pnts2search + 1) avgMask
			
			Make /O/N=(DimSize(avgMask, 0)) ampVals, scaleFactor
			for (index = 0; index < DimSize(avgMask, 0); index += 1)
				MatrixOp /O dataRow = row(avgMask, index)
				ampVals[index] = WaveMax(dataRow) - WaveMin(dataRow)
			endfor
			
//			scaleFactor = ampVals / refAmplitude
			scaleFactor = refAmplitude / ampVals
			break
	endswitch

//	Duplicate /O current, current_adj
//	Make /O /N=(DimSize(current, 0), DimSize(current, 1)) sfactor
	
//	for (index = 0; index < DimSize(current, 1); index += 1)
//		sfactor[][index] = scaleFactor[p]
//	endfor	
	
//	avgMask2 = 0
//	avgMask2[][oxd.startPoint, oxd.endPoint] = 1
//	Variable numAvg = oxd.endPoint - oxd.startPoint
	
//	MatrixOp /O current_adj = current * sfactor
//	MatrixOp /O trans_adj = sumRows(current_adj * avgMask2) / numAvg
//	Redimension /N=(DimSize(current, 0)) trans_adj
	
	DFREF tF = GetWavesDatafolderDFR(transient)
	Duplicate /O transient, tF:$(NameOfWave(transient) + "_adj") /WAVE=adjusted
	
	FastOp adjusted = transient * scaleFactor
//	adjusted /= scaleFactor

	

End





// ************************************************************************************
// *******                  R A M P D E S I G N E R   G U I
// ************************************************************************************

static Function /S VM_showCurrentRamp()
	DFREF package = VoltHelpers#getPackage()
	Wave currRamp = package:$ksVM_VoltageRamp
	String graphName = ""
	if (WaveExists(currRamp))
		Display /K=1 /L=voltage /B=time /W=(40, 40, 440, 440) currRamp
		graphName = S_name
		ModifyGraph freePos(voltage)={0,time},freePos(time)={0,voltage}
		ModifyGraph lblPos(voltage)=70,lblPos(time)=50,standoff=0,ZisZ(voltage)=1,tickZap(time)={0}
		ModifyGraph rgb=(0, 0, 65535)
		Label voltage "voltage (\\U)"
		Label time "time (\\U)"	
		
		SetAxis /A/N=1 voltage
		SetAxis /A/N=1/E=1 time
	else
		DoAlert /T="Missing Ramp" 0, "There is no current ramp. Use the designer to create one first!"
		VM_makeRDCtrlPanel()
	endif
	
	return graphName
End

Function /S VM_makeRDCtrlPanel([panelName, panelTitle])
	String panelName, panelTitle
	
	if (ParamIsDefault(panelName) || strlen(panelName) == 0)
		panelName = ksDesignerPanelName
		panelTitle = ksDesignerPanelTitle
	elseif (ParamIsDefault(panelTitle) || strlen(panelTitle) == 0)
		panelTitle = ReplaceString("_", panelName, " ")
	endif
	
	DoWindow $panelName
	if (V_flag)
		if (WinType(panelName) == 7)
			// we have a panel, so just return the name and we are done
			DoWindow /F $panelName
			return panelName
		endif
		// we have a window with the name, but it is not a panel --> kill it
		DoWindow /K $panelName
	endif
	
	updateRampIndex()
	
	STRUCT RampDesignerWin guiVal
	guiVal.panelName = panelName

	getRampStruct(guiVal.ramp)
	
	if (guiVal.ramp.lastGroupNum == 0)
		Wave ramp = VM_getRampWave()
		if (WaveExists(ramp))
			guiVal.ramp.numPoints = numpnts(ramp)
			guiVal.ramp.xScale = deltax(ramp)
			guiVal.ramp.startV[0] = numpnts(ramp) > 0 ? ramp[0] : 0
		endif
		guiVal.ramp.lastGroupNum = 1
		guiVal.ramp.name = "dopamine"
	endif
	
	DFREF rampFolder = getRampDF()
	Make /N=(guiVal.ramp.numPoints) /O rampFolder:rampModify /WAVE=rampModify
	SetScale /P x 0, guiVal.ramp.xScale, "s", rampModify
		
	rampCreator(rampModify, guiVal.ramp)	
	
	NewPanel /K=1/N=$panelName as panelTitle
	resizeWindowToScreen(panelName, screenFraction=0.3, ratio=4/3)
	Variable /C winSize = moveWindowOnScreen(panelName, location="center", minWidth=600, minHeight=450)
	
	Variable height = imag(winSize)
	Variable width = real(winSize)
	
	guiVal.winHeight = height
	guiVal.winWidth = width
	guiVal.ctrlHeight = height * (ScreenResolution/PanelResolution(panelName))
	guiVal.ctrlWidth = (width - height) * (ScreenResolution/PanelResolution(panelName))
	
	DefineGuide /W=$panelName ctrlBorder={FR, -guiVal.ctrlWidth}
	
	Display /W=(0, 0, height, height) /N=$ksRDGName /HOST=$panelName /FG=(FL,FT,ctrlBorder,FB)
	guiVal.graphName = S_name
	String fullGName = panelName + "#" + guiVal.graphName	
	AppendToGraph /W=$fullGName /C=(0,0,65535) rampModify
	ModifyGraph /W=$fullGName gfont=$ksVM_defPanelFont, gfsize=14
	guiVal.traceName = NameOfWave(rampModify)
	ModifyGraph /W=$fullGName standoff=0, axRGB(bottom)=(30583,30583,30583), tlblRGB(bottom)=(30583,30583,30583)
	ModifyGraph alblRGB(bottom)=(30583,30583,30583)
	
	labelMeasurePoint(guiVal)
	
	DoUpdate
	GetAxis /W=$fullGName left
	if (V_min < 0)
		// draw zero line
		SetDrawLayer UserBack
		SetDrawEnv ycoord= left, dash= 8, linefgc= (30583,30583,30583)
		DrawLine 0,0,1,0
	endif

	SetActiveSubWindow ##
	
	String str = "", rampStr = ""
	StructPut /S guiVal, str
	SetWindow $panelName userdata=str
	
	DefaultGUIFont /W=$panelName /Mac all={"Verdana", 12, 0}
	
	createCntrls4RampDesigner(panelName=panelName)
	
	Variable mvFlag = 0
	str = GetUserData(panelName, "", "")
	StructGet /S guiVal, str
	
	if (guiVal.ctrlWidth < guiVal.minCtrlWidth)
		mvFlag = mvFlag | (2^0)
		width = height + guiVal.minCtrlWidth
		DefineGuide /W=$panelName ctrlBorder={FR, -guiVal.minCtrlWidth}
	endif
	if (guiVal.ctrlHeight < guiVal.minCtrlHeight)
		mvFlag = mvFlag | (2^1)
		height = guiVal.minCtrlHeight
	endif
	
	guiVal.minWinWidth = 2 * guiVal.minCtrlWidth
	guiVal.minWinHeight = guiVal.minCtrlHeight
	
	switch (mvFlag)
		case 1:	// adjust only the width
			winSize = moveWindowOnScreen(panelName, location="center", minWidth=width)
			guiVal.ctrlWidth = real(winSize) - guiVal.ctrlHeight
			guiVal.minCtrlWidth = guiVal.ctrlWidth
			break
		case 2:	// adjust only the height
			winSize = moveWindowOnScreen(panelName, location="center", minHeight=height)
			guiVal.ctrlHeight = imag(winSize)
			guiVal.minCtrlHeight = guiVal.ctrlHeight
			break
		case 3:	// adjust both height and width
			winSize = moveWindowOnScreen(panelName, location="center", minWidth=width, minHeight=height)
			guiVal.ctrlHeight = imag(winSize)
			guiVal.minCtrlHeight = guiVal.ctrlHeight
			guiVal.ctrlWidth = real(winSize) - guiVal.ctrlHeight
			guiVal.minCtrlWidth = guiVal.ctrlWidth
			break
	endswitch
	
	StructPut /S guiVal, str
	SetWindow $panelName hook(winChange)=WH_adjustRampDesigner, userdata=str
	
	DoUpdate /W=$panelName
End

static Function createCntrls4RampDesigner([panelName, resize])
	String panelName
	Variable resize
	
	if (ParamIsDefault(panelName) || strlen(panelName) == 0)
		panelName = ksDesignerPanelName
	endif
	
	resize = ParamIsDefault(resize) || resize < 1 ? 0 : 1
	
	STRUCT RampDesignerWin guiVal
	String str = GetUserData(panelName, "", "")
	StructGet /S guiVal, str
		
	Variable defYDist = 5, defXDist = 10
	Variable topOffset = defXDist, leftOffset = guiVal.winWidth - guiVal.ctrlWidth + defXDist 
	Variable currTop = topOffset, currLeft = leftOffset
	Variable width = 0, height = 0
		
	StructPut /S guiVal, str
	SetWindow $panelName, userdata=str
	
	Variable minCtrlWidth = 0
	Variable /C groupSize = VMRP_loadSaveGroup(panelName, topOffset=topOffset, leftOffset=leftOffset, resize=resize)
	
	topOffset += imag(groupSize) + defYDist
	minCtrlWidth = max(minCtrlWidth, real(groupSize))
	
	groupSize = VMRP_measureGroup(panelName, topOffset=topOffset, leftOffset=leftOffset, resize=resize)
	topOffset += imag(groupSize) + defYDist
	minCtrlWidth = max(minCtrlWidth, real(groupSize))

	groupSize = VMRP_xScalingGroup(panelName, topOffset=topOffset, leftOffset=leftOffset, resize=resize)
	topOffset += imag(groupSize) + defYDist
	minCtrlWidth = max(minCtrlWidth, real(groupSize))

	Variable index = 0
	Variable maxScaleGroup = max(1, guiVal.ramp.lastGroupNum)
	
	for (index = 0; index <= maxScaleGroup; index += 1)
		groupSize = VMRP_rampScalingGroup(panelName, index, topOffset=topOffset, leftOffset=leftOffset, resize=resize)
		topOffset += imag(groupSize) + defYDist
		minCtrlWidth = max(minCtrlWidth, real(groupSize))
	endfor
		
	StructGet /S guiVal, str
	guiVal.minCtrlHeight = topOffset + defYDist
	guiVal.minWinHeight = guiVal.minCtrlHeight
	guiVal.minCtrlWidth = minCtrlWidth + (2 * defXDist)
	guiVal.minWinWidth = 3 * guiVal.minCtrlWidth
	StructPut /S guiVal, str
	SetWindow $panelName, userdata=str
End

static Function /C VMRP_loadSaveGroup(panelName, [topOffset, leftOffset, resize])
	String panelName
	Variable topOffset, leftOffset, resize
	
	Variable gWidth = 0
	Variable gHeight = 0
	
	if (strlen(panelName) == 0 || WinType(panelName) != 7)
		return cmplx(gWidth, gHeight)
	endif
	
	STRUCT RampDesignerWin guiVal
	String str = GetUserData(panelName, "", "")
	StructGet /S guiVal, str
	
	resize = ParamIsDefault(resize) || resize < 1 ? 0 : 1
	topOffset = ParamIsDefault(topOffset) || topOffset < 0 ? 0 : topOffset
	leftOffset = ParamIsDefault(topOffset) || leftOffset < 0 ? 0 : leftOffset
	
	Variable currTop = topOffset, currLeft = leftOffset
	Variable defYDist = 5, defXDist = 5
	
	gWidth = 190
	gHeight = 60
	
	Variable rightBorder = currLeft + gWidth
	currLeft += (2 * defXDist)
	
	Variable width = 155
	Variable height = 22
	
	SetVariable rampName win=$panelName, pos={currLeft, currTop + 2}, size={width, height}
	currLeft += width + (2 * defXDist)
	
	width = 15
	PopupMenu rampSelection win=$panelName, pos={currLeft, currTop}, size={width, height}
	currTop += height + defYDist

	width = 55
	currLeft = rightBorder - width
	Button keepRamp win=$panelName, pos={currLeft, currTop}, size={width, height}

	currLeft -= width + defYDist
	Button saveRamp win=$panelName, pos={currLeft, currTop}, size={width, height}

	width = 60
	currLeft -= width + defYDist
	Button delRamp win=$panelName, pos={currLeft, currTop}, size={width, height}
	
	if (!resize)
//		SVAR rampList = $(getRampListString())
		SetVariable rampName win=$panelName, limits={0,0,0}, title="name"
		SetVariable rampName win=$panelName, bodyWidth=125, value=_STR:guiVal.ramp.name
		SetVariable rampName win=$panelName, help={"choose either a new or existing name for the ramp"}
		SetVariable rampName win=$panelName, proc=VMRP_svar_changeRampVal
		
		PopupMenu rampSelection win=$panelName, bodyWidth=20
		PopupMenu rampSelection win=$panelName, mode=1, value=#(getRampListString())
		PopupMenu rampSelection win=$panelName, help={"select an existing ramp"}
		PopupMenu rampSelection win=$panelName, proc=VMRP_pop_selectRamp

		Button keepRamp win=$panelName, title="keep", proc=VMRP_btn_keepRamp
		
		Button saveRamp win=$panelName, title="save", proc=VMRP_btn_saveRamp
		Button delRamp win=$panelName, title="delete", proc=VMRP_btn_deleteRamp, disable=2
	endif
	
	return cmplx(gWidth, gHeight)
End

static Function /C VMRP_measureGroup(panelName, [topOffset, leftOffset, resize])
	String panelName
	Variable resize, topOffset, leftOffset
	
	Variable gWidth = 0
	Variable gHeight = 0
	
	if (strlen(panelName) == 0 || WinType(panelName) != 7)
		return cmplx(gWidth, gHeight)
	endif
	
	STRUCT RampDesignerWin guiVal
	String str = GetUserData(panelName, "", "")
	StructGet /S guiVal, str
	
	resize = ParamIsDefault(resize) || resize < 1 ? 0 : 1
	
	topOffset = ParamIsDefault(topOffset) || topOffset < 0 ? 0 : topOffset
	leftOffset = ParamIsDefault(topOffset) || leftOffset < 0 ? 0 : leftOffset
	
	Variable currTop = topOffset, currLeft = leftOffset
	Variable defYDist = 5, defXDist = 10

	gWidth = 190
	gHeight = 70

	GroupBox measurementGroup win=$panelName, pos={currLeft, currTop}, size={gWidth, gHeight}
	
	Variable width = 105
	Variable height = 20
	
	currTop += (4 * defYDist)
	SetVariable mVoltage win=$panelName, pos={currLeft, currTop}, size={width, height}
	
	currLeft += width + defYDist
	width = 10
	Titlebox voltMeasureLbl win=$panelName, pos={currLeft, currTop}, size={width, height}
	
	currTop += height + defYDist

	width = 105
	currLeft = leftOffset	
	SetVariable mRange win=$panelName, pos={currLeft, currTop}, size={width, height}
 	currLeft += width + defXDist

	width = 70
	Checkbox oxidation win=$panelName, pos={currLeft, currTop}, size={width, height}
	
	if (!resize)
		Groupbox measurementGroup, win=$panelName, title="measure points", fStyle=2
		
		SetVariable mVoltage, win=$panelName, value=_NUM:guiVal.ramp.oxDetails.voltage
		SetVariable mVoltage, win=$panelName, limits={0, 2, 0.1}, title="voltage"
		SetVariable mVoltage, win=$panelName, bodywidth=50
		SetVariable mVoltage, win=$panelName, proc=VMRP_svar_setOxyValues
		
		Titlebox voltMeasureLbl, win=$panelName, title="V", frame=0

		SetVariable mRange, win=$panelName, value=_NUM:guiVal.ramp.oxDetails.range
		SetVariable mRange, win=$panelName, limits={0, 2, 0.1}, title="range"
		SetVariable mRange, win=$panelName, bodywidth=50
		SetVariable mRange, win=$panelName, proc=VMRP_svar_setOxyValues
		
		Checkbox oxidation, win=$panelName, mode=0, title="oxidation", side=2
		Checkbox oxidation, win=$panelName, value=guiVal.ramp.oxDetails.oxidation
		Checkbox oxidation, win=$panelName, proc=VMRP_ckbx_setOxidation
	endif
	return cmplx(gWidth, gHeight)	
End


static Function /C VMRP_xScalingGroup(panelName, [topOffset, leftOffset, resize])
	String panelName
	Variable resize, topOffset, leftOffset
	
	Variable gWidth = 0
	Variable gHeight = 0
	
	if (strlen(panelName) == 0 || WinType(panelName) != 7)
		return cmplx(gWidth, gHeight)
	endif
	
	STRUCT RampDesignerWin guiVal
	String str = GetUserData(panelName, "", "")
	StructGet /S guiVal, str

	resize = ParamIsDefault(resize) || resize < 1 ? 0 : 1
	
	topOffset = ParamIsDefault(topOffset) || topOffset < 0 ? 0 : topOffset
	leftOffset = ParamIsDefault(topOffset) || leftOffset < 0 ? 0 : leftOffset
	
	Variable currTop = topOffset, currLeft = leftOffset
	Variable defYDist = 5, defXDist = 5
	
	gWidth = 190
	gHeight = 70
	
	Groupbox xScaleGroup win=$panelName, pos={currLeft, currTop}, size={gWidth, gHeight}

	Variable width = 155
	Variable height = 20

	currTop = topOffSet + (4 * defYDist) 
	currLeft = currLeft + defXDist
	SetVariable numPoints win=$panelName, pos={currLeft, currTop}, size={width, height}

	currTop += defYDist + height
	SetVariable xScale win=$panelName, pos={currLeft, currTop}, size={width, height}
	
	Titlebox xScaleLbl win=$panelName, pos={currLeft + width + defYDist, currTop}, size={5, height}	
	
	if (!resize)
		Groupbox xScaleGroup win=$panelName, title = "x scale ", fStyle=2
		
		SetVariable numPoints win=$panelName, limits={0, 1e9, 0}, title="num of pnts"
		SetVariable numPoints win=$panelName, bodyWidth=70, value=_NUM:guiVal.ramp.numPoints
		SetVariable numPoints win=$panelName, proc=VMRP_svar_changeRampVal
		
		SetVariable xScale win=$panelName, limits={0, 1e9, 0}, title="acq. freq. x"
		SetVariable xScale win=$panelName, bodyWidth=70, value=_NUM:(guiVal.ramp.xScale * 1e3)
		SetVariable xScale win=$panelName, proc=VMRP_svar_changeRampVal
		
		Titlebox xScaleLbl win=$panelName, title="ms", frame=0
	endif
	
	StructPut /S guiVal, str
	SetWindow $panelName, userdata=str
	return cmplx(gWidth, gHeight)
End
		

static Function /C VMRP_rampScalingGroup(panelName, number, [topOffset, leftOffset, resize])
	String panelName
	Variable number, topOffset, leftOffset, resize
	
	Variable gWidth = 0
	Variable gHeight = 0
	
	if (strlen(panelName) == 0 || WinType(panelName) != 7)
		return cmplx(gWidth, gHeight)
	endif
	
	if (number < 0 || number > 5)
		return cmplx(gWidth, gHeight)
	else
		number = round(number)
	endif
	
	STRUCT RampDesignerWin guiVal
	String str = GetUserData(panelName, "", "")
	StructGet /S guiVal, str

	resize = ParamIsDefault(resize) || resize < 1 ? 0 : 1
	
	topOffset = ParamIsDefault(topOffset) || topOffset < 0 ? 0 : topOffset
	leftOffset = ParamIsDefault(topOffset) || leftOffset < 0 ? 0 : leftOffset
	
	Variable currTop = topOffset, currLeft = leftOffset
	Variable defYDist = 5, defXDist = 5
	
	gWidth = 190
	gHeight = 100
	
	
	STRUCT RampScalingGroup rsg
	getRampScaleGroup(rsg, number)
	
	Variable hideDel = number <= 1
	Variable hideAdd = number == 0
	guiVal.ramp.lastGroupNum = guiVal.ramp.lastGroupNum < number ? number : guiVal.ramp.lastGroupNum
	
	Groupbox $rsg.groupName win=$panelName, pos={currLeft, currTop}, size={gWidth, gHeight}
	currTop += 4 * defYDist
	currLeft = leftOffset + defXDist
	
	Variable width = 165
	Variable height = 20
	
	SetVariable $rsg.voltage win=$panelName, pos={currLeft, currTop}, size={width, height}
	Titlebox $rsg.voltLbl win=$panelName, pos={currLeft + width + defYDist, currTop}, size={5, height}
	Button $rsg.btnAName win=$panelName, pos={currLeft, currTop}, size={height, height}
	currTop += height + defYDist
	
	width = 145
	SetVariable $rsg.holdVolt win=$panelName, pos={currLeft, currTop}, size={width, height}
	Titlebox $rsg.holdLbl win=$panelName, pos={currLeft + width + defYDist, currTop}, size={5, height}
	Button $rsg.btnDName win=$panelName, pos={currLeft, currTop}, size={height, height}
	
	currTop += height + defYDist
	SetVariable $rsg.voltRate win=$panelName, pos={currLeft, currTop}, size={width, height}
	Titlebox $rsg.voltRateLbl win=$panelName, pos={currLeft + width + defYDist, currTop}, size={5, height}
	
	String groupTitle = "voltage control "
	if (number > 0)
		groupTitle += num2str(number) + " "
	endif
	
	if (!resize)
		Groupbox $rsg.groupName win=$panelName, title=groupTitle, fStyle=2
	
		SetVariable $rsg.voltage win=$panelName, limits={-2, 2, 0.1}, title="voltage"
		SetVariable $rsg.voltage win=$panelName, bodywidth=70, value=_NUM:guiVal.ramp.startV[number]
		SetVariable $rsg.voltage win=$panelName, proc=VMRP_svar_setVoltParams
		Titlebox $rsg.voltLbl win=$panelName, title="V", frame=0

		SetVariable $rsg.holdVolt win=$panelName, limits={0, 2e3, 0}, title="hold for"
		SetVariable $rsg.holdVolt win=$panelName, bodywidth=50, value=_NUM:(guiVal.ramp.holdV[number] * 1e3)
		SetVariable $rsg.holdVolt win=$panelName, proc=VMRP_svar_setVoltParams
		Titlebox $rsg.holdLbl win=$panelName, title="ms", frame=0
		
		SetVariable $rsg.voltRate win=$panelName, limits={-2e3, 2e3, 0}, title="voltage rate"
		SetVariable $rsg.voltRate win=$panelName, bodywidth=50, value=_NUM:guiVal.ramp.VRate[number]
		SetVariable $rsg.voltRate win=$panelName, proc=VMRP_svar_setVoltParams
		if (guiVal.ramp.VRate[number] <= 0)
			SetVariable $rsg.voltRate win=$panelName, fColor=(65535, 0, 0)
		endif
		Titlebox $rsg.voltRateLbl win=$panelName, title="V/s", frame=0
		
		Button $rsg.btnAName win=$panelName, title="+", proc=VMRP_btn_addDelVoltageCtrl
		Button $rsg.btnAName win=$panelName, disable=hideAdd
		Button $rsg.btnDName win=$panelName, title="-", disable=hideDel
		Button $rsg.btnDName win=$panelName, proc=VMRP_btn_addDelVoltageCtrl
	endif

	StructPut /S guiVal, str
	SetWindow $panelName, userdata=str
	
	String addButtonList = SortList(ControlNameList(panelName, ";", "addVoltage_*"))
	String delButtonList = SortList(ControlNameList(panelName, ";", "delVoltage_*"))
	Variable numButtons = ItemsInList(addButtonList)

	Variable index
	for (index = 1; index < (numButtons - 1); index += 1)
		Button $(StringFromList(index, addButtonList)) win=$panelName, disable=1
		Button $(StringFromList(index, delButtonList)) win=$panelName, disable=1
	endfor
	
	return cmplx(gWidth, gHeight)
End

static Function VMRP_delRampScalingGroup(panelName, number)
	String panelName
	Variable number
	
	STRUCT RampScalingGroup rsg
	getRampScaleGroup(rsg, number)

	KillControl /W=$panelName $rsg.groupName
	KillControl /W=$panelName $rsg.voltage
	KillControl /W=$panelName $rsg.voltLbl
	KillControl /W=$panelName $rsg.holdVolt
	KillControl /W=$panelName $rsg.holdLbl
	KillControl /W=$panelName $rsg.voltRate
	KillControl /W=$panelName $rsg.voltRateLbl
	KillControl /W=$panelName $rsg.btnAName
	KillControl /W=$panelName $rsg.btnDName 
End

Function VMRP_svar_changeRampVal(sv) : SetVariableControl
	STRUCT WMSetVariableAction &sv
	
	switch (sv.eventCode)
		case 2:			// enter key pressed
			STRUCT RampDesignerWin guiVal
			String str = GetUserData(sv.win, "", "")
			StructGet /S guiVal, str
						
			String graphName = guiVal.panelName + "#" + guiVal.graphName
			Wave ramp = TraceNameToWaveRef(graphName, guiVal.traceName)
			if (!WaveExists(ramp))
				return 0
			endif
			
			Variable recalc = 0
			
			strswitch (sv.ctrlName)
				case "rampName":
					guiVal.ramp.name = sv.sval
					break
				case "numPoints":
					guiVal.ramp.numPoints = sv.dval
					recalc = 1
					break
				case "xScale":
					guiVal.ramp.xScale = sv.dval * 1e-3
					recalc = 1
					break
			endswitch			
			StructPut /S guiVal, str
			SetWindow $sv.win, userdata=str
			
			if (!recalc)
				return 1
			endif

			Button keepRamp win=$sv.win, fstyle = 1, fsize = 14

			Variable success = rampCreator(ramp, guiVal.ramp)
			if (success == 1)
				SetVariable $sv.ctrlName win=$sv.win, fColor=(0,0,0)
				labelMeasurePoint(guiVal)
			else
				SetVariable $sv.ctrlName win=$sv.win, fColor=(0,0,0)				
			endif
			break
	endswitch
End

Function VMRP_svar_setVoltParams(sv) : SetVariableControl
	STRUCT WMSetVariableAction &sv
	
	switch (sv.eventCode)
		case 1:			// mouse up
		case 2: 			// enter key pressed
			STRUCT RampDesignerWin guiVal
			String str = GetUserData(sv.win, "", "")
			StructGet /S guiVal, str
						
			String graphName = guiVal.panelName + "#" + guiVal.graphName
			Wave ramp = TraceNameToWaveRef(graphName, guiVal.traceName)
			if (!WaveExists(ramp))
				return 0
			endif
			
			Variable lUScr = strsearch(sv.ctrlName, "_", Inf, 1)
			if (lUScr < 0)
				return 0
			endif
			String name = (sv.ctrlName)[0, lUScr - 1]
			Variable number = str2num((sv.ctrlName)[lUScr + 1, strlen(sv.ctrlName) - 1])

			STRUCT RampScalingGroup rsg
			getRampScaleGroup(rsg, number)
			
			strswitch(name)
				case "voltage":
					guiVal.ramp.startV[number] = sv.dval
					break
				case "holdVoltage":
					guiVal.ramp.holdV[number] = sv.dval * 1e-3		// dialog asks for ms -> convert to seconds
					Variable numPoints = guiVal.ramp.holdV[number] / guiVal.ramp.xScale
//					if (numPoints > guiVal.ramp.numPoints)
//						// illegal value, holding is too large for the wave
////						Variable maxNum = guiVal.numPoints * guiVAl.xScale
//						SetVariable numPoints win=$sv.win, fColor=(65535, 0, 0), valueBackColor=(65535, 0,0)
//					else
//						SetVariable numPoints win=$sv.win, fColor=(0, 0, 0), valueBackColor=(65535, 65535, 65535)
//					endif
					
					break
				case "voltRate":
					guiVal.ramp.VRate[number] = sv.dval
					if (sv.dval > 0)
						SetVariable $rsg.voltRate win=$sv.win, fColor=(0, 0, 0)
					endif
					break
			endswitch
			
			Button keepRamp win=$sv.win, fstyle = 1, fsize = 14

			Variable success = rampCreator(ramp, guiVal.ramp)

			StructPut /S guiVal, str
			SetWindow $sv.win, userdata=str
			
			if (success == 1)
				SetVariable numPoints win=$sv.win, fColor=(0,0,0)
				labelMeasurePoint(guiVal)
			else
				SetVariable numPoints win=$sv.win, fColor=(65535,43690,0)
			endif
			break
	endswitch
End

Function VMRP_svar_setOxyValues(sv) : SetVariableControl
	STRUCT WMSetVariableAction &sv
	
	switch (sv.eventCode)
		case 1:			// mouse up
		case 2: 			// enter key pressed
			STRUCT RampDesignerWin guiVal
			String str = GetUserData(sv.win, "", "")
			StructGet /S guiVal, str
			String graphName = guiVal.panelName + "#" + guiVal.graphName
			Wave ramp = TraceNameToWaveRef(graphName, guiVal.traceName)
			if (!WaveExists(ramp))
				return 0
			endif
			
			strswitch (sv.ctrlName)
				case "mVoltage":
					guiVal.ramp.oxDetails.voltage = sv.dval
					break
				case "mRange":
					guiVal.ramp.oxDetails.range = sv.dval
					break
			endswitch
			
			Variable success = rampCreator(ramp, guiVal.ramp)
			if (success)
				labelMeasurePoint(guiVal)
			endif
						
			Button keepRamp win=$sv.win, fstyle = 1, fsize = 14
			
			StructPut /S guiVal, str
			SetWindow $sv.win userdata=str	
			break
	endswitch
End

Function VMRP_btn_addDelVoltageCtrl(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch(bn.eventCode)
		case 2:		// mouse button up
			STRUCT RampDesignerWin guiVal
			String str = GetUserData(bn.win, "", "")
			StructGet /S guiVal, str

			Variable lUScr = strsearch(bn.ctrlName, "_", Inf, 1)
			if (lUScr < 0)
				return 0
			endif
			String name = (bn.ctrlName)[0, lUScr - 1]
			Variable number = str2num((bn.ctrlName)[lUScr + 1, strlen(bn.ctrlName) - 1])			
			String btnList = SortList(ControlNameList(bn.win, ";", name + "_*"))

			String groupList = SortList(ControlNameList(bn.win, ";", "voltGroup_*"))
			Variable numGroups = ItemsInList(groupList)
			String lastGroupName = StringFromList(numGroups - 1, groupList)
			
			Variable top = 0, left = 0, height = 0, width = 0
			Variable index = 0
			Variable /C theSize
			
			strswitch(name)
				case "addVoltage":
					ControlInfo /W=$bn.win $lastGroupName
					if (number < 5 )
						number += 1
					else
						return 0
					endif
					
					Variable topOff = V_top + V_height + 5
					
					theSize = VMRP_rampScalingGroup(bn.win, number, topOffset=topOff, leftOffset=V_left)
					guiVal.ramp.lastGroupNum = number
					
					
					if (guiVal.minCtrlHeight < topOff + imag(theSize) + 5)
						moveWindowOnScreen(bn.win, location="center", minWidth=guiVal.winWidth, minHeight=guiVal.minCtrlHeight)
					endif
										
					break
				case "delVoltage":
					if (number > 1)
						VMRP_delRampScalingGroup(bn.win, number)

						guiVal.ramp.startV[number] = 0
						guiVal.ramp.holdV[number] = 0
						guiVal.ramp.VRate[number] = 0
						
						number -= 1
						guiVal.ramp.lastGroupNum = number
						StructPut /S guiVal, str
						SetWindow $bn.win, userdata=str
						
						String graphName = guiVal.panelName + "#" + guiVal.graphName
						Wave rampModify = TraceNameToWaveRef(graphName, guiVal.traceName)
						rampCreator(rampModify, guiVal.ramp)
						
						if (number > 1)
							Button $("delVoltage_" + num2str(number)) win=$bn.win, disable=0
						endif
						Button $("addVoltage_" + num2str(number)) win=$bn.win, disable=0
					endif
					break
			
			endswitch			
			break
	endswitch
End

Function VMRP_btn_keepRamp(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch (bn.eventCode)
		case 2:			// mouse button up
			STRUCT RampDesignerWin guiVal
			String str = GetUserData(bn.win, "", "")
			StructGet /S guiVal, str
			
			Wave currRamp = VM_getRampWave()
			
			String graphName = guiVal.panelName + "#" + guiVal.graphName
			Wave newRamp = TraceNameToWaveRef(graphName, guiVal.traceName)
			
			Duplicate /O newRamp, currRamp
			saveRampStruct(guiVal.ramp)
			makeDisplayScaleFromRamp()
			
			Button $bn.ctrlName win=$bn.win, fstyle = 0, fsize = 12

			break
	endswitch
End

Function VMRP_btn_saveRamp(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch (bn.eventCode)
		case 2:			// mouse button up
			STRUCT RampDesignerWin guiVal
			String str = GetUserData(bn.win, "", "")
			StructGet /S guiVal, str
			
			Wave currRamp = VM_getRampWave()
			
			String graphName = guiVal.panelName + "#" + guiVal.graphName
			Wave newRamp = TraceNameToWaveRef(graphName, guiVal.traceName)
			
			SVAR rampList = $(getRampListString())
			
			if (WhichListItem(guiVal.ramp.name, rampList) < 0)
				// name doesn't exist yet, everything is ok
				Duplicate /O newRamp, currRamp
				saveRampStruct(guiVal.ramp)
				makeDisplayScaleFromRamp()
				
				addRampToIndex(guiVal.ramp)
				SaveRamp()
				Button keepRamp win=$bn.win, fstyle = 0, fsize = 12
			else
				DoAlert /T="Overwrite Check" 1, "Do you want to overwrite the existing ramp?"
				if (V_flag == 1)
					// user clicked on yes to overwrite
					Duplicate /O newRamp, currRamp
					saveRampStruct(guiVal.ramp)
					makeDisplayScaleFromRamp()					
					SaveRamp()	
					Button keepRamp win=$bn.win, fstyle = 0, fsize = 12
				endif
			endif
			break
	endswitch
End

Function VMRP_btn_deleteRamp(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch (bn.eventCode)
		case 2:			// mouse button up
			STRUCT RampDesignerWin guiVal
			String str = GetUserData(bn.win, "", "")
			StructGet /S guiVal, str
			
			
			break
	endswitch
End


Function VMRP_pop_selectRamp(pm) : PopupMenuControl
	STRUCT WMPopupAction &pm
	
	switch(pm.eventCode)
		case 2:
			STRUCT RampDesignerWin guiVal
			String str = GetUserData(pm.win, "", "")
			StructGet /S guiVal, str
			String graphName = guiVal.panelName + "#" + guiVal.graphName
			Wave ramp = TraceNameToWaveRef(graphName, guiVal.traceName)
			if (!WaveExists(ramp))
				return 0
			endif
			
			Variable prevLastGroup = guiVal.ramp.lastGroupNum
			
			LoadRamp(guiVal.ramp, pm.popStr, noSave = 1)
			StructPut /S guiVal, str
			SetWindow $pm.win, userdata = str
			
			if (prevLastGroup > guiVal.ramp.lastGroupNum)
				Variable index
				for (index = guiVal.ramp.lastGroupNum; index <= prevLastGroup; index += 1)
					VMRP_delRampScalingGroup(pm.win, index)
				endfor
			endif
			
			createCntrls4RampDesigner(panelName = pm.win)
			
			// update gui struct with potential new values
			str = GetUserData(pm.win, "", "")
			StructGet /S guiVal, str
			
			Variable success = rampCreator(ramp, guiVal.ramp)
			if (success)
				labelMeasurePoint(guiVal)
			endif
			
			Button keepRamp, win=$pm.win, fstyle=1
			
			STRUCT WMWinHookStruct wh
			GetWindow $pm.win wsize
			wh.winName = pm.win
			wh.winRect.left = V_left
			wh.winRect.right = V_right
			wh.winRect.top = V_top
			wh.winRect.bottom = V_bottom
			wh.eventName = "resize"
			
			WH_adjustRampDesigner(wh)
			break
	endswitch
End

Function VMRP_ckbx_setOxidation(cb) : CheckboxControl
	STRUCT WMCheckboxAction &cb
	
	switch (cb.eventCode)
		case 2:			// mouse up
			STRUCT RampDesignerWin guiVal
			String str = GetUserData(cb.win, "", "")
			StructGet /S guiVal, str
			
			String graphName = guiVal.panelName + "#" + guiVal.graphName
			Wave ramp = TraceNameToWaveRef(graphName, guiVal.traceName)
			if (!WaveExists(ramp))
				return 0
			endif

			guiVal.ramp.oxDetails.oxidation = cb.checked
			
			StructPut /S guiVal, str
			SetWindow $cb.win, userdata=str

			Variable success = rampCreator(ramp, guiVal.ramp)
			if (success)
				labelMeasurePoint(guiVal)
			endif

			Button keepRamp win=$cb.win, fstyle = 1, fsize = 14
			break
	endswitch
End