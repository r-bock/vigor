#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion = 6.3
#pragma version = 3
#pragma ModuleName = VoltGUI
#if defined(WINDOWS)
	#if Exists("ABFFileOpen") || Exists("bpc_AbfFileOpenDialog")
#include "VM_abfFileLoader", version >= 1
	#endif
#endif
#include "VM_Helpers", version >= 2.4
#include "VM_Analysis", version >= 2.1
#include "VM_Ramp", version >= 1
#include "VM_hookFunctions", version >= 2
#include "VM_tdmFileLoader", version >= 1.1
#include "VM_reanalyzeGUI", version >= 1.3
#include "WindowHelper", version >= 2.7
#include "GraphHelpers", version >= 3.9
#include "ListHelpers", version >= 4.2
#include "SetToZero", version >= 2.2
#include "MeanAndStDev", version >= 6.69
#include "GUIHelpers", version >= 1.4

// by Roland Bock, January 2012
//
// version 3 (AUG 2019): added a function to import voltammetry epochs from another igor experiment.
// version 2.9 (OCT 2018): changed the copying of the max/area wave to a straight duplication for Igor 8.
//									Added a new menu item to update the current category colors from preferences. 
//									Changed the behavior of the category color table to update the preferences only
//										when closing the table (window)
// version 2.8 (MAY 2017): added buttons on the calibration tab to calculate the calbration
//                         factor from measured traces
//                         added checkbox to color/highlight oxidation and reduction phase of 
//										CV trace, including the selection CV
// version 2.5 (JUN 2015): simple stats and decay wave only reports the selected values
// version 2.4 (JUN 2015): added a calibration factor tab to the interface to convert
//							the current from nA into molar dopamine
// version 2.3 (JUN 2015): modified Reanalyze GUI and fixed the correct scaling of
//						the imported traces from the Demon software (TDMS files)
// version 2.2 (MAR 2015): added reanalyze button to epoch panel
// version 2.19 (MAR 2015)
// version 2.18 (DEC 2014): added 'align to fitted max' option to preferences and CV plot
// version 2.12 (MAY 2014): used conditioned compiling for ABF file loading
// version 2.07 (APR 2014): modified decay fits button function
// version 2.05 (AUG 2013)
// version 1 (JAN 2013)
// version 0.07 (JUL 2012)
// ** version 0.05 (JAN 2012)

// ****************************************************
// *****************      CONSTANTS                  ****************
// ****************************************************
Constant kVM_Clear = 0
Constant kVM_Recorded = 1
Constant kVM_Copied = 2

Constant kVM_GUIPrefsVersion = 2
Constant kVM_ColorScaleVersion = 1
Strconstant ksVM_GUIPrefsFileName = "GUIPreferences.bin"
Strconstant ksVM_WinPrefsFileName = "WindowPreferences.bin"

#if defined(MACINTOSH)
Strconstant ksVM_defPanelFont = "Verdana"		//< default panel font for Macintosh
Constant kVM_defFSize = 10							//< default panel font size for Macintosh
#else
Strconstant ksVM_defPanelFont = "Arial"			//< default panel font for Windows
Constant kVM_defFSize = 11							//< default panel font size for Windows
#endif

Constant kVM_version = 2.7
static Strconstant ksVM_version = "version"

static Strconstant ksloadPath = "rbVMDataLoadPath"

// window and panel names
static Strconstant ksCntrlPanelName = "VM_GUICntrl"
static Strconstant ksEpochPanel = "VM_EpochPanel"
static Strconstant ksEpochPanelTitle = "Epoch Panel"
static Strconstant ksDefProgressBarName = "VM_ProgressBarPanel"
static Strconstant ksDefProgressBarTitle = "Progress Window"
static Strconstant ksDefChannelPanel = "VMGUI_ChannelAssignPanel"
static Strconstant ksDefChannelPanelTitle = "Channel Assignment Panel"

static Strconstant ksSimpleFitTableName = "VM_SimpleFitValues"
static Strconstant ksSimpleFitTableTitle = "Simple Fit Values"
static Strconstant ksSimpleDescTableName = "VM_SimpleStatsDesc"
static Strconstant ksSimpleDescTableTitle = "Simple Descriptive Stats"

static Strconstant ks_VGColorScaleName = "VM_SetColorScalePanel"

// variable names
static Strconstant ksCFlag = "VM_RecordedFlag"

// string names
static Strconstant ksAnalysisFuncName = "AnalysisFuncName"
static Strconstant ksControlFuncName = "ControlFuncName"
static Strconstant ksVM_colorScalePrefs = "ColorScalePreferences"

// wave names
static Strconstant ksAvailableEpochs = "VMGUI_epochsList"
static Strconstant ksAvailableEpochsSel = "VMGUI_epochsListSel"
static Strconstant ksGUIEpochCatList = "VMGUI_epochCatList"
static Strconstant ksGUIEpochCatSel = "VMGUI_epochCatSel"

static Strconstant ksGUIChannelAssign = "VMGUI_channelAssignList"
static Strconstant ksGUIChannelAssignSel = "VMGUI_channelAssignSel"
static Strconstant ksGUIChannelClass = "VMGUI_channelClass"
static Strconstant ksGUIChannelClassSel = "VMGUI_channelClassSel"

Strconstant ksVM_xTransientDisp = "VM_EpochTransientXWave"	//< default name for the transient 
Strconstant ksVM_dataPrefix = "AD"					//< default recording channel prefix

static Strconstant ksFileOrgzdExtension = "_orgzd"
static Strconstant ksDefaultFileName = "Voltammetry_Experiment"


// ****************************************************
// *****************      S T R U C T U R E S        ****************
// ****************************************************
//! Structure to keep some of the GUI preferences around //-
Structure VM_GUIPreferences
	int16 version				//< structure version
	char loadpath[400]			//< path of the last loaded experiment (max 400 characters)
	int16 keepCVfront			//< keep the CV plot in front during epoch selection 
	int16 winCVmax				//< keep the cursors around the maximum oxy point on the CV plot
EndStructure

//! Structure to remember the window locations //-
Structure VM_WindowLocations
	STRUCT Rect epochPanel			//< location of the Epoch panel
	STRUCT Rect transientGraph		//< location of the transient graph
EndStructure

// ****************************************************
// *****************              M E N U E S            ****************
// ****************************************************
Menu "Voltammetry", dynamic
	"Organize Data Waves", VM_organizeDataWaves(feedback=1)
	SubMenu "Load data"
		"Igor data file ...", VM_loadIgorDataFiles()
		"Patchmaster igor binary files ...", VM_loadPMIgorBinaryFiles(multisweep = 1, add = 1)
		"Patchmaster sweep data ...", VM_loadPMIgorBinaryFiles(add = 1)
		"-"
		"Import voltammetry data ...", VM_ImportVMDataFromExp()
	End
	"-"
	"Create transient/3", VM_makeTransient()
	"Epoch Panel/S1", VM_EpochControlPanel()
	"-"
	SubMenu "Simple Stats"
		"Show max-area values", SiSt_getMaxAreaTable()
		"Show decay fit table", SiST_getSimpleFitTable()
		"-"
		"Show mean cv trace", VM_showMeanCV("", "")
	End
	SubMenu "Utilities"
	End
	"-"
	SubMenu "Channels and Epochs"
		"Assign channels", VM_createChannelAssign("", refine=1)
		"Show channel assignments", VM_showChannelAssignment()
		"Show epoch categories", VM_showEpochCategories()
	End
	SubMenu "Ramps"
	End
	SubMenu "Adjustments"
		SubMenu "Change Preferences"
			"General GUI preferences", VM_changeGUIPreferences()
			"Dopamine", VM_setHiddenPrefs("dopamine")
			"Fit parameters", VM_changeFitPreferences()
			"Set default calibration factor", VM_changeDopamineDefCalibFactor()
			"Set category colors", VM_setCategoryColorPrefs()
			"Update category colors from preferences", VM_updateCatColorPrefs()
			"-"
			"Disable Exp. Feature Warning", VM_changePSYWarning(1, 0)
		End
		"Reset preferences", VM_resetPrefs2Default()
		"-"
		"Fix raw wave name ending", VM_fixTraceName("", "")
		"-"
		"Remove raw data files", VoltGUI#cleanUp()
	End
	
End


// ****************************************************
// *************    S T A T I C    H E L P E R S        ****************
// ****************************************************


static Function /S getLoadPathName()
	return ksloadPath
end

static Function loadGUIPrefs(prefs)
	STRUCT VM_GUIPreferences &prefs
	
	Variable recordID = getGUIRecordID("GUI")
	LoadPackagePreferences /MIS=1 VM_getPackagePrefsName(), ksVM_GUIPrefsFileName, recordID, prefs
	if (V_flag != 0 || V_bytesRead == 0)
		defaultGUIPrefs(prefs)
	endif
	if (prefs.version != kVM_GUIPrefsVersion)
		saveGUIPrefs(prefs, reset=1)
		defaultGUIPrefs(prefs)
		saveGUIPrefs(prefs)
	endif
	
	NVAR inFront = $(VM_getKeepCVFrontFlag())
	inFront = prefs.keepCVfront
	NVAR cvAligned = $(VM_getCVCsrAlignedFlag())
	cvAligned = prefs.winCVmax	
	return 1
End

static Function saveGUIPrefs(prefs, [reset])
	STRUCT VM_GUIPreferences &prefs
	Variable reset
	
	if (ParamIsDefault(reset) || reset < 0)
		reset = 0
	else
		reset = reset > 1 ? 1 : 0
	endif
	Variable recordID = getGUIRecordID("GUI")
	if (reset)
		SavePackagePreferences /KILL VM_getPackagePrefsName(), ksVM_GUIPrefsFileName, recordID, prefs
	else
		SavePackagePreferences VM_getPackagePrefsName(), ksVM_GUIPrefsFileName, recordID, prefs
		NVAR inFront = $(VM_getKeepCVFrontFlag())
		inFront = prefs.keepCVfront
		NVAR cvAligned = $(VM_getCVCsrAlignedFlag())
		cvAligned = prefs.winCVmax
	endif
	return 1
End

static Function defaultGUIPrefs(prefs)
	STRUCT VM_GUIPreferences &prefs
	
	prefs.version = kVM_GUIPrefsVersion
	prefs.loadpath = SpecialDirPath("Documents", 0, 0, 0)
	prefs.keepCVfront = 0					// keep the CV plot in the foreground during epoch selection
	prefs.winCVmax = 1
	return 1
End

static Function loadWinPrefs(prefs, theName)
	STRUCT VM_WindowLocations &prefs
	String theName
	
	Variable recordID = getWinRecordID(theName)
	Variable isValid = recordID >= 0
		
	STRUCT Rect epanel
	if (isValid)
		LoadPackagePreferences VM_getPackagePrefsName(), ksVM_WinPrefsFileName, recordID, epanel
	endif
End

static Function loadExpFeature(control)
	STRUCT VM_ExpFeatureControl &control
	
	Variable recordID = getGUIRecordID("Experimental")
	LoadPackagePreferences /MIS=1 VM_getPackagePrefsName(), ksVM_GUIPrefsFileName, recordID, control
	if (V_flag != 0 || V_bytesRead == 0)
		defaultExpFeature(control)
		saveExpFeature(control)
	endif
End

static Function saveExpFeature(control, [reset])
	STRUCT VM_ExpFeatureControl &control
	Variable reset
	
	Variable recordID = getGUIRecordID("Experimental")
	if (ParamIsDefault(reset) || reset < 0)
		reset = 0
	else
		reset = reset > 1 ? 1 : round(reset)
	endif
	if (reset)
		SavePackagePreferences /KILL VM_getPackagePrefsName(), ksVM_GUIPrefsFileName, recordID, control
	else
		SavePackagePreferences VM_getPackagePrefsName(), ksVM_GUIPrefsFileName, recordID, control
	endif
End

static Function defaultExpFeature(control)
	STRUCT VM_ExpFeatureControl &control
	
	control.version = kVM_ExpFeatureVersion
	
	STRUCT VM_ExperimentalFeature feature
	feature.version = kVM_ExpFeatureVersion
	feature.feature = "PSY"
	feature.enabled = 1
	feature.warningEnabled = 1
	
	control.features[0] = feature
End	

static Function getGUIRecordID(theName, [max])
	String theName
	Variable max
	
	if (ParamIsDefault(max))
		max = 0
	endif
	if (max == 1)
		return 5
	endif
	Variable recordID
	strswitch (theName)
		case "GUI":
			recordID = 0
			break
		case "Experimental":
			recordID = 1
			break
		case "ColorScale":
			recordID = 2
			break
	endswitch
	return recordID
End
	
static Function getWinRecordID(theName)
	String theName
	
	Variable recordID = -1
	strswitch(theName)
		case ksVM_TransientGraphName:
			recordID = 0
			break
		case ksEpochPanel:
			recordID = 3
			break
	endswitch
	return recordID
End

static Function defaultWinPrefs(prefs, type)
	STRUCT Rect &prefs
	String type
	
	strswitch(type)
		case "EpochPanel":
			prefs.top = 44
			prefs.left = 15
			prefs.bottom = 430
			prefs.right = 250
			break
	endswitch
End

static Function defaultChannelAssignStruct(prefs)
	STRUCT ChannelAssignment &prefs
	
	prefs.version = kVM_ChannelPrefsVersion
	
	setChannelAssign(prefs, ksVM_DefChannelAssignList, classes=ksVM_DefChannelClassList)
End

static Function loadCSPrefs(prefs)
	STRUCT VM_VGColorScale &prefs
	
	DFREF package = VoltHelpers#getPackage()
	SVAR colorPrefs = package:$ksVM_colorScalePrefs
	if (!SVAR_Exists(colorPrefs))
		defaultCSPrefs(prefs)
		saveCSPrefs(prefs)
	else
		StructGet /S prefs, colorPrefs	
	endif
	return 1
End

static Function defaultCSPrefs(prefs)
	STRUCT VM_VGColorScale &prefs

	prefs.version = kVM_ColorScaleVersion
	prefs.method = 0
	prefs.max = kVM_defMaxColorScaleVG
	prefs.min = kVM_defMinColorScaleVG
	prefs.PofUpper = kVM_scaleLofUFraction
	prefs.PofLower = kVM_scaleUofLFraction
	return 1
End

static Function saveCSPrefs(prefs)
	STRUCT VM_VGColorScale &prefs
	
	DFREF package = VoltHelpers#getPackage()
	SVAR colorPrefs = package:$ksVM_colorScalePrefs
	if (!SVAR_Exists(colorPrefs))
		String /G package:$ksVM_colorScalePrefs
		SVAR colorPrefs = package:$ksVM_colorScalePrefs
	endif
	StructPut /S prefs, colorPrefs
	return 1	
End
	
// Only ONE data channel is allowed, so if multiples are assigned, only the first one will be set. 
// theAssignList	key-value list with the channel name as key and the assignment as value
// theClassList		key-value list with the channel name as key and the class as value, the keys have 
//						match the keys in the assignment list
static Function setChannelAssign(prefs, assign, [classes])
	STRUCT ChannelAssignment &prefs
	String assign, classes

	Variable index = 0, dataAssigned = 0
	for (index = 0; index < ItemsInList(assign); index += 1)
		STRUCT ChannelDescription channel
		channel.version = prefs.version
		String item = StringFromList(index, assign)
		channel.name = StringFromList(0, item, ":")
		channel.type = StringFromList(1, item, ":")
		if (cmpstr(channel.type, "data") == 0)
			if (dataAssigned)
				print "** ERROR (setChannelAssign): only one data channel is allowed. Channel", channel.name, "set to 'other'."
				channel.type = "other"
			else
				dataAssigned = 1
			endif
		endif
		if (!ParamIsDefault(classes) && strlen(classes) > 0)
			channel.class = StringByKey(channel.name, classes)
		else
			channel.class = ""
		endif
		
		prefs.channels[index] = channel
	endfor
	prefs.numChannels = index
End

static Function saveChannelAssign(prefs, [reset])
	STRUCT ChannelAssignment &prefs
	Variable reset
	
	Variable recordID = getChannelPrefRecordID(prefs.numChannels)
	if (ParamIsDefault(reset) || reset < 0)
		reset = 0
	else
		reset = reset > 1 ? 1 : round(reset)
	endif
	
	if (!reset)
		SavePackagePreferences /FLSH=1 VM_getPackagePrefsName(), ksVM_GUIPrefsFileName, recordID, prefs
	else
		SavePackagePreferences /FLSH=1 /KILL VM_getPackagePrefsName(), ksVM_GUIPrefsFileName, recordID, prefs	
	endif
End

static Function loadChannelAssign(prefs)
	STRUCT ChannelAssignment &prefs
	
	Variable recordID = getChannelPrefRecordID(prefs.numChannels)
	
	LoadPackagePreferences /MIS=1 VM_getPackagePrefsName(), ksVM_GUIPrefsFileName, recordID, prefs
	if (V_flag != 0 || V_bytesRead == 0)
		defaultChannelAssignStruct(prefs)
	endif
End

static Function getChannelPrefRecordID(numChannels)
	Variable numChannels
	
	return (numChannels + getGUIRecordID("", max=1))
End

static Function /S getCursorFlag()
	
	DFREF path = VoltHelpers#getPackage()
	NVAR flag = path:$ksCFlag
	if (!NVAR_Exists(flag))
		Variable /G path:$ksCFlag = 0
	endif
	
	return (GetDatafolder(1, path)) + ksCFlag
End

static Function /WAVE getTransientFromGraph(theGraphName, [ask])
	String theGraphName
	Variable ask

	if (ParamIsDefault(ask) || ask < 0)
		ask = 0
	else
		ask = ask > 1 ? 1 : round(ask)
	endif
	
	String theTraceList = TraceNameList(theGraphName, ";", 1)
	Wave transient
	Wave nullRef

	// figure out how many traces we have
	switch (ItemsInList(theTraceList))
		case 0:
			print "-- ERROR (" + GetRTStackInfo(2) + "): no wave on the graph '" + theGraphName + "!"
			break
		case 1:
			Wave transient = TraceNameToWaveRef(theGraphName, StringFromList(0, theTraceList))
			break
		case 2:
		default:
			Wave transient = VoltAnalysis#getAnalysisTransient()
			if (!ask)
				if (WhichListItem(NameOfWave(transient), theTraceList) >= 0)
					return transient
				endif
			endif
			String traceName = ""
			Prompt traceName, "Choose the transient", popup, theTraceList
			DoPrompt "Transient Chooser", traceName
			
			if (V_flag)
				// user canceled
				return nullRef
			endif
			Wave transient = TraceNameToWaveRef(theGraphName, traceName)
	endswitch
		
	return transient
End

static Function getEpochFromLabel(theLabel)
	String theLabel
	
	Variable epoch = NaN
	Variable uSc = strsearch(theLabel, "_", 0)
	if (uSc >= 0 && uSc < strlen(theLabel) - 1)
		epoch = str2num(theLabel[uSc + 1, strlen(theLabel) - 1])
	endif
	return epoch
End // END getEpochFromLabel

static Function /WAVE getEpochListWave()

	DFREF package = VoltHelpers#getPackage()
	Wave epochs = VM_getEpochWave()
	Wave /T theListWave = package:$ksAvailableEpochs
	if (WaveExists(theListWave))
		return theListWave
	endif

	Make /N=(DimSize(epochs, 1),1) /T package:$ksAvailableEpochs /WAVE=theListWave
	SetDimLabel 1, 0, epochs, theListWave
	
	Variable index
	for (index = 0; index < DimSize(epochs, 0); index += 1)
		theListWave[index] = GetDimLabel(epochs, 0, index)
		SetDimLabel 0, index, $(GetDimLabel(epochs, 0, index)), theListWave
	endfor
	
	return theListWave
End // END getEpochListWave

static Function /WAVE getEpochSelWave()

	DFREF package = VoltHelpers#getPackage()
	Wave listWave = getEpochListWave()
	Wave selWave = package:$ksAvailableEpochsSel
	if (WaveExists(selWave))
		if (numpnts(selWave) == numpnts(listWave))
			return selWave
		endif
		Redimension /N=(numpnts(listWave)) selWave
		selWave = 0
		selWave[0] = 1
	else
		Make /N=(numpnts(listWave)) package:$ksAvailableEpochsSel /WAVE=selWave
		selWave = 0
	endif
	return selWave
End // END getEpochSelWave

static Function /WAVE getEpochCatListWave()

	DFREF package = VoltHelpers#getPackage()
	Wave /T categories = VM_getEpochCatWave()
	Wave /T theListWave = package:$ksGUIEpochCatList
	String theCategories = makeListFromWave(GetWavesDatafolder(categories, 2), sorted=1,unique=1)
	
	if (WaveExists(theListWave))
		if (ItemsInList(theCategories) == numpnts(theListWave))
			return theListWave
		else
			Redimension /N=(ItemsInList(theCategories),1) theListWave
		endif
	else
		Make /N=(ItemsInList(theCategories),1) /T package:$ksGUIEpochCatList /WAVE=theListWave
		SetDimLabel 1, 0, categories, theListWave
	endif
	
	Variable index
	for (index = 0; index < ItemsInList(theCategories); index += 1)
		theListWave[index] = StringFromList(index, theCategories)
	endfor
	
	return theListWave
End // END getEpochCatListWave

static Function /WAVE getEpochCatSelWave()

	DFREF package = VoltHelpers#getPackage()
	Wave /T theListWave = getEpochCatListWave()
	Wave theSelWave = package:$ksGUIEpochCatSel
	
	if (WaveExists(theSelWave))
		if (numpnts(theSelWave) == numpnts(theListWave))
			return theSelWave
		else
			Redimension /N=(numpnts(theListWave)) theSelWave
		endif
	else
		Make /N=(numpnts(theListWave)) package:$ksGUIEpochCatSel /WAVE=theSelWave
	endif
	
	theSelWave = 2^5
	
	return theSelWave
End // END getEpochCatSelWave

static Function cleanUp()
	
	print "---> start cleaning the helpers."
	Variable success = VoltHelpers#cleanUp()
	print "---> now clean the items from the analysis"
	VoltAnalysis#cleanUp()
	print "---> now clean the items from the GUI"
	
	DFREF cDF = GetDatafolderDFR()
	DFREF path = VoltHelpers#getPackage()
	SetDatafolder path
	KillWaves /Z $ksVM_xTransientDisp
	KillVariables /Z $ksCFlag
	KillStrings /Z $ksAnalysisFuncName, $ksControlFuncName
	SetDatafolder cDF
	print "-> done. Saving experiment."
	SaveExperiment
	return success
End

static Function refreshEpochPanelList()

	// updates the waves, which automatically updates the display
	Wave epochs = VM_getEpochWave()
	Wave /T epochList = getEpochListWave()
	Wave selWave = getEpochSelWave()		
	Redimension /N=(DimSize(epochs, 0), 1) epochList
	Redimension /N=(DimSize(epochs, 0), 1) selWave
	selWave = 0
	selWave[0] = 1
	Variable index
	for (index = 0; index < DimSize(epochs, 0); index += 1)
		epochList[index] = GetDimLabel(epochs, 0, index)
	endfor

	getEpochCatListWave()
	Wave selWave = getEpochCatSelWave()
	if (numpnts(selWave) > 0)
		selWave = selWave[p] & ~(2^4)		// clear bit 4 (status of checkbox)
	endif
	return 1
End

static Function checkWinType(theName, type)
	String theName, type
	
	Variable ok = 0
	
	if (strlen(theName) == 0)
		return ok
	endif
	if (cmpstr(theName, "_new_") == 0)
		return 1
	endif
	strswitch(type)
		case "graph":	
			ok = (WinType(theName) == 1 || WinType(theName) == 0)
			break
		case "table":
			ok = (WinType(theName) == 2 || WinType(theName) == 0)
			break
	endswitch
	
	return ok
End

static Function /S getOrgzdExt()
	return ksFileOrgzdExtension
End


Function VM_changePSYWarning(feature, warning)
	Variable feature, warning

	STRUCT VM_ExpFeatureControl control
	loadExpFeature(control)
	control.features[0].enabled = feature
	control.features[0].warningEnabled = warning
	saveExpFeature(control)
End
	

static Function /S chooseFiles(type)
	String type
	
	String fileFilter = ""
	strswitch ( LowerStr(type) )
		case "tdm":
		case "tdms":
			fileFilter = "LabView data files (*.tdm, *.tdms):.tdm,.tdms;"
			break
		case "abf":
			fileFilter = "pClamp data files (*.abf):.abf;"
			break
		case "ibw":
			fileFilter = "Igor Binary Files (*.ibw):.ibw;"
			break
		case "pxp":
		default:
			fileFilter = "Igor Data Files (*.pxp):.pxp;"
	endswitch
		
	fileFilter += "All Files (.*):.*;"
	String msg = "Choose data file"	

	STRUCT VM_GUIPreferences prefs
	loadGUIPrefs(prefs)

	String folderPath = ""
	Variable refNum
	PathInfo rbVMDataLoadPath

	if (V_flag)
		Open /D/R/F=fileFilter/M=msg/P=rbVMDataLoadPath/MULT=1 refNum
	else
		String pathToFolder = prefs.loadpath
		NewPath /O/Q/Z rbVMDataLoadPath, pathToFolder
		GetFileFolderInfo /D /P=rbVMDataLoadPath /Q /Z
		if (V_flag == 0)
			Open /D/R/F=fileFilter/M=msg/P=rbVMDataLoadPath/MULT=1 refNum
		else
			Open /D/R/F=fileFilter/M=msg/MULT=1 refNum
		endif		
	endif
	if (strlen(S_fileName) == 0)
		print "- User canceled data load (" + date() + "   " + time() + ")."
		return ""
	endif
	
	String fileList = S_fileName
	folderPath = ParseFilePath(1, (StringFromList(0, fileList, "\r")), ":", 1, 0)

	NewPath /O/Q rbVMDataLoadPath, folderPath
	prefs.loadpath = folderPath
	saveGUIPrefs(prefs)

	if (V_flag != 0)
		// something went wrong setting the chosen folder path
		print "- something went wrong setting the folder path '"+ folderPath + "'."
		return ""
	endif

	return fileList
End

static Function /S chooseFolder([fileType])
	String fileType
	
	if (ParamIsDefault(fileType) || strlen(fileType) == 0)
		fileType = "????"
	else
		if (cmpstr(fileType[0], "*") == 0)
			fileType = fileType[1,Inf]
		endif
		if (cmpstr(fileType[0], ".") != 0)
			if (strlen(fileType) == 3 || strlen(fileType) == 4)
				fileType = "." + fileType
			endif
		endif
	endif
	
	String fileList = ""

	STRUCT VM_GUIPreferences prefs
	loadGUIPrefs(prefs)

	String msg = "Browse to the data file folder ..."
	GetFileFolderInfo /Q/D
	if (V_flag != 0)
		// user pressed cancel in the dialog or folder was not found
		return fileList
	endif

	String folderPath = S_path
	prefs.loadPath = folderPath
	NewPath /O/Q $ksloadPath, folderPath
	saveGUIPrefs(prefs)
	
	fileList = IndexedFile($ksloadPath, -1, fileType)

	return fileList
End

// **************************************************************************
// *********************                         M E N U E    I T E M S                                       ***********
// **************************************************************************

Function VM_loadIgorDataFiles()

	String fileList = chooseFiles("pxp")
	Variable filesToLoad = ItemsInList(fileList, "\r")
	
	if (filesToLoad == 0 || strlen(fileList) == 0)
		print "-- no files selected, stopped."
		return 0
	endif

	// An open data browser slows the loading / merging of the files down at least 3-5 fold. To 
	// increase the loading speed, every loading operation will close an open data browser
	// Since I can't find any documentation about the data browser commands, the following code
	// will work, but should be cleaned after more research
	Execute /Z/Q "ModifyBrowser close" 	// does not terminate execution if browser is not active
	
	Variable file = 0
	String fullFileName = "", fileName = "", cmd = ""

	if (cmpstr(IgorInfo(1), "Untitled") == 0)
		fullFileName = StringFromList(0, fileList, "\r")
		fileName = ParseFilePath(3, fullFileName, ":", 0, 0) + ksFileOrgzdExtension + ".pxp"
		SaveExperiment /P=rbVMDataLoadPath as fileName
	endif
	
	String currentDF = GetDatafolder(1)
	SetDatafolder VoltHelpers#getDataLoadFolder()
	
	Variable microseconds = 0
	Variable timerRefNum = startMSTimer
	if (timerRefNum == -1)
		timerRefNum = 0
		do
			microseconds = stopMSTimer(timerRefNum)
			timerRefNum += 1
		while (timerRefNum < 10)
	endif
	
	for (file = 0; file < filesToLoad; file += 1)
		fullFileName = StringFromList(file, fileList, "\r")
		cmd = "MERGEEXPERIMENT " + fullFileName
		fileName = ParseFilePath(3, fullFileName, ":", 0, 0)
		Execute /P cmd
		cmd = "VM_organizeDataWaves(dfn=\"" + fileName + "\", add=1,feedback=1)"
		Execute /P cmd
	endfor
	
	cmd = "print \"\\rLoading and organizing the waves took \" + microSecondsToTimeStr(stopMSTimer(" + num2str(timerRefNum) + ")) + \" on  \",date(), time()"
	Execute /P/Q cmd
	cmd = "VM_checkChannelAssign()"
	Execute /P cmd
	cmd = "SetDatafolder " + currentDF
	Execute /P/Q cmd		
End


//! This function loads a folder full of Igor binary files created by HEKA Patchmaster.
// @param series:	      	Patchmaster series number of the igor binary files to load
// @param rampChannel:		Channel number containing the ramp, default is 2
// @param substance:		set the molecule you are measuring, default is dopamine
// @param add:					add or overwrite the existing waves with the new load, default is add (1)
//-
Function VM_loadPMIgorBinaryFiles([series, rampChannel, substance, multisweep, add])
	Variable series, rampChannel, multisweep, add
	String substance
	
	series = ParamIsDefault(series) || series <= 0 ? 0 : round(series)
	rampChannel = ParamIsDefault(rampChannel) || rampChannel > 4 ? 2 : round(rampChannel)
	add = ParamIsDefault(add) || add < 1 ? 0 : 1
	
	if (ParamIsDefault(substance) || strlen(substance) == 0)
		substance = "dopamine"
	endif
	VMRamps#setCurrentRampName(substance)
	
	String errormsg = VM_getErrorStr("VM_loadIgorBinaryFiles")
	String warnmsg = VM_getWarnStr("VM_loadIgorBinaryFiles")
	
	Variable success = 0
	String fileList = ""
	
	if (multisweep)
		fileList = ReplaceString("\r", chooseFiles("ibw"), ";")
	else
		fileList = chooseFolder(fileType=".ibw")
	endif
	
	Variable filesToLoad = ItemsInList(fileList)

	if (filesToLoad == 0 || strlen(fileList) == 0)
		printf errormsg, "no files selected matched ending '.ibw', stopped"
		return 0
	endif

	if (series > 0)
		fileList = VoltHelpers#filterHEKABySeries(series, fileList)
		filesToLoad = ItemsInList(fileList)
	else
		fileList = SortList(fileList, ";", 16)
	endif

	// An open data browser slows the loading / merging of the files down at least 3-5 fold. To 
	// increase the loading speed, every loading operation will close an open data browser
	// Since I can't find any documentation about the data browser commands, the following code
	// will work, but should be cleaned after more research
	Execute /Z/Q "ModifyBrowser close" 	// does not terminate execution if browser is not active
	
	String fullFileName = "", fileName = ""

	if (cmpstr(IgorInfo(1), "Untitled") == 0)
		fullFileName = StringFromList(0, fileList)
		fileName = ParseFilePath(3, fullFileName, ":", 0, 0) + ksFileOrgzdExtension + ".pxp"
		SaveExperiment /P=rbVMDataLoadPath as fileName
	endif
	
	Variable microseconds = 0
	Variable timerRefNum = startMSTimer
	if (timerRefNum == -1)
		timerRefNum = 0
		do
			microseconds = stopMSTimer(timerRefNum)
			timerRefNum += 1
		while (timerRefNum < 10)
	endif
	
	if (multisweep)
		success = loadHEKAMultipleBinary(fileList, substance=substance, rampChannel=rampChannel, add=add)
	else
		success = loadHEKASweepBinary(fileList, substance=substance, rampChannel=rampChannel, add=add)
	endif
	
	if (success)
		print "\rLoading and organizing the waves took " + microSecondsToTimeStr(stopMSTimer(timerRefNum)) + " on  ", date(), time()
	
		VM_checkChannelAssign()
	else
		print "\rERROR loading the wave files on ", date(), time()
	endif
End

//! This worker function organizes the Igor Pro binary files recorded with HEKA into the Voltammetry data structure.
// This function handles voltammetry data where multiple sweeps have been accumulated into a single trace.
// @param fileList:		string list of semicolon separated file names to load
// @param substance:	name of the substance that is recorded (defaults to dopamine)
// @param rampChannel: 	a number between 0-3 indicating the channel that contains the ramp (defaults to 2)
// @param add:			0 or 1 (default) to overwrite or add the newly loaded files to the existing ones
// @return either 0 or 1 if the function successfully finishes
//-
static Function loadHEKAMultipleBinary(fileList, [substance, rampChannel, cutToTime, add])
	String fileList, substance
	Variable rampChannel, cutToTime, add

	if (ParamIsDefault(substance) || strlen(substance) == 0)
		substance = "dopamine"
	else
		substance = LowerStr(substance)
	endif
	rampChannel = ParamIsDefault(rampChannel) || rampChannel < 0 ? 2 : rampChannel
	cutToTime = ParamIsDefault(cutToTime) || cutToTime <= 0 ? 0.01 : cutToTime
	add = ParamIsDefault(add) || add >= 1 ? 1 : 0	
	
	Variable filesToLoad = ItemsInList(fileList)
	if (filesToLoad == 0)
		return 0
	endif

	Variable file = 0, rOffset = 0, cc = 0, vc = 0
	Variable rows = 0, columns = 0
	DFREF cDF = GetDatafolderDFR()
	
	DFREF package = VoltHelpers#getPackage()
	DFREF loadFolder = VoltHelpers#getDataLoadFolder()

	for (file = 0; file < filesToLoad; file += 1)
		LoadWave /W/A/O/Q /P=rbVMDataLoadPath StringFromList(file, fileList)
		String dataName = StringFromList(0, S_waveNames)
		Wave dataWave = $(dataName)
	
		String acqFreqUnits = WaveUnits(dataWave, 1)
		String dataUnits = WaveUnits(dataWave, 0)
		Variable acqFreqInterval = deltax(dataWave)
		
		rows = (1/kVM_acqFrequency) / acqFreqInterval
		columns = numpnts(dataWave) / rows
		Variable cutToPnts = cutToTime / acqFreqInterval
		
		String matrixName = "AD" + dataName[strlen(dataName)-1]
		Wave datamatrix = loadFolder:$matrixName
		
		Redimension /N=(rows, columns) dataWave
		SetScale /P y 0, 1/kVM_acqFrequency, "s", dataWave
		MatrixTranspose dataWave
		
		if (WaveExists(datamatrix))
			if (add)
				columns = DimSize(datamatrix, 1)
				if (columns != cutToPnts)
					cutToPnts = columns
				endif
				
				Redimension /N=(-1, cutToPnts) dataWave
				Concatenate /NP=0 {dataWave}, datamatrix
			else
				Redimension /N=(-1, cutToPnts) dataWave
				Duplicate /O dataWave, dataMatrix
			endif
		else
			Redimension /N=(-1, cutToPnts + 1) dataWave
			Duplicate /O dataWave, loadFolder:$matrixName
		endif
	
		KillWaves /Z dataWave
	endfor

	if (rampChannel >= 0)
		DFREF rawFolder = VoltHelpers#getDataLoadFolder()
		Wave ramps = rawFolder:$("AD" + num2str(rampChannel))
		if (WaveExists(ramps))
			Make /N=(DimSize(ramps, 1))/O tmp
			tmp = ramps[0][p]
			SetScale /P x 0, DimDelta(ramps, 1), WaveUnits(ramps, 1), tmp
			SetScale d 0,0, WaveUnits(ramps, -1), tmp
			String aNote = VMRamps#analyzeRamp(tmp)
			VMRamps#setCurrentRamp(tmp, rNote = aNote)

			STRUCT VoltammetryPreferences prefs
			VoltHelpers#getTransPrefs(prefs)
			
			STRUCT OxidationDetails od
			od = prefs.oxDetails
			
			od.oxidation = 1			// look for oxidation point
			od.voltage = str2num(StringFromList(0, StringByKey(substance, ksVM_SubstanceOxidations), ",")) / 1000
			od.range = str2num(StringFromList(1, StringByKey(substance, ksVM_SubstanceOxidations), ",")) / 1000 * 2
			od.startPoint = NumberByKey(ksVM_RangeStartP, aNote)
			od.endPoint = NumberByKey(ksVM_RangeEndP, aNote)
			od.startVoltage = od.voltage - (od.range / 2)
			od.endVoltage = od.startVoltage + od.range
			od.startTime = pnt2x(tmp, od.startPoint)
			od.endTime = pnt2x(tmp, od.endPoint)
			
			prefs.oxDetails = od
			VoltHelpers#setTransPrefs(prefs)
			
			// after we set the ramp we don't need the ramp channel any more, so free up the space
			KillWaves /Z tmp, ramps
		endif
	endif

	return 1
End

//! This worker function organizes the Igor Pro binary files recorded with HEKA into the Voltammetry data structure
// @param fileList:		string list of semicolon separated file names to load
// @param substance:	name of the substance that is recorded (defaults to dopamine)
// @param rampChannel: a number between 0-3 indicating the channel that contains the ramp (defaults to 2)
// @param cutToTime:	the time length to cut the 100 ms long trace to (defaults to 0.01 (10 ms) for dopamine)	
// @param add:				0 or 1 (default) to overwrite or add the newly loaded files to the existing ones
// @return either 0 or 1 if the function successfully finishes
//-
static Function loadHEKASweepBinary(fileList, [substance, rampChannel, cutToTime, add])
	String fileList, substance
	Variable rampChannel, cutToTime, add

	if (ParamIsDefault(substance) || strlen(substance) == 0)
		substance = "dopamine"
	else
		substance = LowerStr(substance)
	endif
	rampChannel = ParamIsDefault(rampChannel) || rampChannel < 0 ? 2 : rampChannel
	cutToTime = ParamIsDefault(cutToTime) || cutToTime <= 0 ? 0.01 : cutToTime
	add = ParamIsDefault(add) || add >= 1 ? 1 : 0	
	
	Variable filesToLoad = ItemsInList(fileList)
	if (filesToLoad == 0)
		return 0
	endif

	Variable file = 0, rOffset = 0, cc = 0, vc = 0, scFlag = 0, svFlag = 0
	Variable rows = 0, columns = 0
	DFREF cDF = GetDatafolderDFR()
	
	DFREF package = VoltHelpers#getPackage()
	DFREF loadFolder = VoltHelpers#getDataLoadFolder()

	Wave currentmatrix = loadFolder:AD1
	Wave voltagematrix = loadFolder:AD2
	
	if (WaveExists(currentmatrix))
		if (add)
			rOffset = DimSize(currentmatrix, 0)
			Redimension /N=(rOffset + (filesToLoad/2), -1) currentmatrix, voltagematrix
		else
			rOffset = 0
		endif
		Redimension /N=(rOffset + (filesToLoad/2), -1) currentmatrix, voltagematrix
	else
		Make /N=(filesToLoad/2, 1) /O loadFolder:AD1 /WAVE=currentmatrix, loadFolder:AD2 /WAVE=voltagematrix
		SetScale /P x 0, 1/kVM_AcqFrequency, "s", currentmatrix, voltagematrix
	endif

	for (file = 0; file < filesToLoad; file += 1)
		LoadWave /W/A/O/Q /P=$ksloadPath StringFromList(file, fileList)
		
		Wave dataWave = $(StringFromList(0, S_waveNames))
		String dataName = NameOfWave(dataWave)

		String acqFreqUnits = WaveUnits(dataWave, 0)
		String dataUnits = WaveUnits(dataWave, 1)
		Variable acqFreqInterval = deltax(dataWave)

		if (cmpstr(dataName[strlen(dataName)-1], "1") == 0)
			if (!scFlag)
				SetScale /P y 0, acqFreqInterval, acqFreqUnits, currentmatrix
				SetScale d 0, 0, dataUnits, currentmatrix
				scFlag = 1
			endif				
			if (numpnts(dataWave) > DimSize(currentmatrix, 1))
				Redimension /N=(-1, numpnts(dataWave)) currentmatrix
			endif
			if (rOffset + cc < DimSize(currentmatrix, 0))
				currentmatrix[rOffset + cc][] = dataWave[q]
			endif
			cc += 1
		endif
		if (cmpstr(dataName[strlen(dataName)-1], "2") == 0)
			if (!svFlag)
				SetScale /P y 0, acqFreqInterval, acqFreqUnits, voltagematrix
				SetScale d 0, 0, dataUnits, voltagematrix
				svFlag = 1
			endif
			if (numpnts(dataWave) > DimSize(voltagematrix, 1))
				Redimension /N=(-1, numpnts(dataWave)) voltagematrix
			endif
			if (rOffset + vc < DimSize(voltagematrix, 0))
				voltagematrix[rOffset + vc][] = dataWave[q]
			endif
			vc += 1
		endif
		KillWaves /Z dataWave
	endfor

	rows = (1/kVM_acqFrequency) / acqFreqInterval
	columns = numpnts(dataWave) / rows
	Variable cutToPnts = cutToTime / acqFreqInterval

	Redimension /N=(-1, cutToPnts) currentmatrix, voltagematrix

	if (rampChannel >= 0)
		DFREF rawFolder = VoltHelpers#getDataLoadFolder()
		Wave ramps = rawFolder:$("AD" + num2str(2))
		if (WaveExists(ramps))
			Make /N=(DimSize(ramps, 1))/O tmp
			tmp = ramps[0][p]
			SetScale /P x 0, DimDelta(ramps, 1), WaveUnits(ramps, 1), tmp
			String aNote = VMRamps#analyzeRamp(tmp)
			VMRamps#setCurrentRamp(tmp, rNote = aNote)

			STRUCT VoltammetryPreferences prefs
			VoltHelpers#getTransPrefs(prefs)
			
			STRUCT OxidationDetails od
			od = prefs.oxDetails
			
			od.oxidation = 1			// look for oxidation point
			od.voltage = str2num(StringFromList(0, StringByKey(substance, ksVM_SubstanceOxidations), ",")) / 1000
			od.range = str2num(StringFromList(1, StringByKey(substance, ksVM_SubstanceOxidations), ",")) / 1000 * 2
			od.startPoint = NumberByKey(ksVM_RangeStartP, aNote)
			od.endPoint = NumberByKey(ksVM_RangeEndP, aNote)
			od.startVoltage = od.voltage - (od.range / 2)
			od.endVoltage = od.startVoltage + od.range
			od.startTime = pnt2x(tmp, od.startPoint)
			od.endTime = pnt2x(tmp, od.endPoint)
			
			prefs.oxDetails = od
			VoltHelpers#setTransPrefs(prefs)
			
			// after we set the ramp we don't need the ramp channel any more, so free up the space
			KillWaves /Z tmp, ramps
		endif
	endif
	
	return 1
End


//! This function organizes the raw, single sweep data waves into matrixes sorted by acquisition channel.
// @param dfn:	data folder to look for the loaded waves
// @param acqFreq:	acquisition frequency, used to scale the waves
// @param add:	add the found waves to the existing organized waves
// @param feedback:		flag set to 1 if feedback should be given or 0 for quiet execution
// @param type:			number to indicate wave type 
//								default 1: abf files laoded with Bruxton extension)
//										2: igor binary waves using the last number separated by underscore as channel number
//-
Function VM_organizeDataWaves([dfn, acqFreq, add, feedback, type])
	String dfn
	Variable acqFreq, add, feedback, type
	
	DFREF cDF = GetDatafolderDFR()
	Variable kill = 1

	if (ParamIsDefault(dfn) || strlen(dfn) == 0)
		DFREF dfr = root:
		kill = 0
	else
		DFREF dfr = cDF:$dfn
	endif
	if (DataFolderRefStatus(dfr) == 0)
		DoAlert /T="Error" 0, "Data folder for the waves does not exists!"
		return 0
	endif
	if (ParamIsDefault(acqFreq) || acqFreq <= 0)
		acqFreq = 1/kVM_AcqFrequency
	endif
	if (ParamIsDefault(add) || add < 0)
		add = 0
	else
		add = add > 1 ? 1 : round(add)
	endif
	if (ParamIsDefault(feedback) || feedback < 0)
		feedback = 0
	else
		feedback = feedback > 1 ? 1 : round(feedback)
	endif
	
	type = ParamIsDefault(type) || type < 2 ? 1 : round(type)
	
	String selector = ""
	
	switch (type)
		case 2:
			selector = "*_*_*_*_*"
			break
		case 1:
		default:
			selector = "*_AD"
	endswitch
	
	DFREF cDF = GetDatafolderDFR()
	DFREF package = VoltHelpers#getPackage()
	DFREF loadFolder = VoltHelpers#getDataLoadFolder()

	String experimentName = IgorInfo(1)
	if (cmpstr(experimentName, "Untitled") == 0)
		experimentName = ksDefaultFileName + ksFileOrgzdExtension
	endif
	if (strsearch(experimentName, ksFileOrgzdExtension, 0) == -1)
		experimentName += ksFileOrgzdExtension
	endif
	SaveExperiment /P=home as (experimentName + ".pxp")
	
	Variable index = 0, waves = 0, rows =0, columns = 0, rOffset = 0
	String theList = ""
	
	if (feedback)
		String progressBar = VM_getProgressBarPanel(theTitle="Organizing Data Waves", theName="ProgressWaveOrganziation", theColor="blue")
	endif

	SetDatafolder dfr
	
	Variable maxChannels = 4
	do
		rows = 0
		theList = SortList(WaveList(selector + num2str(index), ";", ""), ";", 16)
		if (strlen(theList) == 0)
			if (index < maxChannels)
				Variable channelNum
				for (channelNum = index + 1; index < maxChannels; index += 1)
					theList = SortList(WaveList(selector + num2str(index), ";", ""), ";", 16)
					if (strlen(theList) > 0)
						break
					endif
				endfor
				if (strlen(theList) == 0)
					if (feedback)
						KillWindow $progressBar
					endif
					break
				endif
			endif
		endif
		Wave channel = loadFolder:$("AD" + num2str(index))
		if (!WaveExists(channel))
			Make /N=(rows, columns) loadFolder:$("AD" + num2str(index)) /WAVE=channel
		endif
		Wave first = $(StringFromList(0, theList))
		if (add)
			rOffset = DimSize(channel, 0)
//			print "--> previsous wave", NameOfWave(channel), "had", DimSize(channel, 0), "rows."
		endif
		rows = ItemsInList(theList)
		columns = numpnts(first)
		Redimension /N=(rows + rOffset, columns) channel
		SetScale /P y, 0, deltax(first), WaveUnits(first, 0), channel
		SetScale /P x, 0, acqFreq, "s", channel
		SetScale d 0, 0, WaveUnits(first, 1), channel
//		print "--> redimensioned wave", NameOfWave(channel), "to", DimSize(channel, 0), "rows."
		
		if (feedback)
			String theInfo = "moving " + num2str(rows) + " waves to channel AD" + num2str(index)
			VM_updateProgressbar(0, info=theInfo, theName=progressBar)
		endif
		
		do
			Wave tmp = $(StringFromList(waves, theList))
			channel[waves + rOffset][] = tmp[q]
			KillWaves /Z tmp
			
			if (feedback && mod(waves, 100) == 0)
				VM_updateProgressbar((waves/rows), theName=progressBar)
			endif
			
			waves += 1
		while ( (waves + rOffset) < DimSize(channel, 0) )

		if (feedback)
			VM_updateProgressbar(1, theName=progressBar)
		endif
		
		waves = 0
		index += 1
	while ( 1 )

	SetDatafolder cDF
	
	STRUCT VM_GUIPreferences prefs
	loadGUIPrefs(prefs)
	
	if (kill)
		KillDatafolder /Z dfr
	endif
	SaveExperiment
	
	VM_checkChannelAssign()
	return 1
End

Function VM_createChannelAssign(channels, [refine])
	String channels
	Variable refine
	
	if (ParamIsDefault(refine) || refine < 0)
		refine = 0
	else
		refine = refine > 1 ? 1 : round(refine)
	endif
	if (refine)
		channels = VM_getChannels()
	endif
	
	Variable numChannels = ItemsInList(channels)
	if (numChannels < 1)
		DoAlert 0, "No channels available!"
		return 0
	endif
	String lableList = "channel;data;control"
	DFREF package = VoltHelpers#getPackage()
	Make /N=(numChannels, 3) /O /T package:$ksGUIChannelAssign /WAVE=assignlist
	Make /N=(numChannels, 3) /O package:$ksGUIChannelAssignSel /WAVE=assignsel
	Variable index
	do
		SetDimLabel 1, index, $(StringFromList(index, lableList)), assignlist
		SetDimLabel 1, index, $(StringFromList(index, lableList)), assignsel
		index += 1
	while (index < ItemsInList(lableList))
	index = 0
	assignsel[][0] = 0
	assignsel[][1,2] = 2^5
	
	Make /N=(numChannels, 1) /T /O package:$ksGUIChannelClass /WAVE=classlist
	Make /N=(numChannels, 1) /O package:$ksGUIChannelClassSel /WAVE=classsel
	SetDimLabel 1, 0, category, classlist
	classsel = 0
	
	STRUCT ChannelAssignment assign
	assign.numChannels = numChannels
	loadChannelAssign(assign)
	
	String loadList = ""
	do
		loadList = AddListItem(assign.channels[index].name, loadList, ";", Inf)
		index += 1
	while( index < numChannels )
	
	String channelList = intersectLists(channels, loadList)
	
	String theChannel = "", theType = "", theClass = ""
	for (index = 0; index < numChannels; index += 1)
		theChannel = StringFromList(index, channels)	
		if (cmpstr(channelList, loadList) == 0)
			theChannel = assign.channels[index].name
			theType = assign.channels[index].type
			theClass = assign.channels[index].class
		else
			Variable assignments = -1
			if (WhichListItem(theChannel, loadList) >= 0)
				// we have a preference for the channel
				do
					assignments += 1
				while( cmpstr(theChannel, assign.channels[assignments].name) != 0 && assignments < assign.numChannels)
				theType = assign.channels[assignments].type
				theClass = assign.channels[assignments].class
			else
				theType= "control"
				theClass = "electrical"
			endif
		endif
		SetDimLabel 0, index, $(theChannel), assignlist
		SetDimLabel 0, index, $(theChannel), assignsel
		assignlist[index][0] = theChannel
		classlist[index][0] = theClass
		if (WhichListItem(theType, lableList) >= 0)
			assignsel[index][%$(theType)] = assignsel[index][%$(theType)] | 2^4
		else
			lableList = AddListItem(theType, lableList, ";", Inf)
			ReDimension /N=(-1, DimSize(assignlist, 1) + 1) assignlist, assignsel
			Variable lastColumn = DimSize(assignlist, 1) -1
			SetDimLabel 1, lastColumn, $(theType), assignlist
			SetDimLabel 1, lastColumn, $(theType), assignsel
			assignsel[][lastColumn] = 2^5
			assignsel[index][lastColumn] = assignsel[index][lastColumn] | 2^4
		endif
	endfor	
	
	DoWindow /F $ksDefChannelPanel
	if (V_flag)
		DoWindow /K $ksDefChannelPanel
	endif
	
	NewPanel /K=2 /W=(50,100,377,244) /N=$ksDefChannelPanel as ksDefChannelPanelTitle
	
	SetWindow $ksDefChannelPanel hook(cleanup) = WH_killChannelAssignPanel
	SetWindow $ksDefChannelPanel userdata(assignlist)= GetWavesDatafolder(assignlist, 2)
	SetWindow $ksDefChannelPanel userdata(assignsel)= GetWavesDatafolder(assignsel, 2)
	SetWindow $ksDefChannelPanel userdata(classes)= GetWavesDatafolder(classlist, 2)
	SetWindow $ksDefChannelPanel userdata(classSel) = GetWavesDatafolder(classsel, 2)
	
	ListBox lstbx_channelAssignment,pos={12,13},size={200,100}
	ListBox lstbx_channelAssignment, mode=0, font="Verdana", fsize=10
	ListBox lstbx_channelAssignment, widths={12, 8}
	ListBox lstbx_channelAssignment, listwave=assignlist
	ListBox lstbx_channelAssignment, selwave=assignsel
	ListBox lstbx_channelAssignment, proc=lbFunc_VM_checkChannelSelection
	
	ListBox lstbx_channelClasses, pos={214, 13}, size={100,100}
	ListBox lstbx_channelClasses, mode=1, font="Verdana", fsize=10
	ListBox lstbx_channelClasses, listwave=classlist
	ListBox lstbx_channelClasses, selwave=classsel
	ListBox lstbx_channelClasses, proc=lbFunc_VM_channelClasses

	Button btn_assignDone, pos={130, 116}, size={70, 20}, title="done"
	Button btn_assignDone, font="Verdana", fStyle=1
	Button btn_assignDone, proc=btnFunc_VM_doneChanAssignCheck
	
	moveWindowOnScreen(ksDefChannelPanel)
	PauseForUser $ksDefChannelPanel
End


Function btnFunc_VM_doneChanAssignCheck(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch( bs.eventCode )
		case 2:
			String killList = ""
			Wave /T assignList = $GetUserData(bs.win, "", "assignlist")
			Wave classsel = $GetUserData(bs.win, "", "classsel")
			
			Wave assignSel = $GetUserData(bs.win, "", "assignsel")
			Wave /T classList = $GetUserData(bs.win, "", "classes")

			killList = AddListItem(GetUserData(bs.win, "", "assignlist"), killList, ",")
			killList = AddListItem(GetUserData(bs.win, "", "assignsel"), killList, ",")
			killList = AddListItem(GetUserData(bs.win, "", "classes"), killList, ",")
			killList = AddListItem(GetUserData(bs.win, "", "classsel"), killList, ",")			
			
			Variable index = 0
			Variable hasData = VM_hasDataAssigned(assignSel)
			if (!hasData)
				DoAlert 0, "Exactly one channel needs to be a data channel!"
				return 0
			endif
			
			String theChannel = "", theAssignList = "", theClassList = ""
			Wave /T assignment = VoltHelpers#getChannelAssignment()
			Wave /T classes = VoltHelpers#getChannelClasses()
			Redimension /N=(DimSize(assignSel, 0)) assignment, classes
			assignment = ""
			classes = ""
			do
				theChannel = GetDimLabel(assignSel, 0, index)
				SetDimLabel 0, index, $theChannel, assignment
				SetDimLabel 0, index, $theChannel, classes 
				
				Make /N=(DimSize(assignSel, 1)) /O /FREE rowSelection = assignSel[index][p]
				Extract /FREE /INDX /O rowSelection, selectedRow, rowSelection & 2^4
				
				assignment[index] = GetDimLabel(assignSel, 1, selectedRow[0])
				theAssignList = ReplaceStringByKey(theChannel, theAssignList, assignment[index])
				classes[index] = classList[index][0]
				theClassList = ReplaceStringByKey(theChannel, theClassList, classes[index])
				index += 1
			while( index < numpnts(classList) )
				
			STRUCT ChannelAssignment prefs
			setChannelAssign(prefs, theAssignList, classes=theClassList)
			saveChannelAssign(prefs)
			
			SetWindow $bs.win hook(cleanup)=$""
			DoWindow /K $bs.win
			Execute /P/Q/Z "KillWaves /Z " + killList	
			VoltHelpers#getChannelAssignment(show=1)
			VoltHelpers#getChannelClasses(show=1)
			break
	endswitch
	return 0
End

Function lbFunc_VM_channelClasses(lbs) : ListboxControl
	STRUCT WMListboxAction &lbs
	
	switch( lbs.eventCode )
		case 1:
			// mouse down
			Wave selw = lbs.selwave
			selw = selw[p] &~(2^1)
			selw[lbs.row][lbs.col] = selw[lbs.row][lbs.col] | 2^1	// make cell editable
//			ListBox $lbs.ctrlName, win=$lbs.win, setEditCell={lbs.row, lbs.col, -1, 0}
			break
	endswitch
End

Function lbFunc_VM_checkChannelSelection(lbs) : ListboxControl
	STRUCT WMListBoxAction &lbs
	
	switch( lbs.eventCode )
		case 2:
			// mouse button up
			Wave /T listw = lbs.listwave
			Wave selw = lbs.selwave
			Wave /T classes = $GetUserData(lbs.win, "", "classes")
			if (cmpstr(GetDimLabel(listw, 1, lbs.col), "channel") == 0)
				// first column is the description, so do nothing
				return 0
			endif
			
			Variable index
			selw[lbs.row][1, DimSize(selw, 1)-1] = 2^5
			selw[lbs.row][lbs.col] = selw[lbs.row][lbs.col] | 2^4
			if (cmpstr(GetDimLabel(listw, 1, lbs.col), "data") != 0)
				classes[lbs.row][0] = "electrical"
			endif
			
			if (cmpstr(GetDimLabel(listw, 1, lbs.col), "data") == 0)
				classes[lbs.row][0] = "data"
				// selected a row in the data column
				Make /FREE /N=(DimSize(selw, 0)) /O dataSelection = selw[p][%data]
				Extract /FREE /INDX /O dataSelection, dataIndex, dataSelection & 2^4
				if (numpnts(dataIndex) > 1)
					Extract /FREE /O dataIndex, removeIndex, dataIndex != lbs.row
					for (index = 0; index < numpnts(removeIndex); index += 1)
						selw[removeIndex[index]][%data] = selw[removeIndex[index]][%data] &~(2^4)
						selw[removeIndex[index]][%control] = selw[removeIndex[index]][%control] | (2^4)
						classes[removeIndex[index]][0] = "electrical"
					endfor
				endif
			endif
			break
	endswitch
	return 0
End

Function VM_hasDataAssigned(selWave)
	Wave selWave
	
	Variable index = 0, hasData = 0
	if (!WaveExists(selWave) || WaveDims(selWave) != 2)
		return hasData
	endif
	do
		hasData = (selWave[index][%data] & 2^4) != 0
		index += 1
	while( !hasData && index < DimSize(selWave, 0) )
	return hasData
End


Function /S VM_updateCatColorPrefs()
	STRUCT VM_CatColorPrefs prefs
	VoltAnalysis#loadColorCatPreferences()
End

Function /S VM_setCategoryColorPrefs()

	Wave /T prefColors = VoltAnalysis#getCatColorPrefWave()
	String title = "Category Color Preferences"
	String name = ReplaceString(" ", title, "")
	
	Edit /K=1 /W=(5,45,242,283) /N=$name prefColors.ld
	ModifyTable /W=$name title(prefColors.l)="category", width(prefColors.l) = 68
	ModifyTable /W=$name title(prefColors.d)="color", width(prefColors.d) = 74
	ModifyTable /W=$name alignment(prefColors.ld)=0, format(Point)=1
	
	SetWindow $name hook(savePrefs)=VM_WH_saveCatColPref
	return name
End


Function VM_WH_saveCatColPref(wh)
	STRUCT WMWinHookStruct &wh

	Variable hookResult = 0
	
	switch(wh.eventCode)
		case 2:			// kill
			SetWindow $wh.winName hook(savePrefs) = $""
			VoltAnalysis#saveColorCatPreferences()
			break
		case 11:			// keyboard
			switch (wh.keycode)
				case 3:		// Enter
				case 9:		// Tab
				case 13:		// Return
				case 28:		// left arrow key
				case 29:		// right arrow key
				case 30:		// up arrrow key
				case 31:		// down arrow key
//					VoltAnalysis#saveColorCatPreferences()
					break
			endswitch
			break
	endswitch
	
	return hookResult
End

// ****************************************************
// ************                     O T H E R                               ***********
// ****************************************************

Function /S VM_getAnalysisFuncStr([name])
	String name
	
	if (ParamIsDefault(name) || strlen(name) == 0)
		return VoltHelpers#getSVarByName(ksAnalysisFuncName)
	else
		return VoltHelpers#getSVarByName(ksAnalysisFuncName, text=name)
	endif
End

Function /S VM_getControlFuncStr([name])
	String name
	
	if (ParamIsDefault(name) || strlen(name) == 0)
		return VoltHelpers#getSVarByName(ksControlFuncName)
	else
		return VoltHelpers#getSVarByName(ksControlFuncName, text=name)
	endif
End

Function VM_getSoftwareVersion()
	return kVM_version
end

Function VM_getFileVersion()
	DFREF cDF = GetDatafolderDFR()
	DFREF package = VoltHelpers#getPackage()
	NVAR version = package:$ksVM_version
	if (!NVAR_Exists(version))
		Variable /G package:$ksVM_version = 0
		NVAR version = package:$ksVM_version
	endif
	
	return version
End

Function VM_needsUpdate()
	return VM_getFileVersion() < VM_getSoftwareVersion()
End

Function VM_setVersion()
	DFREF package = VoltHelpers#getPackage()
	Variable /G package:$ksVM_version = kVM_version
	return kVM_version
End

Function VM_isEpochPanelOpen()

	DoWindow $ksEpochPanel
	return V_flag == 0 ? 0 : 1
End

Function VM_redisplayEpochPanel()

	if (VM_isEpochPanelOpen())
		DoWindow /K $ksEpochPanel
	endif
	VM_EpochControlPanel()
	return 1
End

Function VM_fixTraceName(ending, new)
	String ending, new
	Prompt ending, "Enter the end part of the wave:"
	Prompt new, "Enter the replacement part:"
	
	DoPrompt "Change Wave Names", ending, new
	if (V_flag == 1)
		return 0
	endif
	
	return VM_fixRawDataName(ending, new = new)
End

Function VM_fixRawDataName(ending, [new])
	String ending, new
	
	if (ParamIsDefault(new) || strlen(new) == 0)
		new = "_AD0"
	endif
	
	String theList = WaveList("*" + ending, ";", "")
	Variable size = ItemsInList(theList)
	if (size == 0)
		print "** Couldn't find any waves with this ending: " + ending 
		return 0
	endif
	
	Variable i
	for (i = 0; i < size; i += 1)
		String name = StringFromList(i, theList)
		Duplicate /O $name, $(RemoveEnding(name, ending) + new)
		KillWaves /Z $name
	endfor
	
	return 1
End


Function VM_ImportVMDataFromExp([nums])
	Variable nums
	
	nums = ParamIsDefault(nums) || nums < 1 ? 5 : trunc(nums)
	
	String columnTitles = "epoch number;new name"
	Wave /T params = GH_getParameterWave(rows = nums, columns = 2, columnTitles = columnTitles)
	params[0][0] = "1"
	
	MakeParameterInputPanel(params)
	
	String epochList = "", newNameList = ""
	
	Variable index
	for (index = 0; index < nums; index += 1)
		String ep = params[index][0]
		if (strlen(ep) > 0)
			epochList = AddListItem("ep_" + params[index][0], epochList, ";", Inf)
			if (strlen(params[index][1]) > 0)
				newNameList = AddListItem(params[index][1], newNameList, ";", Inf)
			else
				newNameList = AddListItem("ep_" + params[index][0], newNameList, ";", Inf)
			endif
		else
			continue
		endif
	endfor

	if (ItemsInList(epochList) == 0)
		return 0
	endif
	
	if (ItemsInList(newNameList) > 0 && ItemsInList(epochList) != ItemsInList(newNameList))
		return 0
	endif
	
	ImportEpochsFromFile(epochList, newNameList = newNameList)
	
	return 1
End


Function ImportEpochsFromFile(importList, [newNameList])
	String importList, newNameList
	
	Variable numEpochs = ItemsInList(importList)
	
	if (ItemsInList(newNameList) != numEpochs)
		return 0
	endif

	
	String epochName = "", newName = ""
	String importTraceList = ""

	// extend imported traces 
	Variable index
	for (index = 0; index < numEpochs; index += 1)
		epochName = StringFromList(index, importList)
		importTraceList = AddUniqueToList(epochName, importTraceList)
		importTraceList = AddUniqueToList(epochName + "_cv", importTraceList)
		importTraceList = AddUniqueToList(epochName + "_img", importTraceList)
		importTraceList = AddUniqueToList(epochName + "_traces", importTraceList)
	endfor
	
	
	DFREF cDF = GetDatafolderDFR()
	DFREF package = VoltHelpers#getPackage()
	DFREF epochFolder = VoltHelpers#getEpochFolder()
	DFREF rampFolder = VMRamps#getRampDF()
	
	String loadPackage = GetDataFolder(1, package)
	String loadEpochF = GetDataFolder(1, epochFolder)
	String loadRampF = GetDataFolder(1, rampFolder)
	
	String fileFilters = "Igor Experiments (*.pxp):.pxp;All Files:.*;"
	
	// Import (get the file name and import the traces)
	String pathName = "rbExperimentImportPath"
	Variable refNum
	PathInfo $pathName
	if (V_flag)
		Open /R/D/F=fileFilters/P=$pathName refNum
	else
		Open /R/D/F=fileFilters refNum
	endif

	String fullFileName = S_fileName			
	
	SetDatafolder rampFolder
	LoadData /Q/S=loadRampF /O fullFileName
	
	SetDataFolder package
	LoadData /Q/J="CVRamp" /S=loadPackage /O fullFileName
	
	SetDatafolder cDF
	NewDataFolder /O/S tmpLoad
	DFREF tmp = GetDataFolderDFR()
	LoadData /Q/J="EpochCategories;EpochLocations" /S=loadPackage /O fullFileName
	Wave /T importedCategories = $"EpochCategories"
	Wave importedELocs = $"EpochLocations"
	
	LoadData /J=importTraceList /S=loadEpochF /O fullFileName
	
	// post-processing

	Wave epochs = VM_getEpochWave()
	Wave /T epochCats = VM_getEpochCatWave()
	Variable insIndex = 0

	for (index = 0; index < numEpochs; index += 1)
		epochName = StringFromList(index, importList)
		if (strlen(newNameList) == 0)
			newName = epochName
		else
			newName = StringFromList(index, newNameList)
		endif

		insIndex = FindDimLabel(epochCats, 0, newName)
		if (insIndex < 0)
			// epoch not yet recorded
			insIndex = numpnts(epochCats)
			InsertPoints /M=0 insIndex, 1, epochCats
			InsertPoints /M=0 insIndex, 1, epochs
		endif
			
		SetDimLabel 0, insIndex, $newName, epochCats
		SetDimLabel 0, insIndex, $newName, epochs

		Variable catIndex = FindDimLabel(importedCategories, 0, epochName)
		if (catIndex >= 0)
			epochCats[insIndex] = importedCategories[catIndex]
			epochs[insIndex][] = importedELocs[catIndex][q]
		endif
	
		Duplicate /O $epochName, epochFolder:$newName
		Duplicate /O $(epochName + "_cv"), epochFolder:$(newName + "_cv")
		Duplicate /O $(epochName + "_img"), epochFolder:$(newName + "_img")
		Duplicate /O $(epochName + "_traces"), epochFolder:$(newName + "_traces")
	endfor
	SetDatafolder cDF

	VoltGUI#refreshEpochPanelList()

	// cleanup
	KillDataFolder /Z tmp

	return 1
End





// ****************************************************
// ************      C O N T R O L    I N T E R F A C E S        ***********
// ****************************************************

Function VM_makeGraphCntrlPanel(theWindowName, [ctrlPanelName])
	String theWindowName, ctrlPanelName
	
	if (strlen(theWindowName) == 0 || (WinType(theWindowName) != 1))
		print "** ERROR (VM_makeGraphCntrlPanel): there is no graph window with the name " + theWindowName
		print "**              can not attach the panel"
		return 0
	endif

	if (ParamIsDefault(ctrlPanelName))
		ctrlPanelName = ksCntrlPanelName
	endif
	
	STRUCT VoltammetryPreferences prefs
	VoltHelpers#getTransPrefs(prefs)
	
	Variable defYDist = 2
	Variable topOffset = 10
	Variable cHeight = 20

	Wave epochs = VM_getEpochWave()

	ControlBar /W=$theWindowName /R 180
	NewPanel/FG=(GR,FT,FR,GB)/HOST=$theWindowName
	ModifyPanel frameStyle=1
	RenameWindow #, $ctrlPanelName

	Button btn_prevTransient,pos={27,topOffset},size={25,cHeight},title="<"
	Button btn_prevTransient,help={"go back to the previous transient"}
	Button btn_prevTransient,proc=btnFunc_VM_prevTransient
	if (!WaveExists(epochs) || DimSize(epochs, 0) == 0)
		Button btn_prevTransient, disable = 2		// only enable if we have recorded epochs to move backwards to
	endif

	Button btn_keepTransient,pos={60,topOffset},size={60,cHeight},title="keep"
	Button btn_keepTransient,help={"keep the transient between the cursors"}
	Button btn_keepTransient, valueColor=(3,52428,1),fStyle=1
	Button btn_keepTransient,proc=btnFunc_VM_keepTransient

	Button btn_nextTransient,pos={128,topOffset},size={25,cHeight},title=">"
	Button btn_nextTransient,help={"find the next transient"}
	Button btn_nextTransient, proc=btnFunc_VM_nextTransient
	
	topOffset += cHeight + defYDist
	
	Button btn_done, pos={60, topOffset}, size={60, cHeight}, title="done"
	Button btn_done, help={"finished looking at this transient. Clicking this button will also remove the original raw data to save space."}
//	Button btn_done, valueColor=(65535, 0, 0)
	Button btn_done, proc=btnFunc_VM_doneTransient
	
	Button btn_gangnamStyle, pos={128,topOffset}, size={40,cHeight}, title="PSY"
	Button btn_gangnamStyle, disable=3, valueColor=(52428,17472,1)
	Button btn_gangnamStyle, proc=btnFunc_VM_keepAllAndDone
	topOffset += cHeight + defYDist
	cHeight = 16
	
	SetVariable val_pntsForBaseline,pos={16,topOffset},size={76,cHeight},title="baseline"
	SetVariable val_pntsForBaseline,help={"number of points to consider for the baseline before the peak starts"}
	SetVariable val_pntsForBaseline,fSize=10,limits={-inf,inf,0}
	SetVariable val_pntsForBaseline,value=$(VoltHelpers#getBaselinePoints(init=1))
	SetVariable val_pntsForBaseline, proc=svarFunc_VM_setWinUserData
	
	SetVariable val_epochNum,pos={100,topOffset},size={50,cHeight},title="ep #"
	SetVariable val_epochNum,help={"epoch number"}
	SetVariable val_epochNum,fSize=10,limits={-inf,inf,0}
	SetVariable val_epochNum,value=_NUM:0
	SetVariable val_epochNum, proc=varFunc_VM_currentEpoch
	topOffset += cHeight + defYDist

	SetVariable val_pntsForTransient,pos={12,topOffset + 3},size={80,cHeight},title="transient"
	SetVariable val_pntsForTransient,help={"number of points for the transient from start of peak "}
	SetVariable val_pntsForTransient,fSize=10,limits={-inf,inf,0}
	SetVariable val_pntsForTransient,value=$(VoltHelpers#getTransientPoints(init=1))
	SetVariable val_pntsForTransient, proc=svarFunc_VM_setWinUserData
	cHeight = 20
	
	Button btn_setEpochEnd, pos={100, topOffset}, size={52,cHeight}, title="set end"
	Button btn_setEpochEnd, help={"set the end position (cursor D) of the epoch to the number of points from the start position (cursor C)"}
	Button btn_setEpochEnd, fSize=10
	Button btn_setEpochEnd, proc=btnFunc_VM_setEpochEnd
	topOffset += cHeight + defYDist

	SetVariable val_epochClass, pos={13, topOffset}, size={140, cHeight}, title="ep. class"
	SetVariable val_epochClass, help={"epoch classification"}
	SetVariable val_epochClass, fSize=10
	SetVariable val_epochClass, value=$(VM_getCurrentEpochClass())
	topOffset += cHeight + defYDist	

	Checkbox bx_ignoreCntrl, pos={96, topOffset}, size={60, cHeight}, title="no ctrl"
	Checkbox bx_ignoreCntrl, mode=0, value=0, fsize=10, side=1	
	
	SetVariable val_episodeWin, pos={25, topOffset}, size={65, cHeight},title="length"
	SetVariable val_episodeWin, help={"size of a single episode window in seconds."}
	SetVariable val_episodeWin, fSize=10, limits={-inf,inf,0}
	SetVariable val_episodeWin, value=_NUM:30
	SetVariable val_episodeWin, proc=svarFunc_VM_setWinUserData
	SetVariable val_episodeWin, disable=1
	TitleBox txtbx_episodeWinUnit, pos={92, topOffset + 1},size={20,cHeight}, title="s"
	TitleBox txtbx_episodeWinUnit, fSize=10, frame=0, disable=1
	
	topOffset += cHeight	

	Checkbox bx_calibration, pos={96, topOffset}, size={60, cHeight}, title="calibration"
	Checkbox bx_calibration, mode=0, value=0, fsize=10, side=1
	Checkbox bx_calibration, proc=ckbxFunc_VM_calibration
	
//	Button btn_refineTransient, pos={30, 140}, size={120, 20}, title="refine transient"
//	Button btn_refineTransient, proc=btnFunc_VM_refineTransient

	topOffset += cHeight + 2 * defYDist

	Button btn_scaleTrace, pos={30, topOffset}, size={120,cHeight}, title="zoom to epoch"
	Button btn_scaleTrace, help={"zoom to the episode, only works if the two cursors are set"}
	Button btn_scaleTrace, userdata="zoom"
	Button btn_scaleTrace, proc=btnFunc_VM_scaleTrace
	topOffset += cHeight + defYDist	
		
//	Button btn_showVoltammogram, pos={30, 193}, size={120, 20}, title="voltammogram"
	Button btn_showVoltammogram, pos={30, topOffset}, size={120, cHeight}, title="voltammogram"
	Button btn_showVoltammogram, proc=btnFunc_VM_showVoltammogram
	topOffset += cHeight + defYDist	
	
//	Button btn_showControlTraces, pos={30, 216}, size={120, 20}, title="trigger traces"
	Button btn_showControlTraces, pos={30, topOffset}, size={120, cHeight}, title="trigger traces"
	Button btn_showControlTraces, proc=btnFunc_VM_showControlTraces
	topOffset += cHeight + defYDist	
	
//	Button btn_clearEpochs, pos={30, 239}, size={120,20}, title="clear epochs"
	Button btn_clearEpochs, pos={30, topOffset}, size={120,cHeight}, title="clear epochs"
	Button btn_clearEpochs, help={"clear all epochs from this transient\rUse with caution, this action can not be undone!"}
	Button btn_clearEpochs, proc=btnFunc_VM_clearEpochs
	
	STRUCT VMGUI_analysisFunction funcs
	VoltAnalysis#loadAnalysisPrefs(funcs)
	
	String funcList = VM_getAnalysisFunctions()
	Variable theMode = WhichListItem(funcs.analysisDisp, funcList)
	theMode = theMode < 0 ? 0 : theMode + 1
	
	topOffset += cHeight + 2 * defYDist
	cHeight = 15

	Titlebox txtbx_chooseAnalysisLbl, pos={10, topOffset}, size={160, cHeight}, title="peak search function:"
	Titlebox txtbx_chooseAnalysisLbl, fSize=10, frame=0
	topOffset += cHeight + defYDist	
	cHeight = 20
	PopupMenu popM_chooseAnalysisFunc, pos={10, topOffset}, size={160, cHeight},bodyWidth=160
	PopupMenu popM_chooseAnalysisFunc, help={"choose the analysis function for this experiment"}
	PopupMenu popM_chooseAnalysisFunc, fSize=10, mode=theMode
	PopupMenu popM_chooseAnalysisFunc, value=VM_getAnalysisFunctions()
//	PopupMenu popM_chooseAnalysisFunc, popvalue=(funcs.analysisDisp)
	PopupMenu popM_chooseAnalysisFunc, proc=popFunc_chooseFunc

	topOffset += cHeight + defYDist	
	cHeight = 15
	Titlebox txtbx_chooseControlLbl, pos={10, topOffset}, size={160, cHeight}, title="stimulus search function:"
	Titlebox txtbx_chooseControlLbl, fSize=10, frame=0, disable=1
	topOffset += cHeight + defYDist	
	cHeight = 20
	PopupMenu popM_chooseControlFunc, pos={10, topOffset}, size={160, cHeight}, bodyWidth = 160
	PopupMenu popM_chooseControlFunc, help={"choose the function to find the trigger traces."}
	PopupMenu popM_chooseControlFunc, fSize=10, popvalue=funcs.controlDisp, disable=1
	PopupMenu popM_chooseControlFunc, value=VM_getControlFunctions()
	PopupMenu popM_chooseControlFunc, proc=popFunc_chooseFunc
	
	VM_getControlFuncStr(name=funcs.controlFunc)

	STRUCT WMPopupAction action
	action.ctrlName = "popM_chooseAnalysisFunc"
	action.win = theWindowName + "#" + ctrlPanelName
	action.eventCode = 2
	action.popStr = funcs.analysisDisp
	popFunc_chooseFunc(action)
	
	SetActiveSubWindow ##
	
	if (VM_checkControlWaves() <= 0)
		// no control traces exist, so disable the button to show them
		Button btn_showControlTraces, disable=3
		Button btn_gangnamStyle, disable = 3
		PopupMenu popM_chooseControlFunc, disable=1
		Titlebox txtbx_chooseControlLbl, disable=1
	else
		Button btn_showControlTraces, disable=0
		Button btn_gangnamStyle, disable = 0
		PopupMenu popM_chooseControlFunc, disable=0
		Titlebox txtbx_chooseControlLbl, disable=0
	endif
End


Function /S VM_EpochControlPanel([move])
	Variable move
	
	move = ParamIsDefault(move) || move > 0 ? 1 : 0

	DoWindow /F $ksEpochPanel
	if (V_flag)
		if (move)
			moveWindowOnScreen(ksEpochPanel, location="right")
		endif
		return ksEpochPanel
	endif
	
	Wave /T epList = getEpochListWave()
	Wave epSel = getEpochSelWave()
	Wave /T epListCat = getEpochCatListWave()
	Wave epListCatSel = getEpochCatSelWave()
	
	Variable panelWidth = 235
	Variable panelHeight = 426
	Variable leftWin = 10
	Variable topWin = 44
	Variable leftOff, newLeftOff, topOff, newTopOff, length, height
	
	NewPanel /K=1/W=(leftWin,topWin,(leftWin + panelWidth),(topWin + panelHeight)) /N=$ksEpochPanel as ksEpochPanelTitle
	
	SetWindow $ksEpochPanel, userdata(epochList)=GetWavesDatafolder(epList, 2)
	SetWindow $ksEpochPanel, userdata(epochSel)=GetWavesDatafolder(epSel, 2)
	
	leftOff = 5
	topOff = 4
	length = 32
	height = 16
	Button btn_selectAllEpochs,pos={leftOff, topOff},size={length, height},title="all",font=$ksVM_defPanelFont
	Button btn_selectAllEpochs,fSize=kVM_defFSize, proc=btnFunc_VM_selectEpochs

	newLeftOff = leftOff + length + 1
	length = 50
	Button btn_deselectAllEpochs,pos={newLeftOff, topOff},size={length, height},title="none",font=$ksVM_defPanelFont
	Button btn_deselectAllEpochs,fSize=kVM_defFSize, proc=btnFunc_VM_selectEpochs

	newLeftOff += length + 7
	length = 56
	Button btn_refreshList,pos={newLeftOff, topOff},size={length, height},title="refresh",font=$ksVM_defPanelFont
	Button btn_refreshList,fSize=kVM_defFSize, proc=btnFunc_VM_refreshEpochList
	
	newLeftOff += length + 7
	length = 68
	Button btn_reanalyze, pos={newLeftOff, topOff}, size={length, height}, title="re-analyze", font=$ksVM_defPanelFont
	Button btn_reanalyze, fSize=kVM_defFSize, proc=btnFunc_VM_reanalyze
	
	leftOff = 8
	topOff += height + 3		// 23
	length = 100
	height = 200
	ListBox lb_epochs,pos={leftOff,topOff},size={length,height}, mode=4
	ListBox lb_epochs, font=$ksVM_defPanelFont,fsize=12
	ListBox lb_epochs,help={"a list of available episodes"}
	ListBox lb_epochs,labelBack=(65535,65535,65535)
	ListBox lb_epochs, listWave=epList, selWave=epSel
	Listbox lb_epochs, proc=lbFunc_VM_epochSelection
	
	newLeftOff = leftOff + length + 3
	length = 115
	ListBox lb_epochCategory,pos={newLeftOff, topOff},size={length, height}
	ListBox lb_epochCategory, font=$ksVM_defPanelFont, fsize=12
	ListBox lb_epochCategory,help={"a list of categories for the episodes"}
	ListBox lb_epochCategory,labelBack=(65535,65535,65535)
	ListBox lb_epochCategory, listWave=epListCat, selWave=epListCatSel
	ListBox lb_epochCategory, proc=lbFunc_VM_selectByCategories
	
	leftOff += 3					// 11
	topOff += height + 7		// 230
	length = 216
	height = 61
	GroupBox dataSelection,pos={leftOff, topOff},size={length, height},title="data selection"
	GroupBox dataSelection, font=$ksVM_defPanelFont, fsize=kVM_defFSize
	newTopOff = topOff + height		// 291
	
	newLeftOff = 22
	height = 14
	length = 57
	CheckBox rdbtn_dataTransient,pos={newLeftOff, topOff + 20},size={length, height},title="transient"
	CheckBox rdbtn_dataTransient,value= 1,mode=1, font=$ksVM_defPanelFont, fsize=kVM_defFSize
	CheckBox rdbtn_dataTransient, proc=ckbxFunc_VM_dataSelection
	CheckBox ckbx_withFit, pos={newLeftOff, topOff + 25 + height}, size={length, height}, title="incl. fit"
	CheckBox ckbx_withFit, value=0, font=$ksVM_defPanelFont,fsize=kVM_defFSize
	CheckBox ckbx_withFit, proc=ckbxFunc_VM_includeFits
	length = 109
	CheckBox rdbtn_dataVGTrace,pos={newLeftOff + 88,topOff + 20},size={length, height},title="voltgrm. cv"
	CheckBox rdbtn_dataVGTrace,value= 0,mode=1,font=$ksVM_defPanelFont, fsize=kVM_defFSize
	CheckBox rdbtn_dataVGTrace, proc=ckbxFunc_VM_dataSelection
	
	CheckBox ckbx_cvWithColor, pos={newLeftOff + 88 + length - 23, topOff + 20}, size={20, height}, title="c"
	CheckBox ckbx_cvWithColor, value=0, font=$ksVM_defPanelFont, fsize=kVM_defFSize, disable = 1 // hide checkbox
	CheckBox ckbx_cvWithColor, proc=ckbxFunc_VM_colorCVs
	
	CheckBox rdbtn_dataVGImage,pos={newLeftOff + 88,topOff + 25 + height},size={length + 3, height},title="voltgrm. image"
	CheckBox rdbtn_dataVGImage,value= 0,mode=1,font=$ksVM_defPanelFont, fsize=kVM_defFSize
	CheckBox rdbtn_dataVGImage, proc=ckbxFunc_VM_dataSelection

	SetWindow $ksEpochPanel userdata(displayData)="transient"
	SetWindow $ksEpochPanel userdata(includeFits)="no"
	SetWindow $ksEpochPanel userdata(cvColored)="no"
	
	newTopOff += 4				// 295
	topOff = newTopOff + 25		// 314
	height = 70
	length = 216
	TabControl tabExtraFunctions, pos={leftOff, newTopOff}, size={length, height}, tabLabel(0)="simple stats"
	TabControl tabExtraFunctions, value=0, font=$ksVM_defPanelFont, fsize=kVM_defFSize
	TabControl tabExtraFunctions, proc=tabFunc_extraFunctions
//	GroupBox simpleStatsFuncs, pos={leftOff, newTopOff}, size={length, height}, title="simple stats"
//	GroupBox simpleStatsFuncs, font=$ks_defPanelFont, fSize=k_defPanelFSize
	newTopOff += height			 // 340
	
	Variable tabTopOff = topOff
	
	height = 20
	length = 85
	Button btn_maxAndArea, pos={newLeftOff, topOff}, size={length, height}, title="max/area"
	Button btn_maxAndArea, help={"Calculate the Vmax and aera for all transients"}
	Button btn_maxAndArea, font=$ksVM_defPanelFont, disable=0	
	Button btn_maxAndArea, proc=btnFunc_VM_simpleStats
	
	Button btn_decayFits, pos={newLeftOff + length + 23, topOff}, size={length, height}, title="decay fits"
	Button btn_decayFits, help={"Calculate the tau from a decay fit for all tranients"}
	Button btn_decayFits, font=$ksVM_defPanelFont, disable=0
	Button btn_decayFits, proc=btnFunc_VM_simpleStats
	
	topOff += height + 3
	height = 14
	CheckBox ckbtn_plotMax, pos={newLeftOff, topOff}, size={length, height}, title="plot max"
	CheckBox ckbtn_plotMax, value=0, mode=0, font=$ksVM_defPanelFont, fsize=kVM_defFSize
	CheckBox ckbtn_plotMax, proc=ckbxFunc_VM_plotStats
	
	CheckBox ckbtn_plotTaus, pos={newLeftOff + length + 21, topOff}, size={length, height}, title="plot \\F'Symbol'\\Z12t"
	CheckBox ckbtn_plotTaus, value=0, mode=0, font=$ksVM_defPanelFont, fsize=kVM_defFSize
	CheckBox ckbtn_plotTaus, proc=ckbxFunc_VM_plotStats
	
	CheckBox ckbtn_inNano, pos={newLeftOff + (1.7 * length) + 14, topOff + 2}, size={length, height}, title="nano"
	CheckBox ckbtn_inNano, value=1, mode=0, font=$ksVM_defPanelFont, fsize=kVM_defFSize
	
	TabControl tabExtraFunctions, tabLabel(1)="calibration"
	
	NVAR factor = $(VoltHelpers#getCalibrationFactor())
	length = 105
	Variable tabLeftOff = newLeftOff
	SetVariable svar_calibrationFactor, pos={tabLeftOff, tabTopOff}, size={length, height}, title="calib. factor"
	SetVariable svar_calibrationFactor, font=$ksVM_defPanelFont, fsize=kVM_defFSize, disable= 1
	SetVariable svar_calibrationFactor, limits={0, 999, 0}, value=factor
	
	TitleBox tbx_calibFactorLbl, pos={tabLeftOff + length + 5, tabTopOff}, size={length - 45, height}
	TitleBox tbx_calibFactorLbl, font=$ksVM_defPanelFont, fsize=kVM_defFSize, frame= 0, disable= 1
	TitleBox tbx_calibFactorLbl, title="\f03nM\f02 for each 1 nA"
	
	length = 55
	Button btn_calibrate, pos={tabLeftOff, tabTopOff + height + 5}, size={length, 20}, title="calibrate"
	Button btn_calibrate, font=$ksVM_defPanelFont, fsize=kVM_defFSize, disable= 1
	Button btn_calibrate, proc=btnFunc_VM_calibrate
	tabLeftOff += length
	
	tabLeftOff += 7
	Button btn_calcFactor, pos={tabLeftOff, tabTopOff + height + 5}, size={length, 20}, title="calculate"
	Button btn_calcFactor, font=$ksVM_defPanelFont, fsize=kVM_defFSize, disable= 1
	Button btn_calcFactor, proc=btnFunc_VM_calcFactor
	tabLeftOff += length + 8
	
	length = 55
	SetVariable svar_calibConcentration, pos={tabLeftOff, tabTopOff + height + 8}, size={length, height}
	SetVariable svar_calibConcentration, title="dose "
	SetVariable svar_calibConcentration, font=$ksVM_defPanelFont, fsize=kVM_defFSize, disable= 1
	SetVariable svar_calibConcentration, limits={0, 100, 0}, value=_NUM:1
	tabLeftOff += length + 2
	
	length = 25
	TitleBox tbx_doseLbl, pos={tabLeftOff, tabTopOff + height + 9}, size={length, height}
	TitleBox tbx_doseLbl, font=$ksVM_defPanelFont, fsize=kVM_defFSize, frame= 0, disable= 1
	Titlebox tbx_doseLbl, title=("\f02\F'Symbol'm\F'" + ksVM_defPanelFont + "'\f02M\f00")

	newTopOff += 8		// 344
	length = 216
	height = 49
	GroupBox displaySelection,pos={leftOff,newTopOff},size={length,height}
	topOff = newTopOff	// 344
	newTopOff += height	// 393
	
	topOff += 6			// 350
	length = 43
	height = 14
	CheckBox rdbtn_displayGraph,pos={newLeftOff + 39, topOff},size={length, height},title="graph"
	CheckBox rdbtn_displayGraph,value= 1,mode=1, font=$ksVM_defPanelFont, fsize=10
	CheckBox rdbtn_displayGraph, proc=ckbxFunc_VM_displaySelection
	
	CheckBox rdbtn_displayTable,pos={newLeftOff + length + 52, topOff},size={length - 3, height},title="table"
	CheckBox rdbtn_displayTable,value= 0,mode=1, font=$ksVM_defPanelFont, fsize=10
	CheckBox rdbtn_displayTable, proc=ckbxFunc_VM_displaySelection
	
	CheckBox ckbtn_fromZero, pos={newLeftOff + (2*length) + 60, topOff + 1},size={length + 7, height}, title="from 0"
	CheckBox ckbtn_fromZero, value=0, mode=0, font=$ksVM_defPanelFont, fsize=10, disable=1
	CheckBox ckbtn_fromZero, proc=ckbxFunc_VM_dispXfromZero
	
	SetWindow $ksEpochPanel userdata(displayType)="graph"
	
	topOff += height + 7	// 371
	height = 15
	length = 160
	SetVariable svar_setWinName,pos={newLeftOff + 4,topOff},size={length,height},bodyWidth=(length - 10)
	SetVariable svar_setWinName,help={"enter the name of the graph or table window"}
	SetVariable svar_setWinName,font=$ksVM_defPanelFont,fSize=kVM_defFSize,title="to "
	SetVariable svar_setWinName,value= _STR:"_new_"
	SetVariable svar_setWinName,proc=svarFunc_VM_toWin

	leftOff = newLeftOff + length + 3
	length = 20
	PopupMenu popup_toWin, pos={leftOff, topOff - 4},size={length,length},bodyWidth=20
	PopupMenu popup_toWin, proc=popFunc_VM_toWinPopMenu
	PopupMenu popup_toWin, help={"select the graph or table to plot the waves to"}
	PopupMenu popup_toWin, font=$ksVM_defPanelFont,fSize=10
	PopupMenu popup_toWin, mode=1,popvalue="_new_",value= ("_new_;" + WinList("*", ";", "WIN:1"))

	SetWindow $ksEpochPanel userdata(winName)="_new_"
	
	newTopOff += 5	
	leftOff = 80
	height = 23
	length = 65
	Button btn_displayData, pos={leftOff,newTopOff},size={length,height},title="plot",fStyle=1
	Button btn_displayData, font=$ksVM_defPanelFont,fsize=kVM_defFSize + 4
	Button btn_displayData, userdata(fromZero)="no"								
	Button btn_displayData, proc=btnFunc_VM_displayData

	newTopOff += height + 5

	if (newTopOff > panelHeight)
		adjustPanelSize(ksEpochPanel, pixelHeight = newTopOff)
	endif
	
	moveWindowOnScreen(ksEpochPanel, location="right")

	return ksEpochPanel
End

Function /S VM_getProgressBarPanel([theTitle, theName, theColor])
	String theTitle, theName, theColor
	
	if (ParamIsDefault(theTitle) || strlen(theTitle) == 0)
		theTitle = ksDefProgressBarTitle
	endif
	if (ParamIsDefault(theName) || strlen(theName) == 0)
		theName = ksDefProgressBarName
	endif
	if (ParamIsDefault(theColor) || strlen(theColor) == 0)
		theColor = "green"
	endif
	
	STRUCT RGBColor color
	strswitch(theColor)
		case "red":
			color.red = 65535
			color.green = 0
			color.blue = 0
			break
		case "blue":
			color.red = 0
			color.green = 0
			color.blue = 65535
			break
		case "green":
		default:
			color.red = 2
			color.green = 39321
			color.blue = 1
	endswitch
	
	
	NewPanel /W=(5,5,5,5) /K=1 /N=$theName as theTitle
	theName = S_name
	resizeWindowToScreen(theName, ratio=5)
	Variable width = NumberByKey("WIDTH", getPixelsFromWindow(theName))
	Variable height = NumberByKey("HEIGHT", getPixelsFromWindow(theName))
	Variable dispWidth = round(width * 0.9)
	Variable dispHeight = round(height * 0.25)
	Variable frameLDistance = round((width - dispWidth) / 2)
	Variable frameTDistance = round((height - dispHeight) * (2/3))
	Variable frameTForText = round(frameTDistance / 2)
	
	ValDisplay progressBar, win=$theName, pos={frameLDistance, frameTDistance}, size={dispWidth, dispHeight}
	ValDisplay progressBar, win=$theName, limits={0, 1, 0}, barmisc={0,0}
	ValDisplay progressBar, win=$theName, mode = 3, value=_NUM:0
	ValDisplay progressBar, win=$theName, highColor=(color.red,color.green,color.blue)
	
	TitleBox progressInfo, win=$theName, pos={frameLDistance, round(frameTDistance / 2)}, size={dispWidth, dispHeight}
	TitleBox progressInfo, win=$theName, font="Verdana", fSize=10, frame=0
	Titlebox progressInfo, win=$theName, title = ""
	
	DoUpdate /W=$theName /E=1
	moveWindowOnScreen(theName)
	return theName
End

Function VM_updateProgressbar(progress, [info, theName])
	Variable progress
	String theName, info
	
	if (ParamIsDefault(theName) || strlen(theName) == 0)
		theName = ksDefProgressBarName
	endif
	
	ValDisplay progressBar, win=$theName, value=_NUM:progress
	
	if (!ParamIsDefault(info))
		Titlebox progressInfo, win=$theName, title=info
	endif
	DoWindow /F $theName
	DoUpdate /W=$theName
	return 0
End
// ****************************************************
// *************      C O N T R O L    F U N C T I O N S         ***********
// ****************************************************

// *******     T R A N S I E N T    P A N E L    F U N C T I O N S      ********

//! The control function for the "next transient" button. 
// @param bs:		VMButtonAction structure
//-
Function btnFunc_VM_nextTransient(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch (bs.eventCode)
		case 2:
			// first get the graph window of the panel
			DFREF dataPath = VoltHelpers#getDataLoadfolder()
			String theHostName = getHostWinName(bs.win)
			
			Wave transient = getTransientFromGraph(theHostName)
			if (!WaveExists(transient))
				return 0
			endif
			
			NVAR baseline = $(VoltHelpers#getBaselinePoints())
			NVAR peakDuration = $(VoltHelpers#getTransientPoints())
			ControlInfo /W=$bs.win val_epochNum
			Variable currentEpoch = V_value
			
			SVAR peakClass = $(VM_getCurrentEpochClass())
			
			Wave epochs = VM_getEpochWave()
			Variable row = VM_getRowNumberFromLabel("ep_" + num2str(currentEpoch),  epochs)
			
			Variable cCursorExists = strlen(CsrInfo(C)) > 0
			Variable dCursorExists = strlen(CsrInfo(D)) > 0
			Variable nextStart = 0, nextEpoch = 0
			
			Wave cValues = $(getRGBByName("okker"))
			
			Variable peakPoint = 0, stimPoint = 0
			if (row < 0 || row == DimSize(epochs, 0) - 1)
				// we haven't recorded any peaks yet or we are past the last recorded one, or we are at the last epoch
				SVAR analysisName = $(VM_getAnalysisFuncStr())
				SVAR controlName = $(VM_getControlFuncStr())
				
				STRUCT VMPeakInfo peakInfo
				
				ControlInfo /W=$bs.win bx_calibration
				peakInfo.calibration = V_value				
				
				if (peakInfo.calibration)
					ControlInfo /W=$bs.win val_episodeWin
					peakInfo.episodeLengthPnts = V_value / deltax(transient)
				endif
				
				peakInfo.peakPoint = row < 0 ? 0 : epochs[row][%peakPoint]
				peakInfo.peakStartPoint = row < 0 ? 0 : epochs[row][%peakstartpoint]

				if (peakInfo.calibration)
					peakInfo.episodeEndPoint = peakInfo.peakStartPoint + peakInfo.episodeLengthPnts
				else
					peakInfo.episodeEndPoint = peakInfo.peakStartPoint + peakDuration
				endif

				peakInfo.stimPoint = row < 0 ? -1 : epochs[row][%stimulusPoint]
				peakInfo.hasControl = VM_hasControls()
				peakInfo.advance = VM_getPointAdvance()
				FUNCREF VM_findPeakPrototypeFunc peakInfo.findPeak = $analysisName
				FUNCREF VM_findStimPrototypeFunc peakInfo.findStim = $controlName
				peakInfo.baseline = baseline
				peakInfo.transientPoints = peakDuration
				
				if (peakInfo.calibration)
					peakInfo.maxPointLimit = peakInfo.episodeLengthPnts
				else
					peakInfo.maxPointLimit = (baseline + peakDuration)
				endif
							
				ControlInfo /W=$bs.win bx_ignoreCntrl
				peakInfo.ignoreControl = V_value
				
				if (cCursorExists)
					if (dCursorExists)
						nextStart = pcsr(D, theHostName)
					else
						nextStart = pcsr(C, theHostName) + peakDuration + 1 // get the set number of points +1
					endif
				endif
			
				NVAR cFlag = $(getCursorFlag())

				if (peakInfo.calibration)
					Variable eCursorExists = strlen(CsrInfo(E, theHostName)) > 0
					Variable fCursorExists = strlen(CsrInfo(F, theHostName)) > 0

					cFlag = kVM_Recorded		// prevent the hook function to trigger by cursor removal
					if (eCursorExists)
						Cursor /K/W=$theHostName E
					endif
					if (fCursorExists)
						Cursor /K/W=$theHostName F
					endif
					cFlag = kVM_Clear
				endif			
			
				VM_findTransientPeak(transient=transient, peakInfo=peakInfo, startPoint=nextStart)

				if (peakInfo.peakExists)
					// we found a peak
					cFlag = kVM_Recorded
					Cursor /P /W=$theHostName /H=0 C transient peakInfo.peakStartPoint
					
					if (peakInfo.calibration)
						Cursor /M /W=$theHostName /C=(cValues[0], cValues[1], cValues[2]) C
						peakInfo.episodeEndPoint = peakInfo.peakStartPoint + peakInfo.episodeLengthPnts
						Cursor /P /W=$theHostName /C=(cValues[0], cValues[1], cValues[2]) /H=2 E transient peakInfo.peakStartPoint - baseline
					else
						peakInfo.episodeEndPoint = peakInfo.peakStartPoint + peakDuration
					endif

					Cursor /P /W=$theHostName /H=2 D transient (peakInfo.episodeEndPoint)

					if (peakInfo.calibration)
						getRGBByName("blue")
						Cursor /M /W=$theHostName /C=(cValues[0], cValues[1], cValues[2]) /H=0 D
						Cursor /P /W=$theHostName /C=(cValues[0], cValues[1], cValues[2]) /H=2 F transient (peakInfo.episodeEndPoint + baseline)
					endif

					cFlag = kVM_Clear
					
					if (peakInfo.calibration)
						SetAxis /W=$theHostName bottom, xcsr(E, theHostName) - (2 * baseline * deltax(transient)), xcsr(F, theHostName) + (2 * baseline * deltax(transient))					
					else
						SetAxis /W=$theHostName bottom, xcsr(C, theHostName) - (2 * baseline * deltax(transient)), xcsr(D, theHostName) + (2 * baseline * deltax(transient))
					endif
					Button btn_scaleTrace, win=$bs.win, userdata="full", title="full trace"
					if (DimSize(epochs, 0) == 0)
						// we haven't recorded any epochs yet, so start with the first
						SetVariable val_epochNum, win=$bs.win, value=_NUM:1
					else
						// we have recorded epochs, so add 1 to the last recorded number from the epoch label
						nextEpoch = getEpochFromLabel(GetDimLabel(epochs, 0, DimSize(epochs, 0) - 1)) + 1
						
						if (numtype(nextEpoch) == 2)
							// nextEpoch is a NaN, ie. the number conversion from the label didn't work
							//     so use the last row number + 2 as next epoch number (since we start with epoch 1)
							nextEpoch = DimSize(epochs, 0) + 2
						endif
						SetVariable val_epochNum, win=$bs.win, value=_NUM:nextEpoch
					endif
					
					NVAR cvInFront = $(VM_getKeepCVFrontFlag())
					NVAR cvCsrToMax = $(VM_getCVCsrAlignedFlag())
					String CVName = ""

					if (peakInfo.calibration)
						Wave traceMatrix = VM_getCalibDataTraces(pcsr(E, theHostName), pcsr(C, theHostName), pcsr(D, theHostName), pcsr(F, theHostName))
						Wave image = VM_makeImageVoltammogram(traceMatrix, blEnd=baseline - 1)
						CVName = VM_makeCVPlot(VM_makeCVCurves(image, baseline, numPoints=baseline, calibration = peakInfo.calibration), csr=1, front=cvInFront, toMax=cvCsrToMax, colorize=1, tGraph=theHostName)
					else
						Variable pntDiff = pcsr(C, theHostName) - baseline
						if (pntDiff < 0)
							baseline += pntDiff
							peakInfo.baseline = baseline
						endif
						
						Wave traceMatrix = VM_getDataTraces(pcsr(C, theHostName) - baseline, pcsr(D, theHostName), baseline)
						if (!WaveExists(traceMatrix))
							return 0
						endif

						Wave image = VM_makeImageVoltammogram(traceMatrix, blEnd=baseline - 1)
						CVName = VM_makeCVPlot(VM_makeCVCurves(image, peakInfo.peakPoint - pcsr(C, theHostName) + baseline, numPoints=2), csr=1, front=cvInFront, toMax=cvCsrToMax, colorize=1, tGraph=theHostName)
					endif
					
					SetWindow $CVName userdata(transient) = GetWavesDatafolder(transient, 2)

//					if (peakInfo.calibration)
//						Wave epochTransient = VoltHelpers#getEpochTransient(nums=(2 * peakInfo.baseline))
//						epochTransient[,peakInfo.baseline - 1] = transient[p + (peakInfo.peakStartPoint - peakInfo.baseline)]
//						epochTransient[peakInfo.baseline,] = transient[pcsr(D, theHostName) - p]
//					else
//						Wave epochTransient = VoltHelpers#getEpochTransient(nums=peakInfo.maxPointLimit)
//						epochTransient = transient[p + (peakInfo.peakStartPoint - peakInfo.baseline)]
//					endif
//				
//					SetScale /P x -baseline * deltax(transient), deltax(transient), WaveUnits(transient, 0), epochTransient
//					SetScale d 0,0,"A", epochTransient
					VM_updateControlTraces(peakInfo.peakStartPoint)
					peakClass = peakInfo.class
					
					VM_showImageVoltammogram(filter=1, restricted = 1)
					return 1
				else
					// we think we found a peak but can't confirm it through the control channels
					if (peakInfo.hasControl)
						// if we have control traces, we most likely hit the end of the transient, restore
						//      the previous stim point
						NVAR stimulus = $(VoltHelpers#getStimPoint())
						stimulus = epochs[row][%stimulusPoint]
						VM_retrieveDataForTransient(GetDimLabel(epochs, 0, row))
					endif
					return 0
				endif
			else
				// we have already recorded peaks, so figure out which one we are displaying and go to the next one
				nextEpoch = getEpochFromLabel(GetDimLabel(epochs, 0, row + 1))
				if (numtype(nextEpoch) != 2)
					SetVariable val_epochNum, win=$bs.win, value=_NUM:nextEpoch
					
					STRUCT WMSetVariableAction episode
					episode.ctrlName = "val_epochNum"
					episode.win = bs.win
					episode.dval = nextEpoch
					episode.eventCode = 2
					varFunc_VM_currentEpoch(episode)
				endif
			endif
			break
	endswitch
	
End // END btnFunc_nextTransient : ButtonControl


Function btnFunc_VM_prevTransient(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch (bs.eventCode)
		case 2:
			DFREF dataPath = VoltHelpers#getDataLoadFolder()
			Wave epochs = VM_getEpochWave()
			Variable rows = DimSize(epochs, 0)

			String theHostName = getHostWinName(bs.win)

			Wave transient = getTransientFromGraph(theHostName)
			if (!WaveExists(transient))
				return 0
			endif

			SVAR peakClass = $(VM_getCurrentEpochClass())
			
			NVAR baseline = $(VoltHelpers#getBaselinePoints())
			NVAR peakDuration = $(VoltHelpers#getTransientPoints())

			Variable cCursorExists = strlen(CsrInfo(C)) > 0
			Variable dCursorExists = strlen(CsrInfo(D)) > 0
			Variable peakPoint, stimPoint
			
			if (rows == 0)
				// we never recorded any epochs, so just execute the "next transient" function 
				bs.ctrlName = "btn_nextTransient"
				btnFunc_VM_nextTransient(bs)				
			else
				// we already recored at least one epoch so go to this
				ControlInfo /W=$bs.win val_epochNum
				Variable currentEpoch = V_value
				
				String theEpochLabel = ""
				Variable epochNum = 0, theEnd = 0
				Variable row = VM_getRowNumberFromLabel("ep_" + num2str(currentEpoch),  epochs)
				
				switch (row)
					case -1:
						// we are assuming that the recorded episodes are in chronological order
						theEnd = DimSize(epochs, 0) - 1
						break
					case 0:
						// we reached the beginning of the wave, so do nothing
						return 1
						break
					default:
						theEnd = row - 1
				endswitch
				
				do
					theEpochLabel = GetDimLabel(epochs, 0, theEnd)					// get the last epoch
					row = VM_getRowNumberFromLabel(theEpochLabel, epochs)				// get its epoch label
					epochNum = str2num(theEpochLabel[strsearch(theEpochLabel, "_", Inf, 1) + 1, strlen(theEpochLabel)])
					theEnd -= 1
				while(epochNum > currentEpoch && row > 0)
		
				// now utilize the function from the SetVariable field val_epochNum to go to the desired epoch
				STRUCT WMSetVariableAction episode
				episode.ctrlName = "val_epochNum"
				episode.win = bs.win
				episode.dval = epochNum
				episode.eventCode = 2
				varFunc_VM_currentEpoch(episode)
				
			endif
			break
	endswitch
End // END btnFunc_prevTransient : ButtonControl

Function btnFunc_VM_keepTransient(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch (bs.eventCode)
		case 2:
			DFREF cDF = GetDatafolderDFR()
			
			Variable cCursorExists = strlen(CsrInfo(C)) > 0
			Variable dCursorExists = strlen(CsrInfo(D)) > 0
			
			Variable calibration = cmpstr(bs.userdata, "calibration") == 0
									
			if (cCursorExists && dCursorExists)
				// keep values
				STRUCT VoltammetryPreferences prefs
				VoltHelpers#getTransPrefs(prefs)
				
				NVAR baseline = $(VoltHelpers#getBaselinePoints())
				ControlInfo /W=$bs.win val_epochNum
				Variable currentEpoch = V_value
				if (currentEpoch == 0)
					currentEpoch = 1
					SetVariable val_epochNum, win=$bs.win, value=_NUM:currentEpoch
				endif
				String theHostName = getHostWinName(bs.win)
				
				Wave transient = TraceNameToWaveRef(theHostName, StringByKey("TNAME", CsrInfo(C, theHostName)))
				
				Wave epochs = VM_getEpochWave()
				Variable rowNum = VM_getRowNumberFromLabel("ep_" + num2str(currentEpoch), epochs)

				Wave /T epochCats = VM_getEpochCatWave()
				if ((rowNum) < DimSize(epochs, 0) && rowNum >= 0)
					// we are redefining an existing epoch
					epochs[rowNum][%peakstartpoint] = pcsr(C, theHostName)
				else
					// we record a new epoch
					epochs[DimSize(epochs, 0)][%peakstartpoint] = {pcsr(C, theHostName)}
					rowNum = DimSize(epochs, 0) - 1
					epochCats[rowNum] = {""}			// since we keep a new epoch, make space for category 
				endif
				
				epochs[rowNum][%startpoint] = pcsr(C, theHostName) - baseline
				
				if (calibration)
					epochs[rowNum][%endpoint] = pcsr(D, theHostName) + baseline
				else
					epochs[rowNum][%endpoint] = pcsr(D, theHostName)
				endif
				epochs[rowNum][%peakstartx] = xcsr(C, theHostName)
				epochs[rowNum][%startx] = pnt2x(transient, epochs[DimSize(epochs, 0) -1][%startpoint])
				epochs[rowNum][%endx] = xcsr(D, theHostName)
				epochs[rowNum][%BLstartPnt] = 0
				epochs[rowNum][%BLendPnt] = epochs[rowNum][%peakstartpoint] - epochs[rowNum][%startpoint]
				epochs[rowNum][%calibration] = calibration

				Wave transientColor = VoltAnalysis#getTransientColorWave()
				if (WaveExists(transientColor))
					if (calibration)
						transientColor[epochs[rowNum][%startpoint], pcsr(C, theHostName)] = 1
						transientColor[pcsr(D, theHostName), epochs[rowNum][%endpoint]] = 1
					else
						transientColor[epochs[rowNum][%startpoint], epochs[rowNum][%endpoint]] = 1
					endif
				endif
				
				WaveStats /Q/C=1/W /R=[epochs[rowNum][%startpoint], epochs[rowNum][%endpoint]] transient
				Wave M_WaveStats
				if (WaveDims(M_WaveStats) == 1)
					epochs[rowNum][%peakX] = M_WaveStats[%maxLoc]
					epochs[rowNum][%peakPoint] = x2pnt(transient, M_WaveStats[%maxLoc])
				else
					epochs[rowNum][%peakX] = M_WaveStats[%maxLoc][0]
					epochs[rowNum][%peakPoint] = x2pnt(transient, M_WaveStats[%maxLoc][0])
				endif							
				epochs[rowNum][%peakAvgStart] = prefs.oxDetails.startTime
				epochs[rowNum][%peakAvgEnd] = prefs.oxDetails.endTime
				epochs[rowNum][%peakAvgStartP] = prefs.oxDetails.startPoint
				epochs[rowNum][%peakAvgEndP] = prefs.oxDetails.endPoint
								
				NVAR stimulus = $(VoltHelpers#getStimPoint())
				epochs[rowNum][%stimulusPoint] = stimulus
				epochs[rowNum][%stimulusX] = pnt2x(transient, stimulus)

				if (calibration)
					Wave epochTransient = VoltHelpers#getEpochTransient(nums=(2 * baseline))
					epochTransient[,baseline - 1] = transient[p + epochs[rowNum][%startpoint]]
					epochTransient[baseline,] = transient[pcsr(D, theHostName) - p]
				else
					Wave epochTransient = VoltHelpers#getEpochTransient(nums=epochs[rowNum][%endpoint] - epochs[rowNum][%startpoint])
					epochTransient = transient[p + epochs[rowNum][%startpoint]]
				endif
			
				SetScale /P x -baseline * deltax(transient), deltax(transient), WaveUnits(transient, 0), epochTransient
				SetScale d 0,0,"A", epochTransient
				
				epochs[rowNum][%BLValue] = NumberByKey("BASE", SetWaveToZero(epochTransient, pnt2x(epochTransient, 0), 0))
				epochs[rowNum][%rawDataExists] = 1
				
				String epLabel = "ep_" + num2str(currentEpoch)
				SetDimLabel 0, rowNum, $(epLabel), epochs
				SetDimLabel 0, rowNum, $(epLabel), epochCats
				
				SVAR currentCat = $(VM_getCurrentEpochClass())
				epochCats[%$epLabel] = currentCat
				
				VM_keepData(epLabel, calibration = calibration)
				refreshEpochPanelList()
			else
				DoAlert 0, "Need Cursors C and D on the transient to determine the length\rof the epoch to keep!\r\rClick the left or right arrow button first."
				return 0
			endif
			
			Button btn_prevTransient, win=$bs.win, disable = 0		// we have a recorded epoch, enable the previous button
			
			Variable success = 0
			STRUCT WMButtonAction next
			next.ctrlName = "btn_nextTransient"
			next.win = bs.win
			next.eventCode = 2
			success = btnFunc_VM_nextTransient(next)
			VM_showEpochCategories()
			return success
			break
	endswitch
End // END btnFunc_keepTransient : ButtonControl

Function btnFunc_VM_doneTransient(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch(bs.eventCode)
		case 2:
			Variable success = cleanUp()			
			if (!success)
				DoAlert 0, "Something went wrong cleaning the experiment of the raw data waves.\r\rPlease close all graph windows with raw data waves and try again.\r\rVoltammetry > Clean > Remove raw data files."
			endif
			VM_EpochControlPanel()
			break
	endswitch
	return 0
End  // END btnFunc_doneTransient

Function btnFunc_VM_keepAllAndDone(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch( bs.eventCode )
		case 2:
			Variable cCursorExists = strlen(CsrInfo(C, bs.win)) > 0
			Variable dCursorExists = strlen(CsrInfo(D, bs.win)) > 0
			Variable index = 0
			if (!cCursorExists || !dCursorExists)
				// do next first
				STRUCT WMButtonAction next
				next.ctrlName = "btn_nextTransient"
				next.win = bs.win
				next.eventCode = 2
				btnFunc_VM_nextTransient(next)				
				DoUpdate /W=$bs.win
				DoAlert 0, "Need cursors C and D on the transient. This is and experimental feature!!\r\rClick PSY again if you know what you are doing."
				return 0
			endif
			
			STRUCT VM_ExpFeatureControl control
			loadExpFeature(control)
			
			if (control.features[0].warningEnabled)
				DoAlert	/T="Experimental Feature Alert" 1, "The PSY button implements an experimental feature for Hoon to grab all epochs and delete the orginal data traces.\r\rPlease be sure you know what you are doing!!"
				if (V_flag == 2)
					return 0
				endif			
			endif
			
			Variable result
			STRUCT WMButtonAction keep
			keep.ctrlName = "btn_keepTransient"
			keep.win = bs.win
			keep.eventCode = 2
			do
				result = btnFunc_VM_keepTransient(keep)
			while (result > 0)
			
			bs.ctrlName = "btn_done"
			btnFunc_VM_doneTransient(bs)
			break
	endswitch
End



Function btnFunc_VM_clearEpochs(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch (bs.eventCode)
		case 2:
			Wave epochs = VM_getEpochWave()
			Redimension /N=(0,-1) epochs
			SetVariable val_epochNum, win=$bs.win, value=_NUM:0
			Wave /T epochCats = VM_getEpochCatWave()
			Redimension /N=0 epochCats
			Wave color = VoltAnalysis#getTransientColorWave()
			color = 0
			refreshEpochPanelList()
			break
	endswitch
	return 0
End // END btnFunc_showFullTrace

Function btnFunc_VM_setEpochEnd(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch(bs.eventCode)
		case 2:
			String theHostName = getHostWinName(bs.win)
			Wave transient = getTransientFromGraph(theHostName)
			
			Variable cExists = strlen(CsrInfo(C)) > 0
			if (cExists)
				NVAR peakDuration = $(VoltHelpers#getTransientPoints())
				Cursor /P /W=$theHostName /H=2 D transient (pcsr(C, theHostName) + peakDuration)
			endif
			break
	endswitch
End

Function btnFunc_VM_scaleTrace(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch (bs.eventCode)
		case 2:
			String theHostName = getHostWinName(bs.win)

			strswitch(bs.userdata)
				case "zoom":
					Variable cCursorExists = strlen(CsrInfo(C)) > 0
					Variable dCursorExists = strlen(CsrInfo(D)) > 0
					Variable eCursorExists = strlen(CsrInfo(E)) > 0
					Variable fCursorExists = strlen(CsrInfo(F)) > 0
					
					if (!cCursorExists || !dCursorExists)
						DoAlert 0, "Need the 2 specific cursors (C and D) on the trace to zoom.\r\rClick one of the arrow buttons first."
						return 0
					endif
		
					Wave transient = CsrWaveRef(C, theHostName)
					if (!WaveExists(transient))
						return 0
					endif					


//					String theTraceList = TraceNameList(theHostName, ";", 1)
					// figure out how many traces we have
//					switch (ItemsInList(theTraceList))
//						case 0:
//							print "-- ERROR (btnFunc_nextTransient): no wave on the graph!"
//							return 0
//							break
//						case 1:
//							Wave transient = TraceNameToWaveRef(theHostName, StringFromList(0, theTraceList))
//							break
//						case 2:
//						default:
//							Wave transient = VoltAnalysis#getAnalysisTransient()
//							if (WhichListItem(NameOfWave(transient), theTraceList) < 0)
//								// the transient is named differently, but assume its the first trace on the graph
//								Wave transient = TraceNameToWaveRef(theHostName, StringFromList(0, theTraceList))
//								print "** WARNING: multiple traces on the graph, choose " + StringFromList(0, theTraceList) + " as transient."
//							endif				
//					endswitch

					NVAR baseline = $(VoltHelpers#getBaselinePoints())
					
					Variable rangeStart = 0, rangeEnd = 0
					Variable edge = 2 * baseline * deltax(transient)
										
					if (eCursorExists)
						rangeStart = xcsr(E, theHostName)
					elseif (cCursorExists)
						rangeStart = xcsr(C, theHostName)
					endif
					rangeStart -= edge
					
					if (fCursorExists)
						rangeEnd = xcsr(F, theHostName)
					elseif (dCursorExists)
						rangeEnd = xcsr(D, theHostName)
					endif
					rangeEnd += edge		
					
//					SetAxis /W=$theHostName bottom, xcsr(C, theHostName) - (2 * baseline * deltax(transient)), xcsr(D, theHostName) + (10 * deltax(transient))
					SetAxis /W=$theHostName bottom, rangeStart, rangeEnd
					
					Button btn_scaleTrace, win=$bs.win, userdata="full", title="show full trace"
					break
				case "full":
				default:
					SetAxis /A/N=0 bottom
					Button btn_scaleTrace, win=$bs.win, userdata="zoom", title="zoom to epoch"
			endswitch
			break
	endswitch
	return 0
End // END btnFunc_showFullTrace

Function btnFunc_VM_showVoltammogram(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch(bs.eventCode)
		case 2:
			DFREF dataPath = VoltHelpers#getDataLoadFolder()
			String theHostName = getHostWinName(bs.win)

			Wave transient = getTransientFromGraph(theHostName)
			if (!WaveExists(transient))
				return 0
			endif

			Variable cCursorExists = strlen(CsrInfo(C, theHostName)) > 0
			Variable dCursorExists = strlen(CsrInfo(D, theHostName)) > 0
			if (!cCursorExists || !dCursorExists)
				DoAlert 0, "Need the 2 specific cursors (C and D) on the trace to show the voltammogram\r\rClick one of the arrow buttons first."
				return 0
			endif

			NVAR baseline = $(VoltHelpers#getBaselinePoints())
			ControlInfo /W=$bs.win bx_calibration
			Variable calibration = V_value
			
			if (calibration)
				Variable eCurExists = strlen(CsrInfo(E, theHostName)) > 0
				Variable fCurExists = strlen(CsrInfo(F, theHostName)) > 0
				
				Wave calibData = VM_getCalibDataTraces(pcsr(E, theHostName), pcsr(C, theHostName), pcsr(D, theHostName), pcsr(F, theHostName))
				Wave image = VM_makeImageVoltammogram(calibData, blEnd=baseline-1)
				VM_showImageVoltammogram(filter=1, restricted=1)
				VM_makeCVPlot(VM_makeCVCurves(image, baseline, numPoints = baseline, calibration = 1))
				
			else
//				ControlInfo /W=$bs.win val_pntsForBaseline
				NVAR baseline = $(VoltHelpers#getBaselinePoints())
			
				Button btn_scaleTrace, win=$bs.win, userdata="full", title="full trace"
				
				Wave traceMatrix = VM_getDataTraces(pcsr(C, theHostName) - baseline, pcsr(D, theHostName), baseline)
				Wave image = VM_makeImageVoltammogram(traceMatrix, blEnd=baseline)
				VM_showImageVoltammogram(filter=1)
				
				NVAR thePeakLoc = $(VoltHelpers#getPeakPointVar())
				WaveStats /Q/W/C=1/R=[pcsr(C, theHostName), pcsr(D, theHostName)] transient
				Wave M_WaveStats
				thePeakLoc = x2pnt(transient, M_WaveStats[%maxLoc])
				VM_makeCVPlot(VM_makeCVCurves(image, thePeakLoc - pcsr(C, theHostName) + baseline, numPoints=2))
			endif
	
			break
	endswitch
	return 0
End

Function btnFunc_VM_showControlTraces(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch (bs.eventCode)
		case 2:
			if (VM_checkControlWaves() <= 0)
				Button btn_showControlWaves, disable=3
				return 0	// no control traces exist, so we are done here
			endif

			DFREF package = VoltHelpers#getPackage()
			String theHostName = getHostWinName(bs.win)
			
			Variable cCursorExists = strlen(CsrInfo(C, theHostName)) > 0	// gives us the start of the peak
			if (!cCursorExists)
				DoAlert 0, "Need the C cursor on the trace to show the control traces for that point.\r\rClick the arrow buttons to get one."
				return 0
			endif
									
			VM_showControlTraces()
			break
	endswitch
	
	return 0
End

Function varFunc_VM_currentEpoch(sv) : SetVariableControl
	STRUCT WMSetVariableAction &sv
	
	switch(sv.eventCode)
		case 2:
			// enter key hit
			Wave epochs = VM_getEpochWave()
			Wave /T epochCat = VM_getEpochCatWave()
			String theLabel = "ep_" + num2str(sv.dval)
			Variable row = VM_getRowNumberFromLabel(theLabel,  epochs)
			NVAR cFlag = $(getCursorFlag())
			if (row >= 0)
				// we have a row with the epoch label, so we need to do something
				if (!epochs[row][%rawDataExists])
					// this label refers to an epoch from a previous load, for which the raw data doesn't exist anymore 
					// -> so stop right here and do nothing
					return -1
				endif
				
				cFlag = kVM_Recorded					// indicate that we have all the values and just need to copy
				
				ControlInfo /W=$sv.win bx_calibration
				Variable calibrate = V_Value
				
				String theHostName = getHostWinName(sv.win)
				Wave transient = getTransientFromGraph(theHostName)
				if (!WaveExists(transient))
					return 0
				endif

				SVAR currentClass = $(VM_getCurrentEpochClass())
				currentClass = epochCat[%$theLabel]
				
				NVAR baseline = $(VoltHelpers#getBaselinePoints())

				Cursor /P /W=$theHostName C transient epochs[row][%peakstartpoint]

				if (calibrate)
					Wave cValues = $(getRGBByName("okker"))
					Cursor /M /W=$theHostName /C=(cValues[0], cValues[1], cValues[2]) C
					Cursor /P /W=$theHostName /C=(cValues[0], cValues[1], cValues[2]) /H=2 E transient epochs[row][%startpoint]
					getRGBByName("blue")
					Cursor /P /W=$theHostName /C=(cValues[0], cValues[1], cValues[2]) /H=0 D transient epochs[row][%endpoint] - baseline
					Cursor /P /W=$theHostName /C=(cValues[0], cValues[1], cValues[2]) /H=2 F transient epochs[row][%endpoint]
				else
					Cursor /P /W=$theHostName /H=2 D transient epochs[row][%endpoint]
				endif

				SetAxis /W=$theHostName bottom, epochs[row][%startx] - (2 * baseline * deltax(transient)), epochs[row][%endx] + (2 * baseline * deltax(transient))					

				SetVariable $sv.ctrlName, win=$sv.win, value=_NUM:(sv.dval)
				Button btn_scaleTrace, win=$sv.win, userdata="full", title="full trace"
				
				VM_retrieveDataForTransient(theLabel, plots=1, tGraph=theHostName)
								
				cFlag = kVM_Clear
				
				NVAR thePeakLoc = $(VoltHelpers#getPeakPointVar())
				thePeakLoc = epochs[%$theLabel][%peakPoint]
				NVAR theStimPoint = $(VoltHelpers#getStimPoint())
				theStimPoint = epochs[%$theLabel][%stimulusPoint]
			else
				cFlag = 0			// we have to find the data from the raw loaded data (slower)
			endif
			break
	endswitch
	
	return 0
End

//Function btnFunc_VM_refineTransient(bs) : ButtonControl
//	STRUCT WMButtonAction &bs
//	
//	switch(bs.eventCode)
//		case 2:
//			DFREF dataPath = VoltHelpers#getDataLoadFolder()
//			String theHostName = getHostWinName(bs.win)
//			
//			Variable baseline = str2num(GetUserData(theHostName, "", "baseline"))
//			Wave transient = getTransientFromGraph(theHostName)
//			if (!WaveExists(transient))
//				return 0
//			endif
//
//			Variable cCursorExists = strlen(CsrInfo(C, theHostName)) > 0
//			Variable dCursorExists = strlen(CsrInfo(D, theHostName)) > 0
//
//			if (!cCursorExists || !dCursorExists )
//				return 0
//			endif
//			
//			DoWindow $(VoltHelpers#getVGWinBaseName() + "_" + VoltHelpers#getVGCVExtension())
//			if (!V_flag)
//				btnFunc_VM_showVoltammogram(bs)
//			endif
//			
//			String cvGraphName = VoltHelpers#getVGWinBaseName() + "_" + VoltHelpers#getVGCVExtension()
//			Variable gExists = strlen(CsrInfo(G, cvGraphName)) > 0
//			Variable hExists = strlen(CsrInfo(H, cvGraphName)) > 0
//
//			if (gExists && hExists)
//				VM_makeSingleTransient(VM_getRawDataWave(), mStartP=pcsr(G, cvGraphName), mEndP=pcsr(H, cvGraphName), tStartP=pcsr(C, cvGraphName)-baseline, refine=1)
//			endif
//			break
//	endswitch
//	
//	return 0
//End

Function svarFunc_VM_setWinUserData(sv) : SetVariableControl
	STRUCT WMSetVariableAction &sv
	
	switch (sv.eventCode)
		case 2:
			STRUCT VoltammetryPreferences prefs
			VoltHelpers#getTransPrefs(prefs)
			String theHostName = getHostWinName(sv.win)			
			strswitch (sv.ctrlName)
				case "val_pntsForBaseline":
					SetWindow $theHostName userdata(baseline)=(num2str(sv.dval))
					prefs.baselinePoints = sv.dval
										
					ControlInfo /W=$sv.win bx_calibration
					Variable calibration = V_value
					
					if (calibration)
						Variable cCsrExists = strlen(CsrInfo(C, theHostName)) > 0
						Variable eCsrExists = strlen(CsrInfo(E, theHostName)) > 0
						Variable dCsrExists = strlen(CsrInfo(D, theHostName)) > 0
						Variable fCsrExists = strlen(CsrInfo(F, theHostName)) > 0
						
						if (cCsrExists && dCsrExists && eCsrExists && fCsrExists)
							Wave transient = getTransientFromGraph(theHostName)
							NVAR cFlag = $(getCursorFlag())
							cFlag = kVM_Recorded				// prevent hook function from firing the first time - we just need the point adjustment
							Cursor /P/W=$theHostName E transient pCsr(C, theHostName) - prefs.baselinePoints
							cFlag = kVM_Clear
							Cursor /P/W=$theHostName F transient pCsr(D, theHostName) + prefs.baselinePoints
						endif
					endif
					break
				case "val_pntsForTransient":
					SetWindow $theHostName userdata(duration)=(num2str(sv.dval))
					prefs.transientPoints = sv.dval
					break
				case "val_episodeWin":
					Wave transient = TraceNameToWaveRef(theHostName, StringFromList(0, TraceNameList(theHostName, ";", 1)))
					if (WaveExists(transient))
						Variable numPoints = sv.dval / deltax(transient)
						SetWindow $theHostName userdata(eLength)=(num2str(numPoints))
						prefs.episodePoints = numPoints
					endif
					break
			endswitch
			VoltHelpers#setTransPrefs(prefs)
			break
	endswitch
	
	return 0
End

//! This popup control functions sets the choosen peak find function and enables or disables appropiate control
// elements in the transient graph panel.
// @param pu:		VMPopUpAction structure
// @return       zero (0)
//-
Function popFunc_chooseFunc(pu) : PopupMenuControl
	STRUCT WMPopUpAction &pu
	
	switch( pu.eventCode )
		case 2:
			STRUCT VMGUI_analysisFunction funcs
			VoltAnalysis#loadAnalysisPrefs(funcs)

			strswitch( pu.ctrlName )
				case "popM_chooseAnalysisFunc":
					funcs.analysisDisp = pu.popStr
					funcs.analysisFunc = ksVM_AnalysisFuncPre + "_" + (ReplaceString(" ", funcs.analysisDisp, "_"))
					SVAR var = $(VM_getAnalysisFuncStr(name=funcs.analysisFunc))
					break
				case "popM_chooseControlFunc":
					funcs.controlDisp = pu.popStr
					funcs.controlFunc = ksVM_ControlFuncPre + "_" + (ReplaceString(" ", funcs.controlDisp, "_"))
					SVAR var = $(VM_getControlFuncStr(name=funcs.controlFunc))
					break
			endswitch
			VoltAnalysis#saveAnalysisPrefs(funcs)
			
			if (cmpstr(funcs.analysisDisp, "AL MA episodic") == 0 && cmpstr(funcs.controlDisp, "AL single") == 0)
				SetVariable val_episodeWin, win=$pu.win, disable=1
				TitleBox txtbx_episodeWinUnit, win=$pu.win, disable=1
				
				Checkbox bx_calibration, win=$pu.win, disable=2, value=0
				STRUCT WMCheckboxAction cb				
				cb.win = pu.win
				cb.eventCode = 2
				cb.checked = 0
				ckbxFunc_VM_calibration(cb)
				
				Checkbox bx_ignoreCntrl, win=$pu.win, disable=0

				SetVariable val_pntsforTransient, win=$pu.win, disable=0
				SetVariable val_pntsforBaseline, win=$pu.win, title="baseline"
				SetVariable val_epochClass, win=$pu.win, disable=0
				Button btn_setEpochEnd, win=$pu.win, disable=0
			elseif (cmpstr(funcs.analysisDisp, "AL MA continuous") == 0)
				SetVariable val_episodeWin, win=$pu.win, disable=0
				TitleBox txtbx_episodeWinUnit, win=$pu.win, disable=0
				Checkbox bx_calibration, win=$pu.win, disable=0

				Checkbox bx_ignoreCntrl, win=$pu.win, disable=2
			endif
			break
	endswitch
	return 0
End

// *******        E P O C H    P A N E L    F U N C T I O N S          ***********

Function btnFunc_VM_refreshEpochList(bs) : ButtonControl
	STRUCT WMButtonAction &bs

	switch(bs.eventCode)
		case 2:
			refreshEpochPanelList()
			break
	endswitch
	
	return 0
End

Function btnFunc_VM_reanalyze(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch(bs.eventCode)
		case 2:
			VM_ReanalyzePanel()
			break
	endswitch
	
	return 0
End


Function ckbxFunc_VM_dataSelection(cb) : CheckboxControl
	STRUCT WMCheckboxAction &cb	
	
	switch(cb.eventCode)
		case 2:
			Variable radioValue
			Variable hidden = 0
			
			ControlInfo /W=$cb.win tabExtraFunctions
			hidden = cmpstr(S_value, "calibration") == 0
			
			strswitch(cb.ctrlName)
				case "rdbtn_dataTransient":
					radioValue = 1
					SetWindow $cb.win userdata(displayData)="transient"
					CheckBox ckbx_cvWithColor, win=$cb.win, disable = 1

					Button btn_maxAndArea, win=$cb.win, disable=hidden ? 1 : 0
					Button btn_decayFits, win=$cb.win, disable=hidden ? 1 : 0
					break
				case "rdbtn_dataVGImage":
					radioValue = 2
					SetWindow $cb.win userdata(displayData)="image"
					CheckBox ckbx_cvWithColor, win=$cb.win, disable = 1

					Button btn_maxAndArea, win=$cb.win, disable=hidden ? 1 : 2
					Button btn_decayFits, win=$cb.win, disable=hidden ? 1 : 2
					break
				case "rdbtn_dataVGTrace":
					radioValue = 3
					SetWindow $cb.win userdata(displayData)="cv"
					CheckBox ckbx_cvWithColor, win=$cb.win, disable = 0

					Button btn_maxAndArea, win=$cb.win, disable=hidden ? 1 : 2
					Button btn_decayFits, win=$cb.win, disable=hidden ? 1 : 2
					break
			endswitch

			CheckBox rdbtn_dataTransient, value = radioValue == 1, win=$cb.win
			CheckBox rdbtn_dataVGImage, value = radioValue == 2, win=$cb.win
			CheckBox rdbtn_dataVGTrace, value = radioValue == 3, win=$cb.win
			updateInvisibleControls(cb.win)
			break
	endswitch
	
	return 0
End

Function ckbxFunc_VM_displaySelection(cb) : CheckboxControl
	STRUCT WMCheckboxAction &cb
	
	switch(cb.eventCode)
		case 2:
			Variable radioValue
			strswitch(cb.ctrlName)
				case "rdbtn_displayGraph":
					radioValue = 1
					SetWindow $cb.win userdata(displayType)="graph"
					PopupMenu popup_toWin, value= ("_new_;" + WinList("*", ";", "WIN:1"))
					Button btn_displayData,title="plot",fStyle=1, win=$cb.win
					
					if (!checkWinType(GetUserData(cb.win, "", "winName"), "graph"))
						SetVariable svar_setWinName,value= _STR:"_new_", win=$cb.win
					endif
					break
				case "rdbtn_displayTable":
					radioValue = 2
					SetWindow $cb.win userdata(displayType)="table"
					PopupMenu popup_toWin, value= ("_new_;" + WinList("*", ";", "WIN:2"))
					Button btn_displayData,title="show",fStyle=1, win=$cb.win

					if (!checkWinType(GetUserData(cb.win, "", "winName"), "table"))
						SetVariable svar_setWinName,value= _STR:"_new_", win=$cb.win
					endif
					break
			endswitch
			
			CheckBox rdbtn_displayGraph, value = radioValue == 1, win=$cb.win
			CheckBox rdbtn_displayTable, value= radioValue == 2, win=$cb.win
			updateInvisibleControls(cb.win)

			
			break
	endswitch
	
	return 0
End

Function ckbxFunc_VM_plotStats(cb) : CheckboxControl
	STRUCT WMCheckboxAction &cb
	
	switch(cb.eventCode)
		case 2:
			if (cb.checked)
				strswitch (cb.ctrlName)
					case "ckbtn_plotMax":
						Button btn_maxAndArea, win=$cb.win, userdata="plot max"
						break
					case "ckbtn_plotTaus":
						Button btn_decayFits, win=$cb.win, userdata="plot taus"
						break
				endswitch
			else
				strswitch (cb.ctrlName)
					case "ckbtn_plotMax":
						Button btn_maxAndArea, win=$cb.win, userdata=""
						break
					case "ckbtn_plotTaus":
						Button btn_decayFits, win=$cb.win, userdata=""
						break
				endswitch
			endif
			break
	endswitch
	
	return 0
End


Function ckbxFunc_VM_calibration(cb): CheckboxControl
	STRUCT WMCheckboxAction &cb

	switch (cb.eventCode)
		case 2:
			String theHostName = getHostWinName(cb.win)
			NVAR baseline = $(VoltHelpers#getBaselinePoints())
			Wave cValues = $(getRGBByName("okker"))
			
			Variable cExists = strlen(CsrInfo(C, theHostName)) > 0
			Variable dExists = strlen(CsrInfo(D, theHostName)) > 0

			if (cb.checked)
				SetVariable val_pntsforTransient, win=$cb.win, disable=2
				SetVariable val_pntsforBaseline, win=$cb.win, title="distance"
				SetVariable val_epochClass, win=$cb.win, disable=2
				Button btn_setEpochEnd, win=$cb.win, disable=2
				Checkbox bx_ignoreCntrl, win=$cb.win, disable=2, value=0
				Button btn_keepTransient, win=$cb.win, userdata = "calibration"
				
				SetWindow $theHostName hook(adjust)=WH_adjustCalibrationDataFromCsr

				// switching to calibration mode, if C and D cursors are already on the 
				//    the graph, place the E and F cursors appropiately
				
				if (cExists)
					Wave transient = CsrWaveRef(C, theHostName)
					Cursor /M /W=$theHostName /C=(cValues[0], cValues[1], cValues[2]) C
					Cursor /W=$theHostName /P /H=2 /C=(cValues[0], cValues[1], cValues[2]) E transient, pcsr(C, theHostName) - baseline
				endif
				if (dExists)
					getRGBByName("blue")
					Wave transient = CsrWaveRef(D, theHostName)
					Cursor /M /W=$theHostName /H=0 /C=(cValues[0], cValues[1], cValues[2]) D
					Cursor /W=$theHostName /P /H=2 /C=(cValues[0], cValues[1], cValues[2]) F transient, pcsr(D, theHostName) + baseline
				endif

			else
				SetVariable val_pntsforTransient, win=$cb.win, disable=0
				SetVariable val_pntsforBaseline, win=$cb.win, title="baseline"
				SetVariable val_epochClass, win=$cb.win, disable=0
				Button btn_setEpochEnd, win=$cb.win, disable=0
				Button btn_keepTransient, win=$cb.win, userdata = ""
				Checkbox bx_ignoreCntrl, win=$cb.win, disable=0

				// switch to non-calibration mode, remove the E and F cursor
				Variable eExists = strlen(CsrInfo(E, theHostName)) > 0
				Variable fExists = strlen(CsrInfo(F, theHostName)) > 0
				
				if (dExists)
					Cursor /M /W=$theHostName /H=2 D
				endif
				if (eExists)
					Cursor /K /W=$theHostName E
				endif
				if (fExists)
					Cursor /K /W=$theHostName F
				endif
				
				
				SetWindow $theHostName hook(adjust)=WH_adjustTransientDataFromCsr
			endif
			break
	endswitch
	
	return 0
End

Function btnFunc_VM_selectEpochs(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch(bs.eventCode)
		case 2:
			Wave selWave = getEpochSelWave()
			strswitch(bs.ctrlName)
				case "btn_selectAllEpochs":
					selWave = 1
					break
				case "btn_deselectAllEpochs":
					selWave = 0
					break
			endswitch
			break
	endswitch
End

Function btnFunc_VM_displayData(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch(bs.eventCode)
		case 2:
			String dataSelection = GetUserData(bs.win, "" , "displayData")
			String windowName = GetUserData(bs.win, "", "winName")
			
			Variable isGraph = cmpstr(GetUserData(bs.win, "", "displayType"), "graph") == 0
			Variable new = cmpstr(windowName, "_new_") == 0
			Variable fromZero = cmpstr(GetUserData(bs.win, bs.ctrlName, "fromZero"), "yes") == 0
			Variable includeFit = cmpstr(GetUserData(bs.win, "", "includeFits"), "yes") == 0
			Variable colorCVs = cmpstr(GetUserData(bs.win, "", "cvColored"), "yes") == 0
			
			// if we append to an existing table, ignore the fromZero request
			fromZero = new ? fromZero : 0
			
			DFREF path = VoltHelpers#getEpochFolder()
			DFREF package = VoltHelpers#getPackage()
			Wave /T epochList = getEpochListWave()
			Wave selWave = getEpochSelWave()
			Extract /FREE /INDX /O selWave, selection, selWave == 1
			if (numpnts(selection) == 0)
				DoAlert 0, "Please select an epoch to display!"
				return 0
			endif
			
			Variable index = 0
			strswitch(dataSelection)
				case "transient":
					Variable first = 0
					switch (numpnts(selection))
						case 1:
							// if only one transient is selected
							if (new)
								if (isGraph)
									windowName = epochList[selection[0]][0] + "_TransientGraph"
								else
									windowName = epochList[selection[0]][0] + "_TransientTable"
								endif
								DoWindow $windowName
								if (V_flag)
									// window already exists with the trace, so just bring it to front and be done 
									DoWindow /F $windowName
									return 1
								endif
							endif
							
							DoWindow /F $windowName
							if (V_flag == 0)
								// name is not taken yet
								if (isGraph)
									Display /N=$windowName
								else
									Edit /N=$windowName
									first = fromZero ? 0 : 1
								endif
								windowName = S_name
							endif
							Wave transient = VM_retrieveEpochData(epochList[selection[0]][0], "transient")
							
							if (isGraph)
								AppendToGraph /W=$windowName /C=(0,0,0) transient
								if (includeFit)
									Wave fit = VM_retrieveEpochData(epochList[selection[0]][0], "fit")
									if (WaveExists(fit))
										AppendToGraph /W=$windowName fit
									endif
								endif
							else
								if (first)
									AppendToTable /W=$windowName transient.id
								else
									if (fromZero)
										Make /N=(numpnts(transient)) /O package:$ksVM_xTransientDisp /WAVE=xDisp
										xDisp = p * deltax(transient)
										AppendToTable /W=$windowName xDisp
										SetWindow $windowName, hook(killXDisp)=WH_killXDisplayWave
									endif
									AppendToTable /W=$windowName transient
								endif
							endif
							break
						case 2:
						default:
							// if more than one transient are selected, put all in one window
							if (new)
								if (isGraph)
									Display
								else
									Edit
								endif
								windowName = S_name
							else
								DoWindow $windowName
								if (!V_flag)
									// window does not exist yet
									if (isGraph)
										Display /N=$windowName
									else
										Edit /N=$windowName
									endif
									windowName = S_name
								endif
							endif
							
							first = NumberByKey("COLUMNS", TableInfo(windowName, -2)) <= 1
							index = 0
							do
								Wave transient = VM_retrieveEpochData(epochList[selection[index]][0], "transient")
								if (isGraph)
									AppendToGraph /W=$windowName /C=(0,0,0) transient
									if (includeFit)
										Wave fit = VM_retrieveEpochData(epochList[selection[index]][0], "fit")
										if (WaveExists(fit))
											AppendToGraph /W=$windowName fit
										endif
									endif
								else
									if (index == 0 && first)
										if (fromZero)
											Make /N=(numpnts(transient)) /O package:$ksVM_xTransientDisp /WAVE=xDisp
											xDisp = p * deltax(transient)
											AppendToTable /W=$windowName xDisp
											AppendToTable /W=$windowName transient
											SetWindow $windowName, hook(killXDisp)=WH_killXDisplayWave
										else										
											AppendToTable /W=$windowName transient.id
										endif
									else
										AppendToTable /W=$windowName transient
									endif
								endif
								index += 1
							while (index < numpnts(selection))
					endswitch
					break
				case "image":
					index = 0
					for (index = 0; index < numpnts(selection); index += 1)
						Wave image = VM_retrieveEpochData(epochList[selection[index]][0], "image")
						if (new)
							if (isGraph)
								windowName = epochList[selection[index]][0] + "_2DGraph"
							else
								windowName = epochList[selection[index]][0] + "_VoltgramTable"
							
							endif
							DoWindow /F $windowName
							if (V_flag)
								continue
							elseif (isGraph)
								windowName = VM_showImageVoltammogram(image=image, theGraphName = windowName, filter = 1, restricted = 1)
							else
								Edit /N=$windowName image
								windowName = S_name
							endif
						else
							DoWindow $windowName
							if (V_flag)
								Display /N=$windowName
								windowName = S_name
								KillWindow $windowName
								DoAlert 0, "The chosen window name already exists and images can not be appended to existing windows."
							endif
							if (isGraph)
								windowName = VM_showImageVoltammogram(image=image, theGraphName = windowName, filter = 1)
							else
								Edit /N=$windowName image
								windowName = S_name
							endif						
						endif					
					endfor
					break
				case "cv":
					index = 0

					for (index = 0; index < numpnts(selection); index += 1)
						Wave cv = VM_retrieveEpochData(epochList[selection[index]][0], "cv")

						if (isGraph)
							switch ( numpnts(selection) )
								case 1:
									if (new)
										windowName = epochList[selection[index]][0] + "_CVGraph"
										DoWindow /F $windowName
										if (V_flag)
											continue
										else
											windowName = VM_makeCVPlot(cv, rampPoints=numpnts(cv), theName=windowName)
										endif
									else
										windowName = VM_makeCVPlot(cv, rampPoints=numpnts(cv), theName=windowName, add=1)
									endif
									break
								case 2:
								default:
									if (new && index == 0)
										windowName = UniqueName("CVPlot", 6, 0)
									endif
									windowName = VM_makeCVPlot(cv, rampPoints=numpnts(cv), theName=windowName, add=1)																			
							endswitch
							
							if (colorCVs)
								VM_highlightCVTracePhases(windowName)
							endif
						else
							if (new)
								windowName = epochList[selection[index]][0] + "_CVTable"
								DoWindow /F $windowName
								if (V_flag)
									Edit cv
								else
									Edit /N=$windowName cv
								endif
							else
								AppendToTable /W=$windowName cv							
							endif							
						endif						
					endfor
					break
			endswitch
			break
	endswitch
	
	return 0
End

Function svarFunc_VM_toWin(sva) : SetVariableControl
	STRUCT WMSetVariableAction &sva

	switch( sva.eventCode )
		case 1: // mouse up
		case 2: // Enter key
		case 3: // Live update
			String sval = sva.sval
			SetWindow $sva.win userdata(winName)=sva.sval
			
			break
	endswitch

	return 0
End

Function popFunc_VM_toWinPopMenu(pus) : PopupMenuControl
	STRUCT WMPopupAction &pus

	switch(pus.eventCode)
		case 2:	
			SetVariable svar_setWinName,value= _STR:pus.popStr, win=$pus.win
			SetWindow $pus.win userdata(winName)=pus.popStr
			if (cmpstr(pus.popStr, "_new_") != 0)
				CheckBox ckbtn_fromZero, win=$pus.win, value=0
				STRUCT WMCheckboxAction cb	
 				cb.win = pus.win
 				cb.eventCode = 2
 				cb.checked = 0
 				ckbxFunc_VM_dispXfromZero(cb)
			endif
			break
	endswitch

End

Function lbFunc_VM_epochSelection(lb) : ListboxControl
	STRUCT WMListboxAction &lb
	
	switch(lb.eventCode)
		case 2:
			Wave /T listWave = lb.listWave
			Wave selWave = lb.selWave
			Extract /FREE /INDX /O selWave, selection, selWave == 1
			String selectionList = ""
			if (numpnts(selection) > 0)
				Variable index = 0
				do
					selectionList = AddListItem(listWave[selection[index]], selectionList, ";", Inf)
					index += 1
				while (index < numpnts(selection))
			endif
			SetWindow $lb.win userdata(selection)=selectionList
			break
	endswitch
	return 0
End

Function lbFunc_VM_selectByCategories(lb) : ListboxControl
	STRUCT WMListboxAction &lb
	
	switch(lb.eventCode)
		case 2:
			DFREF cDF = GetDatafolderDFR()
			Wave /T catList = lb.listWave
			Wave catSelWave = lb.selWave
			Wave /T epochList = getEpochListWave()
			String epochListName = NameOfWave(epochList)
			Wave epochSel = getEpochSelWave()
			Wave /T categories = VM_getEpochCatWave()
			
			Extract /FREE /INDX /O catSelWave, selection, catSelWave & 2^4
			if (numpnts(selection) > 0)
				String selectedList = ""
				Variable index = 0
				do
					selectedList += makeLblListFromStatus(categories, text=catList[selection[index]])
					index += 1
				while (index < numpnts(selection))
				
				selectedList = SortList(selectedList, ";", 16)
				
				SetDatafolder GetWavesDatafolderDFR(epochList)
				Wave /T epochList = $(makeTWaveFromList(selectedList, labelName=epochListName))
				Redimension /N=(numpnts(epochList)) epochSel
				epochSel = 0
				epochSel[0] = 1
				Redimension /N=(-1, 1) epochList
				SetDimLabel 1, 0, epochs, epochList
				SetDatafolder cDF
				
			else
				refreshEpochPanelList()				
			endif
			break
	endswitch
	return 0
End

Function ckbxFunc_VM_dispXfromZero(cb) : CheckboxControl
	STRUCT WMCheckboxAction &cb	
	
	switch(cb.eventCode)
		case 2:
			if (cb.checked)
				Button btn_displayData, win=$cb.win, userdata(fromZero)="yes"
			else
				Button btn_displayData, win=$cb.win, userdata(fromZero)="no"	
			endif					
			break
	endswitch
	return 0
End

Function ckbxFunc_VM_includeFits(cb) : CheckboxControl
	STRUCT WMCheckBoxAction &cb
	
	switch( cb.eventCode )
		case 2:
			if (cb.checked)
				SetWindow $cb.win userdata(includeFits)="yes"
			else
				SetWindow $cb.win userdata(includeFits)="no"
			endif
			break
	endswitch
End

Function ckbxFunc_VM_colorCVs(cb) : CheckboxControl
	STRUCT WMCheckBoxAction &cb
	
	switch (cb.eventCode)
		case 2:
			if (cb.checked)
				SetWindow $cb.win userdata(cvColored) = "yes"
			else
				SetWindow $cb.win userdata(cvColored) = "no"
			endif
	endswitch
End

Function btnFunc_VM_simpleStats(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch( bs.eventCode )
		case 2:
			DFREF epochFolder = VoltHelpers#getEpochFolder()
			String epochF = Getdatafolder(1, epochFolder)
			
			Wave /T listwave = $GetUserData(bs.win, "", "epochList")
			Wave selwave = $GetUserData(bs.win, "", "epochSel")
			
			String theSelectList = getSelectedList(listwave, selwave)
			String theList = ReplaceString(";", theSelectList, ";" + epochF)
//			ControlInfo /W=$bs.win lb_epochs
//			String theListWave = S_Datafolder + S_Value
//			String theList = ReplaceString(";", makeListFromWave(theListWave), ";" + epochF)
			theList = RemoveListItem(ItemsInList(theList) - 1, theList)
			theList = epochF + theList
			String theLabel = ""
			
			ControlInfo /W=$bs.win ckbtn_inNano
			Variable dispNano = V_value == 1
			
			strswitch( bs.ctrlName )
				case "btn_maxAndArea":
					String results = descriptiveStatsFromList(theList, tStart=0, noShow=1)
					if (ItemsInList(results) == 0)
						print "** ERROR (btnFunc_VM_simpleStats): no results from the 'descriptiveStatsFromList' function."
						return 0
					endif
					Variable values = 0
					Wave maxes = $StringFromList(0, results)
					Wave areas = $StringFromList(1, results)
					if (WaveExists(maxes) && WaveExists(areas))
						Wave maxSummary = VM_getSimpleMaxWave()
						Wave areasSummary = VM_getSimpleAreaWave()
						Duplicate /O maxes, maxSummary
						Duplicate /O areas, areasSummary
//						Wave maxSummary = VM_getSimpleMaxWave(list=theSelectList)
//						Wave areasSummary = VM_getSimpleAreaWave(list=theSelectList)
//						for (values = 0; values < numpnts(maxes); values += 1)
//							theLabel = StringFromList(values, theSelectList)
//							maxSummary[%$theLabel] = maxes[%$theLabel]
//							areasSummary[%$theLabel] = areas[%$theLabel]
//						endfor
						SetScale d 0, 0, WaveUnits(maxes, 1), maxSummary
						SetScale d 0, 0, WaveUnits(areas, 1), areasSummary
					endif
					
					if (dispNano)
						maxSummary *= 1e9
						SetScale d 0, 0, "n" + WaveUnits(maxSummary, 1), maxSummary
						areasSummary *= 1e9
						SetScale d 0, 0, "n" + WaveUnits(areasSummary, 1) + "^2", areasSummary
					endif
	
					DoWindow /F $ksSimpleDescTableName
					if (!V_flag)
						Edit /K=1 /N=$ksSimpleDescTableName maxSummary.ld, areasSummary as ksSimpleDescTableTitle
					endif
					
					strswitch ( LowerStr(bs.userdata) )
						case "plot max":
							VM_plotSimpleStats(maxSummary, "max", colorCat = 1)
							break
						case "":
						default:
					endswitch
					break
				case "btn_decayFits":
					STRUCT VM_fitPreferences fit
					VoltAnalysis#loadFitPrefs(fit)
					
					Make /N=(fit.guess.numParams) /O masterguesses
					masterguesses = fit.guess.params[p]
					
					SetDimLabel 0, 0, y0, masterguesses
					SetDimLabel 0, 1, A, masterguesses
					SetDimLabel 0, 2, tau, masterguesses
					
					theSelectList = ""
					String difList = VM_differentiateTraces(theList, show=0)
					Variable labels = 0
					for (labels = 0; labels < ItemsInList(difList); labels += 1)
						theSelectList = AddListItem(ParseFilePath(0, StringFromList(labels, difList), ":", 1, 0), theSelectList, ";", Inf)
					endfor
					Wave startPoints = SiSt_getSimpleFitStartPoints(name=ksSimpleFitStartP)
//					Wave fitValues = fitExpDecayToSimpleList(CFit_ExpDecay_Driver, masterguesses, theList, startP=fit.startP, endP=fit.endP, threshold=fit.threshold, repeats=fit.repeats, noShow=1)
					Wave fitValues = fitExpDecayToSimpleList(CFit_ExpDecay_Driver, masterguesses, difList, threshold=fit.threshold, repeats=fit.repeats, startPWave=startPoints, noShow=1)
					Wave summaryFit = VM_getSimpleFitValueWave(labelList=theSelectList)
					if (DimSize(fitValues, 1) != DimSize(summaryFit, 1))
						Redimension /N=(-1, DimSize(fitValues, 1)) summaryFit
						for (labels = 0; labels < DimSize(fitValues, 1); labels += 1)
							SetDimLabel 1, labels, $GetDimLabel(fitValues, 1, labels), summaryFit
						endfor
					endif
					for (labels = 0; labels < ItemsInList(theSelectList); labels += 1)
						theLabel = StringFromList(labels, theSelectList) 
						summaryFit[%$theLabel][] = fitValues[%$theLabel][q]
					endfor
					
					if (dispNano)
						summaryFit[][4,7] = summaryFit[p][q] * 1e9
					endif
					
					KillWaves /Z masterguesses
					
					DoWindow /F $ksSimpleFitTableName
					if (!V_flag)
						Edit /K=1/N=$ksSimpleFitTableName summaryFit.ld as ksSimpleFitTableTitle
					endif
					
					strswitch ( LowerStr(bs.userdata) )
						case "plot taus":
							VM_getSimpleTaus(summaryFit, plot=1, colorCat=1)
							break
						case "":
						default:
					endswitch
					
					String fitList = ReplaceString(";", difList, "_fit;")
					String difGraphs = GH_getGraphFromWaveList(difList)
					String fitGraphs = GH_getGraphFromWaveList(fitList)
					String allGraphs = intersectLists(difGraphs, fitGraphs)
					
					if (ItemsInList(allGraphs) > 0)
						DoWindow /F $(StringFromList(0, allGraphs))
					else
						String graphName = ListToGraph(difList)
						ModifyGraph /W=$graphName mode=0, rgb=(0,0,0), standoff=0
						SetAxis/A/N=1 left
						SetAxis/A/N=1 bottom
						ListToGraph(fitList, theName=graphName)
					endif
					break
			endswitch
			break
	endswitch
End

Function tabFunc_extraFunctions(tc) : TabControl
	STRUCT WMTabControlAction &tc
	
	switch(tc.eventCode)
		case 2:
			Variable dispToggle = 0
			String dispData = GetUserData(tc.win, "", "displayData")
			if (cmpstr(dispData, "image") == 0 || cmpstr(dispData, "cv") == 0)
				dispToggle = 2
			endif
		
			// elements of tab 0
			Button btn_maxAndArea, win=$tc.win, disable= (tc.tab != 0) + dispToggle
			Button btn_decayFits, win=$tc.win, disable= (tc.tab != 0) + dispToggle
			Checkbox ckbtn_plotMax, win=$tc.win, disable= (tc.tab != 0)
			Checkbox ckbtn_plotTaus, win=$tc.win, disable= (tc.tab != 0)
			Checkbox ckbtn_inNano, win=$tc.win, disable= (tc.tab != 0)
			
			// elements of tab 1
			SetVariable svar_calibrationFactor, win=$tc.win, disable= (tc.tab != 1)
			TitleBox tbx_calibFactorLbl, win=$tc.win, disable= (tc.tab != 1)
			Button btn_calibrate, win=$tc.win, disable= (tc.tab != 1)
			Button btn_calcFactor, win=$tc.win, disable= (tc.tab != 1)
			SetVariable svar_calibConcentration, win=$tc.win, disable= (tc.tab != 1)
			TitleBox tbx_doseLbl, win=$tc.win, disable= (tc.tab != 1)
			break
	endswitch
End


Function btnFunc_VM_calibrate(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch( bs.eventCode )
		case 2:
			Wave /T listwave = $GetUserData(bs.win, "", "epochList")
			Wave selwave = $GetUserData(bs.win, "", "epochSel")
			
			String theSelectList = getSelectedList(listwave, selwave)
			VM_calibrateTransients(theSelectList)
			
			break
	endswitch
End

Function btnFunc_VM_calcFactor(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch( bs.eventCode )
		case 2:
			Wave /T listwave = $GetUserData(bs.win, "", "epochList")
			Wave selwave = $GetUserData(bs.win, "", "epochSel")
			
			String theSelectList = getSelectedList(listwave, selwave)
			Variable numItems = ItemsInList(theSelectList)
			if (numItems == 0)
				DoAlert 0, "Please select at least one epoch"
				return 0
			endif
			
			String cvList = ""
			DFREF eFolder = VoltHelpers#getEpochFolder()

			Variable index
			for (index = 0 ; index < numItems; index += 1)
				Wave tmp = eFolder:$(StringFromList(index, theSelectList) + "_cv")
				if (WaveExists(tmp))
					cvList = AddListItem(GetWavesDatafolder(tmp, 2), cvList, ";", index)
				endif
			endfor
			
			numItems = ItemsInList(cvList)
			
			if (numItems == 0)
				DoAlert 0, "None of the selected epochs have CV traces."
				return 0
			endif
			
			ControlInfo /W=$bs.win svar_calibConcentration
			Variable dose = V_value * 1e-6
			
			NVAR factor = $(VoltHelpers#getCalibrationFactor())
			factor = VM_calcCalibrationFactor(cvList, dose = dose)

			STRUCT VoltammetryPreferences prefs
			VoltHelpers#getTransPrefs(prefs)
			prefs.calibrationFactor = factor			
			VoltHelpers#setTransPrefs(prefs)
			
			break
	endswitch
End


// ****************************************************
// *******                 O T H E R    F U N C T I O N S                 ***********
// ****************************************************

static Function updateInvisibleControls(windowName)
	String windowName
	
	String dispData = GetUserData(windowName, "", "displayData")
	String dispType = GetUserData(windowName, "", "displayType")
	if (cmpstr(dispData, "transient") == 0 && cmpstr(dispType, "table") == 0)
		CheckBox ckbtn_fromZero, win=$windowName, disable=0
		ControlInfo /W=$windowName ckbtn_fromZero
		if (V_Value)
			Button btn_displayData, win=$windowName, userdata(fromZero)="yes"
		else
			Button btn_displayData, win=$windowName, userdata(fromZero)="no"
		endif
	else
		CheckBox ckbtn_fromZero, win=$windowName, disable=1
		Button btn_displayData, win=$windowName, userdata(fromZero)="no"
	endif	
		
End

static Function /S getSelectedList(listwave, selwave)
	Wave /T listWave
	Wave selwave
	
	Extract /FREE /O /INDX selWave, selections, (selWave & 2^0) != 0
	String selectList = ""
	Variable index = 0
	for (index = 0; index < numpnts(selections); index += 1)
		selectList = AddListItem(listWave[selections[index]], selectList, ";", Inf)
	endfor
	return selectList
End

Function VM_setHiddenPrefs(type)
	String type
	
	STRUCT VoltammetryPreferences prefs
	VoltHelpers#getTransPrefs(prefs)
	type = prefs.type
	
	Variable CVAvgPoints = (prefs.CVAvgPoints * 2) + 1
	Prompt CVAvgPoints, "Enter total number of points to average for CV trace (must be odd):" 
	strswitch(type)
		case "dopamine":
		default:
			DoPrompt "Set Preferences", CVAvgPoints
			if (!V_flag)
				// continue clicked
				Variable rest = mod(CVAvgPoints, 2)
				if (rest)
					prefs.CVAvgPoints = (CVAvgPoints - 1) / 2
				else
					prefs.CVAvgPoints = CVAvgPoints / 2
				endif
				VoltHelpers#setTransPrefs(prefs)
			endif
	endswitch
	return 1
End	

Function VM_changeDopamineDefCalibFactor()
	String errormsg = ""
	sprintf errormsg, ksVM_ErrorMsg, "VM_changeDopamineDefCalibFactor", "%s"
	
	STRUCT VoltammetryPreferences prefs
	VoltHelpers#getTransPrefs(prefs)

	Variable calibFact = prefs.calibrationFactor
	Prompt calibFact, "Enter the calibration factor (nM at 1 nA)"
	
	DoPrompt "Change Default Calibration Factor", calibFact
	if (V_flag)
		// user canceled
		printf errormsg, "user canceled changed of default calibration factor on " + date() + " " + time()
		return 0
	endif
	
	prefs.calibrationFactor = calibFact
	VoltHelpers#setTransPrefs(prefs)
	return 1
End
	

Function VM_changeFitPreferences()
	STRUCT VM_fitPreferences fit
	VoltAnalysis#loadFitPrefs(fit)
	
	Variable startPoint = fit.startP
	Variable endPoint = fit.endP
	Variable threshold = fit.threshold
	Variable repeats = fit.repeats
	Variable y0 = fit.guess.params[0]
	Variable A = fit.guess.params[1]
	Variable tau = fit.guess.params[2]

	Prompt startPoint, "Start POINT of fit (-1 = from max)"
	Prompt endPoint, "End POINT of fit (-1 = end of wave)"
	Prompt threshold, "Chi square value to achieve"
	Prompt repeats, "Number of repeats to get chi square"
	Prompt y0, "Initial guess for yO"
	Prompt A, "initial guess for A"
	Prompt tau, "initial guess for tau"
	
	DoPrompt "Fit Preference Chooser", startPoint, endPoint, threshold, repeats, y0, A, tau
	if( V_flag )
		// user clicked cancel, so do nothing more
		return 0
	endif
	fit.startP = startPoint
	fit.endP = endPoint
	fit.threshold = threshold
	fit.repeats = repeats
	fit.guess.params[0] = y0
	fit.guess.params[1] = A
	fit.guess.params[2] = tau
	
	VoltAnalysis#saveFitPrefs(fit)
End

Function VM_resetExpFeatureControls()

	STRUCT VM_ExpFeatureControl control
	saveExpFeature(control, reset=1)
End

Function VM_changeGUIPreferences()
	String cvInFront = "No"
	Prompt cvInFront, "CV plot kept in front?", popup, ksVM_DefBinaryChoiceList
	String cvToMax = "Yes - to absolute max"
	Prompt cvToMax, "CV cursor automatically align to the max?", popup, "Yes - to absolute max;Yes - to fitted max;No;"
	
	STRUCT VM_GUIPreferences prefs
	loadGUIPrefs(prefs)
	
	if (prefs.keepCVfront)
		cvInFront = "Yes"
	else
		cvInFront = "No"
	endif

	switch (prefs.winCVmax)
		case 0:
			cvToMax = "No"
			break
		case 1:
			cvToMax = "Yes - to absolute max"
			break
		case 2:
			cvToMax = "Yes - to fitted max"
			break
		default:
			cvToMax = "No"
	endswitch
	
	DoPrompt "GUI preferences", cvInFront, cvToMax
	if (V_flag)
		// user canceled dialog, so do nothing
		return 0
	endif
	prefs.keepCVfront = cmpstr(cvInFront, "Yes") == 0 ? 1 : 0

	strswitch (cvToMax)
		case "No":
			prefs.winCVmax = 0
			break
		case "Yes - to absolute max":
			prefs.winCVmax = 1
			break
		case "Yes - to fitted max":
			prefs.winCVmax = 2
			break
	endswitch
			
	saveGUIPrefs(prefs)
	printf "-- changed GUI preferences to: keep in front: %s    --    align to max: %s\r", cvInFront, cvToMax
	return 1
End
	
//Function /S VM_askForChannelAssignment(theList)
//	String theList
//	
//	String assignmentList = ""
//	Variable numItems = ItemsInList(theList)
//	if (numItems < 1)
//		// we have no channel, so nothing more to do
//		return assignmentList
//	endif
//	if (numItems == 1)
//		assignmentList = ReplaceStringByKey(StringFromList(0, theList), assignmentList, "data")
//		return assignmentList
//	endif
//	
//	
//	
//End

Function VM_setVGColorScaleP(graphName)
	String graphName

	String errormsg = VM_getErrorStr("VM_setVGColorScaleP")
	
	String subWinList = ChildWindowList(graphName)
	if (WhichListItem(ks_VGColorScaleName, subWinList) >= 0)
		return 0
	endif
	
	String theImage = StringFromList(0, ImageNameList(graphName, ";"))
	if (strlen(theImage) == 0)
		// no image on the graph, nothing more to do
		return 0
	endif
	
	Wave image = ImageNameToWaveRef(graphName, theImage)
	Variable restricted = cmpstr(ksVM_restrictedDispValue, StringByKey(ksVM_DisplayKey, note(image))) == 0
	if (restricted)
		Wave ramp = VM_getRampWave(numbers=DimSize(image, 1))
		Variable rampxStart = pnt2x(ramp, NumberByKey(ksVM_RampStartPKey, note(ramp)))
		Variable rampxEnd = pnt2x(ramp, NumberByKey(ksVM_RampEndPKey, note(ramp)))
	endif

	DFREF loc = GetWavesDatafolderDFR(image)
	Variable imgLoc = strsearch(theImage, "_img", 0)
	Variable uScr = strsearch(theImage, "_", Inf, 1)
	if (cmpstr((NameOfWave(image))[0,2], "ep_") == 0 || imgLoc > 0)
		if ( cmpstr(theImage[uScr + 1, Inf], "filtered") == 0)
			Wave cv = loc:$(ReplaceString("_img_filtered", NameOfWave(image), "_cv"))
		else
			Wave cv = loc:$(ReplaceString("_img", NameOfWave(image), "_cv"))
		endif
	else
		String newName = theImage
		if ( cmpstr(theImage[uScr + 1, inf], "filtered") == 0)
			newName = theImage[0, uScr - 1]
		endif	
		newName[0,4] = "CV"
		Wave cv = loc:$newName
		if (!WaveExists(cv))
			printf errormsg,  "can't locate the corresponding CV trace. STOPPED!"
			return 0
		endif
	endif

	Variable theMax, theMin
	if (restricted)
		theMax = WaveMax(cv, rampxStart, rampxEnd)
		theMin = WaveMin(cv, rampxStart, rampxEnd)
	else
		theMax = WaveMax(cv)
		theMin = WaveMin(cv)
	endif


	NewPanel /W=(5,0,240,100)/EXT=0/HOST=$graphName/N=$ks_VGColorScaleName as "Set Color Scale"
	String theWinName = graphName + "#" + S_name
	SetWindow #, userdata(graph)=graphName
	SetWindow #, userdata(image)=theImage

	STRUCT VM_VGColorScale prefs
	loadCSPrefs(prefs)

	PopupMenu pum_selectScalingMethod,pos={17,6},size={199,22},title="select scaling method"
	PopupMenu pum_selectScalingMethod,help={"select the scaling method applied to the 2 dimensional voltamogram"}
	PopupMenu pum_selectScalingMethod,font="Verdana",fSize=10
	PopupMenu pum_selectScalingMethod,mode=1,popvalue="automatic",value= #"\"automatic;manual\""
	PopupMenu pum_selectScalingMethod, proc=popFunc_VM_selectScaleMethod
	
	ValDisplay vdisp_currentMax, pos={17, 30}, size={92, 18}, title="max v."
	ValDisplay vdisp_currentMax, font="Verdana", fsize=10, frame=0, format="%.1e", fstyle=2
	ValDisplay vdisp_currentMax, barmisc={0,1000}, limits={0,0,0}, value=_NUM:theMax
	ValDisplay vdisp_currentMin, pos={128, 30}, size={92, 18}, title="min v."
	ValDisplay vdisp_currentMin, font="Verdana", fsize=10, frame=0, format="%.1e", fstyle=2
	ValDisplay vdisp_currentMin, barmisc={0,1000}, limits={0,0,0}, value=_NUM:theMin


	SetVariable svar_setColorMaxValue,pos={17,52},size={92,15},title="max"
	SetVariable svar_setColorMaxValue,help={"enter the maximum value for the color scale of the 2 dimensional voltamogram"}
	SetVariable svar_setColorMaxValue,font="Verdana",fSize=10, disable=2
	SetVariable svar_setColorMaxValue, proc=svarFunc_VM_setMinMaxValue
	SetVariable svar_setColorMaxValue,limits={-inf,inf,0},value= _NUM:prefs.max
	SetVariable svar_setColorMinValue,pos={128,52},size={92,15},title="min"
	SetVariable svar_setColorMinValue,help={"enter the minimum value for the color scale of the 2 dimensional voltamogram"}
	SetVariable svar_setColorMinValue,font="Verdana",fSize=10, disable=2
	SetVariable svar_setColorMinValue,limits={-inf,inf,0},value= _NUM:prefs.min
	SetVariable svar_setColorMinValue, proc=svarFunc_VM_setMinMaxValue

	Button btn_finishedSetColorScale,pos={140,75},size={50,20},title="Ok"
	Button btn_finishedSetColorScale,font="Verdana", fstyle=1
	Button btn_finishedSetColorScale, proc=btnFunc_VM_FinishedSetCScale
	
	Button btn_reset2DefCSValues, pos={40, 75}, size={50, 20}, title="reset"
	Button btn_reset2DefCSValues, font="Verdana", disable=2
	Button btn_reset2DefCSValues, proc=btnFunc_VM_resetCSValues
	
	if( prefs.method )
		// 1 = manual
		PopupMenu pum_selectScalingMethod, mode=2
		VM_toggleMinMaxValueStatus(!prefs.method, theWinName)
//		SetVariable svar_setColorMaxValue, disable=0
//		SetVariable svar_setColorMinValue, disable=0
//		Button btn_reset2DefCSValues, disable=0
	endif
	
End

Function popFunc_VM_selectScaleMethod(pu) : PopupMenuControl
	STRUCT WMPopupAction &pu
	
	switch (pu.eventCode)
		case 2:
			// mouse up
			Variable filtered = 0
			String theGraph = GetUserData(pu.win, "", "graph")
			String imageName = GetUserData(pu.win, "", "image")
			Wave image = ImageNameToWaveRef(theGraph, imageName)
			Variable restricted = cmpstr(ksVM_restrictedDispValue, StringByKey(ksVM_DisplayKey, note(image))) == 0

			Variable uScr = strsearch(imageName, "_", Inf, 1)
			filtered = cmpstr(imageName[uScr + 1, Inf], "filtered") == 0
			if (filtered)
				DFREF path = GetWavesDatafolderDFR(image)
				Wave image = path:$(imageName[0, uScr - 1])
			endif
			
			STRUCT VM_VGColorScale prefs
			loadCSPrefs(prefs)
			strswitch (pu.popStr)
				case "automatic":
					VM_toggleMinMaxValueStatus(1, pu.win)
					prefs.method = 0
					break
				case "manual":
					VM_toggleMinMaxValueStatus(0, pu.win)
					prefs.method = 1
					break
			endswitch
			saveCSPrefs(prefs)
			VM_showImageVoltammogram(image=image, theGraphName=theGraph, filter=filtered, restricted=restricted)
			break
	endswitch
End

Function svarFunc_VM_setMinMaxValue(sa) : SetVariableControl
	STRUCT WMSetVariableAction &sa
	
	switch ( sa.eventCode)
		case 2:
			// enter key hit - also reacts to tab key
			Variable filtered = 0
			String theGraph = GetUserData(sa.win, "", "graph")
			String imageName = GetUserData(sa.win, "", "image")
			Wave image = ImageNameToWaveRef(theGraph, imageName)
			Variable restricted = cmpstr(ksVM_restrictedDispValue, StringByKey(ksVM_DisplayKey, note(image))) == 0

			Variable uScr = strsearch(imageName, "_", Inf, 1)
			if (cmpstr(imageName[uScr + 1, Inf], "filtered") == 0)
				filtered = 1
				DFREF path = GetWavesDatafolderDFR(image)
				Wave image = path:$(imageName[0, uScr - 1])
			endif

			STRUCT VM_VGColorScale prefs
			loadCSPrefs(prefs)
			strswitch( sa.ctrlName )
				case "svar_setColorMaxValue":
					prefs.max = sa.dval
					prefs.min = -(prefs.max * prefs.PofUpper)
					SetVariable svar_setColorMaxValue, win=$sa.win,value= _NUM:prefs.max
					SetVariable svar_setColorMinValue, win=$sa.win,value= _NUM:prefs.min
					prefs.method = 1
					break
				case "svar_setColorMinValue":
					prefs.min = sa.dval
					prefs.max = -(prefs.min * prefs.PofLower)
					SetVariable svar_setColorMaxValue, win=$sa.win,value= _NUM:prefs.max
					SetVariable svar_setColorMinValue, win=$sa.win,value= _NUM:prefs.min
					prefs.method = 1
					break
			endswitch
			saveCSPrefs(prefs)
			VM_showImageVoltammogram(image=image, theGraphName=theGraph, filter=filtered, restricted=restricted)
			break
	endswitch
End

Function btnFunc_VM_FinishedSetCScale(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch( bs.eventCode )
		case 2:
			// mouse up again
			KillWindow $bs.win
			break
	endswitch
	
End

Function btnFunc_VM_resetCSValues(bs) : ButtonControl
	STRUCT WMButtonAction &bs
	
	switch( bs.eventCode )
		case 2:
			// mouse up
			STRUCT VM_VGColorScale prefs
			defaultCSPrefs(prefs)
			prefs.method = 1
			saveCSPrefs(prefs)
			
			STRUCT WMSetVariableAction sa	 
			sa.win=bs.win
			sa.ctrlName = "svar_setColorMaxValue"
			sa.dval = prefs.max
			sa.eventCode = 2
			svarFunc_VM_setMinMaxValue(sa)
			break
	endswitch
End

Function VM_toggleMinMaxValueStatus(disable, theName)
	Variable disable
	String theName
	
	if (disable)
		SetVariable svar_setColorMaxValue, disable = 2, win=$theName
		SetVariable svar_setColorMinValue, disable = 2, win=$theName
		Button btn_reset2DefCSValues, disable=2, win=$theName
	else
		SetVariable svar_setColorMaxValue, disable = 0, win=$theName
		SetVariable svar_setColorMinValue, disable = 0, win=$theName
		Button btn_reset2DefCSValues, disable=0, win=$theName
	endif
End