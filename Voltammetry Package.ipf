#pragma rtGlobals=3		// Use modern global access method.
#pragma IgorVersion = 6.22
#pragma version = 1
#include "VM_GUI", version >= 2.7

// written by Roland Bock December 2012
// version 1
// 
// This file is the companion file to the "Voltammetry Package Loader.ipf" file, which should
// be linked with an alias/shortcut in the Igor Procedure folder, so it loads at startup.
// The loader file provides a simple function to easily install the package. This file unloads the 
// package by removing the loaded files.

Menu "Voltammetry", dynamic
	"-"
	Submenu "Remove Voltammetry"
		"Unload Voltammetry Package", /Q, UnloadVoltammetryPackage()
		AcquisitionPackageMenuItem(2), /Q, UnloadVoltammetryAcquisition()
	End
	AcquisitionPackageMenuItem(1), /Q, LoadVMAcquisitionPackage()
End

Function UnloadVoltammetryPackage()
	Execute /P/Q/Z "DELETEINCLUDE \"VM_voltaMafBridge\""

	Execute /P/Q/Z "DELETEINCLUDE \"VM_GUI\""
	Execute /P/Q/Z "DELETEINCLUDE \"Voltammetry Package\""
	Execute /P/Q/Z "DELETEINCLUDE \"MeanAndStDev\""
	Execute /P/Q/Z "DELETEINCLUDE \"WindowHelper\""
	Execute /P/Q/Z "COMPILEPROCEDURES "
End

Function UnloadVoltammetryAcquisition()
	Execute /P/Q/Z "DELETEINCLUDE \"VM_mafPCBridge\""
	Execute /P/Q/Z "COMPILEPROCEDURES "
End

Function /S AcquisitionPackageMenuItem(itemNum)
	Variable itemNum
	
	String menuStr = ""
	Variable isLoaded = strlen(FunctionList("VM_mafBridgeIsLoaded", ";", "")) > 0
	
	if (itemNum == 1)
		if (!isLoaded)
			menuStr = "Load Acquisition"
		endif
	endif
	if (itemNum == 2)
		if (isLoaded)
			menuStr = "Unload Voltammetry Acquisition"
		endif
	endif
	
	return menuStr
End